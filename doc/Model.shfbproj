﻿<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="14.0" DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <!-- The configuration and platform will be used to determine which assemblies to include from solution and
				 project documentation sources -->
    <Configuration Condition=" '$(Configuration)' == '' ">Debug</Configuration>
    <Platform Condition=" '$(Platform)' == '' ">AnyCPU</Platform>
    <SchemaVersion>2.0</SchemaVersion>
    <ProjectGuid>{46ea94e2-d9ed-45cd-b138-b8ec85db2e13}</ProjectGuid>
    <SHFBSchemaVersion>2017.9.26.0</SHFBSchemaVersion>
    <!-- AssemblyName, Name, and RootNamespace are not used by SHFB but Visual Studio adds them anyway -->
    <AssemblyName>Documentation</AssemblyName>
    <RootNamespace>Documentation</RootNamespace>
    <Name>Documentation</Name>
    <!-- SHFB properties -->
    <FrameworkVersion>.NET Framework 4.5</FrameworkVersion>
    <OutputPath>.\Help\</OutputPath>
    <HtmlHelpName>Documentation</HtmlHelpName>
    <Language>en-US</Language>
    <HelpTitle>Documentation for RoboRoboRally&amp;#39%3bs Model</HelpTitle>
    <HelpFileVersion>1.0.0.0</HelpFileVersion>
    <RootNamespaceContainer>True</RootNamespaceContainer>
    <RootNamespaceTitle>RoboRoboRally - Model</RootNamespaceTitle>
    <NamespaceGrouping>True</NamespaceGrouping>
    <Preliminary>False</Preliminary>
    <SdkLinkTarget>Blank</SdkLinkTarget>
    <VisibleItems>InheritedMembers, Internals, ProtectedInternalAsProtected, NonBrowsable</VisibleItems>
    <DocumentationSources>
      <DocumentationSource sourceFile="..\src\RoboRoboRally.Model\bin\Debug\netstandard2.0\RoboRoboRally.Model.xml" />
      <DocumentationSource sourceFile="..\src\RoboRoboRally.Model\bin\Debug\netstandard2.0\RoboRoboRally.Model.dll" />
    </DocumentationSources>
    <HelpFileFormat>Website</HelpFileFormat>
    <SyntaxFilters>Standard</SyntaxFilters>
    <PresentationStyle>VS2013</PresentationStyle>
    <CleanIntermediates>True</CleanIntermediates>
    <KeepLogFile>True</KeepLogFile>
    <DisableCodeBlockComponent>False</DisableCodeBlockComponent>
    <IndentHtml>False</IndentHtml>
    <BuildAssemblerVerbosity>OnlyWarningsAndErrors</BuildAssemblerVerbosity>
    <ProjectSummary>&amp;lt%3bh2&amp;gt%3bAbout library&amp;lt%3b/h2&amp;gt%3b
&amp;lt%3bp&amp;gt%3b
This is library that is part of a larger project named RoboRoboRally, which aims to implement RoboRally board game. This library contains mainly two sets of functionalities.
&amp;lt%3b/p&amp;gt%3b

&amp;lt%3bp&amp;gt%3b
First of all, it provides the core functionality for running instances of new games. As such it is able to represent and hold game state, communicate with players %28in our case, via server%29 and it adheres to rules of the RoboRally board game.
&amp;lt%3b/p&amp;gt%3b

&amp;lt%3bp&amp;gt%3b
Secondly, this library provides access to GameData - database of game content that is required by model and other parts of project. GameData contains things like card definitions, map definitions or textures. However, GameData does not contain functionality for loading or manipulating the game content. This functionality is entirely up to Model.
&amp;lt%3b/p&amp;gt%3b

&amp;lt%3bp&amp;gt%3bIt order to better understand different classes in this library and relations between them it is worth mentioning other parts of RoboRoboRally project and how they use this library. First, there is mobile application which will provide input to the game. Mobile application needs model for access to GameData %28textures and card definitions%29. Secondly, there is a desktop application that we call Game Terminal. Game Terminal is used to display current state of the game to players as well as spectators. Also, it is used for setting up new game. For this, Game Terminal needs again access to GameData %28textures, new game definition%29. When a new game is configured, these settings are sent to Server - last component that uses Model.  Server uses it for it&amp;#39%3bs other functionality - running new games with settings sent from Game Terminal.
&amp;lt%3b/p&amp;gt%3b

&amp;lt%3bh2&amp;gt%3bLanguage and requirements&amp;lt%3b/h2&amp;gt%3b
&amp;lt%3bp&amp;gt%3b
This library is written entirely in language C# and it was written against .NET Standard interface version 2.0. Deployed version of library was compiled with dotnet msbuild utility.
&amp;lt%3b/p&amp;gt%3b

&amp;lt%3bh2&amp;gt%3bOther notes&amp;lt%3b/h2&amp;gt%3b
&amp;lt%3bp&amp;gt%3bNote that remaining parts of the documentation rely on the fact that the reader is familiar with rules of RoboRally. Be sure to read them before proceeding.&amp;lt%3b/p&amp;gt%3b 
</ProjectSummary>
  </PropertyGroup>
  <!-- There are no properties for these groups.  AnyCPU needs to appear in order for Visual Studio to perform
			 the build.  The others are optional common platform types that may appear. -->
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x86' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x86' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x64' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x64' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|Win32' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|Win32' ">
  </PropertyGroup>
  <!-- Import the SHFB build targets -->
  <Import Project="$(SHFBROOT)\SandcastleHelpFileBuilder.targets" />
  <!-- The pre-build and post-build event properties must appear *after* the targets file import in order to be
			 evaluated correctly. -->
  <PropertyGroup>
    <PreBuildEvent>
    </PreBuildEvent>
    <PostBuildEvent>
    </PostBuildEvent>
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
  </PropertyGroup>
</Project>