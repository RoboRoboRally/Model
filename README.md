# About library

This is library that is part of a larger project named RoboRoboRally, which aims to implement RoboRally board game. This library contains mainly two sets of functionalities.

First of all, it provides the core functionality for running instances of new games. As such it is able to represent and hold game state, communicate with players (in our case, via server) and it adheres to rules of the RoboRally board game.

Secondly, this library provides access to GameData - database of game content that is required by model and other parts of project. GameData contains things like card definitions, map definitions or textures. However, GameData does not contain functionality for loading or manipulating the game content. This functionality is entirely up to Model.

It order to better understand different classes in this library and relations between them it is worth mentioning other parts of RoboRoboRally project and how they use this library. First, there is mobile application which will provide input to the game. Mobile application needs model for access to GameData (textures and card definitions). Secondly, there is a desktop application that we call Game Terminal. Game Terminal is used to display current state of the game to players as well as spectators. Also, it is used for setting up new game. For this, Game Terminal needs again access to GameData (textures, new game definition). When a new game is configured, these settings are sent to Server - last component that uses Model.  Server uses it for it's other functionality - running new games with settings sent from Game Terminal.

# Language and requirements

This library is written entirely in language C# and it was written against .NET Standard interface version 2.0. Deployed version of library was compiled with dotnet msbuild utility.

