﻿using NUnit.Framework;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.Tests.MapDBTests
{
	//these tests test map's public interface
    [TestFixture]
    public class MapTests
    {
		TestMapBuilder tmb;

		[SetUp]
		public void TestSetup()
		{
			tmb = new TestMapBuilder(new MapSize(10, 9)) { MapID = "someMapID" };
		}

		[Test]
		public void MapIDSetTest()
		{
			Assert.That(tmb.BuildMap().ID == "someMapID");
		}

		[Test]
        public void MapSizeTest()
        {
			var m = tmb.BuildMap();
			Assert.That(m.MapSize.RowCount == 10 && m.MapSize.ColCount == 9);
        }

		[Test]
		public void TileGettersTest()
		{
			var m = tmb.BuildMap();
			Assert.That(m[0, 0] == m[new MapCoordinates(0, 0)]);
		}

		[Test]
		public void TileRecordsGetterTest()
		{
			//this method builds an empty tile map ...
			//let's assert that map object returns each of these tiles correctly
			var m = tmb.BuildMap();
			Assert.That(m.GetTileRecords<EmptyTile>().Length == m.MapSize.ColCount * m.MapSize.RowCount);
		}

		[Test]
		public void WallRecordsGetterTest()
		{
			tmb = new TestMapBuilder();

			var t1 = new EmptyTile();
			var w1 = new EmptyWall(t1, new Direction(DirectionType.Left));

			tmb.Tiles[0, 0] = t1;
			tmb.Walls.Add(w1);

			var t2 = new EmptyTile();
			var w2 = new EmptyWall(t2, new Direction(DirectionType.Down));

			tmb.Tiles[1, 1] = t2;
			tmb.Walls.Add(w2);

			var t3 = new TextTile("abc");
			var w3 = new PushPanelWall(new int[3] { 1, 3, 5 }, t3, new Direction(DirectionType.Up));

			tmb.Tiles[2, 2] = t3;
			tmb.Walls.Add(w3);

			var t4 = new EmptyTile();
			var w4 = new LaserWall(new Damage() { }, 3, t4, new Direction(DirectionType.Right));

			var m1 = tmb.BuildMap();

			var wallRecords = m1.GetWallRecords<EmptyWall>();
			Assert.That(wallRecords.Length == 2);
			Assert.That(wallRecords.Any(wr => wr.Coords == new MapCoordinates(0, 0)));
			Assert.That(wallRecords.Any(wr => wr.Coords == new MapCoordinates(1, 1)));
			
		}

		[Test]
		public void NonEmptyMapTest1()
		{
			tmb.Tiles[0, 0] = new EnergyTile(1);
			var m = tmb.BuildMap();
			Assert.That(m[0, 0] is EnergyTile);
			Assert.That(m.GetTileRecords<EnergyTile>().Length == 1);
		}

		[Test]
		public void NonEmptyMapTest2()
		{
			var t1 = new EnergyTile(1);
			var w1 = new EmptyWall(t1, new Direction(DirectionType.Left));

			tmb.Tiles[0, 0] = t1;
			tmb.Walls.Add(w1);

			var m = tmb.BuildMap();

			Assert.That(m.IsWallBetween(new MapCoordinates(0, 0), new MapCoordinates(0, 1)));
			Assert.That(m.IsWallBetween(new MapCoordinates(0, 1), new MapCoordinates(0, 0)));

			Assert.That(m.GetTileWalls(t1).Length == 1);
			Assert.That(m.GetTileWalls(t1)[0] is EmptyWall);
			Assert.That(m.GetTileWalls<EmptyWall>(t1).Length == 1);
		}

		[Test]
		public void SetNewTileTest()
		{
			var ct = new CheckpointTile(1);
			var w1 = new EmptyWall(tmb.Tiles[0, 0], new Direction(DirectionType.Left));

			tmb.Walls.Add(w1);

			var m = tmb.BuildMap();

			m.SetCheckpointTile(ct, new MapCoordinates(0, 0));

			Assert.That(m.GetTileWalls(ct).Count() == 1);
			Assert.That(m.IsWallBetween(new MapCoordinates(0, 0), new MapCoordinates(0, 1)));
			Assert.That(m.GetTileRecords<CheckpointTile>().Count() == 1);
			Assert.That(m.GetTileRecords<CheckpointTile>()[0].Coords == new MapCoordinates(0, 0));
			Assert.That(m.GetTileRecords<CheckpointTile>()[0].Tile == ct);
			Assert.That(m[0, 0] == ct);
		}

		[Test]
		public void ClearNewTileTest()
		{
			var ct = new CheckpointTile(1);
			var w1 = new EmptyWall(tmb.Tiles[0, 0], new Direction(DirectionType.Left));

			tmb.Walls.Add(w1);

			var m = tmb.BuildMap();

			m.SetCheckpointTile(ct, new MapCoordinates(0, 0));

			m.ClearSetTile(new MapCoordinates(0, 0));

			Assert.That(m.GetTileWalls(ct).Count() == 0);
			Assert.That(m.IsWallBetween(new MapCoordinates(0, 0), new MapCoordinates(0, 1)));
			Assert.That(m.GetTileRecords<CheckpointTile>().Count() == 0);
			Assert.That(m[0, 0] != ct);


		}
	}
}
