﻿using NUnit.Framework;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.MapDBTests
{
    [TestFixture]
    public class MapCoordinatesTest
    {
        [Test]
        public void PlusOperatorTest()
        {
			MapCoordinates mc = new MapCoordinates(4, 4);
			mc = mc + new Direction(DirectionType.Down);

			Assert.That(mc.ColIndex == 4 && mc.RowIndex == 5);
        }

		[Test]
		public void PlusOperatorTest2()
		{
			MapCoordinates mc = new MapCoordinates(4, 4);
			mc = new Direction(DirectionType.Left) + mc;

			Assert.That(mc.ColIndex == 3 && mc.RowIndex == 4);
		}

		[Test]
		public void DistanceTest()
		{
			MapCoordinates mc = new MapCoordinates(4, 4);
			MapCoordinates mc2 = new MapCoordinates(6, 6);
			Assert.That(mc.GetManhattanDistance(mc2) == 4);
		}

		[Test]
		public void DistanceTest2()
		{
			MapCoordinates mc = new MapCoordinates(4, 4);
			MapCoordinates mc2 = new MapCoordinates(4, 4);
			Assert.That(mc.GetManhattanDistance(mc2) == 0);
		}

		[Test]
		public void DistanceTest3()
		{
			MapCoordinates mc = new MapCoordinates(5, 5);
			MapCoordinates mc2 = new MapCoordinates(5, 2);
			Assert.That(mc.GetEuclideanDistance(mc2) == 3);
		}

		[Test]
		public void DistanceTest4()
		{
			MapCoordinates mc = new MapCoordinates(5, 5);
			MapCoordinates mc2 = new MapCoordinates(3, 2);
			//this tests the rounding
			Assert.That(mc.GetEuclideanDistance(mc2) == 4);
		}

		[Test]
		public void EqualityTest()
		{
			MapCoordinates mc = new MapCoordinates(4, 4);
			MapCoordinates mc2 = new MapCoordinates(4, 4);
			Assert.That(mc == mc2);
		}

		[Test]
		public void EqualityTest2()
		{
			MapCoordinates mc = new MapCoordinates(4, 4);
			MapCoordinates mc2 = new MapCoordinates(4, 5);
			Assert.That(mc != mc2);
		}
	}
}
