﻿using NUnit.Framework;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;

namespace RoboRoboRally.Model.Tests.MapDBTests.MapLoadingTests
{
	//these tests are dependend on the map database and they do not have 
	//to pass, if the database changes
	[TestFixture]
    public class LoadedMapTests
    {
        IMap loadedMap;

        [SetUp]
        public void TestSetup()
        {
            MapLoader mapLoader = new MapLoader(MapLoaderTests.mapDBPath);
            loadedMap = mapLoader.LoadGamePlanByID("5B");
        }

        [Test]
        public void MapSizeTest()
        {
            Assert.That(loadedMap.MapSize.ColCount == 10 && loadedMap.MapSize.RowCount == 10);
        }

        [Test]
        public void TilePresenceTest1()
        {
            Assert.That(loadedMap[0, 0] is EmptyTile);
        }

        [Test]
        public void TilePresenceTest2()
        {
            Assert.That(loadedMap[0, 1] is BlueBeltTile);
        }

        [Test]
        public void TilePresenceTest3()
        {
            Assert.That(loadedMap[7, 7] is EnergyTile);
        }

        [Test]
        public void TilePresenceTest4()
        {
            Assert.That(loadedMap[9, 9] is TextTile);
        }

        [Test]
        public void TilePresenceTest5()
        {
            Assert.That(loadedMap[6, 4] is EmptyTile);
        }

        [Test]
        public void TilePresenceTest6()
        {
            Assert.That(loadedMap[0, -1] is OutOfMapTile);
        }

        [Test]
        public void TilePresenceTest7()
        {
            Assert.That(loadedMap[10, 10] is OutOfMapTile);
        }

        [Test]
        public void WallPresenceTest1()
        {
            Assert.That(loadedMap.GetTileWalls(loadedMap[0, 0]).Length == 0);
        }


        [Test]
        public void WallPresenceTest2()
        {
            var walls = loadedMap.GetTileWalls(loadedMap[3, 3]);
            Assert.That(walls.Length == 1);
            Assert.That(walls[0].WallLocation == WallLocation.Up);
            Assert.That(walls[0] is LaserWall);
        }

        [Test]
        public void WallPresenceTest3()
        {
            var walls = loadedMap.GetTileWalls(loadedMap[6, 4]);
            Assert.That(walls.Length == 1);
            Assert.That(walls[0].WallLocation == WallLocation.Right);
            Assert.That(walls[0] is EmptyWall);
        }
    }
}
