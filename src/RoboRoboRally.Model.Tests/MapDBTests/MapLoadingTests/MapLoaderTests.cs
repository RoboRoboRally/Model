﻿using NUnit.Framework;
using RoboRoboRally.Model.MapDB;
using System.Reflection;

namespace RoboRoboRally.Model.Tests.MapDBTests.MapLoadingTests
{
	//these tests are dependend on the map database and they do not have 
	//to pass, if the database changes
    [TestFixture]
    public class MapLoaderTests
    {
		private MapLoader mapLoader;
		public static string mapDBPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"/GameData/DB/MapDB/";
		//public static string mapDBPath = @"D:/Coding/LargeProjects/RoboRoboRally/GameData/GameData/DB/MapDB/";


		[SetUp]
        public void TestSetup()
        {
			mapLoader = new MapLoader(mapDBPath);
        }
        
        [Test]
        public void AllGamePlansLoading()
        {
            var plans = mapLoader.LoadGamePlans();
            Assert.That(plans.Count > 0);
        }

        [Test]
        public void AllStartingPlansLoading()
        {
            var plans = mapLoader.LoadStartingPlans();
            Assert.That(plans.Count > 0);
        }

        [Test]
        public void LoadGamePlanByID()
        {
            var ID = "5B";
            var plan = mapLoader.LoadGamePlanByID(ID);
            Assert.That(plan.ID == ID);
            
        }

        [Test]
        public void LoadStartingPlanByID()
        {
            var ID = "A";
            var plan = mapLoader.LoadStartingPlanByID(ID);
            Assert.That(plan.ID == ID);
        }

        [Test]
        public void LoadNonexistentGamePlan()
        {
            Assert.Throws<MapDBParsingException>(() => mapLoader.LoadGamePlanByID("fň"));
        }

        [Test]
        public void LoadNonexistentStartingPlan()
        {
            Assert.Throws<MapDBParsingException>(() => mapLoader.LoadStartingPlanByID("fň"));
        }
    }
}
