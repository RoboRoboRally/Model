﻿using NUnit.Framework;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.MapDBTests.MapFactoryTests
{
    [TestFixture]
    public class MapRotationTests
    {
		TestMapBuilder tmb;
        MapFactory mapFactory;

        [SetUp]
        public void SetUp()
        {
			tmb = new TestMapBuilder();
            mapFactory = new MapFactory();
        }

        [Test]
        public void MapRotationTest1()
        {
			var m = tmb.BuildMap();
			var oldMs = m.MapSize;
			var oldID = m.ID;

			var rotatedMap = mapFactory.RotateMap(m, RotationType.Right);
            Assert.That(rotatedMap.ID == oldID);
            Assert.That(rotatedMap.MapSize.ColCount == oldMs.RowCount && rotatedMap.MapSize.RowCount == oldMs.ColCount);
        }

        [Test]
        public void MapRotationTest2()
        {
			tmb.Tiles[0, 0] = new TextTile("");
			var m = tmb.BuildMap();
            var rotatedMap = mapFactory.RotateMap(m, RotationType.None);
            Assert.That(rotatedMap[0, 0] is TextTile);
        }

        [Test]
        public void MapRotationTest3()
        {
			tmb.Tiles[0, 0] = new TextTile("");
			var m = tmb.BuildMap();
			var rotatedMap = mapFactory.RotateMap(m, RotationType.Right);
            Assert.That(rotatedMap[0, rotatedMap.MapSize.ColCount - 1] is TextTile);
        }

        [Test]
        public void MapRotationTest4()
        {
			tmb.Tiles[0, 0] = new TextTile("");
			var m = tmb.BuildMap();
			var rotatedMap = mapFactory.RotateMap(m, RotationType.Left);
            Assert.That(rotatedMap[rotatedMap.MapSize.RowCount - 1, 0] is TextTile);
        }

        [Test]
        public void DoubleMapRotationTest1()
        {
			tmb.Tiles[0, 0] = new TextTile("");
			var m = tmb.BuildMap();
			var rotatedMap = mapFactory.RotateMap(m, RotationType.Right);
            rotatedMap = mapFactory.RotateMap(rotatedMap, RotationType.Right);
            Assert.That(rotatedMap[rotatedMap.MapSize.RowCount - 1, rotatedMap.MapSize.ColCount - 1] is TextTile);
        }

		[Test]
		public void NonSquareMapRotationTest()
		{
			tmb = new TestMapBuilder(new MapSize(10, 9));
			var m = tmb.BuildMap();
			var rotatedMap = mapFactory.RotateMap(m, RotationType.Right);
			Assert.That(rotatedMap.MapSize.RowCount == 9 && rotatedMap.MapSize.ColCount == 10);
		}

		[Test]
		public void MapRotationTestWithBelts()
		{
			tmb.Tiles[0, 0] = new GreenBeltTile(new Direction(DirectionType.Right));
			var m = tmb.BuildMap();
			var rotatedMap = mapFactory.RotateMap(m, RotationType.Right);
			Assert.That(rotatedMap[0, rotatedMap.MapSize.ColCount - 1] is GreenBeltTile gbt && gbt.OutDirection == DirectionType.Down);
		}

		[Test]
		public void MapRotationTestWithWalls()
		{
			var t1 = new EmptyTile();
			var w1 = new EmptyWall(t1, new Direction(DirectionType.Right));

			tmb.Tiles[0, 0] = t1;
			tmb.Walls.Add(w1);

			var m = tmb.BuildMap();

			var rotatedMap = mapFactory.RotateMap(m, RotationType.Right);
			var walls = rotatedMap.GetTileWalls(rotatedMap[0, rotatedMap.MapSize.ColCount - 1]);
			Assert.That(walls.Length == 1);
			var wall = walls[0];
			Assert.That(wall.WallLocation == WallLocation.Up);
			Assert.That(wall.FacingDirection == DirectionType.Down);
		}
    }
}
