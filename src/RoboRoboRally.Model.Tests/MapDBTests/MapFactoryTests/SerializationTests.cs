﻿using NUnit.Framework;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Settings;
using RoboRoboRally.Model.Tests.MapDBTests.MapLoadingTests;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.Tests.MapDBTests.MapFactoryTests
{
    [TestFixture]
    public class SerializationTests
    {
		MapFactory mapFactory;

		[SetUp]
		public void TestSetup()
		{
			mapFactory = new MapFactory();
		}

		[Test]
		public void SerializationTest()
		{
			TestMapBuilder tmb1 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "a1" };
			TestMapBuilder tmb2 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "b2" };

			tmb1.Tiles[0, 0] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tmb1.Tiles[0, 1] = new RebootTile(new Direction(DirectionType.Down));
			tmb1.Tiles[0, 2] = new RebootTile(new Direction(DirectionType.Left));
			tmb1.Tiles[0, 3] = new CheckpointTile(1);
			tmb1.Tiles[0, 4] = new CheckpointTile(2);

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var joinedMaps = mapFactory.JoinMaps(m1, m2);

			var settings = mapFactory.SerializeMap(joinedMaps, RotationType.Right);

			Assert.That(settings.GamePlanID == "a1");
			Assert.That(settings.StartingPlanID == "b2");
			Assert.That(settings.MapModifications.GamePlanRotation == RotationType.Right);

			Assert.That(settings.MapModifications.CheckpointTiles.Count() == 2);
			Assert.That(settings.MapModifications.RebootTiles.Count() == 2);
			Assert.That(settings.MapModifications.PriorityAntenna.Coords == new MapCoordinates(0, 0));
		}

		[Test]
		public void InvalidSerializationTest()
		{
			TestMapBuilder tmb1 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "a1" };
			TestMapBuilder tmb2 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "b2" };

			tmb1.Tiles[0, 0] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tmb1.Tiles[0, 1] = new RebootTile(new Direction(DirectionType.Down));
			tmb1.Tiles[0, 2] = new RebootTile(new Direction(DirectionType.Left));
			tmb1.Tiles[0, 3] = new CheckpointTile(1);
			tmb1.Tiles[0, 4] = new CheckpointTile(3);

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var joinedMaps = mapFactory.JoinMaps(m1, m2);

			Assert.Throws<MapFactoryException>(() => mapFactory.SerializeMap(joinedMaps, RotationType.None));
		}

		[Test]
		public void InvalidSerializationTest2()
		{
			TestMapBuilder tmb1 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "a1" };
			TestMapBuilder tmb2 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "b2" };

			tmb1.Tiles[0, 0] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tmb1.Tiles[0, 3] = new CheckpointTile(1);
			tmb1.Tiles[0, 4] = new CheckpointTile(2);

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var joinedMaps = mapFactory.JoinMaps(m1, m2);

			Assert.Throws<MapFactoryException>(() => mapFactory.SerializeMap(joinedMaps, RotationType.None));
		}

		[Test]
		public void InvalidSerializationTest3()
		{
			TestMapBuilder tmb1 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "a1" };
			TestMapBuilder tmb2 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "b2" };

			tmb1.Tiles[0, 0] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tmb1.Tiles[0, 1] = new RebootTile(new Direction(DirectionType.Down));
			tmb1.Tiles[0, 2] = new RebootTile(new Direction(DirectionType.Left));

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var joinedMaps = mapFactory.JoinMaps(m1, m2);

			Assert.Throws<MapFactoryException>(() => mapFactory.SerializeMap(joinedMaps, RotationType.None));
		}

		[Test]
		public void InvalidSerializationTest4()
		{
			TestMapBuilder tmb1 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "a1" };
			TestMapBuilder tmb2 = new TestMapBuilder(new MapSize(10, 10)) { MapID = "b2" };
			
			tmb1.Tiles[0, 1] = new RebootTile(new Direction(DirectionType.Down));
			tmb1.Tiles[0, 2] = new RebootTile(new Direction(DirectionType.Left));
			tmb1.Tiles[0, 3] = new CheckpointTile(1);
			tmb1.Tiles[0, 4] = new CheckpointTile(2);

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var joinedMaps = mapFactory.JoinMaps(m1, m2);

			Assert.Throws<MapFactoryException>(() => mapFactory.SerializeMap(joinedMaps, RotationType.None));
		}

		[Test]
		public void DeserializationTest()
		{
			//this test is dependent on the map database and it does not have 
			//to pass, if the database changes
			MapSettings ms = new MapSettings()
			{
				GamePlanID = "5B",
				StartingPlanID = "A",
				MapModifications = new MapModifications()
				{
					GamePlanRotation = RotationType.None,
					PriorityAntenna = new TileRecord<PriorityAntennaTile>() { Coords = new MapCoordinates(0, 0), Tile = new PriorityAntennaTile(new Direction(DirectionType.Down)) },
					RebootTiles = new TileRecord<RebootTile>[] { new TileRecord<RebootTile> { Coords = new MapCoordinates(1, 0), Tile = new RebootTile(new Direction(DirectionType.Right)) } },
					CheckpointTiles = new TileRecord<CheckpointTile>[] { new TileRecord<CheckpointTile> { Coords = new MapCoordinates(2, 0), Tile = new CheckpointTile(1) } }
				}
			};

			var joinedMap = mapFactory.DeserializeMap(MapLoaderTests.mapDBPath, ms);
			Assert.That(joinedMap[0, 0] is PriorityAntennaTile);
			Assert.That(joinedMap[0, 1] is BlueBeltTile);
			Assert.That(joinedMap[0, 9] is EnergyTile);
		}
	}
}
