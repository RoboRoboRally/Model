﻿using NUnit.Framework;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.MapDBTests.MapFactoryTests
{
    [TestFixture]
    public class MapJoinTests
    {
        MapFactory mapFactory;
		TestMapBuilder tmb1;
		TestMapBuilder tmb2;

        [SetUp]
        public void SetUp()
        {
			mapFactory = new MapFactory();
			tmb1 = new TestMapBuilder { MapID = "map1" };
			tmb2 = new TestMapBuilder { MapID = "map2" };
        }

		[Test]
		public void JoinedMapsSizeTest()
		{
			tmb1 = new TestMapBuilder(new MapSize(10, 5)) { MapID = "a1" };
			tmb2 = new TestMapBuilder(new MapSize(2, 5)) { MapID = "b2" };

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var jm = mapFactory.JoinMaps(m1, m2);
			Assert.That(jm.MapSize.ColCount == 5 && jm.MapSize.RowCount == 12);
		}

		[Test]
		public void JoinedMapsIDTest()
		{
			tmb1 = new TestMapBuilder(new MapSize(10, 5)) { MapID = "a1" };
			tmb2 = new TestMapBuilder(new MapSize(2, 5)) { MapID = "b2" };

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var jm = mapFactory.JoinMaps(m1, m2);
			Assert.That(jm.ID == "a1_b2");
		}

		[Test]
		public void JoinedMapsTilePresenceTest1()
		{
			tmb1.Tiles[2, 2] = new EnergyTile(1);
			tmb1.Tiles[3, 3] = new PitTile();

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var jm = mapFactory.JoinMaps(m1, m2);

			Assert.That(jm[2,2] is EnergyTile);
			Assert.That(jm[3,3] is PitTile);
		}

		[Test]
		public void JoinedMapsTilePresenceTest2()
		{
			tmb1 = new TestMapBuilder(new MapSize(10, 5)) { MapID = "a1" };
			tmb2 = new TestMapBuilder(new MapSize(4, 5)) { MapID = "b2" };

			tmb1.Tiles[2, 2] = new EnergyTile(1);
			tmb2.Tiles[2, 2] = new TextTile("fň");

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var jm = mapFactory.JoinMaps(m1, m2);

			Assert.That(jm[2, 2] is EnergyTile);
			Assert.That(jm[12, 2] is TextTile);
		}

		[Test]
		public void JoinedMapsWallPresenceTest1()
		{
			tmb1 = new TestMapBuilder(new MapSize(10, 5)) { MapID = "a1" };
			tmb2 = new TestMapBuilder(new MapSize(4, 5)) { MapID = "b2" };

			var t1 = new EmptyTile();
			var w1 = new EmptyWall(t1, new Direction(DirectionType.Right));

			tmb1.Tiles[2, 2] = t1;
			tmb1.Walls.Add(w1);

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var jm = mapFactory.JoinMaps(m1, m2);

			Assert.That(jm.GetTileWalls(jm[2, 2]).Length == 1 && jm.GetTileWalls(jm[2, 2])[0].WallLocation == WallLocation.Left);
		}

		[Test]
		public void JoinedMapsWallTileReferenceTest()
		{
			tmb1 = new TestMapBuilder(new MapSize(10, 5)) { MapID = "a1" };
			tmb2 = new TestMapBuilder(new MapSize(4, 5)) { MapID = "b2" };

			var t1 = new PitTile();
			var w1 = new EmptyWall(t1, new Direction(DirectionType.Right));

			tmb1.Tiles[0, 0] = t1;
			tmb1.Walls.Add(w1);

			var m1 = tmb1.BuildMap();
			var m2 = tmb2.BuildMap();

			var jm = mapFactory.JoinMaps(m1, m2);

			Assert.That(jm.GetTileWalls(jm[0, 0])[0].Tile is PitTile);
		}
    }
}
