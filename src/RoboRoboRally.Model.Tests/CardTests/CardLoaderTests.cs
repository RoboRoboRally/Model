﻿using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using System.Reflection;

namespace RoboRoboRally.Model.Tests.CardTests
{
	//these tests are dependend on the card database and they do not have 
	//to pass, if the database changes
	[TestFixture]
	public class CardLoaderTests
	{
		public static string cardDBPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"/GameData/DB/CardDB/";
		//public static string cardDBPath = @"D:/Coding/LargeProjects/RoboRoboRally/GameData/GameData/DB/CardDB";
		CardLoader cl;

		[SetUp]
		public void TestSetup()
		{
			cl = new CardLoader(cardDBPath);
		}

		[Test]
		public void AllPlayerCardsLoading()
		{
			var cards = cl.LoadPlayerProgrammingCards();
			Assert.That(cards.Length > 0);
		}

		[Test]
		public void AllDamageCardsLoading()
		{
			var cards = cl.LoadDamageCards();
			Assert.That(cards.Length > 0);
		}

		[Test]
		public void AllUpgradeCardsLoading()
		{
			var cards = cl.LoadUpgradeCards();
			Assert.That(cards.Length > 0);
		}

		[Test]
		public void AllSpecialProgrammingCardsLoading()
		{
			var cards = cl.LoadSpecialProgrammingCards();
			Assert.That(cards.Length > 0);
		}

		[Test]
		public void SingleCardLoadTest1()
		{
			var c = cl.LoadPlayerProgrammingCard("Move1");
			Assert.That(c.Name == "Move1");
		}

		[Test]
		public void SingleCardLoadTest2()
		{
			var c = cl.LoadDamageCard("Spam");
			Assert.That(c.Name == "Spam");
		}

		[Test]
		public void SingleCardLoadTest3()
		{
			var c = cl.LoadProgrammingCard("Spam");
			Assert.That(c is DamageCard);
			Assert.That(c.Name == "Spam");
		}

		[Test]
		public void SingleCardLoadTest4()
		{
			Assert.Throws<CardDBParsingException>(() => cl.LoadPlayerProgrammingCard("Spam"));
		}

		[Test]
		public void SingleCardLoadTest5()
		{
			Assert.Throws<CardDBParsingException>(() => cl.LoadDamageCard("Move1"));
		}

		[Test]
		public void SingleCardLoadTest6()
		{
			Assert.Throws<CardDBParsingException>(() => cl.LoadProgrammingCard("abc"));
		}

		[Test]
		public void SingleCardLoadTest7()
		{
			var c = cl.LoadProgrammingCard("SpeedRoutine");
			Assert.That(c.Name == "SpeedRoutine");
		}

		[Test]
		public void SingleCardLoadTest8()
		{
			var c = cl.LoadSpecialProgrammingCard("SpeedRoutine");
			Assert.That(c.Name == "SpeedRoutine");
		}

		[Test]
		public void SingleCardLoadTest9()
		{
			Assert.Throws<CardDBParsingException>(() => cl.LoadSpecialProgrammingCard("abc"));
		}

		[Test]
		public void SingleUCardLoadTest1()
		{
			var c = cl.LoadUpgradeCard("MemoryStick");
			Assert.That(c.Name == "MemoryStick");
			Assert.That(c.EnergyCost == 3);
			Assert.That(c.UpgradeCardType == UpgradeCardType.Permanent);
			Assert.That(c.GetHooks().Length == 1);
		}

		[Test]
		public void SingleUCardLoadTest2()
		{
			var c = cl.LoadUpgradeCard("Zoop");
			Assert.That(c.Name == "Zoop");
			Assert.That(c.EnergyCost == 1);
			Assert.That(c.UpgradeCardType == UpgradeCardType.Temporary);
			Assert.That(c.GetHooks().Length == 3);
		}
	}
}
