﻿using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Tests.TestBuilders;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Tests.CardTests
{
    [TestFixture]
    public class UpgradeCardBoardTests
    {
		TestCardBuilder tcb;
		Dictionary<UpgradeCardType, int> limits;

		[SetUp]
		public void TestSetup()
		{
			tcb = new TestCardBuilder();
			limits = new Dictionary<UpgradeCardType, int>() { { UpgradeCardType.Permanent, 3 },{ UpgradeCardType.Temporary, 3 } };

		}

		[Test]
		public void ConstructionTest()
		{
			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			Assert.That(ucb.GetAllUpgradeCards().Length == 0);
			Assert.That(ucb.GetCardLimits() == limits);
			Assert.That(ucb.GetCardCount(UpgradeCardType.Temporary) == 0);
			Assert.That(ucb.GetCardCount(UpgradeCardType.Permanent) == 0);
		}

		[Test]
		public void AddCardTest1()
		{
			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			var c1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent);
			ucb.AddCard(c1);
			ucb.AddCard(tcb.BuildUCard("u2", UpgradeCardType.Temporary));
			ucb.AddCard(tcb.BuildUCard("u3", UpgradeCardType.Permanent));

			Assert.That(ucb.GetCardCount(UpgradeCardType.Permanent) == 2);
			Assert.That(ucb.GetCardCount(UpgradeCardType.Temporary) == 1);

			Assert.That(ucb.GetAllUpgradeCards().Length == 3);

			Assert.That(ucb.GetCard("u1") == c1);
			Assert.Throws<RunningGameException>(() => ucb.GetCard("u4"));
		}

		[Test]
		public void AddCardTest2()
		{
			//lets add card above limits
			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			ucb.AddCard(tcb.BuildUCard("u1", UpgradeCardType.Permanent));
			ucb.AddCard(tcb.BuildUCard("u2", UpgradeCardType.Permanent));
			ucb.AddCard(tcb.BuildUCard("u3", UpgradeCardType.Permanent));

			Assert.Throws<RunningGameException>(() => ucb.AddCard(tcb.BuildUCard("u4", UpgradeCardType.Permanent)));
		}

		[Test]
		public void AddCardTest3()
		{
			//lets add the same card twice
			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			ucb.AddCard(tcb.BuildUCard("u1", UpgradeCardType.Permanent));
			Assert.Throws<RunningGameException>(() => ucb.AddCard(tcb.BuildUCard("u1", UpgradeCardType.Permanent)));
		}

		[Test]
		public void RemoveCardTest1()
		{
			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			ucb.AddCard(tcb.BuildUCard("u1", UpgradeCardType.Permanent));
			ucb.AddCard(tcb.BuildUCard("u2", UpgradeCardType.Permanent));
			ucb.AddCard(tcb.BuildUCard("u3", UpgradeCardType.Permanent));

			ucb.RemoveCard("u1");

			Assert.That(ucb.GetAllUpgradeCards().Length == 2);
			Assert.That(ucb.GetCardCount(UpgradeCardType.Permanent) == 2);
		}

		[Test]
		public void RemoveCardTest2()
		{
			//remove card that has been already removed

			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			ucb.AddCard(tcb.BuildUCard("u1", UpgradeCardType.Permanent));

			ucb.RemoveCard("u1");

			Assert.Throws<RunningGameException>(() => ucb.RemoveCard("u1"));
		}

		[Test]
		public void RemoveAllTest1()
		{
			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			ucb.AddCard(tcb.BuildUCard("u1", UpgradeCardType.Permanent));
			ucb.AddCard(tcb.BuildUCard("u2", UpgradeCardType.Temporary));
			ucb.AddCard(tcb.BuildUCard("u3", UpgradeCardType.Permanent));

			var cards = ucb.RemoveAllUpgradeCards();

			Assert.That(ucb.GetAllUpgradeCards().Length == 0);
			Assert.That(ucb.GetCardCount(UpgradeCardType.Permanent) == 0);
			Assert.That(ucb.GetCardCount(UpgradeCardType.Temporary) == 0);
			Assert.That(cards.Any(x => x.Name == "u1"));
			Assert.That(cards.Any(x => x.Name == "u2"));
		}

		[Test]
		public void RemoveAllTest2()
		{
			//remove all on empty board is ok
			UpgradeCardBoard ucb = new UpgradeCardBoard(limits);

			ucb.RemoveAllUpgradeCards();

			Assert.That(ucb.GetAllUpgradeCards().Length == 0);
		}	
	}
}
