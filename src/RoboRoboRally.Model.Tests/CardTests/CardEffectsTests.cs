﻿using NUnit.Framework;
using Moq;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.Core.Actions;
using System.Linq;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;

namespace RoboRoboRally.Model.Tests.CardTests
{
    [TestFixture]
    public class CardEffectsTests
    {
		//testing without notifier is on purpose - in these tests, we want to only test
		//effects observable from the outside
		TestCardBuilder tcb;
		TestRobotBuilder trb;

		Mock<IGameState> gsMock;

		Robot r;

		[SetUp]
		public void TestSetup()
		{
			tcb = new TestCardBuilder();
			trb = new TestRobotBuilder();

			r = trb.BuildRobot();

			gsMock = new Mock<IGameState>();
			var notMock = new Mock<IGameEventNotifier>();
			gsMock.Setup(x => x.Notifier).Returns(notMock.Object);
			gsMock.Setup(x => x.GetAction<MovementAction>()).Returns(new MovementAction());

			TestMapBuilder tmb = new TestMapBuilder();

			var m = tmb.BuildMap();
			gsMock.Setup(x => x.Map).Returns(m);

		}

		[Test]
		public void TurnEffectTest()
		{
			var c = tcb.BuildPCard("a", new TurnEffect(RotationType.Right));

			var dir = r.FacingDirection;
			c.PlayCard(gsMock.Object, r, 0);
			var rotatedDir = dir.Rotate(RotationType.Right);

			Assert.That(r.FacingDirection == rotatedDir);
		}

		[Test]
		public void MoveEffectTest1()
		{
			var c = tcb.BuildPCard("a", new MoveEffect(2));

			gsMock.Setup(x => x.GetAction<MovementAction>()).Returns(new MovementAction());

			var oldPos = r.Position;
			var newPos = oldPos + r.FacingDirection;
			newPos = newPos + r.FacingDirection;

			c.PlayCard(gsMock.Object, r, 0);

			Assert.That(r.Position == newPos);
		}

		[Test]
		public void MoveEffectTest2()
		{
			var c = tcb.BuildPCard("a", new MoveEffect(-1));

			gsMock.Setup(x => x.GetAction<MovementAction>()).Returns(new MovementAction());

			var oldPos = r.Position;
			var dir = r.FacingDirection;
			var opDir = dir.GetOppositeDirection();
			var newPos = oldPos + opDir;

			c.PlayCard(gsMock.Object, r, 0);

			Assert.That(r.FacingDirection == dir);
			Assert.That(r.Position == newPos);
		}

		[Test]
		public void GainEnergyEffectTest()
		{
			var c = tcb.BuildPCard("a", new GainEnergyEffect(1));

			var oldEnergyAmount = r.EnergyAmount;
			var newAmount = oldEnergyAmount + 1;

			c.PlayCard(gsMock.Object, r, 0);

			Assert.That(r.EnergyAmount == newAmount);
		}

		[Test]
		public void PerformPrevRegAgainEffectTest1()
		{
			var c = tcb.BuildPCard("again", new PerformPrevRegAgainEffect());

			var program = new ProgrammingCard[5] { c, null, null, null, null };

			r.SetNewProgram(program);

			var oldPos = r.Position;
			var oldDir = r.FacingDirection;

			r.PlayNextRegister(gsMock.Object);

			Assert.That(r.FacingDirection == oldDir);
			Assert.That(r.Position == oldPos);
		}

		[Test]
		public void PerformPrevRegAgainEffectTest2()
		{
			var c1 = tcb.BuildPCard("turnleft", new TurnEffect(RotationType.Left));
			var c2 = tcb.BuildPCard("again", new PerformPrevRegAgainEffect());

			var program = new ProgrammingCard[5] { c1, c2, null, null, null };

			r.SetNewProgram(program);

			r.PlayNextRegister(gsMock.Object);

			var oldPos = r.Position;
			var oldDir = r.FacingDirection;

			r.PlayNextRegister(gsMock.Object);

			Assert.That(r.FacingDirection == oldDir.Rotate(RotationType.Left));
			Assert.That(r.Position == oldPos);
		}

		[Test]
		public void PerformPrevRegAgainEffectTest3()
		{
			var c1 = tcb.BuildPCard("turnleft", new TurnEffect(RotationType.Left));
			var c2 = tcb.BuildPCard("moveback", new MoveEffect(-1));
			var c3 = tcb.BuildPCard("again", new PerformPrevRegAgainEffect());

			var program = new ProgrammingCard[5] { c1, c2, c3, c3, null };

			r.SetNewProgram(program);

			var oldPos = r.Position;
			var oldDir = r.FacingDirection;

			r.PlayNextRegister(gsMock.Object);

			r.PlayNextRegister(gsMock.Object);

			r.PlayNextRegister(gsMock.Object);

			r.PlayNextRegister(gsMock.Object);

			Assert.That(r.FacingDirection == oldDir.Rotate(RotationType.Left));
			var opoDir = r.FacingDirection.GetOppositeDirection();
			var newPos = oldPos + opoDir;
			newPos += opoDir;
			newPos += opoDir;

			Assert.That(r.Position == newPos);
		}

		[Test]
		public void TakeDamageEffectTest()
		{
			var c1 = tcb.BuildDCard("dmg", new TakeDamageEffect(new Damage() { DamageCards = new DamageCard[2] { tcb.BuildDCard("dmg1", new CardEffect[0]), tcb.BuildDCard("dmg2", new CardEffect[0]) } }));

			c1.PlayCard(gsMock.Object, r, 0);
			
			//draw "all" cards and verify presence of damage cards
			var drawnCards = r.DrawAllCards();
			Assert.That(drawnCards.Count(c => c.Name == "dmg1") == 1);
			Assert.That(drawnCards.Count(c => c.Name == "dmg2") == 1);
		}

		[Test]
		public void DiscardEffectTest()
		{
			//first deal damage (which puts some cards into discard pack) and then try to burn them
			var c1 = tcb.BuildDCard("dmg", new TakeDamageEffect(new Damage() { DamageCards = new DamageCard[3] { tcb.BuildDCard("dmg1", new CardEffect[0]), tcb.BuildDCard("dmg2", new CardEffect[0]), tcb.BuildDCard("dmg2", new CardEffect[0]) } }));

			c1.PlayCard(gsMock.Object, r, 0);

			var c2 = tcb.BuildPCard("spam folder", new DiscardEffect(tcb.BuildDCard("dmg2", new CardEffect[0])));
			
			c2.PlayCard(gsMock.Object, r, 0);

			var drawnCards = r.DrawAllCards();
			Assert.That(drawnCards.Count(c => c.Name == "dmg2") == 1);
		}

		[Test]
		public void DealAoEDamageEffectTest()
		{
			trb = new TestRobotBuilder()
			{
				Name = "SMASH2",
				Position = new MapCoordinates(5, 2)
			};

			var r2 = trb.BuildRobot();

			//just be careful and mock active robots list
			gsMock.Setup(x => x.GameActiveRobots).Returns((new Robot[2] { r, r2 }));

			var c1 = tcb.BuildPCard("aoe", new DealAoEDamageEffect(new Damage() { DamageCards = new DamageCard[1] { tcb.BuildDCard("dmg1", new CardEffect[0]) } }, 3));

			c1.PlayCard(gsMock.Object, r, 0);

			Assert.That(r2.DrawAllCards().Count(c => c.Name == "dmg1") == 1);
			Assert.That(r.DrawAllCards().Count(c => c.Name == "dmg1") == 0);
		}

		[Test]
		public void DealAoEDamageEffectTest2()
		{
			trb = new TestRobotBuilder()
			{
				Name = "SMASH2",
				Position = new MapCoordinates(5, 2)
			};

			var r2 = trb.BuildRobot();

			//just be careful and mock active robots list
			gsMock.Setup(x => x.GameActiveRobots).Returns((new Robot[2] { r, r2 }));

			var c1 = tcb.BuildPCard("aoe", new DealAoEDamageEffect(new Damage() { DamageCards = new DamageCard[1] { tcb.BuildDCard("dmg1", new CardEffect[0]) } }, 2));

			c1.PlayCard(gsMock.Object, r, 0);

			Assert.That(r2.DrawAllCards().Count(c => c.Name == "dmg1") == 0);
			Assert.That(r.DrawAllCards().Count(c => c.Name == "dmg1") == 0);
		}

		[Test]
		public void ChooseCardEffectTest()
		{
			Mock<IInputHandler> iMock = new Mock<IInputHandler>();
			gsMock.Setup(x => x.InputHandler).Returns(iMock.Object);

			iMock.Setup(i => i.ChooseCard(It.IsAny<string>(), It.IsAny<ProgrammingCard[]>())).Returns((string name, ProgrammingCard[] val) => { return val[0]; });

			gsMock.Setup(x => x.GetAction<MovementAction>()).Returns(new MovementAction());

			var c1 = tcb.BuildPCard("choose2", new CardEffect[1] { new ChooseCardEffect(new ProgrammingCard[2] { tcb.BuildMove(1), tcb.BuildTurn(RotationType.Left) }) });

			var oldPos = r.Position;
			var oldDir = r.FacingDirection;

			c1.PlayCard(gsMock.Object, r, 0);

			Assert.That(r.Position == oldPos + r.FacingDirection);
			Assert.That(r.FacingDirection == oldDir);
		}

		[Test]
		public void PlayTopCardEffectTest()
		{
			trb = new TestRobotBuilder()
			{
				StartingPack = new Pack<ProgrammingCard>(new ProgrammingCard[2] { tcb.BuildMove(1), tcb.BuildMove(2) })
			};

			r = trb.BuildRobot();

			gsMock.Setup(x => x.GetAction<MovementAction>()).Returns(new MovementAction());

			var c1 = tcb.BuildPCard("topCard", new CardEffect[1] { new PlayTopCardEffect() });

			//because of discard and other effects, we have to play this card as a part of program
			var program = new ProgrammingCard[5] { c1, null, null, null, null };

			r.SetNewProgram(program);

			var oldPos = r.Position;

			r.PlayNextRegister(gsMock.Object);
		
			Assert.That(r.Position == oldPos + r.FacingDirection);
		}

		[Test]
		public void RebootEffectTest()
		{
			//first setup map
			var tmb = new TestMapBuilder();
			var rTile = new RebootTile(new Direction(DirectionType.Up));

			tmb.Tiles[2, 2] = rTile;
			tmb.Tiles[9, 9] = new StartingTile(new Direction(DirectionType.Up));

			var m = tmb.BuildMap();
			gsMock.Setup(x => x.Map).Returns(m);

			gsMock.Setup(x => x.TurnActiveRobots).Returns(new Robot[1] { r });
			gsMock.Setup(x => x.GameActiveRobots).Returns(new Robot[1] { r });

			gsMock.Setup(x => x.GetAction<RebootAction>()).Returns(new RebootAction(
				new Damage() { DamageCards = new DamageCard[1] { tcb.BuildDCard("dmg1", new CardEffect[0]) } }));

			var c1 = tcb.BuildPCard("reboot", new CardEffect[1] { new RebootEffect() });

			c1.PlayCard(gsMock.Object, r, 0);

			Assert.That(r.Position == new MapCoordinates(2, 2));
			Assert.That(r.FacingDirection == rTile.OutDirection);
			Assert.That(r.DrawAllCards().Count(c => c.Name == "dmg1") == 1);
		}

	}
}
