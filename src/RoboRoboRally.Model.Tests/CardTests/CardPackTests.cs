﻿using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Tests.CardTests
{
    [TestFixture]
    public class CardPackTests
    {
		ProgrammingCard[] playerProgrammingCards;

		IDictionary<ProgrammingCard, int> packInput;
		Pack<ProgrammingCard> pack;

		ProgrammingCard dummy;

		CardLoader cl;

		[SetUp]
		public void TestSetup()
		{
			cl = new CardLoader(CardLoaderTests.cardDBPath);
			playerProgrammingCards = cl.LoadPlayerProgrammingCards();
			packInput = new Dictionary<ProgrammingCard, int>();

			foreach (var c in playerProgrammingCards)
				packInput.Add(c, 1);

			pack = new Pack<ProgrammingCard>(packInput);

			dummy = new ProgrammingCard("Dummy", null);
		}

        [Test]
        public void PackSizeTest()
        {
			Assert.That(pack.Size() == playerProgrammingCards.Length);
        }

		[Test]
		public void ShuffleTest()
		{
			int size = pack.Size();
			pack.ShuffleDrawingPile();
			Assert.That(pack.Size() == size);
		}
		
		[Test]
		public void DiscardTest()
		{
			int size = pack.Size();
			pack.DiscardCard(dummy);
			Assert.That(size + 1 == pack.Size());
		}

		[Test]
		public void DiscardCardsTest()
		{
			ProgrammingCard[] dummies = new ProgrammingCard[2] { dummy, dummy };
			int size = pack.Size();
			pack.DiscardCards(dummies);
			Assert.That(pack.Size() == size + 2);
		}

		[Test]
		public void DrawCardTest()
		{
			int s = pack.Size();
			var c = pack.Draw();

			Assert.That(pack.Size() + 1 == s);
			Assert.That(playerProgrammingCards.Any(x => x.Name == c.Name));
		}

		[Test]
		public void DrawCardsTest1()
		{
			int s = pack.Size();
			var cs = pack.Draw(2);

			Assert.That(pack.Size() + 2 == s);
			foreach (var c in cs)
				Assert.That(playerProgrammingCards.Any(x => x.Name == c.Name));
		}

		[Test]
		public void DrawCardsTest2()
		{
			//lets draw too many cards 
			int s = pack.Size();
			var cs = pack.Draw(s + 1);

			Assert.That(pack.Size() == 0);
			foreach (var c in cs)
				Assert.That(playerProgrammingCards.Any(x => x.Name == c.Name));

		}

		[Test]
		public void DiscardAndDraw()
		{
			pack.DiscardCard(dummy);

			//first, draw the initial cards
			pack.Draw(playerProgrammingCards.Length);
			//draw the next one - this has to be our dummy card
			var c = pack.Draw();
			Assert.That(c.Name == dummy.Name);
			Assert.That(pack.Size() == 0);
			
		}

		[Test]
		public void CopyPackTest()
		{
			var pack2 = pack.CopyPack();

			Assert.That(pack2.Size() == pack.Size());

			var p1cs = pack.Draw(playerProgrammingCards.Length);
			var p2cs = pack2.Draw(playerProgrammingCards.Length);

			foreach (var c in p1cs)
				Assert.That(p2cs.Any(x => x.Name == c.Name));
		}

		[Test]
		public void CopyPackTest2()
		{
			var pack2 = pack.CopyPack();
			int p2s = pack2.Size();
			int p1s = pack.Size();

			pack2.DiscardCard(dummy);

			Assert.That(pack2.Size() == p2s + 1);
			Assert.That(pack.Size() == p1s);
		}


		[Test]
		public void PackBurnTest()
		{
			Assert.That(pack.TryBurnFromDiscardPile(dummy) == false);
		}

		[Test]
		public void PackBurnTest2()
		{
			pack.DiscardCard(dummy);
			Assert.That(pack.TryBurnFromDiscardPile(dummy) == true);
		}
	}
}
