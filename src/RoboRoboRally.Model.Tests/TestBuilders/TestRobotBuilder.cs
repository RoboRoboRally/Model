﻿using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.TestBuilders
{
    class TestRobotBuilder
    {
		int registerBoardSize = 5;
		public int EnergyAmount { get; set; } = 5;

		public Dictionary<UpgradeCardType, int> UpgradeCardsLimits { get; set; } = new Dictionary<UpgradeCardType, int>() { { UpgradeCardType.Permanent, 3 }, { UpgradeCardType.Temporary, 3 } };
		public Direction FacingDir { get; set; } = new Direction(DirectionType.Left);
		public string Name { get; set; } = "SMASH-SMASH";
		public MapCoordinates Position { get; set; } = new MapCoordinates(5, 5);
		public Pack<ProgrammingCard> StartingPack { get; set; } = new Pack<ProgrammingCard>(new ProgrammingCard[0]);
		public Damage DefaultDamage { get; set; } = new Damage() { DamageCards = new DamageCard[1] { new DamageCard("Spam", new CardEffect[1] { new PlayTopCardEffect() }) } };

		public Robot BuildRobot()
		{
			return new Robot(Name, EnergyAmount, FacingDir, Position, StartingPack, new RegisterBoard(registerBoardSize), new UpgradeCardBoard(UpgradeCardsLimits), DefaultDamage);
		}
    }
}
