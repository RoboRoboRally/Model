﻿using Moq;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoboRoboRally.Model.Tests.TestBuilders
{
    public class TestGameStateBuilder
    {
		int registerCount = 5;

		public Mock<IGameEventNotifier> NotMock { get; set; } = new Mock<IGameEventNotifier>();
		public Mock<IInputHandler> IMock { get; set; } = new Mock<IInputHandler>();

		public IList<IAction> Actions { get; set; } = new List<IAction>();
		public IList<PhaseBase> Phases { get; set; } = new List<PhaseBase>();
		public Pack<UpgradeCard> UpgradeCards { get; set; } = new Pack<UpgradeCard>(new UpgradeCard[0]);
		public IMap Map { get; set; } = new TestMapBuilder().BuildMap();
		public IList<Robot> StartingRobots { get; set; } = new List<Robot>();

		internal GameState BuildGameStateWithTurnBegan()
		{
			var gs = BuildGameStateWOTurnBegan();
			gs.TurnBegan();
			return gs;
		}

		internal GameState BuildGameStateWOTurnBegan()
		{
			var gs = new GameState(Phases.ToArray(), Actions.ToArray(), UpgradeCards, 
				Map, NotMock.Object, IMock.Object, registerCount);

			foreach (var r in StartingRobots)
				gs.AddNewRobot(r);

			return gs;
		}
    }
}
