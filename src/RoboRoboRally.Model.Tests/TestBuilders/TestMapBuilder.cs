﻿using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.TestBuilders
{
    class TestMapBuilder
	{
		private MapSize MapSize { get; set; }
		public string MapID { get; set; } = "map1";
		public Tile[,] Tiles { get; set; }
		public IList<Wall> Walls { get; set; }
		
		public IMap BuildMap()
		{
			return new Map(MapID, MapSize, Tiles, Walls);
		}

		public TestMapBuilder(MapSize mapSize)
		{
			MapSize = mapSize;

			Tiles = new Tile[MapSize.RowCount, MapSize.ColCount];
			for (int i = 0; i < MapSize.RowCount; ++i)
			{
				for (int j = 0; j < MapSize.ColCount; ++j)
				{
					Tiles[i, j] = new EmptyTile();
				}
			}
			Walls = new List<Wall>();
		}

		public TestMapBuilder()
		{
			MapSize = new MapSize(10, 10);

			Tiles = new Tile[MapSize.RowCount, MapSize.ColCount];
			for (int i = 0; i < MapSize.RowCount; ++i)
			{
				for (int j = 0; j < MapSize.ColCount; ++j)
				{
					Tiles[i, j] = new EmptyTile();
				}
			}
			Walls = new List<Wall>();
		}
    }
}
