﻿using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.TestBuilders
{
    public class TestCardBuilder
	{
		internal UpgradeCard BuildUCard(string name, UpgradeCardType type, int energyCost, params IUpgradeCardHookBase[] hooks)
		{
			return new UpgradeCard(name, type, energyCost, hooks);
		}

		public UpgradeCard BuildUDummy()
		{
			return new UpgradeCard("dummy", UpgradeCardType.Permanent, 0, new IUpgradeCardHookBase[0]);
		}

		public UpgradeCard BuildUCard(string name)
		{
			return BuildUCard(name, UpgradeCardType.Permanent, 0);
		}

		public UpgradeCard BuildUCard(string name, UpgradeCardType type)
		{
			return BuildUCard(name, type, 0);
		}

		internal ProgrammingCard BuildPCard(string name, params CardEffect[] cardEffects)
		{
			return new ProgrammingCard(name, cardEffects);
		}

		public ProgrammingCard BuildDummy()
		{
			return new ProgrammingCard("dummy", new CardEffect[0]);
		}

		public ProgrammingCard BuildMove(int amount)
		{
			return new ProgrammingCard("move" + amount, new CardEffect[1] { new MoveEffect(amount) });
		}

		public ProgrammingCard BuildTurn(RotationType rot)
		{
			return new ProgrammingCard("turn" + rot.ToString(), new CardEffect[1] { new TurnEffect(rot) });
		}

		public Pack<ProgrammingCard> BuildBasicPack()
		{
			ProgrammingCard[] cards = new ProgrammingCard[7] {
				BuildMove(-1),
				BuildMove(1),
				BuildMove(2),
				BuildMove(3),
				BuildTurn(RotationType.Back),
				BuildTurn(RotationType.Left),
				BuildTurn(RotationType.Right)
			};

			Pack<ProgrammingCard> pack = new Pack<ProgrammingCard>(cards);
			pack.ShuffleDrawingPile();
			return pack;
		}

		internal DamageCard BuildDCard(string name, params CardEffect[] cardEffects)
		{
			return new DamageCard(name, cardEffects);
		}

		public DamageCard BuildDDummy()
		{
			return new DamageCard("ddummy", new CardEffect[0]);
		}

	}
}
