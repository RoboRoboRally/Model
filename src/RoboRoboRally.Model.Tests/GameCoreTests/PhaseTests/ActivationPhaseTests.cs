﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.PhaseTests
{
    [TestFixture]
    public class ActivationPhaseTests
    {
		GameState gs;

		ActivationPhase ap;

		Robot robot;

		Mock<IGameEventNotifier> notMock;

		[SetUp]
		public void TestSetup()
		{
			TestGameStateBuilder tgsb = new TestGameStateBuilder();
			notMock = tgsb.NotMock;

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[2, 2] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tmb.Tiles[1, 1] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			trb.StartingPack = new Pack<ProgrammingCard>(new ProgrammingCard[0]);
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			tgsb.Actions.Add(new ShootingAction());

			gs = tgsb.BuildGameStateWithTurnBegan();

			ap = new ActivationPhase();
		}

		//main thing that remains to check is that programs are discarded at the end of activation phase
		[Test]
		public void ActivationPhaseTest1()
		{
			TestCardBuilder tcb = new TestCardBuilder();

			var dummy = tcb.BuildDummy();

			robot.SetNewProgram(new ProgrammingCard[5] { tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy() });

			ap.PerformPhase(gs);

			notMock.Verify(n => n.ProgrammingCardRevealed(robot.Name, It.IsAny<int>(), dummy.Name), Times.Exactly(5));

			notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, dummy.Name), Times.Exactly(5));

			Assert.That(robot.DrawAllCards().Count() == 5);
		}

		[Test]
		public void ActivationPhaseTest2()
		{
			TestCardBuilder tcb = new TestCardBuilder();

			var dummy = tcb.BuildDummy();

			robot.SetNewProgram(new ProgrammingCard[5] { tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy() });

			gs.DeactivateRobotForThisTurn(robot);

			ap.PerformPhase(gs);

			notMock.Verify(n => n.ProgrammingCardRevealed(robot.Name, It.IsAny<int>(), dummy.Name), Times.Exactly(0));

			notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, dummy.Name), Times.Exactly(0));

			Assert.That(robot.DrawAllCards().Count() == 5);
		}

	}
}
