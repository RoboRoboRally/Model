﻿using NUnit.Framework;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.Core;
using System.Collections.Generic;
using System.Linq;
using Moq;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;

namespace RoboRoboRally.Model.Tests.GameCoreTests.PhaseTests
{
    [TestFixture]
    public class ProgrammingPhaseTests
    {
		GameState gs;
		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;

		[SetUp]
		public void TestSetup()
		{
			var tgsb = new TestGameStateBuilder();

			tgsb.Map = new TestMapBuilder().BuildMap();

			for (int i = 0; i < 2; ++i)
			{
				TestRobotBuilder trb = new TestRobotBuilder();
				trb.Name = "SMASHITO" + i;
				trb.Position = new MapCoordinates(5+i, 5);

				List<ProgrammingCard> pCards = new List<ProgrammingCard>();
				TestCardBuilder tcb = new TestCardBuilder();

				for (int j = 1; j <= 15; ++j)
				{
					pCards.Add(tcb.BuildPCard("card" + j, new CardEffect[0]));
				}
				trb.StartingPack = new Pack<ProgrammingCard>(pCards.ToArray());

				tgsb.StartingRobots.Add(trb.BuildRobot());
			}

			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			gs = tgsb.BuildGameStateWithTurnBegan();			
		}

        [Test]
        public void BasicProgramSetTest()
        {
			ProgrammingPhase pp = new ProgrammingPhase(9);

			iMock.Setup(i => i.GetPrograms(It.IsAny<ProgramOffer[]>())).Returns(
				(ProgramOffer[] offer) =>
				{
					List<ProgramChoice> choices = new List<ProgramChoice>();

					foreach (var off in offer)
					{
						if (off.RobotName == "SMASHITO1")
						{
							choices.Add(new ProgramChoice()
							{
								RobotName = off.RobotName,
								ChosenCards = off.CardsToChoose.Skip(off.Amount - 1).Take(off.Amount).ToArray()
							});
						}
						else
						{
							choices.Add(new ProgramChoice()
							{
								RobotName = off.RobotName,
								ChosenCards = off.CardsToChoose.Take(off.Amount).ToArray()
							});
						}
					}

					return choices.ToArray();
				});

			pp.PerformPhase(gs);

			var r1 = gs.TurnActiveRobots[0];
			var r2 = gs.TurnActiveRobots[1];

			Assert.That(r1.DrawAllCards().Count() == 10);
			Assert.That(r2.DrawAllCards().Count() == 10);

			for (int i = 1; i <= 5; ++i)
				Assert.That(r1.RevealNextRegister().Name == "card" + i);

			for (int i = 5; i <= 9; ++i)
				Assert.That(r2.RevealNextRegister().Name == "card" + i);
        }

		[Test]
		public void InvalidProgramChoiceTest1()
		{
			ProgrammingPhase pp = new ProgrammingPhase(9);

			iMock.Setup(i => i.GetPrograms(It.IsAny<ProgramOffer[]>())).Returns(
				(ProgramOffer[] offer) =>
				{
					List<ProgramChoice> choices = new List<ProgramChoice>();

					foreach (var off in offer)
					{
						choices.Add(new ProgramChoice()
						{
							RobotName = off.RobotName,
							ChosenCards = off.CardsToChoose.Take(off.Amount + 1).ToArray()
						});
					}

					return choices.ToArray();
				});

			Assert.Throws<RunningGameException>(() => pp.PerformPhase(gs));
		}

		[Test]
		public void InvalidProgramChoiceTest2()
		{
			ProgrammingPhase pp = new ProgrammingPhase(9);

			iMock.Setup(i => i.GetPrograms(It.IsAny<ProgramOffer[]>())).Returns(
				(ProgramOffer[] offer) =>
				{
					List<ProgramChoice> choices = new List<ProgramChoice>();

					foreach (var off in offer)
					{
						choices.Add(new ProgramChoice()
						{
							RobotName = off.RobotName + "a",
							ChosenCards = off.CardsToChoose.Take(off.Amount).ToArray()
						});
					}

					return choices.ToArray();
				});

			Assert.Throws<RunningGameException>(() => pp.PerformPhase(gs));
		}

		[Test]
		public void InvalidProgramChoiceTest3()
		{
			ProgrammingPhase pp = new ProgrammingPhase(9);

			iMock.Setup(i => i.GetPrograms(It.IsAny<ProgramOffer[]>())).Returns(
				(ProgramOffer[] offer) =>
				{
					List<ProgramChoice> choices = new List<ProgramChoice>();

					for (int i = 0; i < offer.Length - 1; ++i)
					{
						var off = offer[i];
						choices.Add(new ProgramChoice()
						{
							RobotName = off.RobotName,
							ChosenCards = off.CardsToChoose.Take(off.Amount).ToArray()
						});
					}
					
					return choices.ToArray();
				});

			Assert.Throws<RunningGameException>(() => pp.PerformPhase(gs));

		}

		[Test]
		public void EmptyProgramTest()
		{
			ProgrammingPhase pp = new ProgrammingPhase(9);

			iMock.Setup(i => i.GetPrograms(It.IsAny<ProgramOffer[]>())).Returns(
				(ProgramOffer[] offer) =>
				{
					List<ProgramChoice> choices = new List<ProgramChoice>();

					foreach (var off in offer)
					{
						choices.Add(new ProgramChoice()
						{
							RobotName = off.RobotName,
							ChosenCards = off.CardsToChoose.Take(off.Amount).ToArray(),
							EmptyProgram = true
						});
					}
					return choices.ToArray();
				});

			pp.PerformPhase(gs);

			var r1 = gs.TurnActiveRobots[0];

			for (int i = 10; i <= 14; ++i)
				Assert.That(r1.RevealNextRegister().Name == "card" + i);

		}

		[Test]
		public void DiscardNonProgrammedCardsTest()
		{
			ProgrammingPhase pp = new ProgrammingPhase(9);

			TestCardBuilder tcb = new TestCardBuilder();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.StartingPack = new Pack<ProgrammingCard>(new Dictionary<ProgrammingCard, int> { { tcb.BuildDummy(), 9 } });
			var robot = trb.BuildRobot();

			var tgsb = new TestGameStateBuilder();
			tgsb.Map = new TestMapBuilder().BuildMap();
			tgsb.Phases.Add(pp);
			tgsb.StartingRobots.Add(robot);
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;
			gs = tgsb.BuildGameStateWithTurnBegan();


			iMock.Setup(i => i.GetPrograms(It.IsAny<ProgramOffer[]>())).Returns(
				(ProgramOffer[] offer) =>
				{
					List<ProgramChoice> programChoices = new List<ProgramChoice>();

					foreach (var off in offer)
					{
						List<ProgrammingCard> chosenCards = new List<ProgrammingCard>();
						for (int i = 0; i < off.Amount; ++i)
							chosenCards.Add(off.CardsToChoose[i]);

						programChoices.Add(new ProgramChoice()
						{
							EmptyProgram = false,
							RobotName = off.RobotName,
							ChosenCards = chosenCards.ToArray()
						});
					}

					return programChoices.ToArray();
				});

			pp.PerformPhase(gs);

			var c = robot.DrawAllCards().Length;

			Assert.That(c == 4);
		}
	}
}
