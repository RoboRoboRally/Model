﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.PhaseTests
{
	[TestFixture]
	public class UpgradePhaseTests
    {
		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;

		TestGameStateBuilder tgsb;
		TestCardBuilder tcb;
		UpgradePhase up;
		Robot robot;

		[SetUp]
		public void TestSetup()
		{
			tcb = new TestCardBuilder();
			tgsb = new TestGameStateBuilder();
			up = new UpgradePhase();

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new PriorityAntennaTile(new Direction(DirectionType.Up));

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;
			
		}

		[Test]
		public void NoUpgradeCardsTest1()
		{
			tgsb.UpgradeCards = new Pack<UpgradeCard>(new UpgradeCard[0]);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			up.PerformPhase(gs);

			notMock.Verify(n => n.UpgradePhaseSkipped());
		}

		[Test]
		public void BasicOfferTestTest1()
		{
			//simple test, which checks that we offer the right cards, for the right robot, with 
			//the right energy amount
			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(new PurchaseResponse() { PurchasedCard = null });
			
			up.PerformPhase(gs);

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }));

			iMock.Verify(i => i.OfferUpgradeCardPurchase(It.Is<PurchaseOffer>
				(p => p.OfferedCards[0] == u1 && p.RobotName == robot.Name && p.OwnedCards.Length==0 &&
				p.EnergyAmount == robot.EnergyAmount && p.UpgradeCardsLimits == robot.GetUpgradeCardLimits())), Times.Once);

			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, It.IsAny<string>()), Times.Never);
		}

		[Test]
		public void NoFirstPurchaseTest()
		{
			//test, that if robot does not buy anything, then we offer different card for the second time
			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 0);
			var u2 = tcb.BuildUCard("u2", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1, u2 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(new PurchaseResponse() { PurchasedCard = null });

			up.PerformPhase(gs);

			up.PerformPhase(gs);

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }), Times.Once);
			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u2" }), Times.Once);

			iMock.Verify(i => i.OfferUpgradeCardPurchase(It.Is<PurchaseOffer>
				(p => p.OfferedCards[0] == u1 && p.RobotName == robot.Name && p.OwnedCards.Length == 0 &&
				p.EnergyAmount == robot.EnergyAmount && p.UpgradeCardsLimits == robot.GetUpgradeCardLimits())), Times.Once);

			iMock.Verify(i => i.OfferUpgradeCardPurchase(It.Is<PurchaseOffer>
				(p => p.OfferedCards[0] == u2 && p.RobotName == robot.Name && p.OwnedCards.Length == 0 &&
				p.EnergyAmount == robot.EnergyAmount && p.UpgradeCardsLimits == robot.GetUpgradeCardLimits())), Times.Once);


			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, It.IsAny<string>()), Times.Never);
		}

		[Test]
		public void NoFirstPurchaseTest2()
		{
			//test that, when we do not buy the card for the first time, that it will be repeatedly offered again
			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1};
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(new PurchaseResponse() { PurchasedCard = null });

			up.PerformPhase(gs);

			up.PerformPhase(gs);

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }), Times.Exactly(2));

			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, It.IsAny<string>()), Times.Never);
		}

		[Test]
		public void FirstPurchaseTest()
		{
			//test behavior with robot buying both cards
			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 3);
			var u2 = tcb.BuildUCard("u2", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1, u2 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(
				(PurchaseOffer p) => new PurchaseResponse() { PurchasedCard = p.OfferedCards[0] });

			var initialEAmount = robot.EnergyAmount;
			up.PerformPhase(gs);
			up.PerformPhase(gs);

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }), Times.Once);
			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u2" }), Times.Once);

			iMock.Verify(i => i.OfferUpgradeCardPurchase(It.Is<PurchaseOffer>
				(p => p.OfferedCards[0] == u1 && p.RobotName == robot.Name && p.OwnedCards.Length == 0 &&
				p.EnergyAmount == initialEAmount && p.UpgradeCardsLimits == robot.GetUpgradeCardLimits())), Times.Once);

			iMock.Verify(i => i.OfferUpgradeCardPurchase(It.Is<PurchaseOffer>
				(p => p.OfferedCards[0] == u2 && p.RobotName == robot.Name && p.OwnedCards[0] == u1 &&
				p.EnergyAmount == initialEAmount - u1.EnergyCost && p.UpgradeCardsLimits == robot.GetUpgradeCardLimits())), Times.Once);

			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, u1.Name), Times.Once);
			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, u2.Name), Times.Once);

			Assert.That(uPack.Size() == 0);
		}

		[Test]
		public void NotEnoughEnergyTest()
		{
			//test when we pick card that we do not have energy for (error should happen)
			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, robot.EnergyAmount + 1);
			var uCards = new UpgradeCard[] { u1 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(
				(PurchaseOffer p) => new PurchaseResponse() { PurchasedCard = p.OfferedCards[0] });

			Assert.Throws<RunningGameException>(() => up.PerformPhase(gs));

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }), Times.Once);

			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, It.IsAny<string>()), Times.Never);
		}

		[Test]
		public void InvalidCardChoiceTest()
		{
			//test when we "buy" a card that was not offered => error
			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 0);
			var u2 = tcb.BuildUCard("u2", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1, u2 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(
				(PurchaseOffer p) => new PurchaseResponse() { PurchasedCard = u2 });

			Assert.Throws<RunningGameException>(() => up.PerformPhase(gs));

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }), Times.Once);

			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, It.IsAny<string>()), Times.Never);
		}

		[Test]
		public void CardLimitReachedTest()
		{
			//test when we reach card limit but we do not discard ( =>  error)
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.UpgradeCardsLimits = new Dictionary<UpgradeCardType, int>() { { UpgradeCardType.Permanent, 1 }, { UpgradeCardType.Temporary, 1 } };
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 0);
			var u2 = tcb.BuildUCard("u2", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1, u2 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;
			tgsb.StartingRobots = new List<Robot>() { robot };

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(
				(PurchaseOffer p) => new PurchaseResponse() { PurchasedCard = p.OfferedCards[0], DiscardedCard = null });

			up.PerformPhase(gs);

			Assert.Throws<RunningGameException>(() => up.PerformPhase(gs));

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }), Times.Once);
			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u2" }), Times.Once);

			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, u1.Name), Times.Once);
			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, u2.Name), Times.Never);

		}

		[Test]
		public void CardLimitReachedTest2()
		{
			//test when we reach card limit and we also discard
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.UpgradeCardsLimits = new Dictionary<UpgradeCardType, int>() { { UpgradeCardType.Permanent, 1 }, { UpgradeCardType.Temporary, 1 } };
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 0);
			var u2 = tcb.BuildUCard("u2", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1, u2 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;
			tgsb.StartingRobots = new List<Robot>() { robot };

			var gs = tgsb.BuildGameStateWithTurnBegan();

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(
				(PurchaseOffer p) =>
				{
					if (p.OfferedCards[0].Name == "u1")
					{
						return new PurchaseResponse() { DiscardedCard = null, PurchasedCard = p.OfferedCards[0] };
					}
					else
					{
						return new PurchaseResponse() { PurchasedCard = p.OfferedCards[0], DiscardedCard = p.OwnedCards[0] };
					}
				});

			up.PerformPhase(gs);

			up.PerformPhase(gs);

			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u1" }), Times.Once);
			notMock.Verify(n => n.RobotIsBuying(robot.Name, new string[] { "u2" }), Times.Once);

			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, u1.Name), Times.Once);
			notMock.Verify(n => n.UpgradeCardObtained(robot.Name, u2.Name), Times.Once);
			notMock.Verify(n => n.UpgradeCardDiscarded(robot.Name, u1.Name), Times.Once);

			Assert.That(uPack.Draw(1)[0] == u1);
		}

		[Test]
		public void BasicOfferTest2()
		{
			//basic offer test but this time with multiple robots
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.Name = "r1";
			var r1 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(6, 6);
			trb.Name = "r2";
			var r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(7, 7);
			trb.Name = "r3";
			var r3 = trb.BuildRobot();

			tgsb.StartingRobots = new List<Robot>() { r1, r2, r3 };

			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 0);
			var u2 = tcb.BuildUCard("u2", UpgradeCardType.Permanent, 0);
			var u3 = tcb.BuildUCard("u3", UpgradeCardType.Permanent, 0);
			var uCards = new UpgradeCard[] { u1, u2, u3 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(new PurchaseResponse() { PurchasedCard = null });

			var gs = tgsb.BuildGameStateWithTurnBegan();

			up.PerformPhase(gs);

			up.PerformPhase(gs);

			notMock.Verify(n => n.UpgradePhasePriorityDetermined(new string[] { "r1", "r2", "r3" }));
			
			// we verify at least once, because, remember, that cards are shuffled when
			//we run out of cards in the pack (Discard pile is shuffled)
			notMock.Verify(n => n.RobotIsBuying(r1.Name, new string[] { "u1", "u2", "u3" }), Times.AtLeastOnce);
			notMock.Verify(n => n.RobotIsBuying(r2.Name, new string[] { "u1", "u2", "u3" }), Times.AtLeastOnce);
			notMock.Verify(n => n.RobotIsBuying(r3.Name, new string[] { "u1", "u2", "u3" }), Times.AtLeastOnce);
		}

		[Test]
		public void SingleRobotPurchaseTest()
		{
			//test that only one of the robots buys some card
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.Name = "r1";
			var r1 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(6, 6);
			trb.Name = "r2";
			var r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(7, 7);
			trb.Name = "r3";
			var r3 = trb.BuildRobot();

			tgsb.StartingRobots = new List<Robot>() { r1, r2, r3 };

			var u1 = tcb.BuildUCard("u1");
			var u2 = tcb.BuildUCard("u2");
			var u3 = tcb.BuildUCard("u3");
			var u4 = tcb.BuildUCard("u4");
			var uCards = new UpgradeCard[] { u1, u2, u3, u4 };
			var uPack = new Pack<UpgradeCard>(uCards);

			tgsb.UpgradeCards = uPack;

			iMock.Setup(i => i.OfferUpgradeCardPurchase(It.IsAny<PurchaseOffer>())).Returns(
				(PurchaseOffer p) =>
				{
					if (p.RobotName == "r1")
					{
						if (p.OfferedCards[0].Name == "u1")
						{
							return new PurchaseResponse() { DiscardedCard = null, PurchasedCard = p.OfferedCards[0] };
						}
						else
						{
							return new PurchaseResponse() { PurchasedCard = null };
						}
					}
					else
					{
						return new PurchaseResponse() { PurchasedCard = null };
					}
				});

			var gs = tgsb.BuildGameStateWithTurnBegan();

			up.PerformPhase(gs);

			notMock.Verify(n => n.RobotIsBuying(r1.Name, new string[] { "u1", "u2", "u3" }), Times.Once);
			notMock.Verify(n => n.RobotIsBuying(r2.Name, new string[] { "u2", "u3" }), Times.Once);
			notMock.Verify(n => n.RobotIsBuying(r3.Name, new string[] { "u2", "u3" }), Times.Once);

			up.PerformPhase(gs);
			
			up.PerformPhase(gs);

			//with these verifications, we also test the cycling of upgrade cards in case
			//no one buys anything, and there are no more cards in the upgrade pack
			notMock.Verify(n => n.RobotIsBuying(r1.Name, It.Is<string[]>(s => s.Contains("u2") && s.Contains("u3") && s.Contains("u4"))), Times.Exactly(2));
			notMock.Verify(n => n.RobotIsBuying(r2.Name, It.Is<string[]>(s => s.Contains("u2") && s.Contains("u3") && s.Contains("u4"))), Times.Exactly(2));
			notMock.Verify(n => n.RobotIsBuying(r3.Name, It.Is<string[]>(s => s.Contains("u2") && s.Contains("u3") && s.Contains("u4"))), Times.Exactly(2));

		}
	}
}
