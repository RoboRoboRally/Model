﻿using NUnit.Framework;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Tests.TestBuilders;

namespace RoboRoboRally.Model.Tests.GameCoreTests
{
    [TestFixture]
    public class RegisterBoardTests
    {
		TestCardBuilder tcb;
		RegisterBoard rb;

		int regCount = 5;

		[SetUp]
		public void TestSetup()
		{
			tcb = new TestCardBuilder();
			rb = new RegisterBoard(regCount);
		}

		[Test]
		public void SetProgramTest()
		{
			var p = tcb.BuildBasicPack();
			var program = p.Draw(regCount + 1);

			Assert.Throws<RunningGameException>(() => rb.PlaceNewProgram(program));
		}

		[Test]
		public void CardGetterTest1()
		{
			var p = tcb.BuildBasicPack();
			var program = p.Draw(regCount);

			rb.PlaceNewProgram(program);

			Assert.That(rb.GetRegisterCard(1).Name == program[0].Name);

		}

		[Test]
		public void CardGetterTest2()
		{
			var p = tcb.BuildBasicPack();
			var program = p.Draw(regCount);

			rb.PlaceNewProgram(program);

			Assert.Throws<RunningGameException>(() => rb.GetRegisterCard(0));
			Assert.Throws<RunningGameException>(() => rb.GetRegisterCard(regCount + 1));

		}

		[Test]
		public void RemoveProgramTest1()
		{
			var p = tcb.BuildBasicPack();
			var program = p.Draw(regCount);

			rb.PlaceNewProgram(program);

			var retrievedProgram = rb.RemoveProgram();

			for (int i = 0; i < program.Length; ++i)
				Assert.That(program[i].Name == retrievedProgram[i].Name);
		}

		[Test]
		public void RemoveProgramTest2()
		{
			Assert.Throws<RunningGameException>(() => rb.RemoveProgram());
		}

		[Test]
		public void RemoveProgramTest3()
		{
			var p = tcb.BuildBasicPack();
			var program = p.Draw(regCount);

			rb.PlaceNewProgram(program);

			var retrievedProgram = rb.RemoveProgram();

			Assert.Throws<RunningGameException>(() => rb.RemoveProgram());
		}

		[Test]
		public void ReplaceCardTest1()
		{
			var p = tcb.BuildBasicPack();
			var program = p.Draw(regCount);

			rb.PlaceNewProgram(program);

			var dummy = tcb.BuildDummy();

			rb.ReplaceCard(3, dummy);

			var rCard = rb.GetRegisterCard(3);

			Assert.That(dummy.Name == rCard.Name);
		}

		[Test]
		public void ReplaceCardTest2()
		{
			var p = tcb.BuildBasicPack();
			var program = p.Draw(regCount);

			rb.PlaceNewProgram(program);

			var dummy = tcb.BuildDummy();

			Assert.Throws<RunningGameException>(() => rb.ReplaceCard(0, dummy));
			Assert.Throws<RunningGameException>(() => rb.ReplaceCard(regCount + 1, dummy));
		}
    }
}
