﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Settings;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RoboRoboRally.Model.Tests.GameCoreTests
{
    class GameInitializerTests
    {
		GameInitializer gi;
		public static string gameDataPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"/GameData/";
		//public static string gameDataPath = @"D:/Coding/LargeProjects/RoboRoboRally/GameData/GameData/";
		GameSettings gs;
		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;
		IList<string> players;

		[SetUp]
		public void TestSetup()
		{
			TestCardBuilder tcb = new TestCardBuilder();
			var basicPack = tcb.BuildBasicPack();

			gs = new GameSettings()
			{
				CardSettings = new CardSettings()
				{
					UpgradeCards = new Pack<UpgradeCard>(new UpgradeCard[0])
				},
				MapSettings = new MapSettings()
				{
					GamePlanID = "5B",
					StartingPlanID = "A",
					MapModifications = new MapModifications()
					{
						GamePlanRotation = RotationType.None,
						PriorityAntenna = new TileRecord<PriorityAntennaTile>()
						{
							Coords = new MapCoordinates(12, 4),
							Tile = new PriorityAntennaTile(new Direction(DirectionType.Up))
						},
						CheckpointTiles = new TileRecord<CheckpointTile>[1]
						{
							new TileRecord<CheckpointTile>()
							{
								Coords = new MapCoordinates(3, 9),
								Tile = new CheckpointTile(1)
							}
						},
						RebootTiles = new TileRecord<RebootTile>[1]
						{
							new TileRecord<RebootTile>()
							{
								Coords = new MapCoordinates(3, 4),
								Tile = new RebootTile(new Direction(DirectionType.Down))
							}
						}
					}
				},
				PlayerSettings = new PlayerSettings()
				{
					EnergyAmount = 5,
					PlayerProgrammingCards = basicPack,
					DefaultPlayerDamage = new Damage() { DamageCards = new DamageCard[0] },
					RegisterBoardSize = 5,
					UpgradeCardsLimits = new Dictionary<UpgradeCardType, int>()
				},
				EnvironmentSettings = new EnvironmentSettings()
				{
					DefaultRebootDamage = new Damage() { DamageCards = new DamageCard[0] }
				},
				GameFlowSettings = new GameFlowSettings()
				{
					Phases = new PhaseBase[1]
					{
						new ProgrammingPhase(9)
					}
				}
			};

			notMock = new Mock<IGameEventNotifier>();
			iMock = new Mock<IInputHandler>();

			players = new List<string>()
			{
				"SMASH-SMASH",
				"SMASHITO"
			};

			gi = new GameInitializer(gameDataPath);
		}

		[Test]
		public void GameInitTest1()
		{
			//setup above should be correct, let's test some things
			var setup = gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray());

			Assert.That(setup.StartingPlayers.Count() == players.Count);
			Assert.That(setup.GameState.Map.GetTileRecords<PriorityAntennaTile>().Count() == 1);
			Assert.That(setup.GameState.Map.GetTileRecords<RebootTile>().Count() == 1);
			Assert.That(setup.GameState.Map.GetTileRecords<CheckpointTile>().Count() == 1);
		}

		[Test]
		public void GameInitTest2()
		{
			//duplicate player name
			players.Add("SMASH-SMASH");

			Assert.Throws<InvalidGameOperationException>(() => gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray()));
		}

		[Test]
		public void GameInitTest3()
		{
			//too many players, more than there are starting tiles (on this map, there are 6)
			for (int i = 0; i < 6; ++i)
				players.Add("robot" + i);

			Assert.Throws<InvalidGameOperationException>(() => gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray()));
		}

		[Test]
		public void GameInitTest4()
		{
			//wrong tile placement

			gs.MapSettings.MapModifications = new MapModifications()
			{
				GamePlanRotation = RotationType.None,
				PriorityAntenna = new TileRecord<PriorityAntennaTile>()
				{
					Coords = new MapCoordinates(12, 4),
					Tile = new PriorityAntennaTile(new Direction(DirectionType.Up))
				},
				CheckpointTiles = new TileRecord<CheckpointTile>[1]
						{
							new TileRecord<CheckpointTile>()
							{
								Coords = new MapCoordinates(12, 4),
								Tile = new CheckpointTile(1)
							}
						},
				RebootTiles = new TileRecord<RebootTile>[1]
						{
							new TileRecord<RebootTile>()
							{
								Coords = new MapCoordinates(3, 4),
								Tile = new RebootTile(new Direction(DirectionType.Down))
							}
						}
			};

			Assert.Throws<InvalidGameOperationException>(() => gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray()));
		}

		[Test]
		public void GameInitTest5()
		{
			//wrong tile placement

			gs.MapSettings.MapModifications = new MapModifications()
			{
				GamePlanRotation = RotationType.None,
				PriorityAntenna = new TileRecord<PriorityAntennaTile>()
				{
					Coords = new MapCoordinates(12, 4),
					Tile = new PriorityAntennaTile(new Direction(DirectionType.Up))
				},
				CheckpointTiles = new TileRecord<CheckpointTile>[0],
				RebootTiles = new TileRecord<RebootTile>[1]
						{
							new TileRecord<RebootTile>()
							{
								Coords = new MapCoordinates(3, 4),
								Tile = new RebootTile(new Direction(DirectionType.Down))
							}
						}
			};

			Assert.Throws<InvalidGameOperationException>(() => gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray()));
		}

		[Test]
		public void GameInitTest6()
		{
			//wrong tile placement

			gs.MapSettings.MapModifications = new MapModifications()
			{
				GamePlanRotation = RotationType.None,
				PriorityAntenna = new TileRecord<PriorityAntennaTile>()
				{
					Coords = new MapCoordinates(12, 4),
					Tile = new PriorityAntennaTile(new Direction(DirectionType.Up))
				},
				CheckpointTiles = new TileRecord<CheckpointTile>[1]
						{
							new TileRecord<CheckpointTile>()
							{
								Coords = new MapCoordinates(-1, 4),
								Tile = new CheckpointTile(1)
							}
						},
				RebootTiles = new TileRecord<RebootTile>[1]
						{
							new TileRecord<RebootTile>()
							{
								Coords = new MapCoordinates(3, 4),
								Tile = new RebootTile(new Direction(DirectionType.Down))
							}
						}
			};

			Assert.Throws<InvalidGameOperationException>(() => gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray()));
		}

		[Test]
		public void GameInitTest7()
		{
			gs.MapSettings.GamePlanID = "fň";
			Assert.Throws<InvalidGameOperationException>(() => gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray()));
		}

		[Test]
		public void GameInitTest8()
		{
			gs.PlayerSettings.PlayerProgrammingCards = new Pack<ProgrammingCard>(new ProgrammingCard[0]);

			Assert.Throws<InvalidGameOperationException>(() => gi.GetNewGameSetup(gs, iMock.Object, notMock.Object, players.ToArray()));
		}
	}
}
