﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.GameCoreTests
{
    [TestFixture]
    public class RegisterActivationTests
    {
		ActivationPhase ap;

		TestGameStateBuilder tgsb;
		TestCardBuilder tcb;

		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;

		[SetUp]
		public void TestSetup()
		{
			tcb = new TestCardBuilder();

			tgsb = new TestGameStateBuilder();
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			tgsb.Map = new TestMapBuilder().BuildMap();

			tgsb.Actions.Add(new MovementAction());


			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			trb.FacingDir = new Direction(DirectionType.Up);
			var r1 = trb.BuildRobot();
			r1.SetNewProgram(new ProgrammingCard[5] { tcb.BuildMove(1), tcb.BuildTurn(RotationType.Left), null, null, null });
			tgsb.StartingRobots.Add(r1);


			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			trb.FacingDir = new Direction(DirectionType.Up);
			var r2 = trb.BuildRobot();
			r2.SetNewProgram(new ProgrammingCard[5] { tcb.BuildMove(2), tcb.BuildTurn(RotationType.Left), null, null, null });
			tgsb.StartingRobots.Add(r2);


			trb.Name = "SMASH3";
			trb.Position = new MapCoordinates(5, 7);
			trb.FacingDir = new Direction(DirectionType.Up);
			var r3 = trb.BuildRobot();
			r3.SetNewProgram(new ProgrammingCard[5] { tcb.BuildMove(3), tcb.BuildTurn(RotationType.Left), null, null, null });
			tgsb.StartingRobots.Add(r3);

			ap = new ActivationPhase();
		}

		[Test]
		public void RegisterActivationTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[7, 5] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateRegister(gs, 1);

			notMock.Verify(n => n.RegisterActivationBegan(1));
			notMock.Verify(n => n.ActivationPhasePriorityDetermined(new string[3] { "SMASH1", "SMASH2", "SMASH3" }));
			notMock.Verify(n => n.ProgrammingCardRevealed(It.IsAny<string>(), 1, It.IsAny<string>()), Times.Exactly(3));
			notMock.Verify(n => n.ProgrammingCardPlayed(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(3));

			Assert.That(tgsb.StartingRobots[0].Position == new MapCoordinates(4, 5));
			Assert.That(tgsb.StartingRobots[1].Position == new MapCoordinates(3, 6));
			Assert.That(tgsb.StartingRobots[2].Position == new MapCoordinates(2, 7));
		}

		[Test]
        public void RegisterActivationTest2()
        {
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[7, 6] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateRegister(gs, 1);

			notMock.Verify(n => n.ActivationPhasePriorityDetermined(new string[3] { "SMASH2", "SMASH3", "SMASH1" }));

			//robots moved

			gs.DeactivateRobotForThisTurn(tgsb.StartingRobots[1]);

			ap.ActivateRegister(gs, 2);

			//smash1 robot is closer after move
			notMock.Verify(n => n.ActivationPhasePriorityDetermined(new string[2] { "SMASH1", "SMASH3" }));

			Assert.That(tgsb.StartingRobots[0].FacingDirection == DirectionType.Left);
			Assert.That(tgsb.StartingRobots[1].FacingDirection == DirectionType.Up);
			Assert.That(tgsb.StartingRobots[2].FacingDirection == DirectionType.Left);
			
        }
		
    }
}
