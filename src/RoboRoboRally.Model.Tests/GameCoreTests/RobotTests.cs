﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests
{
    [TestFixture]
    public class RobotTests
    {
		Robot r;
		TestCardBuilder tcb;

		Mock<IGameState> gsMock;
		Mock<IGameEventNotifier> notMock;

		[SetUp]
		public void TestSetUp()
		{
			tcb = new TestCardBuilder();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.StartingPack = tcb.BuildBasicPack();
			trb.Position = new MapCoordinates(5, 5);

			r = trb.BuildRobot();

			TestMapBuilder tmb = new TestMapBuilder(new MapSize(15, 15));
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new RebootTile(new Direction(DirectionType.Down));
			var map = tmb.BuildMap();

			gsMock = new Mock<IGameState>();
			notMock = new Mock<IGameEventNotifier>();
			gsMock.Setup(x => x.Notifier).Returns(notMock.Object);
			gsMock.Setup(x => x.Map).Returns(map);

			gsMock.Setup(x => x.GetAction<MovementAction>()).Returns(new MovementAction());
			gsMock.Setup(x => x.GetAction<RebootAction>()).Returns(new RebootAction(new Damage() { DamageCards = new DamageCard[0] }));
		}

		[Test]
		public void AddEnergyTest()
		{
			var oldEnergy = r.EnergyAmount;
			r.ChangeEnergyAmount(gsMock.Object, 1);
			Assert.That(r.EnergyAmount == oldEnergy + 1);
		}

		[Test]
		public void SetNewPositionTest()
		{
			var pos = new MapCoordinates(5, 9);
			r.SetNewPosition(pos);
			Assert.That(r.Position == pos);
		}

		[Test]
		public void SetFacingDirectionTest()
		{
			r.SetNewFacingDirection(new Direction(DirectionType.Up));
			Assert.That(r.FacingDirection == DirectionType.Up);
		}

		[Test]
		public void RotateTest()
		{
			var oldDir = r.FacingDirection;
			r.Rotate(gsMock.Object, RotationType.Back);
			Assert.That(r.FacingDirection == oldDir.Rotate(RotationType.Back));
		}

		[Test]
		public void DrawTest1()
		{
			var cards = r.DrawCards(5);
			Assert.That(cards.Length == 5);
		}

		[Test]
		public void DrawTest2()
		{
			Assert.Throws<RunningGameException>(() => r.DrawCards(tcb.BuildBasicPack().Size() + 1));
		}

		[Test]
		public void DrawAllTest()
		{
			var cards = r.DrawAllCards();

			Assert.That(cards.Length == tcb.BuildBasicPack().Size());
		}

		[Test]
		public void SetNewProgramTest()
		{
			var p = r.DrawCards(4);
			Assert.Throws<RunningGameException>(() => r.SetNewProgram(p));
		}

		[Test]
		public void RevealRegisterTest()
		{
			var p = r.DrawCards(5);
			var c1 = p[0];
			var c2 = p[1];

			r.SetNewProgram(p);

			var rc = r.RevealNextRegister();
			Assert.That(rc.Name == c1.Name);

			rc = r.RevealNextRegister();
			Assert.That(rc.Name == c2.Name);
		}

		[Test]
		public void RevealRegisterTest2()
		{
			var p = r.DrawCards(5);

			r.SetNewProgram(p);

			for (int i = 0; i < 5; ++i)
				r.RevealNextRegister();

			Assert.Throws<RunningGameException>(() => r.RevealNextRegister());
		}

		[Test]
		public void PlayRegisterTest1()
		{
			
			var p = r.DrawCards(5);

			r.SetNewProgram(p);

			var c1 = p[0];
			var c2 = p[1];

			r.PlayNextRegister(gsMock.Object);
			r.PlayNextRegister(gsMock.Object);

			notMock.Verify(x => x.ProgrammingCardPlayed(r.Name, c1.Name));
			notMock.Verify(x => x.ProgrammingCardPlayed(r.Name, c2.Name));
		}

		[Test]
		public void PlayRegisterTest2()
		{
			var p = r.DrawCards(5);

			r.SetNewProgram(p);

			for (int i = 0; i < 5; ++i)
				r.PlayNextRegister(gsMock.Object);

			Assert.Throws<RunningGameException>(() => r.PlayNextRegister(gsMock.Object));
		}

		[Test]
		public void PlayRegisterAgainTest1()
		{
			var p = r.DrawCards(5);

			var c1 = p[0];
			var c2 = p[1];

			r.SetNewProgram(p);

			Assert.Throws<RunningGameException>(() => r.PlayRegisterAgain(gsMock.Object, 0));
			Assert.Throws<RunningGameException>(() => r.PlayRegisterAgain(gsMock.Object, 1));

			r.PlayNextRegister(gsMock.Object);
			r.PlayNextRegister(gsMock.Object);

			r.PlayRegisterAgain(gsMock.Object, 1);
			r.PlayRegisterAgain(gsMock.Object, 2);

			notMock.Verify(n => n.ProgrammingCardPlayed(r.Name, c1.Name), Times.Exactly(2));
			notMock.Verify(n => n.ProgrammingCardPlayed(r.Name, c2.Name), Times.Exactly(2));
		}

		[Test]
		public void ReplaceRegisterTest1()
		{
			var p = r.DrawCards(5);
			var c1 = p[1];

			r.SetNewProgram(p);

			var replacement = r.DrawCards(1);
			var replacement2 = r.DrawCards(1);

			r.DrawAllCards();

			r.ReplaceRegisterCard(gsMock.Object, replacement[0], 2);
			r.ReplaceRegisterCard(gsMock.Object, replacement2[0], 2);
			notMock.Verify(n => n.RegisterCardReplaced(r.Name, 2, c1.Name, replacement[0].Name), Times.Once);
			notMock.Verify(n => n.RegisterCardReplaced(r.Name, 2, replacement[0].Name, replacement2[0].Name), Times.Once);
		}

		[Test]
		public void ReplaceRegisterTest2()
		{
			//replacing cards that have not been played yet 
			//(both should be discarded ...)
			var p = r.DrawCards(5);
			p[1] = tcb.BuildDDummy();

			r.DrawAllCards();

			r.SetNewProgram(p);

			r.ReplaceRegisterCard(gsMock.Object, tcb.BuildDummy(), 2);

			r.ReplaceRegisterCard(gsMock.Object, tcb.BuildDummy(), 3);

			//both were discarded
			Assert.That(r.DrawAllCards().Count() == 2);
		}

		[Test]
		public void ReplaceRegisterTest3()
		{
			var p = r.DrawCards(5);

			p[1] = tcb.BuildDDummy();

			r.DrawAllCards();

			r.SetNewProgram(p);

			r.PlayNextRegister(gsMock.Object);
			r.PlayNextRegister(gsMock.Object);
			r.PlayNextRegister(gsMock.Object);

			r.ReplaceRegisterCard(gsMock.Object, tcb.BuildDummy(), 2);
			r.ReplaceRegisterCard(gsMock.Object, tcb.BuildDummy(), 3);

			//damage card was played, it has not been discarded during replacement ..
			Assert.That(r.DrawAllCards().Count() == 1);
		}

		[Test]
		public void DiscardProgramTest()
		{
			var p = r.DrawCards(5);

			r.SetNewProgram(p);

			var c = r.DrawAllCards();

			r.DiscardCurrentProgram();

			Assert.That(r.DrawAllCards().Count() == 5);
		}

		[Test]
		public void DiscardProgramTest2()
		{
			var p = r.DrawCards(5);

			r.SetNewProgram(p);

			r.DiscardCurrentProgram();
			Assert.Throws<RunningGameException>(() => r.DiscardCurrentProgram());
		}

		[Test]
		public void DiscardProgramTest3()
		{
			var p = r.DrawCards(5);

			r.SetNewProgram(p);

			r.DrawAllCards();

			r.PlayNextRegister(gsMock.Object);
			r.PlayNextRegister(gsMock.Object);

			r.DiscardCurrentProgram();

			Assert.That(r.DrawAllCards().Count() == 5);

		}

		[Test]
		public void DiscardProgramTest4()
		{
			var p = r.DrawCards(5);

			p[1] = tcb.BuildDDummy();

			r.SetNewProgram(p);

			r.DrawAllCards();

			r.PlayNextRegister(gsMock.Object);
			r.PlayNextRegister(gsMock.Object);

			r.DiscardCurrentProgram();

			Assert.That(r.DrawAllCards().Count() == 4);
		}

		[Test]
		public void SufferDamageTest()
		{
			r.SufferDamage(gsMock.Object, new Damage() { DamageCards = new DamageCard[1] { tcb.BuildDCard("dmg1", new CardEffect[0]) } });

			Assert.That(r.DrawAllCards().Count(c => c.Name == "dmg1") == 1);

			notMock.Verify(n => n.RobotSufferedDamage(r.Name, It.IsAny<Damage>()), Times.Once);
		}

		[Test]
		public void BurnCardTest()
		{
			var dmgCard = tcb.BuildDCard("dmg1", new CardEffect[0]);
			r.SufferDamage(gsMock.Object, new Damage() { DamageCards = new DamageCard[1] { dmgCard } });

			r.TryBurnCardFromDiscardPile(dmgCard);

			Assert.That(r.DrawAllCards().Count(c => c.Name == "dmg1") == 0);
		}



		[Test]
		public void VisitedCheckpointsTest1()
		{
			Assert.That(r.GetLastVisitedCheckpoint() == 0);
			r.AddVisitedCheckpoint(1);
			Assert.That(r.GetLastVisitedCheckpoint() == 1);
		}

		[Test]
		public void VisitedCheckpointsTest2()
		{
			Assert.Throws<RunningGameException>(() => r.AddVisitedCheckpoint(2));
		}

		[Test]
		public void DefaultPriorityTest1()
		{
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);

			r = trb.BuildRobot();

			var priority = r.GetDefaultPriority(new MapCoordinates(5, 8), new Direction(DirectionType.Up));

			Assert.That(priority.DistanceToAntenna == 3);
			Assert.That(priority.Angle == 270);
		}

		[Test]
		public void DefaultPriorityTest2()
		{
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);

			r = trb.BuildRobot();

			var priority = r.GetDefaultPriority(new MapCoordinates(5, 3), new Direction(DirectionType.Up));

			Assert.That(priority.DistanceToAntenna == 2);
			Assert.That(priority.Angle == 90);
		}

		[Test]
		public void DefaultPriorityTest3()
		{
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);

			r = trb.BuildRobot();

			var priority = r.GetDefaultPriority(new MapCoordinates(2, 2), new Direction(DirectionType.Up));

			Assert.That(priority.DistanceToAntenna == 6);
			Assert.That(priority.Angle == 135);
		}

		[Test]
		public void DefaultPriorityTest4()
		{
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5,2);
			var r1 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 8);
			var r2 = trb.BuildRobot();

			var priority1 = r1.GetDefaultPriority(new MapCoordinates(2, 2), new Direction(DirectionType.Up));
			var priority2 = r2.GetDefaultPriority(new MapCoordinates(2, 2), new Direction(DirectionType.Up));

			//priority1 is closer to antenna
			Assert.That(priority1.CompareTo(priority2) < 0);
		}

		[Test]
		public void ObtainUpgradeCardTest()
		{
			var u1 = tcb.BuildUCard("u1");
			var u2 = tcb.BuildUCard("u2");

			r.ObtainUpgradeCard(gsMock.Object, u1);
			r.ObtainUpgradeCard(gsMock.Object, u2);

			notMock.Verify(n => n.UpgradeCardObtained(r.Name, u1.Name));
			notMock.Verify(n => n.UpgradeCardObtained(r.Name, u2.Name));

			var ownedCards = r.GetOwnedUpgradeCards();

			Assert.That(ownedCards.Contains(u1));
			Assert.That(ownedCards.Contains(u2));

			//test this method again, so that we verify it does not remove
			//the cards from the player
			ownedCards = r.GetOwnedUpgradeCards();

			Assert.That(ownedCards.Contains(u1));
			Assert.That(ownedCards.Contains(u2));
		}

		[Test]
		public void ObtainUpgradeCardTest2()
		{
			//test when we obtain the same card twice (error should happen)
			var u1 = tcb.BuildUCard("u1");

			r.ObtainUpgradeCard(gsMock.Object, u1);
			Assert.Throws<RunningGameException>(() => r.ObtainUpgradeCard(gsMock.Object, u1));
		}

		[Test]
		public void DiscardUpgradeCardTest()
		{
			TestGameStateBuilder tgsb = new TestGameStateBuilder();
			tgsb.StartingRobots.Add(r);
			tgsb.NotMock = notMock;

			var u1 = tcb.BuildUCard("u1");

			var gs = tgsb.BuildGameStateWithTurnBegan();

			r.ObtainUpgradeCard(gs, u1);

			r.DiscardUpgradeCard(gs, u1.Name);

			notMock.Verify(n => n.UpgradeCardDiscarded(r.Name, u1.Name), Times.Once);

			Assert.That(gs.DrawUpgradeCards(1).Contains(u1));
		}

		[Test]
		public void DiscardUpgradeCardTest2()
		{
			//let's try and discard card that does not belong to the player

			Assert.Throws<RunningGameException>(() => r.DiscardUpgradeCard(gsMock.Object, "abc"));
		}

		[Test]
		public void DiscardAllUpgradeCardsTest()
		{
			TestGameStateBuilder tgsb = new TestGameStateBuilder();
			tgsb.StartingRobots.Add(r);
			tgsb.NotMock = notMock;

			var u1 = tcb.BuildUCard("u1");
			var u2 = tcb.BuildUCard("u2");

			var gs = tgsb.BuildGameStateWithTurnBegan();

			r.ObtainUpgradeCard(gs, u1);
			r.ObtainUpgradeCard(gs, u2);

			r.DiscardAllUpgradeCards(gs);

			notMock.Verify(n => n.UpgradeCardDiscarded(r.Name, u1.Name));
			notMock.Verify(n => n.UpgradeCardDiscarded(r.Name, u2.Name));

			Assert.That(gs.DrawUpgradeCards(10).Length == 2);

			Assert.That(r.GetOwnedUpgradeCards().Length == 0);

			//also assert that doing this twice should not be a problem

			r.DiscardAllUpgradeCards(gs);
		}

		[Test]
		public void GetCardForHookTest()
		{
			TestGameStateBuilder tgsb = new TestGameStateBuilder();
			tgsb.Map = new TestMapBuilder(new MapSize(10, 10)).BuildMap();
			tgsb.StartingRobots.Add(r);
			tgsb.Phases.Add(new ProgrammingPhase(9));

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var hook = new IncrementDrawAmountHook(false);
			var u1 = tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook);

			var hook2 = new IncrementDrawAmountHook(false);

			r.ObtainUpgradeCard(gs, u1);

			Assert.That(r.GetUpgradeCardForHook(hook) == u1);
			Assert.Throws<RunningGameException>(() => r.GetUpgradeCardForHook(hook2));
		}

		[Test]
		public void AsyncHookAttachTest()
		{
			r.AttachAsyncHook("u1", new AddEnergyHook(3));

			Assert.That(r.HasAsyncHook("u1", out var asyncHook) == true);
		}

		[Test]
		public void AsnycHookAttachTest2()
		{
			r.AttachAsyncHook("u1", new AddEnergyHook(3));
			Assert.Throws<RunningGameException>(() => r.AttachAsyncHook("u1", new AddEnergyHook(2)));
		}

		[Test]
		public void HasAsyncHookTest()
		{
			Assert.That(r.HasAsyncHook("u1", out var hook) == false);

			r.AttachAsyncHook("u1", new AddEnergyHook(3));

			Assert.That(r.HasAsyncHook("u2", out var h2) == false);
		}

		[Test]
		public void DetachAsyncHookTest()
		{
			var h1 = new AddEnergyHook(3);

			r.AttachAsyncHook("u1", h1);

			Assert.That(r.HasAsyncHook("u1", out var h2) == true);

			r.DetachAsyncHook("u1", h1);

			Assert.That(r.HasAsyncHook("u1", out var h3) == false);
		}

		[Test]
		public void DetachAsyncHookTest2()
		{
			var h1 = new AddEnergyHook(3);
			var h2 = new AddEnergyHook(2);

			Assert.Throws<RunningGameException>(() => r.DetachAsyncHook("u1", h1));

			r.AttachAsyncHook("u1", h2);

			Assert.Throws<RunningGameException>(() => r.DetachAsyncHook("u1", h1));
		}
	}
}

