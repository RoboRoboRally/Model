﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests.AsyncHookTests
{
    [TestFixture]
    public class AddEnergyHookTests : HookTestsBase
    {
		AddEnergyHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new AddEnergyHook(5);
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldEnergy = robot.EnergyAmount;

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			Assert.That(robot.EnergyAmount == oldEnergy + 5);

			notMock.Verify(n => n.RobotEnergyChanged(robot.Name, robot.EnergyAmount, oldEnergy));
		}

		[Test]
		public void AttachedHookTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook));

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncUpgradeCardUseInfo() { RobotName = robot.Name, CardName = "u1" });

			var oldEnergyAmount = robot.EnergyAmount;

			gs.ProcessAsyncUpgradeCardUses();

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncUpgradeCardUseInfo() { RobotName = robot.Name, CardName = "u1" });

			//for the second time, the upgrade card should be discarded and nothing should happen
			gs.ProcessAsyncUpgradeCardUses();

			notMock.Verify(n => n.RobotEnergyChanged(robot.Name, It.IsAny<int>(), It.IsAny<int>()), Times.Once);
		}
    }
}
