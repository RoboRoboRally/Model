﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks.AsyncCardHooks;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;
using RoboRoboRally.Model.Core.Phases;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests.AsyncHookTests
{
    [TestFixture]
    public class AsyncCardOfferHookTests : HookTestsBase
    {
		AsyncCardOfferHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new AsyncCardOfferHook();

			tgsb.Phases.Add(new ActivationPhase());
		}

		[Test]
		public void HookEffectTest()
		{
			AddProgrammingCardHook h2 = new AddProgrammingCardHook(tcb.BuildDummy());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook, h2));

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			iMock.Verify(i => i.OfferAsyncChoicelessUpgradeCard(robot.Name, "u1"), Times.Once);
		}

		[Test]
		public void HookEffectTest2()
		{
			RotateHook h2 = new RotateHook(
				new UpgradeCardChoice[]
				{
					new UpgradeCardChoice() { ChoiceCardName = "c1" },
					new UpgradeCardChoice() { ChoiceCardName = "c2" }
				},
				new ChoiceValue<RotationType>[]
				{
					new ChoiceValue<RotationType>() { ChoiceCardName = "c1", Value = RotationType.Left },
					new ChoiceValue<RotationType>() { ChoiceCardName = "c2", Value = RotationType.Right }
				});

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook, h2));

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			iMock.Verify(i => i.OfferAsyncChoiceUpgradeCard(robot.Name, "u1", It.IsAny<UpgradeCardChoice[]>()));
		}

		[Test]
		public void AttachedHookTest()
		{
			AddProgrammingCardHook h2 = new AddProgrammingCardHook(tcb.BuildDummy());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook, h2));

			gs.GetPhase<ActivationPhase>().BeforeActivationPhaseStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			iMock.Verify(i => i.OfferAsyncChoicelessUpgradeCard(robot.Name, "u1"), Times.Once);

			//performing the whole phase does not make much sense, as i am not able to verify 
			//the call order easily ... 
		}
    }
}
