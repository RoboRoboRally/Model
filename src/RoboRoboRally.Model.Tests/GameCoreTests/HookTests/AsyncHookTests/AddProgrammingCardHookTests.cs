﻿using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;
using RoboRoboRally.Model.Core.HookableGameSteps;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests.AsyncHookTests
{
    [TestFixture]
    public class AddProgrammingCardHookTests: HookTestsBase
    {
		AddProgrammingCardHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new AddProgrammingCardHook(tcb.BuildPCard("p1"));
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			Assert.That(robot.DrawCards(1)[0].Name == "p1");
		}

		[Test]
		public void AttachedHookTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook));

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncUpgradeCardUseInfo() { RobotName = robot.Name, CardName = "u1" });

			gs.ProcessAsyncUpgradeCardUses();

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncUpgradeCardUseInfo() { RobotName = robot.Name, CardName = "u1" });

			//second use info should be ignored and nothing should happen, as the 
			//upgrade card is discarded
			gs.ProcessAsyncUpgradeCardUses();

			Assert.That(robot.DrawAllCards().Where(c => c.Name == "p1").Count() == 1);

		}
    }
}
