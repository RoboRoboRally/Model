﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.MapDB.Tiles;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests.AsyncHookTests
{
    [TestFixture]
    public class AsyncCardOfferEndHookTests : HookTestsBase
    {
		AsyncCardOfferEndHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new AsyncCardOfferEndHook();

			tgsb.Phases.Add(new ActivationPhase());
			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[0] }));

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new RebootTile(new Direction(DirectionType.Down));
			tgsb.Map = tmb.BuildMap();
		}

		[Test]
		public void HookEffectTest()
		{
			AddProgrammingCardHook h2 = new AddProgrammingCardHook(tcb.BuildDummy());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook, h2));

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			iMock.Verify(i => i.EndAsyncUpgradeCardOffer(robot.Name, "u1"), Times.Once);
		}

		[Test]
		public void AttachedHookTest()
		{
			AddProgrammingCardHook h2 = new AddProgrammingCardHook(tcb.BuildDummy());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook, h2));

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });


			gs.GetPhase<ActivationPhase>().AfterActivationPhaseStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			iMock.Verify(i => i.EndAsyncUpgradeCardOffer(robot.Name, "u1"), Times.Once);

		}

		[Test]
		public void AttachedHookTest2()
		{
			AddProgrammingCardHook h2 = new AddProgrammingCardHook(tcb.BuildDummy());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook, h2));

			gs.GetPhase<ActivationPhase>().AfterActivationPhaseStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			iMock.Verify(i => i.EndAsyncUpgradeCardOffer(robot.Name, "u1"), Times.Once);

		}
    }
}
