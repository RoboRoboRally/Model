﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks.AsyncCardHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests.AsyncHookTests
{
    [TestFixture]
    public class MoveToAdjacentSpaceHookTests : HookTestsBase
    {
		MoveToAdjacentHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new MoveToAdjacentHook(
				new UpgradeCardChoice[]
				{
					new UpgradeCardChoice() { ChoiceCardName = "c1" },
					new UpgradeCardChoice() { ChoiceCardName = "c2" }
				},
				new ChoiceValue<DirectionType>[]
				{
					new ChoiceValue<DirectionType>() { ChoiceCardName = "c1", Value = DirectionType.Up },
					new ChoiceValue<DirectionType>() { ChoiceCardName = "c2", Value = DirectionType.Down }
				});

			tgsb.Actions.Add(new MovementAction());

			robot.SetNewPosition(5, 5);
			robot.SetNewFacingDirection(DirectionType.Right);
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase(),
				new UpgradeCardChoice() { ChoiceCardName = "c1" });

			Assert.That(robot.Position == new MapCoordinates(4, 5));
			Assert.That(robot.FacingDirection == DirectionType.Right);

			notMock.Verify(n => n.RobotMoved(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()));
		}

		[Test]
		public void AttachedHookTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook));

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncChoiceUpgradeCardUseInfo() { CardName = "u1", RobotName = robot.Name, ChosenChoice = new UpgradeCardChoice() { ChoiceCardName = "c1" } });

			gs.ProcessAsyncUpgradeCardUses();

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncChoiceUpgradeCardUseInfo() { CardName = "u1", RobotName = robot.Name, ChosenChoice = new UpgradeCardChoice() { ChoiceCardName = "c1" } });

			gs.ProcessAsyncUpgradeCardUses();


			Assert.That(robot.Position == new MapCoordinates(4, 5));
			Assert.That(robot.FacingDirection == DirectionType.Right);

			notMock.Verify(n => n.RobotMoved(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);

		}
    }
}
