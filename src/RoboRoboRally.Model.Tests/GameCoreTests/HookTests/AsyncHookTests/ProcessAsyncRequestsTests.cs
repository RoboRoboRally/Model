﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks.AsyncCardHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests.AsyncHookTests
{
    [TestFixture]
    public class ProcessAsyncRequestsTests : HookTestsBase
    {
		AddEnergyHook asyncHook;
		MoveToAdjacentHook asyncChoiceHook;

		[SetUp]
		public void LocalSetup()
		{
			asyncHook = new AddEnergyHook(2);
			asyncChoiceHook = new MoveToAdjacentHook(
				new UpgradeCardChoice[]
				{
					new UpgradeCardChoice() { ChoiceCardName = "c1" },
					new UpgradeCardChoice() { ChoiceCardName = "c2" }
				},
				new ChoiceValue<DirectionType>[]
				{
					new ChoiceValue<DirectionType>() { ChoiceCardName = "c1", Value = DirectionType.Up },
					new ChoiceValue<DirectionType>() { ChoiceCardName = "c2", Value = DirectionType.Down }
				});

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new RebootTile(new Direction(DirectionType.Down));
			tgsb.Map = tmb.BuildMap();

			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[0] }));
			tgsb.Actions.Add(new MovementAction());
		}

		[TestCase(1)]
		[TestCase(2)]
		public void ChoicelessHookTest1(int useNumber)
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, asyncHook));

			for(int i = 0; i < useNumber; ++i)
				gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncUpgradeCardUseInfo() { CardName = "u1", RobotName = robot.Name });

			gs.ProcessAsyncUpgradeCardUses();

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1"), Times.Once);

			notMock.Verify(n => n.UpgradeCardDiscarded(robot.Name, "u1"), Times.Once);

			iMock.Verify(i => i.EndAsyncUpgradeCardOffer(robot.Name, "u1"), Times.Once);
		}

		[Test]
		public void RebootedRobotTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, asyncHook));

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncUpgradeCardUseInfo() { CardName = "u1", RobotName = robot.Name });

			gs.ProcessAsyncUpgradeCardUses();

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1"), Times.Never);
		}

		[Test]
		public void ChoiceHookTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, asyncChoiceHook));

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncChoiceUpgradeCardUseInfo() { CardName = "u1",
					ChosenChoice = new UpgradeCardChoice() { ChoiceCardName = "c1" },
					RobotName = robot.Name });

			gs.ProcessAsyncUpgradeCardUses();

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1", new UpgradeCardChoice() { ChoiceCardName = "c1" }));

			notMock.Verify(n => n.UpgradeCardDiscarded(robot.Name, "u1"));

			iMock.Verify(n => n.EndAsyncUpgradeCardOffer(robot.Name, "u1"));
		}

		[Test]
		public void InvalidUseInfoTest()
		{
			//invalid messages should be ignored

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncUpgradeCardUseInfo() { CardName = "abc", RobotName = "def" });
			
			//nothing happens...
			gs.ProcessAsyncUpgradeCardUses();
		}
    }
}
