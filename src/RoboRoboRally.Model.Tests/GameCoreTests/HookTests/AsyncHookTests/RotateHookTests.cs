﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks.AsyncCardHooks;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;


namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests.AsyncHookTests
{
    [TestFixture]
    public class RotateHookTests : HookTestsBase
    {
		RotateHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new RotateHook(
				new UpgradeCardChoice[]
				{
					new UpgradeCardChoice() { ChoiceCardName = "c1" },
					new UpgradeCardChoice() { ChoiceCardName = "c2" }
				},
				new ChoiceValue<RotationType>[]
				{
					new ChoiceValue<RotationType>() { ChoiceCardName = "c1", Value = RotationType.Left },
					new ChoiceValue<RotationType>() { ChoiceCardName = "c2", Value = RotationType.Right }
				});

			robot.SetNewPosition(5, 5);
			robot.SetNewFacingDirection(DirectionType.Up);
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot },
				new HookRetBase(), new UpgradeCardChoice() { ChoiceCardName = "c1" });

			notMock.Verify(n => n.RobotRotated(robot.Name, RotationType.Left));

			Assert.That(robot.FacingDirection == DirectionType.Left);
		}

		[Test]
		public void AttachedHookTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook));

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncChoiceUpgradeCardUseInfo() { RobotName = robot.Name, CardName = "u1",
				ChosenChoice  = new UpgradeCardChoice() { ChoiceCardName = "c1" }
				});

			gs.ProcessAsyncUpgradeCardUses();

			gs.AsyncUpgradeCardUseInfoQueue.Enqueue(
				new AsyncChoiceUpgradeCardUseInfo()
				{
					RobotName = robot.Name,
					CardName = "u1",
					ChosenChoice = new UpgradeCardChoice() { ChoiceCardName = "c1" }
				});

			gs.ProcessAsyncUpgradeCardUses();

			notMock.Verify(n => n.RobotRotated(robot.Name, It.IsAny<RotationType>()), Times.Once);

			Assert.That(robot.FacingDirection == DirectionType.Left);
			Assert.That(robot.Position == new MapCoordinates(5, 5));
		}
		
    }
}
