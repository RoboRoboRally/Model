﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class HookableGameStepTests : HookTestsBase
    {
        [Test]
        public void ChoicelessHookTest()
        {
			tgsb.Phases.Add(new ProgrammingPhase(9));

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, new IncrementDrawAmountHook(false)));

			gs.GetPhase<ProgrammingPhase>().DrawAmountStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new DrawAmountStepRet() { DrawAmount = 5 });

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1"), Times.Once);
        }

		[TestCase(UpgradeCardType.Permanent)]
		[TestCase(UpgradeCardType.Temporary)]
		public void RebootedRobotTest(UpgradeCardType uct)
		{
			//test that card is not used when robot has rebooted
			tgsb.Phases.Add(new ProgrammingPhase(9));
			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[0] }));

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new RebootTile(new Direction(DirectionType.Up));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", uct, 5, new IncrementDrawAmountHook(false)));

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			gs.GetPhase<ProgrammingPhase>().DrawAmountStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new DrawAmountStepRet() { DrawAmount = 5 });

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1"), Times.Never);

			if (uct == UpgradeCardType.Temporary)
				notMock.Verify(n => n.UpgradeCardDiscarded(robot.Name, "u1"), Times.Never);
		}

		[TestCase(UpgradeCardType.Permanent)]
		[TestCase(UpgradeCardType.Temporary)]
		public void ChoicelessHookTest2(UpgradeCardType uct)
		{
			//let's test that when robot looses the card, the effect is not performed
			tgsb.Phases.Add(new ProgrammingPhase(9));

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", uct, 5, new IncrementDrawAmountHook(false)));

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetPhase<ProgrammingPhase>().DrawAmountStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new DrawAmountStepRet() { DrawAmount = 5 });

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1"), Times.Never);
		}

		[TestCase(1, true, UpgradeCardType.Permanent)]
		[TestCase(1, true, UpgradeCardType.Temporary)]
		[TestCase(0, false, UpgradeCardType.Permanent)]
		[TestCase(0, false, UpgradeCardType.Temporary)]
		public void ChoicelessHookWithApproveTest(int times, bool b, UpgradeCardType uct)
		{
			tgsb.Phases.Add(new ProgrammingPhase(9));

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", uct, 5, new IncrementDrawAmountHook(true)));

			iMock.Setup(i => i.OfferChoicelessUpgradeCard(robot.Name, "u1")).Returns(new UpgradeCardUseInfo() { UseCard = b });

			gs.GetPhase<ProgrammingPhase>().DrawAmountStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new DrawAmountStepRet() { DrawAmount = 5 });

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1"), Times.Exactly(times));

			if (uct == UpgradeCardType.Temporary)
				notMock.Verify(n => n.UpgradeCardDiscarded(robot.Name, "u1"), Times.Exactly(times));
		}

		[TestCase(1, true, UpgradeCardType.Permanent)]
		[TestCase(1, true, UpgradeCardType.Temporary)]
		[TestCase(0, false, UpgradeCardType.Permanent)]
		[TestCase(0, false, UpgradeCardType.Temporary)]
		public void ChoiceHookTest(int times, bool b, UpgradeCardType uct)
		{
			tgsb.Phases.Add(new ActivationPhase());

			//in order for this hook to work, we need to set some program
			robot.SetNewProgram(new ProgrammingCard[] { tcb.BuildDummy(), tcb.BuildDummy(), null, null, null });

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", uct, 5, new ChangeRegisterProgrammingHook(
				new UpgradeCardChoice[] { new UpgradeCardChoice() { ChoiceCardName = "c1" }, new UpgradeCardChoice() { ChoiceCardName = "c2" } },
				new ChoiceValue<ProgrammingCard>[] {
					new ChoiceValue<ProgrammingCard>() { ChoiceCardName = "c1", Value = tcb.BuildPCard("p1", new CardEffect[0]) },
					new ChoiceValue<ProgrammingCard>() { ChoiceCardName = "c2", Value = tcb.BuildPCard("p2", new CardEffect[0]) }
				})));

			iMock.Setup(i => i.OfferChoiceUpgradeCard(robot.Name, "u1", It.IsAny<UpgradeCardChoice[]>())).Returns(
				(string rName, string uName, UpgradeCardChoice[] choices) => { return new ChoiceUpgradeCardUseInfo() { UseCard = b, ChosenChoice = choices[0] }; });

			robot.RevealNextRegister();

			gs.GetPhase<ActivationPhase>().BeforePlayingRegisterStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			notMock.Verify(n => n.UpgradeCardUsed(robot.Name, "u1", It.IsAny<UpgradeCardChoice>()), Times.Exactly(times));

			if (uct == UpgradeCardType.Temporary)
				notMock.Verify(n => n.UpgradeCardDiscarded(robot.Name, "u1"), Times.Exactly(times));
		}

		public void ChoiceHookTest2()
		{
			//lets test that if we take invalid choice, exception is thrown

			tgsb.Phases.Add(new ActivationPhase());

			//in order for this hook to work, we need to set some program
			robot.SetNewProgram(new ProgrammingCard[] { tcb.BuildDummy(), tcb.BuildDummy(), null, null, null });

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, new ChangeRegisterProgrammingHook(
				new UpgradeCardChoice[] { new UpgradeCardChoice() { ChoiceCardName = "c1" }, new UpgradeCardChoice() { ChoiceCardName = "c2" } },
				new ChoiceValue<ProgrammingCard>[] {
					new ChoiceValue<ProgrammingCard>() { ChoiceCardName = "c1", Value = tcb.BuildPCard("p1", new CardEffect[0]) },
					new ChoiceValue<ProgrammingCard>() { ChoiceCardName = "c2", Value = tcb.BuildPCard("p2", new CardEffect[0]) }
				})));

			iMock.Setup(i => i.OfferChoiceUpgradeCard(robot.Name, "u1", It.IsAny<UpgradeCardChoice[]>())).Returns(
				(string rName, string uName, UpgradeCardChoice[] choices) => { return new ChoiceUpgradeCardUseInfo()
				{ UseCard = true, ChosenChoice = new UpgradeCardChoice() { ChoiceCardName = "not present choice" } }; });

			robot.RevealNextRegister();

			Assert.Throws<RunningGameException>(() => gs.GetPhase<ActivationPhase>().BeforePlayingRegisterStep.PerformHooks(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase()));

		}


	}
}
