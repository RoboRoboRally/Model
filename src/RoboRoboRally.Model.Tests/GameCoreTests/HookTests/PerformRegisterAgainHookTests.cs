﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class PerformRegisterAgainHookTests : HookTestsBase
    {
		PerformRegisterAgainHook hook;
		ProgrammingCard[] program;

		[SetUp]
		public void LocalSetup()
		{
			hook = new PerformRegisterAgainHook(false);

			List<ProgrammingCard> pList = new List<ProgrammingCard>();
			for (int i = 1; i <= 5; ++i)
			{
				pList.Add(tcb.BuildPCard("p" + i));
			}
			program = pList.ToArray();
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.SetNewProgram(program);

			robot.PlayNextRegister(gs);

			var playedCard = program[0];

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new HookRetBase());

			notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, "p1"), Times.Exactly(2));
		}

		[Test]
		public void AttachedHookTest()
		{
			tgsb.Actions.Add(new ShootingAction());
			tgsb.Phases.Add(new ActivationPhase());

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new PriorityAntennaTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			robot.SetNewProgram(program);

			gs.GetPhase<ActivationPhase>().PerformPhase(gs);

			for (int i = 1; i <= 5; ++i)
			{
				notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, "p" + i), Times.Exactly(2));
			}

			robot.DiscardUpgradeCard(gs, "u1");

			List<ProgrammingCard> newProgram = new List<ProgrammingCard>();
			for (int i = 1; i <= 5; ++i)
				newProgram.Add(tcb.BuildPCard("p2" + i));
			program = newProgram.ToArray();

			robot.SetNewProgram(program);

			gs.GetPhase<ActivationPhase>().PerformPhase(gs);

			for (int i = 1; i <= 5; ++i)
				notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, "p2" + i), Times.Once);
		}
		
    }
}
