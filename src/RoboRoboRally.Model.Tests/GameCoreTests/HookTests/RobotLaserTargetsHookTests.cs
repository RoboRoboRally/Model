﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class RobotLaserTargetsHookTests : HookTestsBase
    {
		Robot r2;
		RobotLaserTargetsHook hook;

		[SetUp]
		public void LocalSetup()
		{
			robot.SetNewPosition(new MapCoordinates(5, 5));
			robot.SetNewFacingDirection(new Direction(DirectionType.Up));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(3, 5);
			trb.FacingDir = new Direction(DirectionType.Left);
			trb.Name = "r2";
			r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			hook = new RobotLaserTargetsHook(false);
		}

		[Test]
		public void HookEffectTest()
		{
			//simple test for directions
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new ShootingTargetsStepArgs()
			{
				GameState = gs,
				Robot = robot,
				ShootingDirections = new List<Direction>() { new Direction(DirectionType.Up) }
			},
			new ShootingTargetsStepRet() { RobotsToShoot = new HashSet<Robot>() });

			Assert.That(ret.RobotsToShoot.Contains(r2));

			ret = hook.PerformHook(new ShootingTargetsStepArgs()
			{
				GameState = gs,
				Robot = robot,
				ShootingDirections = new List<Direction>() { new Direction(DirectionType.Down) }
			},
			new ShootingTargetsStepRet() { RobotsToShoot = new HashSet<Robot>() });

			Assert.That(!ret.RobotsToShoot.Contains(r2));
		}

		[Test]
		public void HookEffectTest2()
		{
			//test with walls
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 5], new Direction(DirectionType.Down)));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[3, 5], new Direction(DirectionType.Up)));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new ShootingTargetsStepArgs()
			{
				GameState = gs,
				Robot = robot,
				ShootingDirections = new List<Direction>() { new Direction(DirectionType.Up) }
			},
			new ShootingTargetsStepRet() { RobotsToShoot = new HashSet<Robot>() });

			Assert.That(ret.RobotsToShoot.Contains(r2));
		}

		[Test]
		public void HookEffectTest3()
		{
			//test with priority antenna tile
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[4, 5] = new PriorityAntennaTile(new Direction(DirectionType.Left));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new ShootingTargetsStepArgs()
			{
				GameState = gs,
				Robot = robot,
				ShootingDirections = new List<Direction>() { new Direction(DirectionType.Up) }
			},
			new ShootingTargetsStepRet() { RobotsToShoot = new HashSet<Robot>() });

			Assert.That(!ret.RobotsToShoot.Contains(r2));
		}

		[Test]
		public void HookEffectTest4()
		{
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(1, 5);
			trb.FacingDir = new Direction(DirectionType.Left);
			trb.Name = "r3";
			var r3 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r3);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new ShootingTargetsStepArgs()
			{
				GameState = gs,
				Robot = robot,
				ShootingDirections = new List<Direction>() { new Direction(DirectionType.Up) }
			},
			new ShootingTargetsStepRet() { RobotsToShoot = new HashSet<Robot>() });

			Assert.That(ret.RobotsToShoot.Contains(r2) && ret.RobotsToShoot.Contains(r3));
		}

		[Test]
		public void AttachedHookTest()
		{
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(1, 5);
			trb.FacingDir = new Direction(DirectionType.Left);
			trb.Name = "r3";
			var r3 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r3);

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[2, 5], new Direction(DirectionType.Down)));
			tgsb.Map = tmb.BuildMap();

			tgsb.Actions.Add(new ShootingAction());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Exactly(2));

			notMock.Verify(n => n.RobotSufferedDamage(r3.Name, It.IsAny<Damage>()), Times.Once);
		}
	}
}
