﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.MapDB.Tiles;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class PullShotRobotsHookTests : HookTestsBase
    {
		PullShotRobotsHook hook;
		Robot r2;
		Robot r3;

		[SetUp]
		public void LocalSetup()
		{
			hook = new PullShotRobotsHook(false);
			
			robot.SetNewPosition(5, 5);
			robot.SetNewFacingDirection(DirectionType.Up);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "r2";
			trb.Position = new MapCoordinates(3, 5);
			trb.FacingDir = new Direction(DirectionType.Left);
			r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "r3";
			trb.Position = new MapCoordinates(1, 5);
			trb.FacingDir = new Direction(DirectionType.Left);
			r3 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);
			tgsb.StartingRobots.Add(r3);

			tgsb.Actions.Add(new PushAction());
			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[0] }));
			tgsb.Actions.Add(new ShootingAction());
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			string callOrder = "";

			notMock.Setup(n => n.RobotGotPushed(r2.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>())).Callback(() => callOrder += "r2");
			notMock.Setup(n => n.RobotGotPushed(r3.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>())).Callback(() => callOrder += "r3");

			hook.PerformHook(new ShotTargetsStepArgs() { GameState = gs, Robot = robot, ShotRobots = new List<Robot>() { r2, r3 } }, new HookRetBase());

			Assert.That(r2.Position == new MapCoordinates(4, 5));
			Assert.That(r3.Position == new MapCoordinates(2, 5));

			Assert.That(callOrder == "r2r3");
		}

		[Test]
		public void HookEffectTest2()
		{
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "r4";
			trb.Position = new MapCoordinates(4, 5);
			trb.FacingDir = new Direction(DirectionType.Up);
			var r4 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r4);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			hook.PerformHook(new ShotTargetsStepArgs() { GameState = gs, Robot = robot, ShotRobots = new List<Robot>() { r2, r3, r4 } }, new HookRetBase());

			Assert.That(r4.Position == new MapCoordinates(4, 5));
			Assert.That(r2.Position == new MapCoordinates(3, 5));
			Assert.That(r3.Position == new MapCoordinates(2, 5));

			notMock.Verify(n => n.RobotGotPushed(r3.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);
			notMock.Verify(n => n.RobotGotPushed(r2.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
			notMock.Verify(n => n.RobotGotPushed(r4.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
		}

		[Test]
		public void HookEffectTest3()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new RebootTile(new Direction(DirectionType.Down));
			tmb.Tiles[2, 5] = new PitTile();
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			hook.PerformHook(new ShotTargetsStepArgs() { GameState = gs, Robot = robot, ShotRobots = new List<Robot>() { r2, r3} }, new HookRetBase());

			notMock.Verify(n => n.RobotIsRebooting(r3.Name, It.IsAny<MapCoordinates>(), It.IsAny<Direction>()));
		}

		[Test]
		public void AttachedHookTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			//let's actually set r2 far enough so that we could pull it twice
			r2.SetNewPosition(2, 5);
			r3.SetNewPosition(0, 5);

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			Assert.That(r2.Position == new MapCoordinates(3, 5));
			Assert.That(r3.Position == new MapCoordinates(0, 5));

			notMock.Verify(n => n.RobotGotPushed(r2.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);
			notMock.Verify(n => n.RobotGotPushed(r3.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
			
		}
	}
}
