﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class PushToSideHookTests : HookTestsBase
    {
		PushToSideHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new PushToSideHook(new UpgradeCardChoice[]
			{
				new UpgradeCardChoice() { ChoiceCardName = "Left" },
				new UpgradeCardChoice() { ChoiceCardName = "Right" }
			},
			new ChoiceValue<RotationType>[]
			{
				new ChoiceValue<RotationType> { ChoiceCardName = "Left", Value = RotationType.Left },
				new ChoiceValue<RotationType> { ChoiceCardName = "Right", Value = RotationType.Right }
			});
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot },
				new PushDirectionStepRet() { Amount = 1, Direction = new Direction(DirectionType.Down) },
				new UpgradeCardChoice() { ChoiceCardName = "Left" });

			Assert.That(ret.Direction == DirectionType.Right);
		}

		[Test]
		public void HookEffectTest2()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot },
				new PushDirectionStepRet() { Amount = 1, Direction = new Direction(DirectionType.Down) },
				new UpgradeCardChoice() { ChoiceCardName = "Right" });

			Assert.That(ret.Direction == DirectionType.Left);
		}

        [Test]
        public void AttachedHookTest()
        {
			robot.SetNewPosition(5, 5);
			robot.SetNewFacingDirection(DirectionType.Up);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "r2";
			trb.Position = new MapCoordinates(4, 5);
			trb.FacingDir = new Direction(DirectionType.Up);
			var r2 = trb.BuildRobot();

			tgsb.Actions.Add(new MovementAction());
			tgsb.Actions.Add(new PushAction());

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			iMock.Setup(i => i.OfferChoiceUpgradeCard(robot.Name, "u1", It.IsAny<UpgradeCardChoice[]>())).Returns(
				(string name, string c, UpgradeCardChoice[] choices) =>
				{
					return new ChoiceUpgradeCardUseInfo() { UseCard = true, ChosenChoice = new UpgradeCardChoice() { ChoiceCardName = "Left" } };
				});

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { GameState = gs, Robot = robot, Amount = 1, Direction = new Direction(DirectionType.Up) });

			Assert.That(r2.Position == new MapCoordinates(4, 4));
			Assert.That(r2.FacingDirection == DirectionType.Up);
			Assert.That(robot.Position == new MapCoordinates(4, 5));
			Assert.That(robot.FacingDirection == DirectionType.Up);

			robot.Rotate(gs, RotationType.Left);

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { GameState = gs, Robot = robot, Amount = 1, Direction = new Direction(DirectionType.Left) });

			Assert.That(r2.Position == new MapCoordinates(4, 3));
		}
	}
}
