﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class HookTestsBase
    {
		protected TestCardBuilder tcb;
		protected Robot robot;
		protected Mock<IGameEventNotifier> notMock;
		protected Mock<IInputHandler> iMock;
		protected TestGameStateBuilder tgsb;

        [SetUp]
        public void TestSetup()
        {
			tcb = new TestCardBuilder();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();
			
			TestMapBuilder tmb = new TestMapBuilder();

			tgsb = new TestGameStateBuilder();
			tgsb.Map = tmb.BuildMap();
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			tgsb.StartingRobots.Add(robot);
        }
    }
}
