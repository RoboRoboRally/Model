﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class ChangeRegisterProgrammingHookTests : HookTestsBase
    {
		ChangeRegisterProgrammingHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new ChangeRegisterProgrammingHook(
				new UpgradeCardChoice[]
				{
					new UpgradeCardChoice() { ChoiceCardName = "c1" },
					new UpgradeCardChoice() { ChoiceCardName = "c2" }
				},
				new ChoiceValue<ProgrammingCard>[]
				{
					new ChoiceValue<ProgrammingCard>() { ChoiceCardName = "c1", Value = tcb.BuildPCard("pc1") },
					new ChoiceValue<ProgrammingCard>() { ChoiceCardName = "c2", Value = tcb.BuildPCard("pc2") }
				});
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			ProgrammingCard[] program = new ProgrammingCard[] { tcb.BuildDummy(), null, null, null, null };

			robot.SetNewProgram(program);

			var c = robot.RevealNextRegister();
			Assert.That(c.Name == tcb.BuildDummy().Name);

			hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot },
				new HookRetBase(),
				new UpgradeCardChoice() { ChoiceCardName = "c1" });

			robot.PlayNextRegister(gs);

			notMock.Verify(n => n.RegisterCardReplaced(robot.Name, 1, tcb.BuildDummy().Name, "pc1"));
			notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, "pc1"));
		}

		[Test]
		public void AttachedHookTest()
		{
			tgsb.Actions.Add(new ShootingAction());
			tgsb.Phases.Add(new ActivationPhase());

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new PriorityAntennaTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Temporary, 5, hook));

			iMock.Setup(i => i.OfferChoiceUpgradeCard(robot.Name, "u1", It.IsAny<UpgradeCardChoice[]>())).Returns(
				(string n, string c, UpgradeCardChoice[] choice) =>
				{
					return new ChoiceUpgradeCardUseInfo() { ChosenChoice = new UpgradeCardChoice() { ChoiceCardName = "c1" }, UseCard = true };
				});

			List<ProgrammingCard> program = new List<ProgrammingCard>() { tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy() };

			robot.SetNewProgram(program.ToArray());

			gs.GetPhase<ActivationPhase>().PerformPhase(gs);

			notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, "pc1"));
			notMock.Verify(n => n.ProgrammingCardPlayed(robot.Name, tcb.BuildDummy().Name), Times.Exactly(4));
		}
    }
}
