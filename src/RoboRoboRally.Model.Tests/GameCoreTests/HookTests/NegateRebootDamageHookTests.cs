﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class NegateRebootDamageHookTests : HookTestsBase
    {
		NegateRebootDamageHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new NegateRebootDamageHook(false);
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new DamageStepRet() { Damage = new Damage() { DamageCards = new DamageCard[] { tcb.BuildDDummy() } } });

			Assert.That(ret.Damage.DamageCards.Length == 0);
		}

		[Test]
		public void AttachedHookTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Down));
			tmb.Tiles[1, 1] = new RebootTile(new Direction(DirectionType.Left));
			tgsb.Map = tmb.BuildMap();

			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[] { tcb.BuildDDummy() } }));

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			notMock.Verify(n => n.RobotIsRebooting(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<Direction>()), Times.Exactly(2));

			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Once);

		}
    }
}
