﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class PushDamageHookTests : HookTestsBase
    {
		PushDamageHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new PushDamageHook(false, new Damage() { DamageCards = new DamageCard[] { tcb.BuildDDummy() } });
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new DamageStepRet() { Damage = new Damage() { DamageCards = new DamageCard[0] } });

			Assert.That(ret.Damage.DamageCards.Any(c => c.Name == tcb.BuildDDummy().Name));
		}

		[Test]
		public void AttachedHookTest()
		{
			robot.SetNewPosition(5, 5);
			robot.SetNewFacingDirection(DirectionType.Up);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(4, 5);
			trb.Name = "r2";
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			tgsb.Actions.Add(new MovementAction());
			tgsb.Actions.Add(new PushAction());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { GameState = gs, Robot = robot, Amount = 1, Direction = new Direction(DirectionType.Up) });

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { GameState = gs, Robot = robot, Amount = 1, Direction = new Direction(DirectionType.Up) });

			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Once);
		}
    }
}
