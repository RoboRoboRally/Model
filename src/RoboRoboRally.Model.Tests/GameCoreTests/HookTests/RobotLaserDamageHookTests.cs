﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class RobotLaserDamageHookTests : HookTestsBase
    {
		[Test]
		public void HookEffectTest()
		{
			TestCardBuilder tcb = new TestCardBuilder();

			var additionalDamage = new DamageCard[] { tcb.BuildDDummy() };

			var hook = new RobotLaserDamageHook(false, new Damage() { DamageCards = additionalDamage });

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new ShotTargetsStepArgs() { Robot = robot, GameState = gs, ShotRobots = new System.Collections.Generic.List<Core.Robot>() { robot } }, 
				new DamageStepRet() { Damage = new Damage() { DamageCards = new DamageCard[] { tcb.BuildDDummy() } } });

			var dmgCards = ret.Damage.DamageCards;

			Assert.That(dmgCards.Length == 2);
			Assert.That(dmgCards[0].Name == tcb.BuildDDummy().Name && dmgCards[1].Name == tcb.BuildDDummy().Name);
		}

		[Test]
		public void AttachedHookTest()
		{
			robot.SetNewPosition(new MapCoordinates(5, 5));
			robot.SetNewFacingDirection(new Direction(DirectionType.Up));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "r2";
			trb.Position = new MapCoordinates(3, 5);
			trb.FacingDir = new Direction(DirectionType.Right);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			tgsb.Actions.Add(new ShootingAction());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var hook = new RobotLaserDamageHook(false, new Damage() { DamageCards = new DamageCard[] { tcb.BuildDDummy() } });

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			//default damage for Robot "robot" contains "Spam" card, so these checks should be correct
			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, 
				It.Is<Damage>(d => d.DamageCards.Any(c => c.Name == tcb.BuildDDummy().Name))), Times.Once);

			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Never);

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			notMock.Verify(n => n.RobotSufferedDamage(r2.Name,
				It.Is<Damage>(d => !d.DamageCards.Any(c => c.Name == tcb.BuildDDummy().Name))), Times.Once);


		}
    }
}
