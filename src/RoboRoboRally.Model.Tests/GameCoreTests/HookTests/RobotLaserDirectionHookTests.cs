﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class RobotLaserDirectionHookTests : HookTestsBase
    {
		RobotLaserDirectionHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new RobotLaserDirectionHook(false);
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var ret = hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, new ShootingDirectionStepRet() { Directions = new HashSet<Direction>() });

			Assert.That(ret.Directions.Contains(robot.FacingDirection.GetOppositeDirection()));
		}

		[Test]
		public void AttachedHookTest()
		{
			robot.SetNewPosition(new MapCoordinates(5, 5));
			robot.SetNewFacingDirection(new Direction(DirectionType.Up));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "r2";
			trb.Position = new MapCoordinates(3, 5);
			var r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "r3";
			trb.Position = new MapCoordinates(7, 5);
			var r3 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "r4";
			trb.Position = new MapCoordinates(5, 7);
			var r4 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);
			tgsb.StartingRobots.Add(r3);
			tgsb.StartingRobots.Add(r4);

			tgsb.Actions.Add(new ShootingAction());

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Exactly(2));

			notMock.Verify(n => n.RobotSufferedDamage(r3.Name, It.IsAny<Damage>()), Times.Once);

			notMock.Verify(n => n.RobotSufferedDamage(r4.Name, It.IsAny<Damage>()), Times.Never);

			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Never);
		}
	}
}
