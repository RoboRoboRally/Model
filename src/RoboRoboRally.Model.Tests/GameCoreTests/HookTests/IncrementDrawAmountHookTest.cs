﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class IncrementDrawAmountHookTest : HookTestsBase
    {
		IncrementDrawAmountHook hook;

		[SetUp]
		public void LocalSetup()
		{
			hook = new IncrementDrawAmountHook(false);
		}

		[Test]
		public void HookEffectTest()
		{
			var gs = tgsb.BuildGameStateWithTurnBegan();

			var res = hook.PerformHook(new HookArgsBase() { GameState = gs, Robot = robot }, 
				new DrawAmountStepRet() { DrawAmount = 1 });

			Assert.That(res.DrawAmount == 2);			
        }

		[Test]
		public void AttachedHookTest()
		{
			tgsb.Phases.Add(new ProgrammingPhase(5));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "r2";
			trb.Position = new MapCoordinates(6, 6);
			var r2 = trb.BuildRobot();

			for (int i = 0; i < 15; ++i)
			{
				robot.DiscardCards(new ProgrammingCard[] { tcb.BuildDummy() });
				r2.DiscardCards(new ProgrammingCard[] { tcb.BuildDummy() });
			}
			

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			iMock.Setup(i => i.GetPrograms(It.IsAny<ProgramOffer[]>())).Returns((ProgramOffer[] off) =>
			{
				var x = new ProgramChoice[off.Length];

				for (int i = 0; i < x.Length; ++i)
				{
					x[i].RobotName = off[i].RobotName;
					x[i].EmptyProgram = true;
				}
				return x;
			});

			gs.GetPhase<ProgrammingPhase>().PerformPhase(gs);

			iMock.Verify(i => i.GetPrograms(It.Is<ProgramOffer[]>
				(p => p[0].RobotName == robot.Name && p[0].CardsToChoose.Length == 6 &&
				p[1].RobotName == r2.Name && p[1].CardsToChoose.Length == 5)));

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetPhase<ProgrammingPhase>().PerformPhase(gs);

			iMock.Verify(i => i.GetPrograms(It.Is<ProgramOffer[]>
				(p => p[0].RobotName == robot.Name && p[0].CardsToChoose.Length == 5 &&
				p[1].RobotName == r2.Name && p[1].CardsToChoose.Length == 5)));
		}
    }
}
