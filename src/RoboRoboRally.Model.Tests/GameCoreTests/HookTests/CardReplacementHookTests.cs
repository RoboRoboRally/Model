﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.GameCoreTests.HookTests
{
    [TestFixture]
    public class CardReplacementHookTests : HookTestsBase
    {
		CardReplacementHook hook;

		Robot r2;

		[SetUp]
		public void LocalSetup()
		{
			hook = new CardReplacementHook(false);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.DefaultDamage = new Damage() { DamageCards = new DamageCard[0] };
			trb.Position = new MapCoordinates(5, 5);
			trb.FacingDir = new Direction(DirectionType.Up);
			trb.Name = "r1";
			robot = trb.BuildRobot();

			tgsb.StartingRobots = new List<Robot>();


			trb = new TestRobotBuilder();
			trb.Name = "r2";
			trb.Position = new MapCoordinates(3, 5);
			trb.FacingDir = new Direction(DirectionType.Left);
			//set empty pack
			trb.StartingPack = new Pack<ProgrammingCard>(new ProgrammingCard[0]);

			r2 = trb.BuildRobot();

			List<ProgrammingCard> program = new List<ProgrammingCard>();
			for (int i = 0; i < 5; ++i)
				program.Add(tcb.BuildDummy());

			r2.SetNewProgram(program.ToArray());

			tgsb.StartingRobots.Add(robot);
			tgsb.StartingRobots.Add(r2);			
		}

		[Test]
		public void HookEffectTest()
		{
			var newCard = tcb.BuildPCard("newCard");

			r2.DiscardCards(new ProgrammingCard[] { newCard });

			var gs = tgsb.BuildGameStateWithTurnBegan();

			hook.PerformHook(new ShotTargetsStepArgs() { GameState = gs, Robot = robot, ShotRobots = new List<Robot>() { r2 } }, new HookRetBase());

			notMock.Verify(n => n.RegisterCardReplaced(r2.Name, 1, It.IsAny<string>(), It.IsAny<string>()));

			var revealedCard = r2.RevealNextRegister();
			Assert.That(revealedCard == newCard);
		}

		[Test]
		public void HookEffectTest2()
		{
			//test that we do not replace anything if all registers were played
			var newCard = tcb.BuildPCard("newCard");

			r2.DiscardCards(new ProgrammingCard[] { newCard });

			var gs = tgsb.BuildGameStateWithTurnBegan();

			for (int i = 0; i < 5; ++i)
				r2.PlayNextRegister(gs);

			hook.PerformHook(new ShotTargetsStepArgs() { GameState = gs, Robot = robot, ShotRobots = new List<Robot>() { r2 } }, new HookRetBase());

			notMock.Verify(n => n.RegisterCardReplaced(r2.Name, It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
		}

		[Test]
		public void AttachedHookTest()
		{
			tgsb.Actions.Add(new ShootingAction());

			var newCard = tcb.BuildPCard("newCard");

			r2.DiscardCards(new ProgrammingCard[] { newCard });

			var gs = tgsb.BuildGameStateWithTurnBegan();

			robot.ObtainUpgradeCard(gs, tcb.BuildUCard("u1", UpgradeCardType.Permanent, 5, hook));

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			robot.DiscardUpgradeCard(gs, "u1");

			gs.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			notMock.Verify(n => n.RegisterCardReplaced(r2.Name, 1, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
			notMock.Verify(n => n.RegisterCardReplaced(r2.Name, It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

			var c = r2.RevealNextRegister();
			Assert.That(c == newCard);
		}

    }
}
