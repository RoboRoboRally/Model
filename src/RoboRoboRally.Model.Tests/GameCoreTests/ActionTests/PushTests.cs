﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class PushTests
    {
		TestMapBuilder tmb;
		Robot robot;
		TestGameStateBuilder tgsb;
		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;

		[SetUp]
		public void TestSetup()
		{
			tmb = new TestMapBuilder();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.FacingDir = new Direction(DirectionType.Up);
			robot = trb.BuildRobot();

			TestCardBuilder tcb = new TestCardBuilder();

			tgsb = new TestGameStateBuilder();
			tgsb.Actions.Add(new MovementAction());
			tgsb.Actions.Add(new PushAction());
			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[2] { tcb.BuildDDummy(), tcb.BuildDDummy() } }));
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			tgsb.StartingRobots.Add(robot);

		}

		[TestCase(1)]
		[TestCase(2)]
		public void BasicPushTest(int amount)
		{
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;
			var oldDir = robot.FacingDirection;

			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() {Amount = amount, Direction = new Direction(DirectionType.Left),
				GameState = gs, Robot = robot });

			var intendedPos = oldPos;

			for (int i = 0; i < amount; ++i)
				intendedPos += DirectionType.Left;

			Assert.That(robot.Position == intendedPos);
			Assert.That(robot.FacingDirection == oldDir);

			notMock.Verify(n => n.RobotGotPushed(robot.Name, oldPos, robot.Position));
		}

		[Test]
		public void PushTestWithObsactles1()
		{
			tmb.Tiles[5, 4] = new PriorityAntennaTile(new Direction(DirectionType.Down));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() {Amount = 1, Direction = new Direction(DirectionType.Left), GameState = gs, Robot = robot });

			Assert.That(robot.Position == oldPos);

			notMock.Verify(n => n.RobotGotPushed(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
		}

		[Test]
		public void PushTestWithObstacles2()
		{
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 4], new Direction(DirectionType.Left)));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() { Amount = 1, Direction = new Direction(DirectionType.Left), GameState = gs, Robot = robot });

			Assert.That(robot.Position == oldPos);

			notMock.Verify(n => n.RobotGotPushed(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
		}

		[Test]
		public void PushTestWithObstacles3()
		{
			//test that we push up to the obstacle (in this case, antenna)
			tmb.Tiles[5, 3] = new PriorityAntennaTile(new Direction(DirectionType.Down));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() { Amount = 2, Direction = new Direction(DirectionType.Left), GameState = gs, Robot = robot });

			Assert.That(robot.Position == oldPos + DirectionType.Left);

			notMock.Verify(n => n.RobotGotPushed(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);
		}

		[Test]
		public void PushTestWithObstacles4()
		{
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 3], new Direction(DirectionType.Right)));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() { Amount = 3, Direction = new Direction(DirectionType.Left), GameState = gs, Robot = robot });

			var intendedPos = oldPos + DirectionType.Left;
			intendedPos += DirectionType.Left;

			Assert.That(robot.Position == intendedPos);

			notMock.Verify(n => n.RobotGotPushed(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);

		}

		[Test]
		public void PushTestWithObstacles5()
		{
			//test that robot stops on pit tile
			//and ALSO !!! test that the reboot is NOT called automatically

			tmb.Tiles[5, 4] = new PitTile();

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() { Amount = 2, Direction = new Direction(DirectionType.Left), GameState = gs, Robot = robot });

			Assert.That(robot.Position == oldPos + DirectionType.Left);

			notMock.Verify(n => n.RobotGotPushed(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);
			notMock.Verify(n => n.RobotIsRebooting(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<Direction>()), Times.Never);
		}

		[Test]
		public void PushTestWithObstacles6()
		{
			//test that robot stops on out of map tile
			//and ALSO !!! test that the reboot is NOT called automatically

			tmb.Tiles[5, 4] = new OutOfMapTile();

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() { Amount = 2, Direction = new Direction(DirectionType.Left), GameState = gs, Robot = robot });

			Assert.That(robot.Position == oldPos + DirectionType.Left);

			notMock.Verify(n => n.RobotGotPushed(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);
			notMock.Verify(n => n.RobotIsRebooting(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<Direction>()), Times.Never);
		}


		[Test]
		public void RecursivePushTest1()
		{
			var r1OldPos = new MapCoordinates(5, 5);
			robot.SetNewPosition(r1OldPos);

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			var r2OldPos = new MapCoordinates(4, 5);
			trb.Position = r2OldPos;
			trb.Name = "SMASH__";
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var pushDir = new Direction(DirectionType.Up);
			gs.GetAction<PushAction>().PerformAction(new PushActionArgs() {Amount = 1, GameState = gs,
				Robot = robot, Direction = new Direction(DirectionType.Up) });

			Assert.That(robot.Position == r1OldPos + pushDir);
			Assert.That(r2.Position == r2OldPos + pushDir);

			notMock.Verify(n => n.RobotGotPushed(r2.Name, r2OldPos, r2.Position));
		}

		[Test]
		public void RecursivePushTest2()
		{
			var r1OldPos = new MapCoordinates(5, 5);
			robot.SetNewPosition(r1OldPos);

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			var r2OldPos = new MapCoordinates(3, 5);
			trb.Position = r2OldPos;
			trb.Name = "SMASH__";
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var pushDir = new Direction(DirectionType.Up);
			gs.GetAction<PushAction>().PerformAction(new PushActionArgs()
			{
				Amount = 3,
				GameState = gs,
				Robot = robot,
				Direction = new Direction(DirectionType.Up)
			});

			//this push should be implemented like this
			//	1. push r1 to [4,5]
			//	2. push r2 to [1,5]
			//	3. push r1 to [2,5]

			Assert.That(robot.Position == new MapCoordinates(2, 5));
			Assert.That(r2.Position == new MapCoordinates(1, 5));

			notMock.Verify(n => n.RobotGotPushed(r2.Name, r2OldPos, new MapCoordinates(1, 5)));
			notMock.Verify(n => n.RobotGotPushed(robot.Name, r1OldPos, new MapCoordinates(4, 5)));
			notMock.Verify(n => n.RobotGotPushed(robot.Name, new MapCoordinates(4, 5), new MapCoordinates(2, 5)));
		}

		[Test]
		public void RecursivePushToObstacleTest1()
		{
			var r1OldPos = new MapCoordinates(5, 5);
			robot.SetNewPosition(r1OldPos);

			tmb.Walls.Add(new EmptyWall(tmb.Tiles[1, 5], new Direction(DirectionType.Up)));

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			var r2OldPos = new MapCoordinates(3, 5);
			trb.Position = r2OldPos;
			trb.Name = "SMASH__";
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var pushDir = new Direction(DirectionType.Up);
			gs.GetAction<PushAction>().PerformAction(new PushActionArgs()
			{
				Amount = 4,
				GameState = gs,
				Robot = robot,
				Direction = new Direction(DirectionType.Up)
			});

			//in normal situation, the result should be r1.pos = [1,5], r2.pos = [0,5], but with the wall
			//the sequence should be like this:
			//	1. push r1 to [4, 5]
			//	2. push r2 only to [2, 5] as there is wall
			//	3. push r3 to [3, 5]

			Assert.That(robot.Position == new MapCoordinates(3, 5));
			Assert.That(r2.Position == new MapCoordinates(2, 5));

			notMock.Verify(n => n.RobotGotPushed(r2.Name, r2OldPos, new MapCoordinates(2, 5)));
			notMock.Verify(n => n.RobotGotPushed(robot.Name, r1OldPos, new MapCoordinates(4, 5)));
			notMock.Verify(n => n.RobotGotPushed(robot.Name, new MapCoordinates(4, 5), new MapCoordinates(3, 5)));

		}
	}
}
