﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class MovementTests
    {
		TestMapBuilder tmb;
		Robot robot;
		TestGameStateBuilder tgsb;
		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;

		[SetUp]
		public void TestSetup()
		{
			tmb = new TestMapBuilder();
			
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.FacingDir = new Direction(DirectionType.Up);
			robot = trb.BuildRobot();

			TestCardBuilder tcb = new TestCardBuilder();

			tgsb = new TestGameStateBuilder();
			tgsb.Actions.Add(new MovementAction());
			tgsb.Actions.Add(new PushAction());
			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[2] { tcb.BuildDDummy(), tcb.BuildDDummy() } }));
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			tgsb.StartingRobots.Add(robot);
		}

        [Test]
        public void BasicMovementTest1()
        {
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;
			var oldDir = robot.FacingDirection;

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 3, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			var expPos = oldPos + DirectionType.Up;
			expPos += DirectionType.Up;
			expPos += DirectionType.Up;

			Assert.That(robot.Position == expPos);
			Assert.That(robot.FacingDirection == oldDir);

			notMock.Verify(n => n.RobotMoved(robot.Name, oldPos, robot.Position));
        }

		[Test]
		public void BasicMovementTest2()
		{
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;
			var oldDir = robot.FacingDirection;

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = -1, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			var expPos = oldPos + robot.FacingDirection.GetOppositeDirection();

			Assert.That(robot.Position == expPos);
			Assert.That(robot.FacingDirection == oldDir);

			notMock.Verify(n => n.RobotMoved(robot.Name, oldPos, robot.Position));
		}

		[Test]
		public void MovementWithObstacleTest1()
		{
			tmb.Tiles[4, 5] = new PriorityAntennaTile(new Direction(DirectionType.Down));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 1, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });
			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 2, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			Assert.That(robot.Position == oldPos);

			notMock.Verify(n => n.RobotMoved(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
		}

		[Test]
		public void MovementWithObsatecleTest2()
		{
			tmb.Tiles[2, 5] = new PriorityAntennaTile(new Direction(DirectionType.Down));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 3, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			var expPos = oldPos + DirectionType.Up;
			expPos += DirectionType.Up;

			Assert.That(robot.Position == expPos);

			notMock.Verify(n => n.RobotMoved(robot.Name, oldPos, robot.Position));
			notMock.Verify(n => n.RobotMoved(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);
		}

		[TestCase (4, DirectionType.Down)]
		[TestCase (3, DirectionType.Up)]
		public void MovementWithObstacleTest3(int rowIndex, DirectionType facingDir)
		{
			var w1 = new EmptyWall(tmb.Tiles[rowIndex, 5], new Direction(facingDir));

			tmb.Walls.Add(w1);

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 2, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			var expPos = oldPos + DirectionType.Up;

			Assert.That(robot.Position == expPos);

			notMock.Verify(n => n.RobotMoved(robot.Name, oldPos, robot.Position));
			notMock.Verify(n => n.RobotMoved(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Once);
		}

		[Test]
		public void MoveWithPushTest1()
		{
			robot.SetNewPosition(5,5);
			robot.SetNewFacingDirection(DirectionType.Up);

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(3, 5);
			trb.Name = "SMASH__";
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 2, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(3, 5));
			Assert.That(r2.Position == new MapCoordinates(2, 5));

			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(4, 5)));
			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(4, 5), robot.Position));

			notMock.Verify(n => n.RobotGotPushed(r2.Name, new MapCoordinates(3, 5), r2.Position));
		}

		[Test]
		public void MoveWithPushTest2()
		{
			robot.SetNewPosition(5, 5);
			robot.SetNewFacingDirection(DirectionType.Up);

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(4, 5);
			trb.Name = "SMASH__";
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 3, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(2, 5));
			Assert.That(r2.Position == new MapCoordinates(1, 5));

			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(2, 5)));
			notMock.Verify(n => n.RobotGotPushed(r2.Name, new MapCoordinates(4, 5), new MapCoordinates(1, 5)));
		}

		[Test]
		public void MoveWithRebootTest1()
		{
			robot.SetNewPosition(5, 5);
			robot.SetNewFacingDirection(DirectionType.Up);

			//we also have to setup some starting tiles
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Up));
			tmb.Tiles[3, 5] = new PitTile();
			tmb.Tiles[3, 3] = new RebootTile(new Direction(DirectionType.Up));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 3, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(3, 3));

			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(3, 5)));
			notMock.Verify(n => n.RobotIsRebooting(robot.Name, new MapCoordinates(3, 3), new Direction(DirectionType.Up)));
		}

		[Test]
		public void MoveWithRebootTest2()
		{
			robot.SetNewPosition(2, 5);
			robot.SetNewFacingDirection(DirectionType.Up);

			//we also have to setup some starting tiles
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Up));
			tmb.Tiles[3, 3] = new RebootTile(new Direction(DirectionType.Up));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { Amount = 3, Direction = new Direction(DirectionType.Up), GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(3, 3));

			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(2, 5), new MapCoordinates(-1, 5)));
			notMock.Verify(n => n.RobotIsRebooting(robot.Name, new MapCoordinates(3, 3), new Direction(DirectionType.Up)));
		}
	}
}
