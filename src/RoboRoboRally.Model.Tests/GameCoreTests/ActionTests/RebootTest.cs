﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class RebootTest
    {
		TestMapBuilder tmb;
		Robot robot;
		TestGameStateBuilder tgsb;
		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;

		[SetUp]
		public void TestSetup()
		{
			tmb = new TestMapBuilder();
			tmb.Tiles[3, 3] = new RebootTile(new Direction(DirectionType.Up));
			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Up)); //dummy

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.FacingDir = new Direction(DirectionType.Up);
			robot = trb.BuildRobot();

			TestCardBuilder tcb = new TestCardBuilder();

			tgsb = new TestGameStateBuilder();
			tgsb.Actions.Add(new MovementAction());
			tgsb.Actions.Add(new PushAction());
			tgsb.Actions.Add(new RebootAction(new Damage() { DamageCards = new DamageCard[2] { tcb.BuildDDummy(), tcb.BuildDDummy() } }));
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			tgsb.StartingRobots.Add(robot);
		}

		[TestCase(1)]
		[TestCase(2)]
		public void BasicRebootTest(int rebootCount)
		{
			var rebootPos = new MapCoordinates(3, 3);
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var dDummyName = new TestCardBuilder().BuildDDummy().Name;

			var oldPos = robot.Position;

			var c1 = gs.TurnActiveRobots.Count();
			var c2 = gs.TurnDeactivatedRobots.Count();

			for(int i = 0; i < rebootCount; ++i)
				gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(3, 3));
			Assert.That(robot.FacingDirection == DirectionType.Up);
			Assert.That(robot.DrawAllCards().Count(c => c.Name == dDummyName) == rebootCount * 2);

			notMock.Verify(n => n.RobotIsRebooting(robot.Name, rebootPos, new Direction(DirectionType.Up)), Times.Exactly(rebootCount));
			notMock.Verify(n => n.RobotTeleported(robot.Name, oldPos, rebootPos, new Direction(DirectionType.Up)), Times.Once);
			if(rebootCount > 1)
				notMock.Verify(n => n.RobotTeleported(robot.Name, rebootPos, rebootPos, new Direction(DirectionType.Up)), Times.Exactly(rebootCount - 1));
			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Exactly(rebootCount));

			Assert.That(gs.TurnActiveRobots.Count() == c1 - 1);
			Assert.That(gs.TurnDeactivatedRobots.Count() == c2 + 1);
			
		}

		[Test]
		public void MoveAndRebootTest()
		{
			robot.SetNewPosition(new MapCoordinates(0, 5));

			var rebootPos = new MapCoordinates(3, 3);
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { GameState = gs, Robot = robot, Amount = 1, Direction = new Direction(DirectionType.Up) });

			Assert.That(robot.Position == rebootPos);
		}

		[Test]
		public void MoveAndRebootTest2()
		{
			tmb = new TestMapBuilder(new MapSize(13, 10));
			tmb.Tiles[11, 8] = new StartingTile(new Direction(DirectionType.Up));
			tmb.Tiles[8, 9] = new RebootTile(new Direction(DirectionType.Right));

			robot.SetNewPosition(new MapCoordinates(0, 8));

			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { GameState = gs, Robot = robot, Amount = 1, Direction = new Direction(DirectionType.Up) });

			notMock.Verify(n => n.RobotIsRebooting(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<Direction>() ));
		}

		[Test]
		public void OccupiedRebootTileTest1()
		{
			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(3, 3); //coordinates of reboot tile
			trb.Name = "SMASH__";
			var occRobot = trb.BuildRobot();

			tgsb.StartingRobots.Add(occRobot);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var occRobotOldPos = occRobot.Position;

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(3, 3));
			Assert.That(occRobot.Position == occRobotOldPos + DirectionType.Up);

			notMock.Verify(n => n.RobotGotPushed(occRobot.Name, occRobotOldPos, occRobot.Position));
		}

		[Test]
		public void OccupiedRebootTileTest2()
		{
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[3, 3], new Direction(DirectionType.Down)));

			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Left));

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(3, 3); //coordinates of reboot tile
			trb.Name = "SMASH__";
			var occRobot = trb.BuildRobot();

			tgsb.StartingRobots.Add(occRobot);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(0, 0));
			Assert.That(occRobot.Position == new MapCoordinates(3, 3));
		}

		[Test]
		public void OccupiedRebootTileTest3()
		{
			//test that robots swap places 
			tmb.Tiles[3, 3] = new EmptyTile();

			tmb.Tiles[5, 4] = new RebootTile(new Direction(DirectionType.Right));

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 4); //coordinates of reboot tile
			trb.Name = "SMASH__";
			var occRobot = trb.BuildRobot();

			tgsb.StartingRobots.Add(occRobot);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(5, 4));
			Assert.That(occRobot.Position == new MapCoordinates(5, 5));
		}

		[Test]
		public void CyclicRebootTileTest()
		{
			//assert that cycle is detected and robot is placed to starting tile
			tmb.Tiles[3, 3] = new EmptyTile();

			tmb.Tiles[0, 0] = new StartingTile(new Direction(DirectionType.Right));

			tmb.Tiles[4, 4] = new RebootTile(new Direction(DirectionType.Right));
			tmb.Tiles[4, 5] = new PitTile();

			tgsb.Map = tmb.BuildMap();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(4, 4); //coordinates of reboot tile
			trb.Name = "SMASH__";
			var occRobot = trb.BuildRobot();

			tgsb.StartingRobots.Add(occRobot);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gs, Robot = robot });

			Assert.That(robot.Position == new MapCoordinates(0, 0));
			Assert.That(occRobot.Position == new MapCoordinates(4, 4));

		}
	}
}
