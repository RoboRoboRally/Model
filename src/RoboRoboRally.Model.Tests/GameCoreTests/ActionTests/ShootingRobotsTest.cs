﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class ShootingRobotsTest
    {
		TestGameStateBuilder tgsb;

		ActivationPhase ap;

		Robot r1;
		Robot r2;

		Mock<IGameEventNotifier> notMock;

		private Damage GetDummyDamage(int count)
		{
			TestCardBuilder tcb = new TestCardBuilder();
			List<DamageCard> dmgCards = new List<DamageCard>();

			for (int i = 0; i < count; ++i)
				dmgCards.Add(tcb.BuildDCard("dummy", new CardEffect[0]));

			return new Damage() { DamageCards = dmgCards.ToArray() };
		}

		[SetUp]
		public void TestSetup()
		{
			tgsb = new TestGameStateBuilder();

			notMock = tgsb.NotMock;

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			trb.DefaultDamage = GetDummyDamage(1);
			r1 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 2);
			trb.DefaultDamage = GetDummyDamage(2);
			r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r1);
			tgsb.StartingRobots.Add(r2);

			tgsb.Actions.Add(new ShootingAction());

			ap = new ActivationPhase();
		}

		[Test]
		public void ShootingRobotsTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[6, 3] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			r1.SetNewPosition(5, 5);
			r1.SetNewFacingDirection(DirectionType.Up);

			r2.SetNewPosition(5, 1);
			r2.SetNewFacingDirection(DirectionType.Up);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ShootWithRobots(gs);

			notMock.Verify(n => n.RobotLasersActivated(r1.Name));
			notMock.Verify(n => n.RobotLasersActivated(r2.Name));

			notMock.Verify(n => n.RobotSufferedDamage(It.IsAny<string>(), It.IsAny<Damage>()), Times.Never);
		}

		[Test]
		public void ShootingRobotsTest2()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[6, 3] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			r1.SetNewPosition(5, 5);
			r1.SetNewFacingDirection(DirectionType.Left);

			r2.SetNewPosition(5, 1);
			r2.SetNewFacingDirection(DirectionType.Right);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ShootWithRobots(gs);

			notMock.Verify(n => n.RobotSufferedDamage(r1.Name, It.IsAny<Damage>()), Times.Once);
			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Once);

			var x = r1.DrawAllCards().Count(c => c.Name == "dummy");
			Assert.That(x == 2);
			Assert.That(r2.DrawAllCards().Count(c => c.Name == "dummy") == 1);
		}

		[Test]
		public void ShootingRobotsTest3()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 3] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			r1.SetNewPosition(5, 5); 
			r1.SetNewFacingDirection(DirectionType.Left);

			r2.SetNewPosition(5, 1);
			r2.SetNewFacingDirection(DirectionType.Right);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ShootWithRobots(gs);

			//robots can not shoot through antenna tile
			notMock.Verify(n => n.RobotSufferedDamage(r1.Name, It.IsAny<Damage>()), Times.Never);
			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Never);
		}

		[Test]
		public void ShootingRobotsTest4()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[6, 3] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tgsb.Map = tmb.BuildMap();

			r1.SetNewPosition(5, 5);
			r1.SetNewFacingDirection(DirectionType.Left);

			r2.SetNewPosition(5, 1);
			r2.SetNewFacingDirection(DirectionType.Right);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ShootWithRobots(gs);

			//robots can not shoot through walls
			notMock.Verify(n => n.RobotSufferedDamage(r1.Name, It.IsAny<Damage>()), Times.Never);
			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Never);
		}

		[Test]
		public void ShootingRobotsTest5()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[6, 3] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			r1.SetNewPosition(5, 5);
			r1.SetNewFacingDirection(DirectionType.Left);

			r2.SetNewPosition(5, 1);
			r2.SetNewFacingDirection(DirectionType.Right);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH3";
			trb.Position = new MapCoordinates(5, 6);
			trb.FacingDir = new Direction(DirectionType.Left);
			trb.DefaultDamage = GetDummyDamage(3);
			var r3 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r3);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ShootWithRobots(gs);

			//robots can not shoot trhough each other
			notMock.Verify(n => n.RobotSufferedDamage(r1.Name, It.IsAny<Damage>()), Times.Exactly(2));
			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Once);
			notMock.Verify(n => n.RobotSufferedDamage(r3.Name, It.IsAny<Damage>()), Times.Never);

			Assert.That(r1.DrawAllCards().Count(c => c.Name == "dummy") == 5);
		}





	}
}
