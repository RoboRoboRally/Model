﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboRoboRally.Model.Tests.GameCoreTests
{
    [TestFixture]
    public class GameStateTests
    {
		GameState gs;
		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;
		Robot robot;
		

		[SetUp]
		public void TestSetup()
		{
			TestGameStateBuilder tgsb = new TestGameStateBuilder();
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			TestCardBuilder tcb = new TestCardBuilder();
			var pack = tcb.BuildBasicPack();

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.StartingPack = pack;
			robot = trb.BuildRobot();
			tgsb.StartingRobots.Add(robot);

			tgsb.Actions.Add(new MovementAction());

			gs = tgsb.BuildGameStateWOTurnBegan();
		}

        [Test]
        public void TurnBeganTest()
        {
			gs.TurnBegan();

			notMock.Verify(n => n.NewTurnBegan(), Times.Once);
			Assert.That(gs.TurnActiveRobots.Count() == gs.GameActiveRobots.Count());
			Assert.That(gs.TurnDeactivatedRobots.Count() == 0);
        }

		[Test]
		public void TurnOverTest()
		{
			gs.TurnOver();

			notMock.Verify(n => n.TurnEnded(), Times.Once);
			Assert.That(gs.TurnActiveRobots.Count() == 0);
			Assert.That(gs.TurnDeactivatedRobots.Count() == 0);
		}

		[Test]
		public void PuttingRobotToSleepTest1()
		{
			var activeRobotsCount = gs.GameActiveRobots.Count();
			gs.PutRobotToSleepBeforeTurn(robot);

			Assert.That(gs.DormantRobots.Contains(robot));

			Assert.That(gs.GameActiveRobots.Count() == activeRobotsCount - 1);

			notMock.Verify(n => n.RobotLeftGame(robot.Name));			
		}

		[Test]
		public void PuttingRobotToSleepTest2()
		{
			gs.PutRobotToSleepBeforeTurn(robot);
			Assert.Throws<RunningGameException>(() => gs.PutRobotToSleepBeforeTurn(robot));
		}

		[Test]
		public void PuttingRobotToSleepTest3()
		{
			Assert.Throws<RunningGameException>(() => gs.PutRobotToSleepDuringTurn(robot));
		}

		[Test]
		public void PuttingRobotToSleepTest4()
		{
			gs.TurnBegan();

			var program = robot.DrawCards(5);

			robot.SetNewProgram(program);

			var c1 = gs.GameActiveRobots.Count();
			var c2 = gs.TurnActiveRobots.Count() + gs.TurnDeactivatedRobots.Count();
			var c3 = gs.DormantRobots.Count();

			gs.PutRobotToSleepDuringTurn(robot);

			Assert.That(gs.GameActiveRobots.Count() == c1 - 1);
			Assert.That(gs.TurnDeactivatedRobots.Count() + gs.TurnDeactivatedRobots.Count() == c2 - 1);
			Assert.That(gs.DormantRobots.Count() == c3 + 1);

			Assert.That(gs.DormantRobots.Contains(robot));

			notMock.Verify(n => n.RobotLeftGame(robot.Name));
		}

		[Test]
		public void PuttingRobotToSleepTest5()
		{
			gs.TurnBegan();

			var program = robot.DrawCards(5);

			robot.SetNewProgram(program);

			gs.PutRobotToSleepDuringTurn(robot);
			Assert.Throws<RunningGameException>(() => gs.PutRobotToSleepDuringTurn(robot));
		}

		[Test]
		public void AddingNewRobotTest1()
		{
			Assert.Throws<RunningGameException>(() => gs.AddNewRobot(robot));
		}

		[Test]
		public void AddingNewRobotTest2()
		{
			robot.SetNewPosition(new MapCoordinates(5, 5));
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASHITO";
			trb.Position = new MapCoordinates(4, 4);
			trb.EnergyAmount = 4;

			var r = trb.BuildRobot();

			var c1 = gs.GameActiveRobots.Count();

			gs.AddNewRobot(r);

			Assert.That(gs.GameActiveRobots.Count() == c1 + 1);

			notMock.Verify(n => n.RobotJoinedGame(r.Name, r.FacingDirection, 
				r.Position, r.EnergyAmount, 0,	new string[0]));
		}

		[Test]
		public void AddingNewRobotTest3()
		{
			robot.SetNewPosition(new MapCoordinates(5, 5));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASHITO1";
			trb.Position = new MapCoordinates(5, 5);
			var r1 = trb.BuildRobot();
			
			Assert.Throws<RunningGameException>(() => gs.AddNewRobot(r1));
		}

		[Test]
		public void TileOccupationTest1()
		{
			robot.SetNewPosition(new MapCoordinates(4, 4));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.Name = "SMASH1";
			var r1 = trb.BuildRobot();

			gs.AddNewRobot(r1);

			Assert.That(gs.IsTileOccupied(new MapCoordinates(4, 4), out Robot occ) && occ == robot);

			Assert.That(gs.IsTileOccupied(new MapCoordinates(4, 5)) == false);

			Assert.That(gs.IsTileOccupied(new MapCoordinates(5, 5), out Robot occ2) && occ2 == r1);
		}

		[Test]
		public void RestoringDormantRobotTest1()
		{
			Assert.Throws<RunningGameException>(() => gs.TryRestoreDormantRobot(robot));
		}

		[Test]
		public void RestoringDormantRobotTest2()
		{
			robot.SetNewPosition(new MapCoordinates(5, 5));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.Name = "SMASHITO";
			var r1 = trb.BuildRobot();

			gs.PutRobotToSleepBeforeTurn(robot);

			gs.AddNewRobot(r1);

			Assert.That(gs.TryRestoreDormantRobot(robot) == false);
		}

		[Test]
		public void RestoringDormantRobotTest3()
		{
			robot.SetNewPosition(new MapCoordinates(4, 4));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.Name = "SMASHITO";
			var r1 = trb.BuildRobot();

			gs.PutRobotToSleepBeforeTurn(robot);

			gs.AddNewRobot(r1);

			var c1 = gs.DormantRobots.Count();
			var c2 = gs.GameActiveRobots.Count();

			Assert.That(gs.TryRestoreDormantRobot(robot));
			Assert.That(gs.DormantRobots.Count() == c1 - 1);
			Assert.That(gs.GameActiveRobots.Count() == c2 + 1);

			notMock.Verify(n => n.RobotJoinedGame(robot.Name, robot.FacingDirection, robot.Position,
				robot.EnergyAmount, 0, new string[0]));
		}

		[Test]
		public void RobotFinishedTest1()
		{
			//robot can finish only with turn started
			Assert.Throws<RunningGameException>(() => gs.RobotFinished(robot));
		}

		[Test]
		public void RobotFinishedTest2()
		{
			//robot has to be part of the game in order to finish
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASHITO";
			trb.Position = new MapCoordinates(5, 5);
			var r1 = trb.BuildRobot();

			Assert.Throws<RunningGameException>(() => gs.RobotFinished(r1));
		}

		[Test]
		public void RobotFinishedTest3()
		{
			gs.TurnBegan();

			var c1 = gs.GameActiveRobots.Count();
			var c2 = gs.TurnActiveRobots.Count() + gs.TurnDeactivatedRobots.Count();
			var c3 = gs.FinishedRobots.Count();

			gs.RobotFinished(robot);

			Assert.That(gs.GameActiveRobots.Count() == c1 - 1);
			Assert.That(gs.TurnActiveRobots.Count() + gs.TurnDeactivatedRobots.Count() == c2 - 1);
			Assert.That(gs.FinishedRobots.Count() == c3 + 1);

			notMock.Verify(n => n.RobotFinishedGame(robot.Name));
		}

		[Test]
		public void AddingNewRobotTest4()
		{
			robot.SetNewPosition(new MapCoordinates(0, 0));

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASHITO";
			trb.Position = new MapCoordinates(5, 5);
			var r1 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASHITO";
			trb.Position = new MapCoordinates(4, 4);
			var r2 = trb.BuildRobot();

			gs.AddNewRobot(r1);

			gs.TurnBegan();

			gs.RobotFinished(r1);

			gs.TurnOver();

			Assert.Throws<RunningGameException>(() => gs.AddNewRobot(r2));
		}

		[Test]
		public void DeactivatingRobotForTurnTest1()
		{
			//cannot deactivate robot with the turn not started
			Assert.Throws<RunningGameException>(() => gs.DeactivateRobotForThisTurn(robot));
		}

		[Test]
		public void DeactivatingRobotForTurnTest2()
		{
			gs.TurnBegan();

			var c1 = gs.GameActiveRobots.Count();
			var c2 = gs.TurnActiveRobots.Count();
			var c3 = gs.TurnDeactivatedRobots.Count();

			gs.DeactivateRobotForThisTurn(robot);

			Assert.That(gs.GameActiveRobots.Count() == c1);
			Assert.That(gs.TurnActiveRobots.Count() == c2 - 1);
			Assert.That(gs.TurnDeactivatedRobots.Count() == c3 + 1);
		}

		[Test]
		public void DeactivatingRobotForTurnTest3()
		{
			gs.TurnBegan();

			var c1 = gs.GameActiveRobots.Count();
			var c2 = gs.TurnActiveRobots.Count();
			var c3 = gs.TurnDeactivatedRobots.Count();

			gs.DeactivateRobotForThisTurn(robot);
			//second deactivation is ok, robot can reboot for the second time
			gs.DeactivateRobotForThisTurn(robot);

			Assert.That(gs.GameActiveRobots.Count() == c1);
			Assert.That(gs.TurnActiveRobots.Count() == c2 - 1);
			Assert.That(gs.TurnDeactivatedRobots.Count() == c3 + 1);
		}

		[Test]
		public void GettingActionTest1()
		{
			Assert.DoesNotThrow(() => gs.GetAction<MovementAction>());
			Assert.Throws<RunningGameException>(() => gs.GetAction<RebootAction>());
		}

		[Test]
		public void GettingRobotsByDefaultPriorityTest1()
		{
			//in order to test this method, we need to rebuild the map and place
			//the antenna tile
			//but first, test, that it throws if there is no antenna

			Assert.Throws<RunningGameException>(() => gs.GetTurnActiveRobotsByDefaultPriority());
		}

		[Test]
		public void GettingRobotsByDefaultPriorityTest2()
		{
			TestGameStateBuilder tgsb = new TestGameStateBuilder();

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWOTurnBegan();

			//last priority
			robot.SetNewPosition(new MapCoordinates(4, 3));

			//first priority
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASHITO";
			trb.Position = new MapCoordinates(4, 7);
			var r1 = trb.BuildRobot();

			//second priority
			trb = new TestRobotBuilder();
			trb.Name = "SMASHITO2";
			trb.Position = new MapCoordinates(7, 6);
			var r2 = trb.BuildRobot();

			gs.AddNewRobot(robot);
			gs.AddNewRobot(r1);
			gs.AddNewRobot(r2);

			gs.TurnBegan();

			//notice that distance to antenna is, for each robot, the same

			var priorityList = gs.GetTurnActiveRobotsByDefaultPriority();

			Assert.That(priorityList[0] == r1);
			Assert.That(priorityList[1] == r2);
			Assert.That(priorityList[2] == robot);
		}

		[Test]
		public void GettingRobotsByDefaultPriorityTest3()
		{
			//same as previous test, but with different rotation on antenna
			TestGameStateBuilder tgsb = new TestGameStateBuilder();

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new PriorityAntennaTile(new Direction(DirectionType.Down));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWOTurnBegan();

			//first priority
			robot.SetNewPosition(new MapCoordinates(4, 3));

			//second priority
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASHITO";
			trb.Position = new MapCoordinates(4, 7);
			var r1 = trb.BuildRobot();

			//last priority
			trb = new TestRobotBuilder();
			trb.Name = "SMASHITO2";
			trb.Position = new MapCoordinates(7, 6);
			var r2 = trb.BuildRobot();

			gs.AddNewRobot(robot);
			gs.AddNewRobot(r1);
			gs.AddNewRobot(r2);

			gs.TurnBegan();

			//notice that distance to antenna is, for each robot, the same

			var priorityList = gs.GetTurnActiveRobotsByDefaultPriority();

			Assert.That(priorityList[0] == robot);
			Assert.That(priorityList[1] == r1);
			Assert.That(priorityList[2] == r2);

		}

		[Test]
		public void GettingRobotsByDefaultPriorityTest4()
		{
			//this test has different setting, two robots are closer to the antenna than the other
			TestGameStateBuilder tgsb = new TestGameStateBuilder();

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new PriorityAntennaTile(new Direction(DirectionType.Down));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWOTurnBegan();

			//last priority
			robot.SetNewPosition(new MapCoordinates(4, 3));

			//first priority
			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASHITO";
			trb.Position = new MapCoordinates(4, 6);
			var r1 = trb.BuildRobot();

			//second priority
			trb = new TestRobotBuilder();
			trb.Name = "SMASHITO2";
			trb.Position = new MapCoordinates(6, 6);
			var r2 = trb.BuildRobot();

			gs.AddNewRobot(robot);
			gs.AddNewRobot(r1);
			gs.AddNewRobot(r2);

			gs.TurnBegan();

			//notice that distance to antenna is, for each robot, the same

			var priorityList = gs.GetTurnActiveRobotsByDefaultPriority();

			Assert.That(priorityList[0] == r1);
			Assert.That(priorityList[1] == r2);
			Assert.That(priorityList[2] == robot);
		}

		[Test]
		public void DrawUpgradeCardsTest()
		{
			TestCardBuilder tcb = new TestCardBuilder();
			var u1 = tcb.BuildUCard("u1");
			var u2 = tcb.BuildUCard("u2");
			var u3 = tcb.BuildUCard("u3");
			var u4 = tcb.BuildUCard("u4");
			var uCards = new UpgradeCard[] { u1, u2 };

			TestGameStateBuilder tgsb = new TestGameStateBuilder();
			tgsb.UpgradeCards = new Pack<UpgradeCard>(uCards);

			tgsb.StartingRobots.Add(robot);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var drawn = gs.DrawUpgradeCards(50);
			Assert.That(drawn.Length == 2 && drawn.Contains(u1) && drawn.Contains(u2));

			gs.PlaceUpgradeCardToPack(u3);
			gs.PlaceUpgradeCardToPack(u4);

			drawn = gs.DrawUpgradeCards(50);

			Assert.That(drawn.Length == 2 && drawn.Contains(u3) && drawn.Contains(u4));
		}

		//TODO - add tests that are related to async cards
	}
}
