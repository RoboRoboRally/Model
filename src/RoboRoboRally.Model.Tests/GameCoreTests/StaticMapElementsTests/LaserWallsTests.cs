﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class LaserWallsTests
    {
		TestGameStateBuilder tgsb;

		ActivationPhase ap;

		Robot robot;

		Mock<IGameEventNotifier> notMock;

		[SetUp]
		public void TestSetup()
		{
			tgsb = new TestGameStateBuilder();

			notMock = tgsb.NotMock;

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Position = new MapCoordinates(5, 5);
			trb.Name = "SMASH1";
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			ap = new ActivationPhase();
		}

		private Damage GetDummyDamage(int count)
		{
			TestCardBuilder tcb = new TestCardBuilder();
			List<DamageCard> dmgCards = new List<DamageCard>();

			for (int i = 0; i < count; ++i)
				dmgCards.Add(tcb.BuildDDummy());

			return new Damage() { DamageCards = dmgCards.ToArray() };
		}

		[Test]
		public void LaserWallTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new LaserWall(GetDummyDamage(3), 3, tmb.Tiles[4, 6], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateLasers(gs);

			notMock.Verify(n => n.WallLasersActivated(), Times.Once);

			notMock.Verify(n => n.RobotSufferedDamage(It.IsAny<string>(), It.IsAny<Damage>()), Times.Never);
		}

		[Test]
		public void LaserWallTest2()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new LaserWall(GetDummyDamage(3), 3, tmb.Tiles[5, 6], new Direction(DirectionType.Left)));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 6], new Direction(DirectionType.Right)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateLasers(gs);

			notMock.Verify(n => n.WallLasersActivated(), Times.Once);

			notMock.Verify(n => n.RobotSufferedDamage(It.IsAny<string>(), It.IsAny<Damage>()), Times.Never);
		}

		[Test]
		public void LaserWallTest3()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new LaserWall(GetDummyDamage(3), 3, tmb.Tiles[5, 6], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateLasers(gs);

			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Once);

			var dDummy = new TestCardBuilder().BuildDDummy();

			Assert.That(robot.DrawAllCards().Count(c => c.Name == dDummy.Name) == 3);
		}

		[Test]
		public void LaserWallTest4()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new LaserWall(GetDummyDamage(3), 3, tmb.Tiles[5, 6], new Direction(DirectionType.Left)));
			tmb.Walls.Add(new LaserWall(GetDummyDamage(2), 2, tmb.Tiles[4, 5], new Direction(DirectionType.Down)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);


			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateLasers(gs);

			notMock.Verify(n => n.WallLasersActivated(), Times.Once);

			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Exactly(2));

			var dDummy = new TestCardBuilder().BuildDDummy();

			Assert.That(robot.DrawAllCards().Count(c => c.Name == dDummy.Name) == 5);

		}

		[Test]
		public void LaserWallTest5()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new LaserWall(GetDummyDamage(3), 3, tmb.Tiles[5, 6], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "XYZ";
			trb.Position = new MapCoordinates(5, 4);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateLasers(gs);

			//only first robot should get hit
			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Exactly(1));
			notMock.Verify(n => n.RobotSufferedDamage(r2.Name, It.IsAny<Damage>()), Times.Never);
		}

		[Test]
		public void LaserWallTest6()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 6] = new PriorityAntennaTile(new Direction(DirectionType.Up));
			tmb.Walls.Add(new LaserWall(GetDummyDamage(3), 3, tmb.Tiles[5, 6], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateLasers(gs);

			//lasers can not go through antenna tile

			notMock.Verify(n => n.RobotSufferedDamage(robot.Name, It.IsAny<Damage>()), Times.Never);
		}
	}
}
