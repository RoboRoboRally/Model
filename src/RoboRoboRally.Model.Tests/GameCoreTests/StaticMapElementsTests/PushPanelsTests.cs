﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class PushPanelsTests
    {
		TestGameStateBuilder tgsb;

		ActivationPhase ap;

		Robot robot;

		Mock<IGameEventNotifier> notMock;

		[SetUp]
		public void TestSetup()
		{
			tgsb = new TestGameStateBuilder();

			tgsb.Actions.Add(new MovementAction());
			tgsb.Actions.Add(new PushAction());

			notMock = tgsb.NotMock;

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			ap = new ActivationPhase();
		}

		[Test]
		public void PushPanelBasicTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 4);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			var r2OldPos = r2.Position;
			var r2OldDir = r2.FacingDirection;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.PushPanelsActivated());

			notMock.Verify(n => n.RobotGotPushed(robot.Name, r1OldPos, robot.Position));

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(robot.FacingDirection == r1OldDir);

			Assert.That(r2.Position == r2OldPos);
			Assert.That(r2.FacingDirection == r2OldDir);
		}

		[Test]
		public void PushPanelBasicTest2()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.DeactivateRobotForThisTurn(robot);

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.PushPanelsActivated());

			notMock.Verify(n => n.RobotGotPushed(robot.Name, r1OldPos, robot.Position));

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(robot.FacingDirection == r1OldDir);
		}

		[Test]
		public void PushPanelBasicTest3()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 2 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.DeactivateRobotForThisTurn(robot);

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.PushPanelsActivated());

			Assert.That(robot.Position == r1OldPos);
			Assert.That(robot.FacingDirection == r1OldDir);
		}

		[Test]
		public void PushPanelWithOccupyingRobotTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.RobotGotPushed(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(5, 6)));
			notMock.Verify(n => n.RobotGotPushed(r2.Name, new MapCoordinates(5, 6), new MapCoordinates(5, 7)));

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(r2.Position == r2OldPos + DirectionType.Right);
		}

		[Test]
		public void PushPanelWithWallTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 5], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.RobotGotPushed(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);

			Assert.That(robot.Position == r1OldPos);
		}

		[Test]
		public void PushPanelWithAntennaTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Tiles[5, 6] = new PriorityAntennaTile(new Direction(DirectionType.Right));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.RobotGotPushed(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);

			Assert.That(robot.Position == r1OldPos);
		}

		[TestCase (DirectionType.Down)]
		[TestCase (DirectionType.Left)]
		public void MultiplePushPanelsOnOneTileTest(DirectionType dir)
		{
			//in this case, nothing should happen (situation 6 basically)
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(dir)));

			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.RobotGotPushed(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);

			Assert.That(robot.Position == r1OldPos);
		}

		[Test]
		public void CollidingPushPanelsTest()
		{
			//this is situation 1
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[6, 6], new Direction(DirectionType.Up)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(6, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivatePushPanels(gs, 1);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos);	
		}

		[Test]
		public void CollidingPushPanelsWithWallTest()
		{
			//this is situation 2
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[6, 6], new Direction(DirectionType.Up)));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[6, 6], new Direction(DirectionType.Down)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(6, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.RobotGotPushed(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(5, 6)));

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(r2.Position == r2OldPos);
		}

		[Test]
		public void MultiplePushesTest()
		{
			//this is situation 3
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 6], new Direction(DirectionType.Up)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.RobotGotPushed(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(5, 6)));
			notMock.Verify(n => n.RobotGotPushed(r2.Name, new MapCoordinates(5, 6), new MapCoordinates(4, 6)));

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(r2.Position == r2OldPos + DirectionType.Up);

		}

		[Test]
		public void CollidingPushesTest()
		{
			//this is situation 7
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 6], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivatePushPanels(gs, 1);

			notMock.Verify(n => n.RobotGotPushed(It.IsAny<string>(), It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos);
		}

		[Test]
		public void PushPanelsAlmostCycleTest()
		{
			//this is situation 5
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 6], new Direction(DirectionType.Down)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[6, 6], new Direction(DirectionType.Left)));
			//tmb.Walls.Add(new PushPanelWall(new List<int> { 1 }, tmb.Tiles[6, 5], new Direction(DirectionType.Up)));

			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH3";
			trb.Position = new MapCoordinates(6, 6);
			var r3 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH4";
			trb.Position = new MapCoordinates(6, 5);
			var r4 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);
			tgsb.StartingRobots.Add(r3);
			tgsb.StartingRobots.Add(r4);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;
			var r3OldPos = r3.Position;
			var r4OldPos = r4.Position;

			ap.ActivatePushPanels(gs, 1);

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(r2.Position == r2OldPos + DirectionType.Down);
			Assert.That(r3.Position == r3OldPos + DirectionType.Left);
			Assert.That(r4.Position == r4OldPos + DirectionType.Left);
		}

		[Test]
		public void PushPanelsCycleTest()
		{
			//this is situation 4
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 5], new Direction(DirectionType.Right)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[5, 6], new Direction(DirectionType.Down)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[6, 6], new Direction(DirectionType.Left)));
			tmb.Walls.Add(new PushPanelWall(new int[1] { 1 }, tmb.Tiles[6, 5], new Direction(DirectionType.Up)));

			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH3";
			trb.Position = new MapCoordinates(6, 6);
			var r3 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH4";
			trb.Position = new MapCoordinates(6, 5);
			var r4 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);
			tgsb.StartingRobots.Add(r3);
			tgsb.StartingRobots.Add(r4);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			Assert.Throws<RunningGameException>(() => ap.ActivatePushPanels(gs, 1));
		}
		
	}
}
