﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
	[TestFixture]
	public class BeltsTests
	{
		TestGameStateBuilder tgsb;

		ActivationPhase ap;

		Robot robot;

		Mock<IGameEventNotifier> notMock;

		//NOTE: blue belts are implemented as two steps of green belt
		//and we do not have to test them (apart from some basic tests)
		//... if we test green belts, blue belts should work too

		[SetUp]
		public void TestSetup()
		{
			tgsb = new TestGameStateBuilder();

			tgsb.Actions.Add(new MovementAction());
			tgsb.Actions.Add(new PushAction());

			notMock = tgsb.NotMock;

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			ap = new ActivationPhase();
		}

		[Test]
		public void GreenBeltBasicTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 4] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Right));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(2, 2);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			var r2OldPos = r2.Position;
			var r2OldDir = r2.FacingDirection;

			ap.ActivateGreenBelts(gs);

			notMock.Verify(n => n.GreenBeltsActivated());

			notMock.Verify(n => n.RobotMoved(robot.Name, r1OldPos, robot.Position));

			Assert.That(r2.Position == r2OldPos);
			Assert.That(r2.FacingDirection == r2OldDir);

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(robot.FacingDirection == r1OldDir);
		}

		[Test]
		public void GreenBeltBasicTest2()
		{
			//same as previous, but this time make the robot inactive
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Right));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			gs.DeactivateRobotForThisTurn(robot);

			ap.ActivateGreenBelts(gs);

			notMock.Verify(n => n.GreenBeltsActivated());

			notMock.Verify(n => n.RobotMoved(robot.Name, r1OldPos, robot.Position));

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(robot.FacingDirection == r1OldDir);
		}

		[Test]
		public void GreenBeltTestWithEmptyTile()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new EmptyTile();
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
		}

		[Test]
		public void GreenBeltWithRotationTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Down));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(robot.FacingDirection == r1OldDir.Rotate(RotationType.Right));

			notMock.Verify(n => n.RobotRotated(robot.Name, RotationType.Right));
			notMock.Verify(n => n.RobotMoved(robot.Name, r1OldPos, robot.Position));
		}

		[Test]
		public void GreenBeltWithRotationTest2()
		{
			//same as previous but different rotation
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(robot.FacingDirection == r1OldDir.Rotate(RotationType.Left));

			notMock.Verify(n => n.RobotRotated(robot.Name, RotationType.Left));
		}

		[Test]
		public void GreenBeltWithOppositeDirectionTest()
		{
			//we've decided that in this situation, robot is not rotated
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Left));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(robot.FacingDirection == r1OldDir);

			notMock.Verify(n => n.RobotRotated(robot.Name, It.IsAny<RotationType>()), Times.Never);
		}

		[Test]
		public void GreenBeltWithWallTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 5], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r1OldDir = robot.FacingDirection;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(robot.FacingDirection == r1OldDir);

			notMock.Verify(n => n.RobotMoved(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
		}

		[Test]
		public void GreenBeltWithBlockingRobotTest()
		{
			//(situation 1)
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new EmptyTile();
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos);

			notMock.Verify(n => n.RobotMoved(robot.Name, It.IsAny<MapCoordinates>(), It.IsAny<MapCoordinates>()), Times.Never);
		}

		[Test]
		public void GreenBeltWithTwoAdjacentRobots()
		{
			//(situation 2)
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Right));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos + DirectionType.Right);
			Assert.That(r2.Position == r2OldPos + DirectionType.Right);

			notMock.Verify(n => n.RobotMoved(r2.Name, new MapCoordinates(5, 6), new MapCoordinates(5, 7)));
			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(5, 6)));
		}

		[Test]
		public void GreenBeltWithTwoAdjacentRobotsAndBlockingOneTest()
		{
			//(situation 5)
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 7] = new EmptyTile();
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH3";
			trb.Position = new MapCoordinates(5, 7);
			var r3 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);
			tgsb.StartingRobots.Add(r3);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;
			var r3OldPos = r3.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos);
			Assert.That(r3.Position == r3OldPos);
		}


		[Test]
		public void GreenBeltWithTwoAdjacentRobotsAndWallTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 6], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos);
		}

		[Test]
		public void GreenBeltWithTwoAdjacentRobotsAndWallTest2()
		{
			//situation 3
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Walls.Add(new EmptyWall(tmb.Tiles[5, 5], new Direction(DirectionType.Left)));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos + DirectionType.Right);
		}

		[Test]
		public void GreenBeltWithTwoOppositeRobotsTest()
		{
			//(situation 6)
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Left));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos);
		}

		[Test]
		public void GreenBeltWithConflictingRobotsTest()
		{
			//basically situation 4
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new EmptyTile();
			tmb.Tiles[5, 7] = new GreenBeltTile(new Direction(DirectionType.Left));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 7);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var r1OldPos = robot.Position;
			var r2OldPos = r2.Position;

			ap.ActivateGreenBelts(gs);

			Assert.That(robot.Position == r1OldPos);
			Assert.That(r2.Position == r2OldPos);
		}

		[Test]
		public void BeltCycleTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new GreenBeltTile(new Direction(DirectionType.Right));
			tmb.Tiles[5, 6] = new GreenBeltTile(new Direction(DirectionType.Down));
			tmb.Tiles[6, 6] = new GreenBeltTile(new Direction(DirectionType.Left));
			tmb.Tiles[6, 5] = new GreenBeltTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(5, 6);
			var r2 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH3";
			trb.Position = new MapCoordinates(6, 6);
			var r3 = trb.BuildRobot();

			trb = new TestRobotBuilder();
			trb.Name = "SMASH4";
			trb.Position = new MapCoordinates(6, 5);
			var r4 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);
			tgsb.StartingRobots.Add(r3);
			tgsb.StartingRobots.Add(r4);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			Assert.Throws<RunningGameException>(() => ap.ActivateGreenBelts(gs));
		}



		[Test]
        public void BlueBeltsSimpleTest1()
        {
			robot.SetNewPosition(5, 5);

			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new BlueBeltTile(new Direction(DirectionType.Up));
			tmb.Tiles[4, 5] = new BlueBeltTile(new Direction(DirectionType.Up));
			tgsb.Map = tmb.BuildMap();

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldPos = robot.Position;
			var oldDir = robot.FacingDirection;

			ap.ActivateBlueBelts(gs);

			notMock.Verify(n => n.BlueBeltsActivated());

			var expPos = oldPos + DirectionType.Up;
			expPos += DirectionType.Up;

			Assert.That(robot.Position == expPos);
			Assert.That(robot.FacingDirection == oldDir);

			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(5, 5), new MapCoordinates(4, 5)));
			notMock.Verify(n => n.RobotMoved(robot.Name, new MapCoordinates(4, 5), new MapCoordinates(3, 5)));
        }
    }
}
