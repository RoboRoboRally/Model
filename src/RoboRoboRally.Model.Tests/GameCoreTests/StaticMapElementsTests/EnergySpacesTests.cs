﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class EnergySpacesTests
    {
		TestGameStateBuilder tgsb;

		ActivationPhase ap;

		Robot robot;

		Mock<IGameEventNotifier> notMock;

		[SetUp]
		public void TestSetup()
		{
			tgsb = new TestGameStateBuilder();

			notMock = tgsb.NotMock;

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			ap = new ActivationPhase();
		}

		[Test]
		public void EnergySpacesTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new EnergyTile(2);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldE = robot.EnergyAmount;

			ap.ActivateEnergySpaces(gs, 1);

			notMock.Verify(n => n.EnergySpacesActivated(), Times.Never);

			Assert.That(robot.EnergyAmount == oldE);

			notMock.Verify(n => n.RobotEnergyChanged(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
		}

		[Test]
		public void EnergySpacesTest2()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new EnergyTile(2);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldE = robot.EnergyAmount;

			ap.ActivateEnergySpaces(gs, gs.RegisterCount);

			notMock.Verify(n => n.EnergySpacesActivated(), Times.Once);

			Assert.That(robot.EnergyAmount == oldE + 2);

			notMock.Verify(n => n.RobotEnergyChanged(robot.Name, robot.EnergyAmount, oldE));
		}

		[Test]
		public void EnergySpacesTest3()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new EnergyTile(2);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			gs.DeactivateRobotForThisTurn(robot);

			var oldE = robot.EnergyAmount;

			ap.ActivateEnergySpaces(gs, gs.RegisterCount);

			notMock.Verify(n => n.EnergySpacesActivated(), Times.Once);

			Assert.That(robot.EnergyAmount == oldE + 2);
		}

		[Test]
		public void EnergySpacesTest4()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new EnergyTile(2);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(4, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldE = robot.EnergyAmount;

			ap.ActivateEnergySpaces(gs, gs.RegisterCount);

			notMock.Verify(n => n.EnergySpacesActivated(), Times.Once);

			Assert.That(robot.EnergyAmount == oldE);

			notMock.Verify(n => n.RobotEnergyChanged(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
		}

	}
}
