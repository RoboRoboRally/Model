﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class RotatingGearTests
    {
		TestGameStateBuilder tgsb;

		ActivationPhase ap;

		Robot robot;

		Mock<IGameEventNotifier> notMock;


		[SetUp]
		public void TestSetup()
		{
			tgsb = new TestGameStateBuilder();

			notMock = tgsb.NotMock;

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			ap = new ActivationPhase();
		}

		[Test]
		public void RotatingGearsTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new RotatingTile(RotationType.Left);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldR = robot.FacingDirection;
			var oldPos = robot.Position;

			ap.ActivateRotatingGears(gs);

			Assert.That(robot.FacingDirection == oldR.Rotate(RotationType.Left));
			Assert.That(robot.Position == oldPos);

			notMock.Verify(n => n.RotatingGearsActivated(), Times.Once);

			notMock.Verify(n => n.RobotRotated(robot.Name, RotationType.Left));
		}

		[Test]
		public void RotatingGearsTest2()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new RotatingTile(RotationType.Left);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(4, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var oldR = robot.FacingDirection;

			ap.ActivateRotatingGears(gs);

			Assert.That(robot.FacingDirection == oldR);

			notMock.Verify(n => n.RotatingGearsActivated(), Times.Once);
			notMock.Verify(n => n.RobotRotated(It.IsAny<string>(), It.IsAny<RotationType>()), Times.Never);
		}
	}
}
