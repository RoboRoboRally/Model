﻿using Moq;
using NUnit.Framework;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Tests.TestBuilders;
using System.Linq;

namespace RoboRoboRally.Model.Tests.GameCoreTests.ActionTests
{
    [TestFixture]
    public class CheckpointTests
    {
		Robot robot;

		TestGameStateBuilder tgsb;

		ActivationPhase ap;

		Mock<IGameEventNotifier> notMock;
		Mock<IInputHandler> iMock;

		[SetUp]
		public void TestSetup()
		{
			tgsb = new TestGameStateBuilder();
			notMock = tgsb.NotMock;
			iMock = tgsb.IMock;

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH1";
			trb.Position = new MapCoordinates(5, 5);
			robot = trb.BuildRobot();

			tgsb.StartingRobots.Add(robot);

			ap = new ActivationPhase();
		}

		[Test]
		public void CheckpointActivationTest1()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[4, 4] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateCheckpoints(gs);

			notMock.Verify(n => n.CheckpointsActivated());

			notMock.Verify(n => n.RobotVisitedCheckpoint(It.IsAny<string>(), It.IsAny<int>()), Times.Never);

			Assert.That(robot.GetLastVisitedCheckpoint() == 0);
		}

		[Test]
		public void CheckpointActivationTest2()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new CheckpointTile(1);
			tmb.Tiles[6, 6] = new CheckpointTile(2);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateCheckpoints(gs);

			notMock.Verify(n => n.RobotVisitedCheckpoint(robot.Name, 1));

			Assert.That(robot.GetLastVisitedCheckpoint() == 1);
		}

		[Test]
		public void CheckpointActivationTest3()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new CheckpointTile(2);
			tmb.Tiles[6, 6] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			ap.ActivateCheckpoints(gs);

			notMock.Verify(n => n.RobotVisitedCheckpoint(robot.Name, It.IsAny<int>()), Times.Never);

			Assert.That(robot.GetLastVisitedCheckpoint() == 0);
		}

		[Test]
		public void CheckpointActivationTest4()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var fC = gs.FinishedRobots.Count();
			var aC = gs.GameActiveRobots.Count();

			ap.ActivateCheckpoints(gs);

			notMock.Verify(n => n.RobotVisitedCheckpoint(robot.Name, 1));

			notMock.Verify(n => n.RobotFinishedGame(robot.Name));

			Assert.That(gs.GameActiveRobots.Count() == aC - 1);
			Assert.That(gs.FinishedRobots.Count() == fC + 1);
		}

		[Test]
		public void CheckpointActivationWithoutContinuningTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(4, 4);
			var r2 = trb.BuildRobot();

			//we need to setup program, becuase it will be discarded
			TestCardBuilder tcb = new TestCardBuilder();
			r2.SetNewProgram(new ProgrammingCard[5] { tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy(), tcb.BuildDummy() });

			tgsb.StartingRobots.Add(r2);

			iMock.Setup(i => i.ShouldGameContinue()).Returns(false);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var fC = gs.FinishedRobots.Count();
			var gC = gs.GameActiveRobots.Count();

			ap.ActivateCheckpoints(gs);

			notMock.Verify(n => n.RobotLeftGame(r2.Name));

			Assert.That(gs.GameActiveRobots.Count() == 0);
			Assert.That(gs.FinishedRobots.Count() == fC + 1);
		}

		[Test]
		public void CheckpointActivationWithContinuingTest()
		{
			TestMapBuilder tmb = new TestMapBuilder();
			tmb.Tiles[5, 5] = new CheckpointTile(1);
			tgsb.Map = tmb.BuildMap();

			robot.SetNewPosition(5, 5);

			TestRobotBuilder trb = new TestRobotBuilder();
			trb.Name = "SMASH2";
			trb.Position = new MapCoordinates(4, 4);
			var r2 = trb.BuildRobot();

			tgsb.StartingRobots.Add(r2);

			iMock.Setup(i => i.ShouldGameContinue()).Returns(true);

			var gs = tgsb.BuildGameStateWithTurnBegan();

			var fC = gs.FinishedRobots.Count();
			var gC = gs.GameActiveRobots.Count();

			ap.ActivateCheckpoints(gs);

			Assert.That(gs.GameActiveRobots.Count() == gC - 1);
			Assert.That(gs.FinishedRobots.Count() == fC + 1);
		}
		
    }
}
