﻿using NUnit.Framework;
using RoboRoboRally.Model.Util;
using System;

namespace RoboRoboRally.Model.Tests.UtilTests
{
	[TestFixture]
    public class DirectionTests
    {
        [Test]
        public void DirectionRotationTest1()
        {
            Direction dir = new Direction(DirectionType.Down);
            var rotatedDir = dir.Rotate(RotationType.Back);
            Assert.That(rotatedDir.X == 0 && rotatedDir.Y == 1);
        }

        [Test]
        public void DirectionRotationTest2()
        {
            Direction dir = new Direction(DirectionType.Down);
            var rotatedDir = dir.Rotate(RotationType.None);
            Assert.That(rotatedDir.X == 0 && rotatedDir.Y == -1);
        }

        [Test]
        public void DirectionRotationTest3()
        {
            Direction dir = new Direction(DirectionType.Down);
            var rotatedDir = dir.Rotate(RotationType.Left);
            Assert.That(rotatedDir.X == 1 && rotatedDir.Y == 0);
        }

        [Test]
        public void DirectionRotationTest4()
        {
            Direction dir = new Direction(DirectionType.Down);
            var rotatedDir = dir.Rotate(RotationType.Right);
            Assert.That(rotatedDir.X == -1 && rotatedDir.Y == 0);
        }

		[TestCase(1,1)]
		[TestCase(0,2)]
		[TestCase(-1,-2)]
		public void InvalidDirectionArgumentsTest(int x, int y)
		{
			Assert.Throws<ArgumentException>(() => new Direction(x, y));
		}

		[TestCase(0, 1)]
		[TestCase(-1, 0)]
		public void CorrectDirectionArgumentsTest(int x, int y)
		{
			var dir = new Direction(x, y);
		}
    }
}
