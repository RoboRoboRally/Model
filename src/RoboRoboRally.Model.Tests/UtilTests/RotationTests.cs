﻿using NUnit.Framework;
using RoboRoboRally.Model.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoboRoboRally.Model.Tests.UtilTests
{
    [TestFixture]
    public class RotationTests
    {
        [Test]
        public void RotationLeftTest1()
        {
            Rotation rot = new Rotation(RotationType.Left);
            rot.Increase();
            Assert.That(rot.RotationType == RotationType.Back);
        }

        [Test]
        public void RotationLeftTest2()
        {
            Rotation rot = new Rotation(RotationType.Left);
            rot.Decrease();
            Assert.That(rot.RotationType == RotationType.None);
        }

        [Test]
        public void RotationBackTest1()
        {
            Rotation rot = new Rotation(RotationType.Back);
            rot.Increase();
            Assert.That(rot.RotationType == RotationType.Right);
        }

        [Test]
        public void RotationBackTest2()
        {
            Rotation rot = new Rotation(RotationType.Back);
            rot.Decrease();
            Assert.That(rot.RotationType == RotationType.Left);
        }

        [Test]
        public void RotationRightTest1()
        {
            Rotation rot = new Rotation(RotationType.Right);
            rot.Decrease();
            Assert.That(rot.RotationType == RotationType.Back);
        }

        [Test]
        public void RotationRightTest2()
        {
            Rotation rot = new Rotation(RotationType.Right);
            rot.Increase();
            Assert.That(rot.RotationType == RotationType.None);
        }
    }
}
