﻿using NUnit.Framework;
using RoboRoboRally.Model.Textures.CardTextures;

namespace RoboRoboRally.Model.Tests.TextureTests
{
    [TestFixture]
    public class CardTextureLoaderTests
    {
		CardTextureLoader ctl;

		[SetUp]
		public void TestSetup()
		{
			ctl = new CardTextureLoader();
		}

		[Test]
		public void PathTest1()
		{
			var s1 = ctl.GetProgrammingCardTexturePath("Move1", "Red");
			Assert.That(s1 == "GameData/Textures/CardTextures/ProgrammingCards/Red/Move1.jpg");
		}

		[Test]
		public void PathTest2()
		{
			var s1 = ctl.GetProgrammingCardTexturePath("Spam", "Blue");
			Assert.That(s1 == "GameData/Textures/CardTextures/ProgrammingCards/Blue/Spam.jpg");
		}

		[Test]
		public void PathTest3()
		{
			var s = ctl.GetProgrammingCardBackTexturePath();
			Assert.That(s == "GameData/Textures/CardTextures/ProgrammingCards/ProgrammingCardBack.jpg");
		}

		[Test]
		public void PathTest4()
		{
			var s = ctl.GetInvalidCardTexturePath();
			Assert.That(s == "GameData/Textures/CardTextures/InvalidTexture.jpg");
		}

		[Test]
		public void PathTest5()
		{
			var s = ctl.GetUpgradeCardTexturePath("Boink");
			Assert.That(s == "GameData/Textures/CardTextures/UpgradeCards/Boink.jpg");
		}

		[Test]
		public void PathTest6()
		{
			var s = ctl.GetUpgradeCardBackTexturePath();
			Assert.That(s == "GameData/Textures/CardTextures/UpgradeCards/UpgradeCardBack.jpg");
		}

		[Test]
		public void PathTest7()
		{
			var s = ctl.GetChoiceCardTexturePath("Boink", "Up");
			Assert.That(s == "GameData/Textures/CardTextures/UpgradeCards/ChoiceCards/Boink_Up.jpg");
		}
    }
}
