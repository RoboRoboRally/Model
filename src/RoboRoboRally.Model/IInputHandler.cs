﻿using RoboRoboRally.Model.CardDB.Cards;
using System.Collections.Generic;

namespace RoboRoboRally.Model
{
	/// <summary>
	/// <para>
	/// From the perspective of model, this is one of the interfaces that enables
	/// it to communicate with server (and therefore clients) and it is concerned with 
	/// input from players that has to be passed to model (like new programs for robots).
	/// There is one more interface for this direction of communication and 
	/// that is interface <see cref="IGameEventNotifier"/>. However, this other interface
	/// is concerned only with notifications to server (and so, potentially to other clients).
	/// </para>
	/// 
	/// <para>
	/// In order to see main class for communication from server to model, see
	/// class <see cref="Game"/>.
	/// </para>
	/// 
	/// <para>
	/// There is some input required from players in the following main situations.
	/// </para>
	/// 
	/// <para>
	/// 1. Programming phase - players need to choose upgrades
	/// 2. Upgrades - their purchase as well as playing them
	/// 3. What should happen if someone finishes the game
	/// 4. Some special cases like using special programming card
	/// </para>
	/// </summary>
	public interface IInputHandler
    {
		/// <summary>
		/// This method is used in Programming phase in order to retrieve programs 
		/// from players for their robots for next turn.
		/// </summary>
		/// <param name="robotProgramOffers">Program offer - one for each active player,
		/// see <see cref="ProgramOffer"/> for a more detailed description.</param>
		/// <returns>Programs - one for each active player, see <see cref="ProgramChoice"/> for
		/// a more detailed description.</returns>
		ProgramChoice[] GetPrograms(ProgramOffer[] robotProgramOffers);

		/// <summary>
		/// This method is used e.g. while playing special programming cards
		/// like Sandbox routine - player has to choose from one card that will
		/// be played instead of the actual sandbox routine card.
		/// </summary>
		/// <param name="robotName">player identifier</param>
		/// <param name="cardsToChoose">cards to choose from</param>
		/// <returns>chosen card</returns>
		ProgrammingCard ChooseCard(string robotName, ProgrammingCard[] cardsToChoose);

		/// <summary>
		/// This method is used when one of the robots finishes game and there are still
		/// some players left that are active
		/// </summary>
		/// <returns>Bool value whether or not should the game continue.</returns>
		bool ShouldGameContinue();

		/// <summary>
		/// Function for buying upgrades during upgrade phase.
		/// </summary>
		/// <param name="offer">Purchase offer, see <see cref="PurchaseOffer"/>
		/// for a more detailed description.</param>
		/// <returns>Response about the purchase, see <see cref="PurchaseResponse"/>
		/// for more details.</returns>
		PurchaseResponse OfferUpgradeCardPurchase(PurchaseOffer offer);

		/// <summary>
		/// This method asks player if he wants to use one 
		/// of his upgrade cards that does not have any choices (only yes vs no).
		/// See <see cref="UpgradeCard"/> for a more detailed description about
		/// implementation of upgrade cards.
		/// </summary>
		/// <param name="robotName">player identifier</param>
		/// <param name="upgradeCardName">upgrade card identifier</param>
		/// <returns>Whether or not player wants to use the card, see <see cref="UpgradeCardUseInfo"/>
		/// for more details.</returns>
		UpgradeCardUseInfo OfferChoicelessUpgradeCard(string robotName, string upgradeCardName);

		/// <summary>
		/// This method asks player if he wants to use one 
		/// of his upgrade cards that does have some choices (not only yes vs no).
		/// See <see cref="UpgradeCard"/> for a more detailed description about
		/// implementation of upgrade cards.
		/// </summary>
		/// <param name="robotName">player identifier</param>
		/// <param name="upgradeCardName">upgrade card identifier</param>
		/// <param name="choices">choices of the upgrade card that is being offered</param>
		/// <returns>Whether or not player wants to use the offered card as well 
		/// as info about the taken choice (if he wants to use that card). See
		/// <see cref="ChoiceUpgradeCardUseInfo"/>.</returns>
		ChoiceUpgradeCardUseInfo OfferChoiceUpgradeCard(string robotName, string upgradeCardName, UpgradeCardChoice[] choices);

		/// <summary>
		/// This method enables player to use one of his
		/// upgrade cards with choices for a longer period of time,
		/// like over whole activation phase.
		/// See <see cref="UpgradeCard"/> for a more detailed description about
		/// implementation of upgrade cards.
		/// </summary>
		/// <param name="robotName">player identifier</param>
		/// <param name="upgradeCardName">upgrade card identifier</param>
		/// <param name="choices">choices of the upgrade card that is being offered</param>
		void OfferAsyncChoiceUpgradeCard(string robotName, string upgradeCardName, UpgradeCardChoice[] choices);

		/// <summary>
		/// This method enables player to use one of his
		/// upgrade cards without choices (only yes vs no response)
		/// for a longer period of time, like over whole activation phase.
		/// See <see cref="UpgradeCard"/> for a more detailed description about
		/// implementation of upgrade cards.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="upgradeCardName">upgrade card identifier</param>
		void OfferAsyncChoicelessUpgradeCard(string robotName, string upgradeCardName);

		/// <summary>
		/// This method cancels offer to use some upgrade cards
		/// (this concerns only ugprade cards that were offered
		/// to play for longer period of time). In this method
		/// we do not need to differentiate between choiceless
		/// and choice upgrade cards.
		/// See <see cref="UpgradeCard"/> for a more detailed description about
		/// implementation of upgrade cards.
		/// </summary>
		/// <param name="robotName">player identifier</param>
		/// <param name="upgradeCardName">upgrade card identifier</param>
		void EndAsyncUpgradeCardOffer(string robotName, string upgradeCardName);		
    }

	/// <summary>
	/// This struct is used during programming phase - it represents
	/// player's response to model's call for programming robots.
	/// </summary>
	public struct ProgramChoice
	{
		/// <summary>
		/// Chosen cards - this is the program for the next turn
		/// </summary>
		public ProgrammingCard[] ChosenCards { get; set; }

		/// <summary>
		/// player identifier
		/// </summary>
		public string RobotName { get; set; }

		/// <summary>
		/// <para>
		/// This flag indicates that the program is invalid - this
		/// can happen if e.g. player fails to program robot in time.
		/// In that case, model has to take some action to program this
		/// player's robot somehow.
		/// </para>
		/// 
		/// <para>
		/// Value true repesents invalid program. False value
		/// represents valid program.
		/// </para>
		/// </summary>
		public bool EmptyProgram { get; set; }
	}

	/// <summary>
	/// This struct is used during programming phase - it represents
	/// cards from which player can program it's robot for next turn.
	/// </summary>
	public struct ProgramOffer
	{
		/// <summary>
		/// Cards from which player picks program for the robot.
		/// </summary>
		public ProgrammingCard[] CardsToChoose { get; set; }

		/// <summary>
		/// player identifier
		/// </summary>
		public string RobotName { get; set; }

		/// <summary>
		/// This field states how many cards should the player pick
		/// from the offered ones.
		/// </summary>
		public int Amount { get; set; }
	}

	/// <summary>
	/// This struct represents player's response to the model's
	/// call for buying upgrades.
	/// </summary>
	public struct PurchaseResponse
	{
		/// <summary>
		/// Bought card. If player does not want to buy
		/// any upgrade, this field has to be set to null.
		/// </summary>
		public UpgradeCard PurchasedCard { get; set; }

		/// <summary>
		/// It can happen, that by buying some upgrade card, player
		/// reached limit for owned upgrade cards - in that case, he
		/// has to discard one of its cards. This field represents the
		/// discarded card. If the limit had not been reached, this field
		/// is ignored.
		/// </summary>
		public UpgradeCard DiscardedCard { get; set; }
	}

	/// <summary>
	/// This struct is used during upgrade phase - it represents 
	/// upgrade cards from which player can buy new ones.
	/// </summary>
	public struct PurchaseOffer
	{
		/// <summary>
		/// player identifier
		/// </summary>
		public string RobotName { get; set; }

		/// <summary>
		/// amount of energy robot has
		/// </summary>
		public int EnergyAmount { get; set; }

		/// <summary>
		/// From these cards, player can choose and buy new ones.
		/// </summary>
		public UpgradeCard[] OfferedCards { get; set; }

		/// <summary>
		/// By buying new card, player may reach limit on owned
		/// upgrade cards. In that case, he has to discard some. 
		/// These owned upgrade cards are in this field.
		/// </summary>
		public UpgradeCard[] OwnedCards { get; set; }

		/// <summary>
		/// Players can own only certain number of upgrade cards. These
		/// limits are present in this field.
		/// </summary>
		public Dictionary<UpgradeCardType, int> UpgradeCardsLimits { get; set; }
	}

	/// <summary>
	/// Struct for immediate response on whether or not player wishes to use 
	/// some upgrade card (only yes vs no cards).
	/// </summary>
	public struct UpgradeCardUseInfo
	{
		/// <summary>
		/// Flag on card use.
		/// </summary>
		public bool UseCard { get; set; }
	}

	/// <summary>
	/// Struct for immediate response on whether or not player wishes to use 
	/// some upgrade card (cards with choices).
	/// </summary>
	public struct ChoiceUpgradeCardUseInfo
	{
		/// <summary>
		/// Flag on card use.
		/// </summary>
		public bool UseCard { get; set; }

		/// <summary>
		/// Chosen choice - if player does not use offered card,
		/// this field is ignored.
		/// </summary>
		public UpgradeCardChoice ChosenChoice { get; set; }
	}


	/// <summary>
	/// Classes derived from this base class are used to pass asnychronous message
	/// from clients to model - currently, this concernes only upgrade
	/// cards that are used over longer period. We use classes 
	/// instead of structs so that we can have hierarchy of classes 
	/// that are placed into one queue (for timestamps).
	/// </summary>
	public abstract class AsyncClientMessage
	{
		/// <summary>
		/// player identifier
		/// </summary>
		public string RobotName { get; set; }
	}

	/// <summary>
	/// This message represnts player's asynchronous intent to use one
	/// of the upgrade cards without choice (this only concernes 
	/// upgrade cards that have been offered to player for a longer
	/// period of time). 
	/// See <see cref="UpgradeCard"/> for a more detailed description about
	/// implementation of upgrade cards.
	/// </summary>
	public class AsyncUpgradeCardUseInfo : AsyncClientMessage
	{
		/// <summary>
		/// card player wishes to play
		/// </summary>
		public string CardName { get; set; }
	}

	/// <summary>
	/// This message represnts player's asynchronous intent to use one
	/// of the upgrade cards with choices (this only concernes 
	/// upgrade cards that have been offered to player for a longer
	/// period of time). 
	/// See <see cref="UpgradeCard"/> for a more detailed description about
	/// implementation of upgrade cards.
	/// </summary>
	public class AsyncChoiceUpgradeCardUseInfo : AsyncUpgradeCardUseInfo
	{
		/// <summary>
		/// Taken upgrade card choice.
		/// </summary>
		public UpgradeCardChoice ChosenChoice { get; set; }
	}
}
