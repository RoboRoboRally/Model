﻿using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.CardDB.Cards;

namespace RoboRoboRally.Model
{
	/// <summary>
	/// <para>
	/// From the perspective of model, this is one of the interfaces that enables
	/// it to communicate with server (and therefore clients). This interface
	/// is concerned only with notifications like robot moved, robot rotated,
	/// robot played card and so on. There is one more interface for this direction
	/// of communication and that is interface <see cref="IInputHandler"/>. This
	/// other interface is concerned noly with input from server (and transitively, 
	/// players).
	/// </para>
	/// 
	/// <para>
	/// In order to see main class for communication from server to model, see
	/// class <see cref="Game"/>.
	/// </para>
	/// </summary>
	public interface IGameEventNotifier
    {
		/// <summary>
		/// Notifies that in a running instance of a game occurred some error
		/// that was caught by some prepared try catch clause.
		/// </summary>
		/// <param name="errorMessage">error message</param>
		void FatalRunningGameError(string errorMessage);
		/// <summary>
		/// Notifies that in a running instance of a game occurred error
		/// that was not expected (not caught by any try catch clause).
		/// </summary>
		/// <param name="errorMessage"></param>
		void FatalUnexpectedError(string errorMessage);

		/// <summary>
		/// Notifies that a new instance of running game was started. 
		/// We need to pass one information to server that it could not know
		/// while launching game - map size which was read from databases.
		/// </summary>
		/// <param name="mapSize">size of map</param>
		void GameStarted(MapSize mapSize);
		/// <summary>
		/// Notifies that running game was paused.
		/// </summary>
		void GamePaused();
		/// <summary>
		/// Notifies that running game was resumed.
		/// </summary>
		void GameResumed();
		/// <summary>
		/// Notifies that running game initiated termination sequence.
		/// </summary>
		void GameIsEnding();
		/// <summary>
		/// Notifies that game is over.
		/// </summary>
		void GameOver();

		/// <summary>
		/// Notifies that new player joined game.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="facingDir">robot's initial facing direction</param>
		/// <param name="position">robot's initial position</param>
		/// <param name="energyAmount">robot's energy</param>
		/// <param name="lastVisitedCheckpoint">last visited checkpoint of this robot,
		/// is zero if no checkpoints were visited by this robot</param>
		/// <param name="ownedUpgradeCards">array of robot's upgrade cards</param>
		void RobotJoinedGame(string robotName, Direction facingDir, MapCoordinates position,
			int energyAmount, int lastVisitedCheckpoint, string[] ownedUpgradeCards);
		/// <summary>
		/// Notifies that player left game.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		void RobotLeftGame(string robotName);
		/// <summary>
		/// Notifies that player finished game.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		void RobotFinishedGame(string robotName);

		/// <summary>
		/// Notifies that new turn began.
		/// </summary>
		void NewTurnBegan();
		/// <summary>
		/// Notifies that turn ended.
		/// </summary>
		void TurnEnded();
		
		/// <summary>
		/// Notifies that new phase started.
		/// </summary>
		/// <param name="phaseName">name of the started phase</param>
		void PhaseStarted(string phaseName);
		/// <summary>
		/// Notifies that phase ended.
		/// </summary>
		/// <param name="phaseName">name of the finished phase</param>
		void PhaseEnded(string phaseName);

		//Upgrade phase related
		/// <summary>
		/// Notifies about priority of robots for a ceratin upgrade phase.
		/// </summary>
		/// <param name="robotsByPriority">robots ordered by priority, robot
		/// at index [0] is robot with highest priority - one that will be buying first</param>
		void UpgradePhasePriorityDetermined(string[] robotsByPriority);
		/// <summary>
		/// Notifies whose turn it is to buy upgrades.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="upgradeCards">offered upgrade cards for purchase</param>
		void RobotIsBuying(string robotName, string[] upgradeCards);
		/// <summary>
		/// This notification is used when the whole upgrade phase is
		/// skipped because there are no more upgrade cards left to buy
		/// (for example, upgrade pack is empty).
		/// </summary>
		void UpgradePhaseSkipped();
		/// <summary>
		/// Notifies that one of the robots acquired some upgrade card
		/// (either through purchase or by some other means)
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="cardName">card identifier</param>
		void UpgradeCardObtained(string robotName, string cardName);
		/// <summary>
		/// Notifies that one of the robots got rid of some
		/// upgrade card (this may be called e.g. during upgrade
		/// phase due to player reaching upgrade card limit, or 
		/// when player finishes game).
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="cardName">card identifier</param>
		void UpgradeCardDiscarded(string robotName, string cardName);
		/// <summary>
		/// Notifies that one of the robots used some upgrade
		/// card that does not have any choices.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="cardName">card identifier</param>
		void UpgradeCardUsed(string robotName, string cardName);
		/// <summary>
		/// Notifies that one of the robots used some upgrade
		/// card that has some choices.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="cardName">card identifier</param>
		/// <param name="takenChoice">taken choice identifier</param>
		void UpgradeCardUsed(string robotName, string cardName, UpgradeCardChoice takenChoice);
		//NOTE!!!: notifications like "upgrade card offered for play" are expected to be
		//part of calls on input handler 


		//Programming phase related
		//nothing for now...


		//Activation Phase related
		/// <summary>
		/// Notifies that activation for one of the registers began.
		/// </summary>
		/// <param name="registerNumber">register number</param>
		void RegisterActivationBegan(int registerNumber);
		/// <summary>
		/// Notifies that some card was revealed at a certain register
		/// number.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="registerNumber">register number</param>
		/// <param name="cardName">card identifier</param>
		void ProgrammingCardRevealed(string robotName, int registerNumber, string cardName);
		/// <summary>
		/// Notifies that priority for activation of one of the registers
		/// was determined
		/// </summary>
		/// <param name="robotsByPriority">robots ordered by priority, robot's register
		/// at index [0] will be activated first</param>
		void ActivationPhasePriorityDetermined(string[] robotsByPriority);
		/// <summary>
		/// Notifies, that one of the robots played some programming card.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="cardName">card identifier</param>
		void ProgrammingCardPlayed(string robotName, string cardName);

		/// <summary>
		/// Notifiest that activation of static map elements 
		/// (including robots' lasers) began. These elements
		/// will be now activated in order that is specified by rules.
		/// </summary>
		void MapElementsActivationBegan();
		/// <summary>
		/// Notifies that blue belts were activated.
		/// </summary>
		void BlueBeltsActivated();
		/// <summary>
		/// Notifies that green belts were activated.
		/// </summary>
		void GreenBeltsActivated();
		/// <summary>
		/// Notifies that push panels were activated.
		/// </summary>
		void PushPanelsActivated();
		/// <summary>
		/// Notifies that rotating gears were activated.
		/// </summary>
		void RotatingGearsActivated();
		/// <summary>
		/// Notifies that lasers on walls were activated.
		/// </summary>
		void WallLasersActivated();
		/// <summary>
		/// Notifies that lasers for one of the robots were activated.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		void RobotLasersActivated(string robotName);
		/// <summary>
		/// Notifies that energy spaces were activated.
		/// </summary>
		void EnergySpacesActivated();
		/// <summary>
		/// Notifies that checkpoints were activated.
		/// </summary>
		void CheckpointsActivated();

		
		/// <summary>
		/// <para>
		/// Notifies, that a programming card was replaced at some
		/// register for one of the players. This can be caused by
		/// some upgrades or by spam card. 
		/// </para>
		/// <para>
		/// Note that we want to send the new card even in cases
		/// when we are replacing card that was not revealed. This
		/// is because of mobile application.
		/// </para>
		/// </summary>
		/// <param name="robotName">robot identifer</param>
		/// <param name="registerNumber">register number of replacing card</param>
		/// <param name="oldCardName">identifier of card that is being replaced</param>
		/// <param name="newCardName">identifier of card that is replacing 
		/// the old card</param>
		void RegisterCardReplaced(string robotName, int registerNumber, string oldCardName, string newCardName);

		/// <summary>
		/// Notifies, that one of the robots acquired new checkpoint number.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="checkpointNumber">checkpoint number</param>
		void RobotVisitedCheckpoint(string robotName, int checkpointNumber);
		/// <summary>
		/// Some cards allow to "burn" cards from pack, like
		/// spam folder card that burns one spam card from player's
		/// pack. This method notifies about this event.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="cardName">burned card</param>
		void RobotBurnedCard(string robotName, string cardName);
		/// <summary>
		/// Notifies that robot moved (without differetianting the source,
		/// as it should be obvious from previous notifications)
		/// </summary>
		/// <param name="robotName">robot identifer</param>
		/// <param name="oldPosition">old position</param>
		/// <param name="newPosition">new position</param>
		void RobotMoved(string robotName, MapCoordinates oldPosition, MapCoordinates newPosition);
		/// <summary>
		/// Notifies, that one of the robots got pushed (without differentiating
		/// the source, as it should be obvious from previous notifications).
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="oldPosition">old position</param>
		/// <param name="newPosition">new position</param>
		void RobotGotPushed(string robotName, MapCoordinates oldPosition, MapCoordinates newPosition);
		/// <summary>
		/// Notifies, that robot initiated reboot sequence
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="rebootPosition">position to which robot is rebooting</param>
		/// <param name="facingDirection">robot's facing direction after reboot</param>
		void RobotIsRebooting(string robotName, MapCoordinates rebootPosition, Direction facingDirection);
		/// <summary>
		/// Notifies, that robot suddenly changed position across map. 
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="oldPosition">old position</param>
		/// <param name="newPosition">new position</param>
		/// <param name="newFacingDirection">robot's facing direction after teleport</param>
		void RobotTeleported(string robotName, MapCoordinates oldPosition, MapCoordinates newPosition, Direction newFacingDirection);
		/// <summary>
		/// Notifies, that robot's energy changed.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="newEnergyAmount">robot's energy amount after change</param>
		/// <param name="oldEnergyAmount">robot's energy amount before change</param>
		void RobotEnergyChanged(string robotName, int newEnergyAmount, int oldEnergyAmount);
		/// <summary>
		/// Notifies, that robot suffered some form of damage from
		/// sources like lasers or reboot. We do not differentiate between
		/// the sources as they should be obvious from previous notifications.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="damage">suffered damage</param>
		void RobotSufferedDamage(string robotName, Damage damage);
		/// <summary>
		/// Notifies, that robot rotated and changed it's facing
		/// direction.
		/// </summary>
		/// <param name="robotName">robot identifier</param>
		/// <param name="rotationType">robot's rotation, relative to it's
		/// previous facing direction</param>
		void RobotRotated(string robotName, RotationType rotationType);
    }
}
