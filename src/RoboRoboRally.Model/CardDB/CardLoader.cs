﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks.AsyncCardHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB
{
	/// <summary>
	/// This exception is used if an error occurs
	/// during loading of some cards with <see cref="CardLoader"/>.
	/// </summary>
	class CardDBParsingException : Exception
	{
		/// <summary>
		/// Exception ctor with message.
		/// </summary>
		public CardDBParsingException(string message) :
			base("Exception occurred during the parsing of xml card database files: " + message)
		{
		}
	}

	/// <summary>
	/// This class is responsible for loading cards that
	/// are present in the GameData database. Cards and their
	/// effects are stored as xml files in the GameData.
	/// </summary>
	public class CardLoader
	{
		private string cardDatabaseDirPath;
		private IDictionary<string, DamageCard> cachedDamageCards;
		private IDictionary<string, ProgrammingCard> cachedPlayerProgrammingCards;
		private IDictionary<string, ProgrammingCard> cachedSpecialProgrammingCards;
		private IDictionary<string, UpgradeCard> cachedUpgradeCards;

		/// <summary>
		/// Ctor with path to card database directory inside 
		/// GameData. This path should point to directory
		/// ... /DB/CardDB
		/// </summary>
		public CardLoader(string cardDatabaseDirPath)
		{
			this.cardDatabaseDirPath = cardDatabaseDirPath;
		}

		private XmlDocument LoadCardDocument(string cardFilePath)
		{
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.IgnoreComments = true;
			XmlReader reader = XmlReader.Create(cardFilePath, settings);
			XmlDocument document = new XmlDocument();
			document.Load(reader);
			return document;
		}

		#region EffectParsing

		private MoveEffect ParseMoveEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.Attributes.Count != 1)
				throw new CardDBParsingException("expecting one attribute with amount number in move effect");

			if (!Int32.TryParse(cardEffectNode.Attributes[0].Value, out int amount))
				throw new CardDBParsingException("expecting one attribute with amount number in move effect");

			return new MoveEffect(amount);
		}

		private GainEnergyEffect ParseGainEnergyEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.Attributes.Count != 1)
				throw new CardDBParsingException("expecting one attribute with positive amount number in gain energy effect");

			if (!Int32.TryParse(cardEffectNode.Attributes[0].Value, out int amount) || amount < 1)
				throw new CardDBParsingException("expecting one attribute with positive amount number in gain energy effect");

			return new GainEnergyEffect(amount);
		}

		private PerformPrevRegAgainEffect ParseAgainEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.Attributes.Count != 0 || cardEffectNode.ChildNodes.Count != 0)
				throw new CardDBParsingException("expecting zero attributes and zero child elements in again effect");

			return new PerformPrevRegAgainEffect();
		}

		private PlayTopCardEffect ParsePlayTopCardEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.Attributes.Count !=  0|| cardEffectNode.ChildNodes.Count != 0)
				throw new CardDBParsingException("expecting zero attributes and zero child elements in play top card effect");

			return new PlayTopCardEffect();
		}

		private RebootEffect ParseRebootEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.Attributes.Count != 0 || cardEffectNode.ChildNodes.Count != 0)
				throw new CardDBParsingException("expecting zero attributes and zero child elements in reboot effect");

			return new RebootEffect();
		}

		private TurnEffect ParseTurnEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.Attributes.Count != 1)
				throw new CardDBParsingException("expecting one attribute with rotation in turn effect");

			string rotation = cardEffectNode.Attributes[0].Value;
			RotationType rotType;

			switch (rotation)
			{
				case nameof(RotationType.Right):
					rotType = RotationType.Right;
					break;
				case nameof(RotationType.Left):
					rotType = RotationType.Left;
					break;
				case nameof(RotationType.Back):
					rotType = RotationType.Back;
					break;
				case nameof(RotationType.None):
					rotType = RotationType.None;
					break;
				default:
					throw new CardDBParsingException("expecting valid rotation type in turn effect");
			}

			return new TurnEffect(rotType);
		}

		private Damage ParseDamage(XmlNode damageNode)
		{
			if (damageNode.ChildNodes.Count == 0)
				throw new CardDBParsingException("parsing damage - expecting at least one element with damage card");

			IList<DamageCard> damageCards = new List<DamageCard>();

			foreach (XmlNode dmg in damageNode.ChildNodes)
			{
				string cardName = dmg.Name;

				if (!cachedDamageCards.ContainsKey(cardName))
					throw new CardDBParsingException("fatal error during parsing - damage card that was referenced in damage effect has not been parsed yet - this is not currently allowed");

				damageCards.Add(cachedDamageCards[cardName]);
			}

			return new Damage() { DamageCards = damageCards.ToArray() };
		}

		private TakeDamageEffect ParseTakeDamageEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.ChildNodes.Count != 1)
				throw new CardDBParsingException("parsing take damage effect - expecting one non empty damage element");

			var Damage = ParseDamage(cardEffectNode.ChildNodes[0]);

			return new TakeDamageEffect(Damage);
		}

		private DealAoEDamageEffect ParseDealAoEDamageEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.ChildNodes.Count != 1)
				throw new CardDBParsingException("parsing deal aoe damage effect - expecting one non empty damage element");

			if (cardEffectNode.Attributes.Count != 1 || !Int32.TryParse(cardEffectNode.Attributes[0].Value, out int radius))
				throw new CardDBParsingException("parsing deal aoe damage effect - expecting one positive attribute with aoe radius");

			if (radius < 1)
				throw new CardDBParsingException("parsing deal aoe damage effect - expecting one positive attribute with aoe radius");

			var dmg = ParseDamage(cardEffectNode.ChildNodes[0]);

			return new DealAoEDamageEffect(dmg, radius);
		}

		private ChooseCardEffect ParseChooseCardEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.ChildNodes.Count == 0)
				throw new CardDBParsingException("parsing choose card effect - expecting at least one card sub element");

			List<ProgrammingCard> cardsToChoose = new List<ProgrammingCard>();
			
			foreach (XmlNode node in cardEffectNode.ChildNodes)
			{
				cardsToChoose.Add(FindCard(LoadPlayerProgrammingCards(), node.Name));
			}

			return new ChooseCardEffect(cardsToChoose.ToArray());
		}

		private DiscardEffect ParseDiscardEffect(XmlNode cardEffectNode)
		{
			if (cardEffectNode.Attributes.Count != 1)
				throw new CardDBParsingException("parsing discard effect - expecting one attribute with card name");

			var c = FindCard(LoadDamageCards(), cardEffectNode.Attributes[0].Value);

			return new DiscardEffect(c);
		}

		private CardEffect ParseSingleCardEffect(XmlNode cardEffectNode)
		{
			switch (cardEffectNode.Name)
			{
				case nameof(MoveEffect):
					return ParseMoveEffect(cardEffectNode);
				case nameof(GainEnergyEffect):
					return ParseGainEnergyEffect(cardEffectNode);
				case nameof(PerformPrevRegAgainEffect):
					return ParseAgainEffect(cardEffectNode);
				case nameof(PlayTopCardEffect):
					return ParsePlayTopCardEffect(cardEffectNode);
				case nameof(RebootEffect):
					return ParseRebootEffect(cardEffectNode);
				case nameof(TurnEffect):
					return ParseTurnEffect(cardEffectNode);
				case nameof(TakeDamageEffect):
					return ParseTakeDamageEffect(cardEffectNode);
				case nameof(DealAoEDamageEffect):
					return ParseDealAoEDamageEffect(cardEffectNode);
				case nameof(DiscardEffect):
					return ParseDiscardEffect(cardEffectNode);
				case nameof(ChooseCardEffect):
					return ParseChooseCardEffect(cardEffectNode);
				default:
					throw new CardDBParsingException("unknown card effect type");

			}
		}

		private CardEffect[] GetCardEffects(XmlNode programmingCardNode)
		{
			XmlNodeList childList = programmingCardNode.ChildNodes;

			if (childList.Count != 1)
				throw new CardDBParsingException("can not parse programming card - card has to have single child with effects");

			XmlNode effectsNode = childList[0];

			if (effectsNode.ChildNodes.Count == 0)
				throw new CardDBParsingException("can not parse programming card - no effects specified");

			IList<CardEffect> cardEffects = new List<CardEffect>();

			foreach(XmlNode effectNode in effectsNode.ChildNodes)
			{
				cardEffects.Add(ParseSingleCardEffect(effectNode));
			}

			return cardEffects.ToArray();
		}

		#endregion

		private bool GetApproveValue(XmlNode hookNode)
		{
			foreach (XmlNode child in hookNode.ChildNodes)
			{
				if (child.Name == "RequiresApprove")
				{
					if (child.Attributes.Count != 1 || !Boolean.TryParse(child.Attributes[0].Value, out bool approve))
						throw new CardDBParsingException("can not parse upgrade card - expecting requires approve element with one attribute specifying the value");

					return approve;
				}
			}
			throw new CardDBParsingException("can not parse upgrade card - expecting requires approve element with one attribute specifying the value");
		}

		private Damage GetHookDamage(XmlNode hookNode)
		{
			LoadDamageCards();
			foreach (XmlNode child in hookNode.ChildNodes)
			{
				if (child.Name == "Damage")
				{
					return ParseDamage(child);
				}
			}

			throw new CardDBParsingException("can not parse upgrade card - expecting damage for hook");
		}

		private ProgrammingCard GetHookProgrammingCard(XmlNode hookNode)
		{
			foreach (XmlNode child in hookNode.ChildNodes)
			{
				if (child.Name == "ProgrammingCard")
				{
					return LoadProgrammingCard(child.InnerText);
				}
			}

			throw new CardDBParsingException("can not parse upgrade card - expecting programming card for hook");
		}

		private int GetHookAmount(XmlNode hookNode)
		{
			foreach (XmlNode child in hookNode.ChildNodes)
			{
				if (child.Name == "Amount")
				{
					if (!Int32.TryParse(child.InnerText, out int res))
						throw new CardDBParsingException("can not parse upgrade card - can not parse hook amount");

					return res;
				}
			}

			throw new CardDBParsingException("can not parse upgrade card - expecting amount for hook");
		}


		private (UpgradeCardChoice[], ChoiceValue<T>[]) ParseChoice<T>(XmlNode choiceNode, Func<string, T> choiceValueFunc)
		{
			List<UpgradeCardChoice> choices = new List<UpgradeCardChoice>();
			List<ChoiceValue<T>> choiceValues = new List<ChoiceValue<T>>();
			
			if (choiceNode.ChildNodes.Count == 0)
				throw new CardDBParsingException("can not parse upgrade card - choice card is expected to have at least one choice");
			
			foreach (XmlNode optionNode in choiceNode.ChildNodes)
			{
				if (optionNode.ChildNodes.Count != 2)
					throw new CardDBParsingException("can not parse upgrade card - invalid choice in choice hook");

				var choiceCardName = optionNode.ChildNodes[0].InnerText;
				var stringChoiceValue = optionNode.ChildNodes[1].InnerText;

				T choiceValue = choiceValueFunc(stringChoiceValue);

				choices.Add(new UpgradeCardChoice() { ChoiceCardName = choiceCardName });
				choiceValues.Add(new ChoiceValue<T>() { ChoiceCardName = choiceCardName, Value = choiceValue });
			}

			return (choices.ToArray(), choiceValues.ToArray());
		}

		private (UpgradeCardChoice[], ChoiceValue<RotationType>[]) ParseRotationChoice(XmlNode choiceNode)
		{
			return ParseChoice<RotationType>(choiceNode,
						(string choiceValue) =>
						{
							if (!Enum.TryParse<RotationType>(choiceValue, out RotationType res))
								throw new CardDBParsingException("can not parse upgrade card - can not parse hook choice");

							return res;
						});
		}

		private (UpgradeCardChoice[], ChoiceValue<ProgrammingCard>[]) ParseCardChoice(XmlNode choiceNode)
		{
			return ParseChoice<ProgrammingCard>(choiceNode,
				(string choiceValue) =>
				{
					return LoadProgrammingCard(choiceValue);
				});
		}

		private (UpgradeCardChoice[], ChoiceValue<DirectionType>[]) ParseDirectionChoice(XmlNode choiceNode)
		{
			return ParseChoice<DirectionType>(choiceNode,
				(string choiceValue) =>
				{
					if(!Enum.TryParse<DirectionType>(choiceValue, out DirectionType res))
						throw new CardDBParsingException("can not parse upgrade card - can not parse hook choice");

					return res;
				});
		}

		private XmlNode GetChoiceNode(XmlNode hookNode)
		{
			foreach (XmlNode child in hookNode.ChildNodes)
			{
				if (child.Name.ToLower().Contains("choice"))
					return child;
			}

			throw new CardDBParsingException("can not parse upgrade card - expecting choice element in choice hook");
		}

		private IUpgradeCardHookBase GetAsyncChoiceHook(XmlNode hookNode, string hookName)
		{
			var choiceNode = GetChoiceNode(hookNode);

			switch (hookName)
			{
				case nameof(MoveToAdjacentHook):
					var choice = ParseDirectionChoice(choiceNode);
					return new MoveToAdjacentHook(choice.Item1, choice.Item2);
				case nameof(RotateHook):
					var choice2 = ParseRotationChoice(choiceNode);
					return new RotateHook(choice2.Item1, choice2.Item2);
				default:
					throw new CardDBParsingException($"can not parse upgrade card - unknown hook type: {hookName}");

			}
		}

		private IUpgradeCardHookBase GetAsyncChoicelessHook(XmlNode hookNode, string hookName)
		{
			switch (hookName)
			{
				case nameof(AddEnergyHook):
					return new AddEnergyHook(GetHookAmount(hookNode));
				case nameof(AddProgrammingCardHook):
					return new AddProgrammingCardHook(GetHookProgrammingCard(hookNode));
				default:
					throw new CardDBParsingException($"can not parse upgrade card - unknown hook type: {hookName}");
			}
		}

		private IUpgradeCardHookBase GetChoiceHook(XmlNode hookNode, string hookName)
		{
			var choiceNode = GetChoiceNode(hookNode);

			switch (hookName)
			{
				case nameof(PushToSideHook):
					var choice = ParseRotationChoice(choiceNode);
					return new PushToSideHook(choice.Item1, choice.Item2);
				case nameof(ChangeRegisterProgrammingHook):
					var choice2 = ParseCardChoice(choiceNode);
					return new ChangeRegisterProgrammingHook(choice2.Item1, choice2.Item2);
				default:
					throw new CardDBParsingException($"can not parse upgrade card - unknown hook type: {hookName}");
			}
		}

		private IUpgradeCardHookBase GetChoicelessHook(XmlNode hookNode, string hookName)
		{
			bool requiresApprove = GetApproveValue(hookNode);			

			switch (hookName)
			{
				case nameof(IncrementDrawAmountHook):
					return new IncrementDrawAmountHook(requiresApprove);
				case nameof(RobotLaserDamageHook):
					return new RobotLaserDamageHook(requiresApprove, GetHookDamage(hookNode));
				case nameof(RobotLaserTargetsHook):
					return new RobotLaserTargetsHook(requiresApprove);
				case nameof(RobotLaserDirectionHook):
					return new RobotLaserDirectionHook(requiresApprove);
				case nameof(PushDamageHook):
					return new PushDamageHook(requiresApprove, GetHookDamage(hookNode));
				case nameof(NegateRebootDamageHook):
					return new NegateRebootDamageHook(requiresApprove);
				case nameof(CardReplacementHook):
					return new CardReplacementHook(requiresApprove);
				case nameof(PushShotRobotsHook):
					return new PushShotRobotsHook(requiresApprove);
				case nameof(PullShotRobotsHook):
					return new PullShotRobotsHook(requiresApprove);
				case nameof(PerformRegisterAgainHook):
					return new PerformRegisterAgainHook(requiresApprove);
				default:
					throw new CardDBParsingException($"can not parse upgrade card - unknown hook type: {hookName}");
			}
		}

		private IUpgradeCardHookBase GetHook(XmlNode hookNode)
		{
			if (hookNode.Attributes.Count != 1)
				throw new CardDBParsingException("can not parse hooks for upgrade card - expecting one attribute with hook name");

			var hookName = hookNode.Attributes[0].Value;

			switch (hookNode.Name)
			{
				case "ChoicelessHook":
					return GetChoicelessHook(hookNode, hookName);
				case "ChoiceHook":
					return GetChoiceHook(hookNode, hookName);
				case "AsyncChoicelessHook":
					return GetAsyncChoicelessHook(hookNode, hookName);
				case "AsyncChoiceHook":
					return GetAsyncChoiceHook(hookNode, hookName);
				default:
					throw new CardDBParsingException("can not parse upgrade card - unknown hook type");
			}
		}

		private string GetCardName(XmlNode cardNode)
		{
			return cardNode.Name;
		}

		private int GetUpgradeCardCost(XmlNode cardNode)
		{
			foreach (XmlNode childNode in cardNode.ChildNodes)
			{
				if (childNode.Name == "Cost")
				{
					if (childNode.Attributes.Count != 1 || !Int32.TryParse(childNode.Attributes[0].Value, out int cost))
						throw new CardDBParsingException("can not parse upgrade card - expecting Cost element with one attribute - cost");

					return cost;
				}
			}

			throw new CardDBParsingException("can not parse upgrade card - expecting Cost element with one attribute - cost");
		}

		private UpgradeCardType GetUpgradeCardType(XmlNode cardNode)
		{
			foreach (XmlNode childNode in cardNode.ChildNodes)
			{
				if (childNode.Name == "Type")
				{
					if (childNode.Attributes.Count != 1 || !Enum.TryParse<UpgradeCardType>(childNode.Attributes[0].Value, out UpgradeCardType type))
						throw new CardDBParsingException("can not parse upgrade card - expecting Type element with one attribute - type");

					return type;
				}
			}

			throw new CardDBParsingException("can not parse upgrade card - expecting Type element with one attribute - type");
		}

		private IUpgradeCardHookBase[] GetUpgradeCardHooks(XmlNode cardNode)
		{
			foreach (XmlNode childNode in cardNode.ChildNodes)
			{
				if (childNode.Name == "Hooks")
				{
					List<IUpgradeCardHookBase> hooks = new List<IUpgradeCardHookBase>();

					foreach (XmlNode hookNode in childNode.ChildNodes)
					{
						hooks.Add(GetHook(hookNode));

						if (hookNode.Name.ToLower().Contains("async"))
						{
							hooks.Add(new AsyncCardOfferHook());
							hooks.Add(new AsyncCardOfferEndHook());
						}
					}
					
					return hooks.ToArray();
				}
			}

			throw new CardDBParsingException("can not parse upgrade card - expecting Hooks element with hooks");
		}

		private T ParseProgrammingCard<T>(XmlNode progCardNode, Func<string, CardEffect[], T> progCardFunc)
		{
			var name = GetCardName(progCardNode);
			var effects = GetCardEffects(progCardNode);
			return progCardFunc(name, effects);
		}

		private void ParseProgrammingCards<T>(XmlDocument xmlDocument, string elementName, 
			Func<string, CardEffect[], T> progCardFunc,	IDictionary<string, T> toFill) where T: ProgrammingCard
		{
			XmlNode programmingCardsTopNode = xmlDocument.DocumentElement.SelectSingleNode("//" + elementName);
			XmlNodeList programmingCardsNodes = programmingCardsTopNode.ChildNodes;

			if (programmingCardsNodes.Count == 0)
				throw new CardDBParsingException("parsing programming cards - card list is empty");

			foreach (XmlNode progCard in programmingCardsNodes)
			{
				var card = ParseProgrammingCard<T>(progCard, progCardFunc);
				toFill.Add(card.Name, card);
			}
		}

		private void ParseAllProgrammingCards(XmlDocument xmlDocument)
		{
			cachedPlayerProgrammingCards = new Dictionary<string, ProgrammingCard>();
			cachedDamageCards = new Dictionary<string, DamageCard>();
			cachedSpecialProgrammingCards = new Dictionary<string, ProgrammingCard>();

			ParseProgrammingCards<ProgrammingCard>(xmlDocument, "PlayerProgrammingCards",
				(string cardName, CardEffect[] cardEffects) => { return new ProgrammingCard(cardName, cardEffects); }, cachedPlayerProgrammingCards);
			ParseProgrammingCards<DamageCard>(xmlDocument, "DamageCards",
				(string cardName, CardEffect[] cardEffects) => { return new DamageCard(cardName, cardEffects); }, cachedDamageCards);
			ParseProgrammingCards<ProgrammingCard>(xmlDocument, "SpecialProgrammingCards",
				(string cardName, CardEffect[] cardEffects) => { return new ProgrammingCard(cardName, cardEffects); }, cachedSpecialProgrammingCards);
		}

		private UpgradeCard ParseSingleUpgradeCardNode(XmlNode upgradeCardNode)
		{
			var cName = GetCardName(upgradeCardNode);
			var cost = GetUpgradeCardCost(upgradeCardNode);
			var type = GetUpgradeCardType(upgradeCardNode);
			var hooks = GetUpgradeCardHooks(upgradeCardNode);
			return new UpgradeCard(cName, type, cost, hooks);
		}

		private void ParseAllUpgradeCards(XmlDocument xmlDocument)
		{
			cachedUpgradeCards = new Dictionary<string, UpgradeCard>();

			XmlNode topNode = xmlDocument.DocumentElement.SelectSingleNode("//UpgradeCards");
			XmlNodeList uNodes = topNode.ChildNodes;

			foreach (XmlNode uCard in uNodes)
			{
				var c = ParseSingleUpgradeCardNode(uCard);
				cachedUpgradeCards.Add(c.Name, c);
			}
		}

		private void LoadAllProgrammingCards()
		{
			string programmingCardsFilePath = cardDatabaseDirPath + @"/ProgrammingCards.xml";
			XmlDocument doc = LoadCardDocument(programmingCardsFilePath);
			ParseAllProgrammingCards(doc);
		}

		private void LoadAllUpgradeCards()
		{
			string upgradeCardsFilePath = cardDatabaseDirPath + @"/UpgradeCards.xml";
			XmlDocument doc = LoadCardDocument(upgradeCardsFilePath);
			ParseAllUpgradeCards(doc);
		}

		/// <summary>
		/// Returns all player programming cards that are present in
		/// the GameData database (so this does not include e.g. damage
		/// cards or special programming cards).
		/// May throw exception if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException"> is thrown
		/// if an error occurs during the loading of cards</exception>
		public ProgrammingCard[] LoadPlayerProgrammingCards()
		{
			if (cachedPlayerProgrammingCards == null)
				LoadAllProgrammingCards();
			return cachedPlayerProgrammingCards.Values.ToArray();
		}

		/// <summary>
		/// Returns all damage cards that are present in
		/// the GameData database.
		/// May throw exception if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException"> is thrown
		/// if an error occurs during the loading of cards</exception>
		public DamageCard[] LoadDamageCards()
		{
			if (cachedDamageCards == null)
				LoadAllProgrammingCards();
			return cachedDamageCards.Values.ToArray();
		}

		/// <summary>
		/// Returns all upgrade cards that are present in
		/// the GameData database.
		/// May throw exception if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException"> is thrown
		/// if an error occurs during the loading of cards</exception>
		public UpgradeCard[] LoadUpgradeCards()
		{
			if (cachedUpgradeCards == null)
				LoadAllUpgradeCards();
			return cachedUpgradeCards.Values.ToArray();
		}

		/// <summary>
		/// Returns all special programming cards that are present in
		/// the GameData database.
		/// May throw exception if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException"> is thrown
		/// if an error occurs during the loading of cards</exception>
		public ProgrammingCard[] LoadSpecialProgrammingCards()
		{
			if (cachedSpecialProgrammingCards == null)
				LoadAllProgrammingCards();
			return cachedSpecialProgrammingCards.Values.ToArray();
		}

		private T FindCard<T>(T[] cards, string cardName) where T : Card
		{
			foreach (var c in cards)
			{
				if (c.Name == cardName)
					return c;
			}

			throw new CardDBParsingException("requested card was not found in the database");
		}

		/// <summary>
		/// Returns a single player programming card with the
		/// passed name. 
		/// May throw an exception if no such card is present in the
		/// database or if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException">is thrown
		/// if no such card is present in the databse or
		/// if an error occurs during the loading</exception>
		public ProgrammingCard LoadPlayerProgrammingCard(string cardName)
		{
			return FindCard(LoadPlayerProgrammingCards(), cardName);
		}

		/// <summary>
		/// Returns a single damage card with the
		/// passed name. 
		/// May throw an exception if no such card is present in the
		/// database or if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException">is thrown
		/// if no such card is present in the databse or
		/// if an error occurs during the loading</exception>
		public DamageCard LoadDamageCard(string cardName)
		{
			return FindCard(LoadDamageCards(), cardName);
		}

		/// <summary>
		/// Returns a single upgrade card with the
		/// passed name. 
		/// May throw an exception if no such card is present in the
		/// database or if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException">is thrown
		/// if no such card is present in the databse or
		/// if an error occurs during the loading</exception>
		public UpgradeCard LoadUpgradeCard(string cardName)
		{
			return FindCard(LoadUpgradeCards(), cardName);
		}

		/// <summary>
		/// Returns a single special programming card with the
		/// passed name. 
		/// May throw an exception if no such card is present in the
		/// database or if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException">is thrown
		/// if no such card is present in the databse or
		/// if an error occurs during the loading</exception>
		public ProgrammingCard LoadSpecialProgrammingCard(string cardName)
		{
			return FindCard(LoadSpecialProgrammingCards(), cardName);
		}

		/// <summary>
		/// Returns a single programming card of any type (this includes
		/// player programming cards, special programming cards and 
		/// damage cards) with the passed name. 
		/// May throw an exception if no such card is present in the
		/// database or if an error occurs during the loading.
		/// </summary>
		/// <exception cref="CardDBParsingException">is thrown
		/// if no such card is present in the databse or
		/// if an error occurs during the loading</exception>
		public ProgrammingCard LoadProgrammingCard(string cardName)
		{
			var playerProgrammingCards = LoadPlayerProgrammingCards();
			var dmgCards = LoadDamageCards();
			var specialProgrammingCards = LoadSpecialProgrammingCards();

			foreach (var c in playerProgrammingCards)
			{
				if (c.Name == cardName)
					return c;
			}

			foreach (var c in dmgCards)
			{
				if (c.Name == cardName)
					return c;
			}

			foreach (var c in specialProgrammingCards)
			{
				if (c.Name == cardName)
					return c;
			}

			throw new CardDBParsingException("requested programming card was not found in the database");
		}	
	}
}
