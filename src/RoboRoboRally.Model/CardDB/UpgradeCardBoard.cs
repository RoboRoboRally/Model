﻿using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.CardDB
{
	/// <summary>
	/// This class represents players board with upgrade cards.
	/// It is specified by the limits on each upgrade card type
	/// and does checks that adhere these limits.
	/// </summary>
    class UpgradeCardBoard
    {
		Dictionary<UpgradeCardType, int> cardLimits;

		Dictionary<UpgradeCardType, int> cardCount;

		List<UpgradeCard> upgradeCards;

		/// <summary>
		/// Returns all upgrade cards in this board, does not
		/// remove the cards from the board.
		/// </summary>
		public UpgradeCard[] GetAllUpgradeCards()
		{
			return upgradeCards.ToArray();
		}

		/// <summary>
		/// Returns limits on upgrade cards specified 
		/// for this upgrade card board.
		/// </summary>
		public Dictionary<UpgradeCardType, int> GetCardLimits()
		{
			return cardLimits;
		}

		/// <summary>
		/// Returns number of cards present in the board
		/// of a particular type.
		/// </summary>
		public int GetCardCount(UpgradeCardType cardType)
		{
			return cardCount[cardType];
		}

		/// <summary>
		/// Adds new card to this upgrade card board. May
		/// throw exception if the limit on that particular card
		/// type has been reached.
		/// </summary>
		/// <exception cref="RunningGameException"> is thrown
		/// if the limit on the type of the passed card
		/// has been already reached</exception>
		public void AddCard(UpgradeCard card)
		{
			if(cardCount[card.UpgradeCardType] + 1 > cardLimits[card.UpgradeCardType])
				throw new RunningGameException("can not add new card to upgrade board - card limit reached");

			if (upgradeCards.Any(x => x.Name == card.Name))
				throw new RunningGameException("can not add new card to upgrade board - same card is already present");

			cardCount[card.UpgradeCardType]++;

			upgradeCards.Add(card);
		}

		/// <summary>
		/// Returns card with a given name from the upgrade
		/// card board (does not remove the card).
		/// </summary>
		/// <exception cref="RunningGameException">is thrown
		/// if no such card is present on the board</exception>
		public UpgradeCard GetCard(string cardName)
		{
			if (!upgradeCards.Any(c => c.Name == cardName))
				throw new RunningGameException("can not retrieve card from upgrade board - no such card");

			return upgradeCards.First(c => c.Name == cardName);
		}

		/// <summary>
		/// Removes card with the passed name from this board.
		/// May throw exception if no such card exists.
		/// </summary>
		/// <exception cref="RunningGameException">is thrown
		/// if no such card is present on the board</exception>
		public UpgradeCard RemoveCard(string cardName)
		{
			if (!upgradeCards.Any(c => c.Name == cardName))
				throw new RunningGameException("can not remove card from upgrade board - no such card");

			var card = upgradeCards.First(c => c.Name == cardName);

			upgradeCards.Remove(card);

			cardCount[card.UpgradeCardType]--;

			return card;
		}

		/// <summary>
		/// Removes all upgrade cards from this board.
		/// </summary>
		public UpgradeCard[] RemoveAllUpgradeCards()
		{
			var arr = upgradeCards.ToArray();

			upgradeCards.Clear();

			var upgradeCardTypes = Enum.GetValues(typeof(UpgradeCardType)).Cast<UpgradeCardType>();
			foreach (var type in upgradeCardTypes)
			{
				cardCount[type] = 0;
			}
			
			return arr;
		}

		/// <summary>
		/// Ctor with specified limits on each upgrade card type.
		/// </summary>
		public UpgradeCardBoard(Dictionary<UpgradeCardType, int> cardLimits)
		{
			this.upgradeCards = new List<UpgradeCard>();
			this.cardLimits = cardLimits;

			cardCount = new Dictionary<UpgradeCardType, int>();

			var upgradeCardTypes = Enum.GetValues(typeof(UpgradeCardType)).Cast<UpgradeCardType>();
			foreach (var type in upgradeCardTypes)
			{
				cardCount.Add(type, 0);
			}
		}
    }
}
