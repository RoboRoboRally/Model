﻿using RoboRoboRally.Model.CardDB.Cards.CardEffects;
using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards
{
	/// <summary>
	/// <para>
	/// Base class for all programming cards. This type
	/// includes basically three different types of programming
	/// cards. There is a player programming card, special programming
	/// card and damage card. (note that player programming card
	/// and special programming card are represented by this class,
	/// we only need special class <see cref="DamageCard"/> for 
	/// damage cards).
	/// </para>
	/// 
	/// <para>
	/// Each programming card is, apart from its name
	/// specified by its effects - what happens when
	/// the card is played. Each card can have various
	/// card effects, all of current effects can be found
	/// in the <see cref="CardEffects"/> namespace.
	/// </para>
	/// </summary>
	public class ProgrammingCard : Card
	{
		private CardEffect[] cardEffects;
		
		/// <summary>
		/// This method plays this programming card and performs
		/// all of its effects. Note that each card has to be played
		/// at some register and such register number has to be passed to
		/// this method. Also, this method does not handle any discards,
		/// and they are handled elsewhere.
		/// </summary>
		internal void PlayCard(IGameState gameState, Robot cardPlayer, int cardRegisterNumber)
		{
			//always notify about card being played
			//this notification has to be here, as there are multiple ways 
			//one can play a programming card
			gameState.Notifier.ProgrammingCardPlayed(cardPlayer.Name, Name);

			foreach (var effect in cardEffects)
			{
				effect.PerformEffect(gameState, cardPlayer, cardRegisterNumber);
			}
		}

		/// <summary>
		/// Programming card ctor with card's name a it's
		/// effects.
		/// </summary>
		public ProgrammingCard(string name, CardEffect[] cardEffects) : 
			base(name)
		{
			this.cardEffects = cardEffects;
		}
	}
}
