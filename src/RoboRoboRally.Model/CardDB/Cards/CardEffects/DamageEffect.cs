﻿using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// Base class for each damage effect.
	/// </summary>
	abstract class DamageEffect : CardEffect
	{
		protected Damage Damage { get; private set; }

		protected DamageEffect(Damage damage)
		{
			this.Damage = damage;
		}
	}
}
