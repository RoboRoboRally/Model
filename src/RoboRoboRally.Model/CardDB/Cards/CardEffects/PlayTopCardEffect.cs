﻿using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This card replaces the current register card 
	/// with the top card from player's pack and plays it.
	/// </summary>
    class PlayTopCardEffect : CardEffect
    {
		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			//this effect plays first card drawn from the top of @cardPlayer's programming deck
			var cards = cardPlayer.DrawCards(1);
			var card = cards[0];

			cardPlayer.ReplaceRegisterCard(gameState, card, cardRegisterNumber);

			cardPlayer.PlayRegisterAgain(gameState, cardRegisterNumber);
		}
	}
}
