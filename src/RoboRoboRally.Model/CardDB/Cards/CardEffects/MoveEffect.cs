﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect moves player by the amount specified
	/// in the ctor in player's facing direction.
	/// </summary>
    class MoveEffect : CardEffect
    {
		int amount;

		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			//this effect moves the @cardPlayer by @amount by using a movement action
			var action = gameState.GetAction<MovementAction>();
			action.PerformAction(new MovementActionArgs() { Amount = amount, GameState = gameState, Robot = cardPlayer, Direction = cardPlayer.FacingDirection });
		}

		public MoveEffect(int amount)
		{
			this.amount = amount;
		}
	}
}
