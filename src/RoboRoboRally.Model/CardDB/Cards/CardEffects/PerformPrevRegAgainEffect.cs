﻿using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect performs again previous register.
	/// (If card with this effect is at register 1 then
	/// nothing happens).
	/// </summary>
	class PerformPrevRegAgainEffect : CardEffect
	{
		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			//this effect tries to get card at previous register and tries to
			//play it again  

			if (cardRegisterNumber == 1)
				return;

			cardPlayer.PlayRegisterAgain(gameState, cardRegisterNumber - 1);
		}
	}
}
