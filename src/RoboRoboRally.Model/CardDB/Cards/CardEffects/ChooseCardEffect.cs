﻿using System.Linq;
using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect allows the player to play some new card 
	/// in the register.
	/// </summary>
    class ChooseCardEffect : CardEffect
    {
		ProgrammingCard[] cardsToChoose;

		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			//this effect offers @cardToChoose to a @cardPlayer ... he can pick one
			//that will be played

			var card = gameState.InputHandler.ChooseCard(cardPlayer.Name, cardsToChoose);

			if (!cardsToChoose.Contains(card))
				throw new RunningGameException("invalid chosen card");

			// we do not need to do anything else, we can simply play the card and that is it
			//this is correct with respect to discarding cards at the end of each turn
			//as we do not call replace (unlike with playTopCardsEffect)
			card.PlayCard(gameState, cardPlayer, cardRegisterNumber);
		}

		public ChooseCardEffect(ProgrammingCard[] cardsToChoose)
		{
			this.cardsToChoose = cardsToChoose;
		}
	}
}
