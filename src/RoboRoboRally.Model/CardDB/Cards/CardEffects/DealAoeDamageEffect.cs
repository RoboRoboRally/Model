﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effects deals damage to each player, except
	/// the card player, in the radius specified in the ctor.
	/// </summary>
    class DealAoEDamageEffect : DamageEffect
    {
		int radius;

		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			foreach (var robot in gameState.GameActiveRobots)
			{
				if (robot == cardPlayer)
					continue;

				if (robot.Position.GetEuclideanDistance(cardPlayer.Position) <= radius)
				{
					robot.SufferDamage(gameState, Damage);
				}
			}
		}

		public DealAoEDamageEffect(Damage damage, int radius) :
			base(damage)
		{
			this.radius = radius;
		}
	}
}
