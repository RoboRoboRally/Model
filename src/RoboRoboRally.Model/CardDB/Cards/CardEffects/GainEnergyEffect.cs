﻿using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect adds the amount of energy specified
	/// in the ctor to the player.
	/// </summary>
    class GainEnergyEffect  : CardEffect
    {
		int amount;

		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			cardPlayer.ChangeEnergyAmount(gameState, amount);
		}

		public GainEnergyEffect(int amount)
		{
			this.amount = amount;
		}
	}
}
