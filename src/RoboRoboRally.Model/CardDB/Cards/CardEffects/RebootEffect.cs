﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect immediately reboots the card player.
	/// </summary>
	class RebootEffect : CardEffect
	{
		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			//this effect immediately reboots the @cardPlayer
			var rebootAction = gameState.GetAction<RebootAction>();
			rebootAction.PerformAction(new ActionArgsBase() { GameState = gameState, Robot = cardPlayer });
		}
	}
}
