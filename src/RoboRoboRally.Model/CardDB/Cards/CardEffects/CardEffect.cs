﻿using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// <para>
	/// This namespace contains definition of each effect
	/// of programming cards. Note that this includes three
	/// types of cards - player programming cards, damage 
	/// cards and special programming cards.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{ }

	/// <summary>
	/// Base class for each effect. Derived classes
	/// need to implement the <see cref="PerformEffect(IGameState, Robot, int)"/>
	/// method which is the effect's body.
	/// </summary>
    public abstract class CardEffect
    {
		/// <summary>
		/// Main body of the effect.
		/// </summary>
		/// <param name="cardPlayer">owner of programming card with this
		/// effect</param>
		/// <param name="cardRegisterNumber">register number of card with
		/// this effect</param>
		public abstract void PerformEffect(IGameState gameState, Robot cardPlayer, 
			int cardRegisterNumber);
    }
}
