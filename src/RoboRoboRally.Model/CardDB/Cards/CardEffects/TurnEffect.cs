﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect rotates the card player by the rotation
	/// amount specified in it's ctor.
	/// </summary>
	class TurnEffect : CardEffect
	{
		RotationType rotationType;

		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			//with this effect, the @cardPlayer is rotated according to the @rotationType
			cardPlayer.Rotate(gameState, rotationType);
		}

		public TurnEffect(RotationType rotationType)
		{
			this.rotationType = rotationType;
		}
	}
}
