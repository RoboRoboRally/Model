﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect deals damage specified in the ctor 
	/// to the card player.
	/// </summary>
	class TakeDamageEffect : DamageEffect
	{
		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			cardPlayer.SufferDamage(gameState, Damage);
		}

		public TakeDamageEffect(Damage damage) :
			base(damage)
		{
		}
	}
}
