﻿using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards.CardEffects
{
	/// <summary>
	/// This effect tries to burn one card, specified 
	/// in the ctor, from the player's discard pile.
	/// (This is used by e.g. spam folder card).
	/// </summary>
    class DiscardEffect : CardEffect
    {
		ProgrammingCard programmingCard;

		///<inheritdoc/>
		public override void PerformEffect(IGameState gameState, Robot cardPlayer,
			int cardRegisterNumber)
		{
			//this effect tries to "burn" one card from players' discard
			//pile ... this is used by e.g. spam folder card
			if (cardPlayer.TryBurnCardFromDiscardPile(programmingCard))
				gameState.Notifier.RobotBurnedCard(cardPlayer.Name, programmingCard.Name);
		}

		public DiscardEffect(ProgrammingCard programmingCard)
		{
			this.programmingCard = programmingCard;
		}
	}
}
