﻿using RoboRoboRally.Model.CardDB.Cards.CardEffects;

namespace RoboRoboRally.Model.CardDB.Cards
{
	/// <summary>
	/// Class for damage cards. See <see cref="ProgrammingCard"/>
	/// for more details.
	/// </summary>
    public class DamageCard : ProgrammingCard
    {
		/// <summary>
		/// Ctor with card's name and it's effects.
		/// </summary>
		public DamageCard(string name, CardEffect[] cardEffects) :
			base(name, cardEffects)
		{
		}
    }
}
