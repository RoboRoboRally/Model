﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks
{
	/// <summary>
	/// This hook is used by e.g. Refresh upgrade card. It's effect 
	/// is: "Change programming in your current register to one of these cards:
	/// Move 1, Move 2, ..."
	/// </summary>
	class ChangeRegisterProgrammingHook : ChoiceHookBase<HookRetBase, HookArgsBase, ProgrammingCard>
	{
		/// <inheritdoc/>
		public ChangeRegisterProgrammingHook(UpgradeCardChoice[] choices, ChoiceValue<ProgrammingCard>[] choiceValues) :
			base(choices, choiceValues)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().BeforePlayingRegisterStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().BeforePlayingRegisterStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		protected override HookRetBase PerformHookImpl(HookArgsBase arg, HookRetBase defaultValue, ProgrammingCard takenChoice)
		{
			arg.Robot.ChangeCurrentRegisterProgramming(arg.GameState, takenChoice);

			return defaultValue;
		}
	}
}
