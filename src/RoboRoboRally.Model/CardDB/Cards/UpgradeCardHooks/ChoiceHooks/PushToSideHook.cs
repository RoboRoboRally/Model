﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks
{
	/// <summary>
	/// This hook is used by e.g. Side arms upgrade card. It's effect
	/// is: "when you push a robot, you may choose to push it to the left or right".
	/// </summary>
	class PushToSideHook : ChoiceHookBase<PushDirectionStepRet, HookArgsBase, RotationType>
	{
		//even though the rotation type may seem weird, it actually works -- we can not implement
		//this with direction, as the push direction is relative to the direction passed into hook
		/// <inheritdoc/>
		public PushToSideHook(UpgradeCardChoice[] choices, ChoiceValue<RotationType>[] choiceValues) :
			base(choices, choiceValues)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<MovementAction>().PushDirectionStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<MovementAction>().PushDirectionStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		protected override PushDirectionStepRet PerformHookImpl(HookArgsBase arg, PushDirectionStepRet defaultValue, RotationType takenChoice)
		{
			return new PushDirectionStepRet() { Direction = defaultValue.Direction.Rotate(takenChoice), Amount = 1 };
		}
	}
}
