﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks.AsyncCardHooks
{
	/// <summary>
	/// This hook is used by e.g. Boink upgrade card. It's effect is:
	/// "Move to an adjacent space without changing direction".
	/// </summary>
	class MoveToAdjacentHook : ChoiceHookBase<HookRetBase, HookArgsBase, DirectionType>
    {
		/// <inheritdoc/>
		public MoveToAdjacentHook(UpgradeCardChoice[] choices, ChoiceValue<DirectionType>[] choiceValues) :
			base(choices, choiceValues)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			robot.AttachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			robot.DetachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		protected override HookRetBase PerformHookImpl(HookArgsBase arg, HookRetBase defaultValue, DirectionType takenChoice)
		{
			arg.GameState.GetAction<MovementAction>().PerformAction(new MovementActionArgs() { GameState = arg.GameState, Robot = arg.Robot, Amount = 1, Direction = new Direction(takenChoice) });

			return defaultValue;
		}
	}
}
