﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks.AsyncCardHooks
{
	/// <summary>
	/// This hook is used by e.g. Zoop upgrade card. It's effect is:
	/// "Rotate to face any direction".
	/// </summary>
	class RotateHook : ChoiceHookBase<HookRetBase, HookArgsBase, RotationType>
    {
		/// <inheritdoc/>
		public RotateHook(UpgradeCardChoice[] choices, ChoiceValue<RotationType>[] choiceValues) :
			base(choices, choiceValues)
		{ 	}

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			robot.AttachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			robot.DetachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		protected override HookRetBase PerformHookImpl(HookArgsBase arg, HookRetBase defaultValue, RotationType takenChoice)
		{
			arg.Robot.Rotate(arg.GameState, takenChoice);

			return defaultValue;
		}
	}
}
