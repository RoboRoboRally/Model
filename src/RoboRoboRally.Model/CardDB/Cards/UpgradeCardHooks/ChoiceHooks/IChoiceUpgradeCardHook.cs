﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks
{
	/// <summary>
	/// This namespace contains all upgrade card hooks with choices 
	/// (including the async ones).
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{ }

	/// <summary>
	/// Base interface for choice hooks.
	/// </summary>
	interface IChoiceUpgradeCardHookBase : IUpgradeCardHookBase
	{
		/// <summary>
		/// Returns choices of the this choice hook.
		/// </summary>
		UpgradeCardChoice[] GetChoices();
	}

	/// <summary>
	/// Base typed interface for choice hooks.
	/// </summary>
	interface IChoiceUpgradeCardHook<TRet, TArg> : IChoiceUpgradeCardHookBase, IUpgradeCardHook<TRet, TArg> where TRet : HookRetBase where TArg : HookArgsBase
	{
		/// <summary>
		/// This method represents hook's main method and it performs the
		/// ugprade card's effect. First argument represents hook arguments with
		/// robot, game state and other possible arguments (like robots that were
		/// shot by the upgrade card owner). The second argument represents
		/// default value that should be modified by this hook and returned. Some
		/// hooks behave like this - they first take some input value - like 
		/// number of programming cards at the start of programming phase
		/// and they modify it - for example increment it by one. In the case
		/// of "void" hooks, an empty struct is passed as default value, then
		/// returned and ignored. Last argument represents option choice 
		/// performed by player.
		/// </summary>
		TRet PerformHook(TArg arg, TRet defaultValue, UpgradeCardChoice takenChoice);		
	}

	/// <summary>
	/// Base class for choice hooks from which they should inherit.
	/// </summary>
	abstract class ChoiceHookBase<TRet, TArg, TChoice> : IChoiceUpgradeCardHook<TRet, TArg> where TRet : HookRetBase where TArg : HookArgsBase
	{
		/// <summary>
		/// Choices of this hook.
		/// </summary>
		protected UpgradeCardChoice[] Choices;

		/// <summary>
		/// Values associated with each choice in the <see cref="Choices"/>
		/// list.
		/// </summary>
		protected ChoiceValue<TChoice>[] ChoiceValues;

		/// <inheritdoc/>
		public abstract void AttachHook(IGameState gameState, Robot robot);

		/// <inheritdoc/>
		public abstract void DetachHook(IGameState gameState, Robot robot);

		/// <summary>
		/// Main method of each hook that has to be implemented by
		/// derived classes. Interpretation of arguments is same as with
		/// method <see cref="PerformHook(TArg, TRet, UpgradeCardChoice)"/>.
		/// </summary>
		protected abstract TRet PerformHookImpl(TArg arg, TRet defaultValue, TChoice takenChoice);

		/// <inheritdoc/>
		public UpgradeCardChoice[] GetChoices()
		{
			return Choices;
		}

		/// <inheritdoc/>
		public virtual bool VerifyHookUse(TArg arg)
		{
			return true;
		}

		/// <inheritdoc/>
		public TRet PerformHook(TArg arg, TRet defaultValue, UpgradeCardChoice takenChoice)
		{
			foreach (var choice in ChoiceValues)
			{
				if (choice.ChoiceCardName == takenChoice.ChoiceCardName)
					return PerformHookImpl(arg, defaultValue, choice.Value);
			}
			throw new RunningGameException("can not perform choice hook - mismatch between taken choice and choice values");
		}

		/// <summary>
		/// Ctor with choices and their choice values.
		/// </summary>
		protected ChoiceHookBase(UpgradeCardChoice[] choices, ChoiceValue<TChoice>[] choiceValues)
		{
			this.Choices = choices;
			this.ChoiceValues = choiceValues;
		}
	}

	/// <summary>
	/// Helper struct that associates a choice card
	/// with some actual value.
	/// </summary>
	struct ChoiceValue<T>
	{
		/// <summary>
		/// Choice value.
		/// </summary>
		public T Value { get; set; }

		/// <summary>
		/// Name of the choice card.
		/// </summary>
		public string ChoiceCardName { get; set; }
	}

}
