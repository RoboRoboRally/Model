﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Scrambler upgrade card. It's effect is:
	/// "if you attack a robot, that player replaces the card
	/// in their next register with the top card of their deck, 
	/// unless the next register is the final register". 
	/// </summary>
	class CardReplacementHook : ChoicelessHookBase<HookRetBase, ShotTargetsStepArgs>
	{
		/// <inheritdoc/>
		public CardReplacementHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		private List<Robot> FilterAffectedRobots(ShotTargetsStepArgs arg)
		{
			//this hook replaces card in the next register for each robot that was shot
			//however ... we do not want to include robots that are no longer active

			List<Robot> affectedRobots = new List<Robot>();

			foreach (var r in arg.ShotRobots)
			{
				if (arg.GameState.TurnActiveRobots.Contains(r))
				{
					if (r.RegistersPlayedThisTurn != arg.GameState.RegisterCount)
						affectedRobots.Add(r);
				}
			}

			return affectedRobots;
		}

		public override bool VerifyHookUse(ShotTargetsStepArgs arg)
		{
			if (FilterAffectedRobots(arg).Count > 0)
				return true;

			return false;
		}

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShotTargetsStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShotTargetsStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(ShotTargetsStepArgs arg, HookRetBase defaultValue)
		{
			var gs = arg.GameState;

			var affectedRobots = FilterAffectedRobots(arg);

			foreach (var affectedRobot in affectedRobots)
			{
				var drawnCard = affectedRobot.DrawCards(1)[0];
				affectedRobot.ReplaceRegisterCard(gs, drawnCard, affectedRobot.RegistersPlayedThisTurn + 1);
			}

			return defaultValue;
		}
	}
}
