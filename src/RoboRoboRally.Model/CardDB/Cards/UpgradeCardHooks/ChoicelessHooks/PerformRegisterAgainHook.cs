﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Hack upgrade card. It's effect is:
	/// "Execute insruction in your current register again".
	/// </summary>
	class PerformRegisterAgainHook : ChoicelessHookBase<HookRetBase, HookArgsBase>
	{
		/// <inheritdoc/>
		public PerformRegisterAgainHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().AfterPlayingRegisterStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().AfterPlayingRegisterStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(HookArgsBase arg, HookRetBase defaultValue)
		{
			arg.Robot.PlayRegisterAgain(arg.GameState, arg.Robot.RegistersPlayedThisTurn);

			return defaultValue;
		}
	}
}
