﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks
{
	/// <summary>
	/// Hook that is used by several upgrade cards, e.g. sandbox routine.
	/// It's effect is: "Add the sandbox routine programming card to your discard pile".
	/// </summary>
	class AddProgrammingCardHook : ChoicelessHookBase<HookRetBase, HookArgsBase>
	{
		ProgrammingCard cardToAdd;

		/// <summary>
		/// Ctor with a programming card that is received by the 
		/// player that used upgrade card asociated with this hook.
		/// </summary>
		public AddProgrammingCardHook(ProgrammingCard cardToAdd):
			base(false)
		{
			this.cardToAdd = cardToAdd;
		}

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			robot.AttachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			robot.DetachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(HookArgsBase arg, HookRetBase defaultValue)
		{
			arg.Robot.DiscardCards(new ProgrammingCard[1] { cardToAdd });

			return defaultValue;
		}
	}
}
