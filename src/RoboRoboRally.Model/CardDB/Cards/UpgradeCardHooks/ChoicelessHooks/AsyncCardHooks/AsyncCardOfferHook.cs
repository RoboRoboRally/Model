﻿using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks
{
	/// <summary>
	/// This hook is used by every async card - it is invoked at the
	/// beggining of activation phase and it offers the asociated
	/// upgrade card for use.
	/// </summary>
    class AsyncCardOfferHook : ChoicelessHookBase<HookRetBase, HookArgsBase>
    {
		public AsyncCardOfferHook() :
			base(false)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().BeforeActivationPhaseStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().BeforeActivationPhaseStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(HookArgsBase arg, HookRetBase defaultValue)
		{
			//we do not have to check that robot is active, this is already checked

			var card = arg.Robot.GetUpgradeCardForHook(this);
			
			foreach (var hook in card.GetHooks())
			{
				if (hook is IChoiceUpgradeCardHookBase casted)
				{
					var choices = casted.GetChoices();

					arg.GameState.InputHandler.OfferAsyncChoiceUpgradeCard(arg.Robot.Name, card.Name, choices);
					
					return defaultValue;
				}
			}

			arg.GameState.InputHandler.OfferAsyncChoicelessUpgradeCard(arg.Robot.Name, card.Name);

			return defaultValue;
		}
	}
}
