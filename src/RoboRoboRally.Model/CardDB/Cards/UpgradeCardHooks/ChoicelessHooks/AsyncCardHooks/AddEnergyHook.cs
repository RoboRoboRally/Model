﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks
{
	/// <summary>
	/// Hook that is used by e.g. Recharge upgrade card. It's effect is:
	/// "Gain three energy".
	/// </summary>
    class AddEnergyHook : ChoicelessHookBase<HookRetBase, HookArgsBase>
    {
		int energyAmount;

		/// <summary>
		/// Ctor with amount of energy that is obtained
		/// upon invoking this hook.
		/// </summary>
		public AddEnergyHook(int energyAmount) :
			base(false)
		{
			this.energyAmount = energyAmount;
		}

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			robot.AttachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			robot.DetachAsyncHook(robot.GetUpgradeCardForHook(this).Name, this);
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(HookArgsBase arg, HookRetBase defaultValue)
		{
			arg.Robot.ChangeEnergyAmount(arg.GameState, energyAmount);

			return defaultValue;
		}
	}
}
