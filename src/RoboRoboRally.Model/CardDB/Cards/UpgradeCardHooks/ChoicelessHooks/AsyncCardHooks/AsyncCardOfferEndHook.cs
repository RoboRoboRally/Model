﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks
{
	/// <summary>
	/// This hook is used by every async card - it is invoked at the end 
	/// of activation phase and it ends offer to use the asociated
	/// upgrade card.
	/// </summary>
    class AsyncCardOfferEndHook : ChoicelessHookBase<HookRetBase, HookArgsBase>
    {
		public AsyncCardOfferEndHook() :
			base(false)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().AfterActivationPhaseStep.AttachHook(robot, this);
			//we need to be careful - the offer also ends with reboot ...
			gameState.GetAction<RebootAction>().BeforeRebootStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ActivationPhase>().AfterActivationPhaseStep.DetachHook(robot, this);
			gameState.GetAction<RebootAction>().BeforeRebootStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(HookArgsBase arg, HookRetBase defaultValue)
		{
			var card = arg.Robot.GetUpgradeCardForHook(this);

			arg.GameState.InputHandler.EndAsyncUpgradeCardOffer(arg.Robot.Name, card.Name);

			return defaultValue;
		}
	}
}
