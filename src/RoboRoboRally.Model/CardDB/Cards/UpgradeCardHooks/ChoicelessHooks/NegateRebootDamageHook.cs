﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Firewall upgrade card. It's effect is:
	/// "take no spam damage cards when rebooting".
	/// </summary>
	class NegateRebootDamageHook : ChoicelessHookBase<DamageStepRet, HookArgsBase>
	{
		/// <inheritdoc/>
		public NegateRebootDamageHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<RebootAction>().RebootDamageStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<RebootAction>().RebootDamageStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override DamageStepRet PerformHook(HookArgsBase arg, DamageStepRet defaultValue)
		{
			return new DamageStepRet() { Damage = new Damage() { DamageCards = new DamageCard[0] } };
		}
	}
}
