﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// This namespace contains all choiceless upgrade card hooks
	/// (including the async ones).
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{ }

	/// <summary>
	/// Base interface for choiceless hooks.
	/// </summary>
	interface IChoicelessUpgradeCardHook<TRet, TArg> : IUpgradeCardHook<TRet, TArg> where TRet : HookRetBase where TArg : HookArgsBase
	{
		/// <summary>
		/// This method represents hook's main method and it performs the
		/// ugprade card's effect. First argument represents hook arguments with
		/// robot, game state and other possible arguments (like robots that were
		/// shot by the upgrade card owner) and the second argument represents
		/// default value that should be modified by this hook and returned. Some
		/// hooks behave like this - they first take some input value - like 
		/// number of programming cards at the start of programming phase
		/// and they modify it - for example increment it by one. In the case
		/// of "void" hooks, an empty struct is passed as default value, then
		/// returned and ignored.
		/// </summary>
		TRet PerformHook(TArg arg, TRet defaultValue);

		/// <summary>
		/// Returns wheter using this hook requires player's confirmation.
		/// </summary>
		bool RequiresApprove();
	}

	/// <summary>
	/// Base class for choiceless hook from which they should inherit.
	/// </summary>
	abstract class ChoicelessHookBase<TRet, TArg> : IChoicelessUpgradeCardHook<TRet, TArg> where TRet : HookRetBase where TArg : HookArgsBase
	{
		/// <summary>
		/// Flag, whether or not this hook requires approve.
		/// </summary>
		protected bool requiresApprove;

		/// <inheritdoc/>
		public abstract void AttachHook(IGameState gameState, Robot robot);

		/// <inheritdoc/>
		public abstract void DetachHook(IGameState gameState, Robot robot);

		/// <inheritdoc/>
		public abstract TRet PerformHook(TArg arg, TRet defaultValue);
		

		/// <inheritdoc/>
		public bool RequiresApprove()
		{
			return requiresApprove;
		}

		/// <inheritdoc/>
		public virtual bool VerifyHookUse(TArg arg)
		{
			return true;
		}

		/// <summary>
		/// Ctor with flag that indicates, whether this hook 
		/// requires confirmation from player or not.
		/// </summary>
		protected ChoicelessHookBase(bool requiresApprove)
		{
			this.requiresApprove = requiresApprove;
		}
	}


}
