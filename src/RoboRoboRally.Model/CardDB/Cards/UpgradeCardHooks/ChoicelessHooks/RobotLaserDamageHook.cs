﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by several upgrades, e.g. Double barrel laser upgrade card.
	/// It's effect is: "deal one additional SPAM damage card to any robot you shoot".
	/// </summary>
	class RobotLaserDamageHook : ChoicelessHookBase<DamageStepRet, ShotTargetsStepArgs>
    {
		Damage additionalDamage;

		/// <summary>
		/// Ctor with flag that indicates, whether this
		/// flag requires confirmation from player and with
		/// additional damage, that is dealt to player after it got shot.
		/// </summary>
		public RobotLaserDamageHook(bool requiresApprove, Damage additionalDamage):
			base(requiresApprove)
		{
			this.additionalDamage = additionalDamage;
		}

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().LaserDamageStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().LaserDamageStep.DetachHook(robot, this);
		}

		public override bool VerifyHookUse(ShotTargetsStepArgs arg)
		{
			if (arg.ShotRobots.Count > 0)
				return true;
			return false;
		}

		/// <inheritdoc/>
		public override DamageStepRet PerformHook(ShotTargetsStepArgs arg, DamageStepRet defaultValue)
		{
			var dmg = defaultValue.Damage.DamageCards.ToList();
			dmg.AddRange(additionalDamage.DamageCards.ToList());

			defaultValue.Damage = new Damage() { DamageCards = dmg.ToArray() };

			return defaultValue;
		}
	}
}
