﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;
using System.Linq;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by multiple upgrade cards, e.g. Virus module upgrade card.
	/// It's effect is "when you push a robot, give that player one virus damage card".
	/// </summary>
	class PushDamageHook : ChoicelessHookBase<DamageStepRet, HookArgsBase>
	{
		Damage additionalDamage;

		/// <summary>
		/// Ctor with flag that indicates, whether this
		/// flag requires confirmation from player and with
		/// damage, that is dealt to player during push.
		/// </summary>
		public PushDamageHook(bool requiresApprove, Damage additionalDamage):
			base(requiresApprove)
		{
			this.additionalDamage = additionalDamage;
		}

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<MovementAction>().PushDamageStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<MovementAction>().PushDamageStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override DamageStepRet PerformHook(HookArgsBase arg, DamageStepRet defaultValue)
		{
			var dmg = defaultValue.Damage.DamageCards.ToList();
			dmg.AddRange(additionalDamage.DamageCards.ToList());

			defaultValue.Damage = new Damage() { DamageCards = dmg.ToArray() };

			return defaultValue;
		}
	}
}
