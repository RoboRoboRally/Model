﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB.Tiles;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Rail gun upgrade card. It's effect is:
	/// "you may shoot through any number of walls and/or robots".
	/// </summary>
	class RobotLaserTargetsHook : ChoicelessHookBase<ShootingTargetsStepRet, ShootingTargetsStepArgs>
	{
		/// <inheritdoc/>
		public RobotLaserTargetsHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShootingTargetsStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShootingTargetsStep.DetachHook(robot, this);
		}

		public override bool VerifyHookUse(ShootingTargetsStepArgs arg)
		{
			var directions = arg.ShootingDirections;

			bool use = false;

			foreach (var dir in directions)
			{
				var curCoords = arg.Robot.Position;

				while (true)
				{
					var nextCoords = curCoords + dir;

					if (arg.GameState.IsTileOccupied(nextCoords))
					{
						use = true;
						break;
					}

					if (arg.GameState.Map[nextCoords] is OutOfMapTile ||
						arg.GameState.Map[nextCoords] is PriorityAntennaTile)
						break;

					if (arg.GameState.Map.IsWallBetween(curCoords, nextCoords))
					{
						use = true;
						break;
					}

					curCoords = nextCoords;
				}
			}

			return use;
		}

		/// <inheritdoc/>
		public override ShootingTargetsStepRet PerformHook(ShootingTargetsStepArgs arg, ShootingTargetsStepRet defaultValue)
		{
			var directions = arg.ShootingDirections;

			foreach (var direction in directions)
			{
				var curCoords = arg.Robot.Position;

				while (true)
				{
					var nextCoords = curCoords + direction;

					if (arg.GameState.IsTileOccupied(nextCoords, out Robot occRobot))
					{
						defaultValue.RobotsToShoot.Add(occRobot);
					}

					if (arg.GameState.Map[nextCoords] is OutOfMapTile ||
						arg.GameState.Map[nextCoords] is PriorityAntennaTile)
						break;

					curCoords = nextCoords;
				}
			}

			return defaultValue;
		}
	}
}
