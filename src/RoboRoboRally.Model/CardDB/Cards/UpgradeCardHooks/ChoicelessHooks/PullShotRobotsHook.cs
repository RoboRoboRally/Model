﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Tractor beam upgrade card. It's effect is:
	/// "when you shoot a robot, you may pull it toward you
	/// one space (unless it is on adjacent tile)".
	/// </summary>
	class PullShotRobotsHook : ChoicelessHookBase<HookRetBase, ShotTargetsStepArgs>
	{
		/// <inheritdoc/>
		public PullShotRobotsHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShotTargetsStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShotTargetsStep.DetachHook(robot, this);
		}

		private List<Robot> FilterAffectedRobots(ShotTargetsStepArgs arg)
		{
			//in combination with pull upgrade, weird things can happen ...
			//we will only pull those robots, that remain on the axes with the shooting robot

			var shootingRobot = arg.Robot;

			List<Robot> affectedRobots = arg.ShotRobots.Where(
				x => x.Position.RowIndex == shootingRobot.Position.RowIndex ||
				x.Position.ColIndex == shootingRobot.Position.ColIndex).ToList();

			for (int i = 1; ; ++i)
			{
				//we do not pull robots that are packed around the shooting robot
				if (affectedRobots.RemoveAll(x => x.Position.GetManhattanDistance(shootingRobot.Position) == i) == 0)
					break;
			}

			return affectedRobots;
		}

		/// <inheritdoc/>
		public override bool VerifyHookUse(ShotTargetsStepArgs arg)
		{
			if (FilterAffectedRobots(arg).Count > 0)
				return true;

			return false;
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(ShotTargetsStepArgs arg, HookRetBase defaultValue)
		{			
			var shootingRobot = arg.Robot;

			var affectedRobots = FilterAffectedRobots(arg);			

			//now, finally, order the remaining affected robots by distance to shooting robot
			//(in ascending order, we will be pushing the robots from the closest to furthest)
			var orderedAffectedRobots = affectedRobots.OrderBy(x => x.Position.GetManhattanDistance(shootingRobot.Position));

			foreach (var affectedRobot in orderedAffectedRobots)
			{
				//now, push each affected robot towards the shooting robot

				DirectionType pushDir;
				if (affectedRobot.Position.ColIndex == shootingRobot.Position.ColIndex)
				{
					pushDir = affectedRobot.Position.RowIndex < shootingRobot.Position.RowIndex ? DirectionType.Down : DirectionType.Up;
				}
				else
				{
					pushDir = affectedRobot.Position.ColIndex < shootingRobot.Position.ColIndex ? DirectionType.Right : DirectionType.Left;
				}

				arg.GameState.GetAction<PushAction>().PerformAction(
					new PushActionArgs() { GameState = arg.GameState, Robot = affectedRobot, Amount = 1, Direction = new Direction(pushDir) }
					);
			}

			//some of the robots could end up on pit tiles, so we have to reboot them
			//(remember, push action does not handle reboots)
			foreach (var robot in arg.GameState.GameActiveRobots)
			{
				if (arg.GameState.Map.IsRebootPosition(robot.Position))
				{
					arg.GameState.GetAction<RebootAction>().PerformAction(
						new ActionArgsBase() { GameState = arg.GameState, Robot = robot });
				}
			}

			return defaultValue;
		}
	}
}
