﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Core.Phases;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Memory stick upgrade card. It's effect is:
	/// "draw one additional programming card at the start of each round".
	/// </summary>
	class IncrementDrawAmountHook : ChoicelessHookBase<DrawAmountStepRet, HookArgsBase>
	{
		/// <inheritdoc/>
		public IncrementDrawAmountHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ProgrammingPhase>().DrawAmountStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetPhase<ProgrammingPhase>().DrawAmountStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override DrawAmountStepRet PerformHook(HookArgsBase arg, DrawAmountStepRet defaultValue)
		{
			return new DrawAmountStepRet() { DrawAmount = defaultValue.DrawAmount + 1 };
		}
	}
}
