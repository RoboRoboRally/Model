﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Rear laser upgrade card. It's effect is:
	/// "your robot may shoot backwards as well as forward".
	/// </summary>
	class RobotLaserDirectionHook : ChoicelessHookBase<ShootingDirectionStepRet, HookArgsBase>
	{
		/// <inheritdoc/>
		public RobotLaserDirectionHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShootingDirectionStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShootingDirectionStep.DetachHook(robot, this);
		}

		/// <inheritdoc/>
		public override ShootingDirectionStepRet PerformHook(HookArgsBase arg, ShootingDirectionStepRet defaultValue)
		{
			defaultValue.Directions.Add(arg.Robot.FacingDirection.GetOppositeDirection());

			return defaultValue;
		}
	}
}
