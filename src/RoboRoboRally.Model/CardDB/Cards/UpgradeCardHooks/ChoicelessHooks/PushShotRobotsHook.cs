﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks
{
	/// <summary>
	/// Hook that is used by e.g. Pressor beam upgrade card. It's effect is:
	/// "you may push any robot you shoot one space in the direction you are shooting".
	/// </summary>
	class PushShotRobotsHook : ChoicelessHookBase<HookRetBase, ShotTargetsStepArgs>
    {
		/// <inheritdoc/>
		public PushShotRobotsHook(bool requiresApprove) :
			base(requiresApprove)
		{ }

		/// <inheritdoc/>
		public override void AttachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShotTargetsStep.AttachHook(robot, this);
		}

		/// <inheritdoc/>
		public override void DetachHook(IGameState gameState, Robot robot)
		{
			gameState.GetAction<ShootingAction>().ShotTargetsStep.DetachHook(robot, this);
		}

		private List<Robot> FilterAffectedRobots(ShotTargetsStepArgs arg)
		{
			//in combination with pull upgrade, weird things can happen ...
			//we will only pull those robots, that remain on the axes with the shooting robot
			var shootingRobot = arg.Robot;

			List<Robot> affectedRobots = arg.ShotRobots.Where(
				x => x.Position.RowIndex == shootingRobot.Position.RowIndex ||
				x.Position.ColIndex == shootingRobot.Position.ColIndex).ToList();

			return affectedRobots;
		}

		/// <inheritdoc/>
		public override bool VerifyHookUse(ShotTargetsStepArgs arg)
		{
			if (FilterAffectedRobots(arg).Count > 0)
				return true;
			return false;
		}

		/// <inheritdoc/>
		public override HookRetBase PerformHook(ShotTargetsStepArgs arg, HookRetBase defaultValue)
		{
			var shootingRobot = arg.Robot;

			var affectedRobots = FilterAffectedRobots(arg); 

			//we want the robots sorted by DESCENDING order - we are pushing to robots
			//from furthest to closest (from bigger to smaller distances)
			var affectedRobotsOrdered = affectedRobots.OrderByDescending(x => x.Position.GetManhattanDistance(shootingRobot.Position));

			foreach (var affectedRobot in affectedRobotsOrdered)
			{
				//now, push each affected robot towards the shooting robot

				DirectionType pushDir;
				if (affectedRobot.Position.ColIndex == shootingRobot.Position.ColIndex)
				{
					pushDir = affectedRobot.Position.RowIndex < shootingRobot.Position.RowIndex ? DirectionType.Up : DirectionType.Down;
				}
				else
				{
					pushDir = affectedRobot.Position.ColIndex < shootingRobot.Position.ColIndex ? DirectionType.Left : DirectionType.Right;
				}

				arg.GameState.GetAction<PushAction>().PerformAction(
					new PushActionArgs() { GameState = arg.GameState, Robot = affectedRobot, Amount = 1, Direction = new Direction(pushDir) }
					);
			}

			//some of the robots could end up on pit tiles, so we have to reboot them
			//(remember, push action does not handle reboots)
			foreach (var robot in arg.GameState.GameActiveRobots)
			{
				if (arg.GameState.Map.IsRebootPosition(robot.Position))
				{
					arg.GameState.GetAction<RebootAction>().PerformAction(
						new ActionArgsBase() { GameState = arg.GameState, Robot = robot });
				}
			}

			return defaultValue;
		}
	}
}
