﻿using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks
{
	/// <summary>
	/// This namespace contains effects of upgrade cards - their hooks.
	/// In order to understand classes in this namespace, first
	/// read the explanation about implementation of upgrade cards.
	/// (see <see cref="UpgradeCard"/>).
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{ }

	/// <summary>
	/// Interface for each hook - each hook can attach and detach itself.
	/// </summary>
	interface IUpgradeCardHookBase
	{
		/// <summary>
		/// Attaches hook.
		/// </summary>
		void AttachHook(IGameState gameState, Robot robot);
		/// <summary>
		/// Detaches hook.
		/// </summary>
		void DetachHook(IGameState gameState, Robot robot);
	}

	/// <summary>
	/// Typed interface for hooks - each hook receives in its main method
	/// some arguments, and it returns some value (in the case of "void" hooks
	/// this returned values is an empty struct that is ignored).
	/// </summary>
	interface IUpgradeCardHook<TRet, TArg> : IUpgradeCardHookBase where TRet : HookRetBase where TArg : HookArgsBase 
	{
		/// <summary>
		/// Special purpose method - some hooks need to first verify their
		/// arguments before they are used.
		/// </summary>
		/// <returns>true if the hook should be used, false
		/// otherwise</returns>
		bool VerifyHookUse(TArg arg);
	}	
}
