﻿using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks;
using RoboRoboRally.Model.Core;

namespace RoboRoboRally.Model.CardDB.Cards
{
	/// <summary>
	/// Type of upgrade card.
	/// </summary>
	public enum UpgradeCardType
	{
		/// <summary>
		/// Permanent upgrade card type.
		/// </summary>
		Permanent,
		/// <summary>
		/// Temporary upgrade card type.
		/// </summary>
		Temporary
	}

	/// <summary>
	/// <para>
	/// Helper struct that is used to store choices
	/// of upgrade cards with choices. See <see cref="UpgradeCard"/>
	/// for further explanation.
	/// </para>
	/// 
	/// <para>
	/// Each upgrade card choice is represented simply as a special
	/// type of card.
	/// </para>
	/// </summary>
	public struct UpgradeCardChoice
	{
		/// <summary>
		/// Name of the choice card.
		/// </summary>
		public string ChoiceCardName { get; set; }
	}

	/// <summary>
	/// <para>
	/// This class represents a single upgrade card.
	/// Upgrade cards are one of the most complicated 
	/// systems in the whole model - there are many upgrade
	/// cards that can affect various game mechanics and coming
	/// up with a system that would support this and be extensible
	/// in a sensible manner was a tough piece. Current system of
	/// upgrade cards support about 24 upgrade cards from the original
	/// set of 40 cards. This is mainly because some upgrades in the original
	/// game were resistant to any general design whatsover. If one would 
	/// want to support them, some portion of the current system would 
	/// need to be rethinked and reworked.
	/// </para>
	/// 
	/// <para>
	/// There are several types of upgrade cards. First,
	/// we distinguish between two types of upgrade cards - 
	/// there are upgrade cards with choices - when player 
	/// wants to use them, he must take some option. For example
	/// there is upgrade card that allows player to be rotate
	/// to any direction. Such card would be considered a choice
	/// card and the rotation direction are it's choices.
	/// Then, there are choiceless upgrade cards. These do not
	/// have any choices.
	/// </para>
	/// 
	/// <para>
	/// With choiceless cards, we further distinguish ones that
	/// require approve from the user and those that do not.
	/// For example - there is an upgrade card, that makes player
	/// draw one more programming card at the start of each phase,
	/// this upgrade does not require any approve from the user and
	/// it is performed automatically. Then, there is upgrade
	/// that allows player to pull that he shot one space towards him - 
	/// this upgrade requires player's approve, as the benefit 
	/// of pulling the players is uncertain.
	/// </para>
	/// 
	/// <para>
	/// Now, as previously said, upgrade cards can affect many game
	/// mechanics. There are upgrade cards that take effect at the
	/// beggining of programming phase and there are upgrades that
	/// take effect after robot's shooting. In order to implement this,
	/// there have to be some parts of code onto which the upgrade cards
	/// can "hook" when player obtains a certain upgrade card and that
	/// would take effect when execution passes through these parts of code.
	/// We call these parts of code HookableGameSteps. Now, each 
	/// upgrade cards has some hooks associated with it. These 
	/// are basically effects of the upgrade cards. Now, when player obtains
	/// an upgrade card, these hooks hook to a prepared hookable game step.
	/// When code passes through such step, first, we decide if such hook
	/// requires some input from the player (choices or approve) - asks player
	/// for this response and if he wants to use that card, the hook is performed
	/// and affects some game mechanic.
	/// </para>
	/// 
	/// <para>
	/// There are many hookable game steps defined. All of them can be seen
	/// in namespace <see cref="Core.HookableGameSteps"/>. They are just empty
	/// classes, as each hookable game step performs the same steps and they
	/// only differ in types of accepted paramaters and types of returned values.
	/// The steps thmeselves are placed in several different classes (meaning, when
	/// they are invoked). There are several of them in activation phase, or in some
	/// of the actions.
	/// The main body of each such step is in the base class, namely
	/// <see cref="Core.HookableGameSteps.HookableGameStep{TRet, TArg}"/>.
	/// It is worth noting, that this single base class supports both
	/// hooks that return, or modify some value as well as "void" hooks.
	/// Such hooks simply return an empty value that is ignored.
	/// </para>
	/// 
	/// <para>
	/// Now, the effects of individual upgrade cards - the hooks, can be found
	/// in namespace <see cref="UpgradeCardHooks"/>. As with steps, there 
	/// are many of them as each upgrade card required different step. It
	/// is important to note, that as we talked about different types of ugprade
	/// cards, this was transformed onto the hooks themselves. So, there are
	/// choiceless hooks, hooks that require approve and hooks with choices.
	/// This allows one upgrade card to have multiple varying effects. Despite
	/// this, there are some wierd combinations that do not make sense. For 
	/// example, it does not make sense to have a temporary upgrade card
	/// with choiceless approve hook, however, current system allows it.
	/// </para>
	/// 
	/// <para>
	/// There is last class of upgrade cards, that does not work completely
	/// with the designed systems. In the already introduced cases, 
	/// each upgrade had some predefined moments when they can be used, like
	/// at the beggining of programming phase. At such moments, model asks
	/// player if he wants to use that upgrade (or if it did not require any input
	/// then used it), waits for response and continues on.
	/// However, there is class of upgrade cards, that can be used through
	/// a longer period, like through the whole activation phase. As using these
	/// upgrade cards is from the perspective of model an asychronous event, we 
	/// call such cards "async cards". An example of this card is a card 
	/// that allows player to rotate to any direction. As with other cards,
	/// we distinguish between choiceless and choice async cards.
	/// </para>
	/// 
	/// <para>
	/// Async cards could not be implemented the same way with hookable game steps
	/// as other cards and several things had to be changed for them to work.
	/// Hooks are still used - they still largely mean the card's effect, but
	/// hookable game steps are not used the same way. They are used only
	/// for two things - at the start of activation phase, the card is offerred
	/// for the player to play and at the end of this phase, this offer is ended 
	/// (unless player actually plays that card meantime).
	/// </para>
	/// 
	/// <para>
	/// Now, when an async card is used, a special message is passed from 
	/// client (and then server) to model through class <see cref="Game"/>.
	/// Such async messages are handled by model at certain time points.
	/// For example, the message queue is checked after activation of
	/// each static map element. When such message arrives, it is processed
	/// inside method <see cref="GameState.ProcessAsyncUpgradeCardUses"/>
	/// and an appropriate hook attached to robot that owns the upgrade
	/// is invoked.
	/// </para>
	/// </summary>
	public class UpgradeCard : Card
	{
		IUpgradeCardHookBase[] hooks;

		/// <summary>
		/// Type of upgrade card.
		/// </summary>
		public UpgradeCardType UpgradeCardType { get; private set; }

		/// <summary>
		/// Energy cost of upgrade card.
		/// </summary>
		public int EnergyCost { get; private set; }

		internal IUpgradeCardHookBase[] GetHooks()
		{
			return hooks;
		}

		internal void CardObtained(IGameState gameState, Robot robot)
		{
			//before we call attach hook, we have to be sure that the card was placed
			foreach (var hook in hooks)
				hook.AttachHook(gameState, robot);
		}

		internal void CardDiscarded(IGameState gameState, Robot robot)
		{
			foreach (var hook in hooks)
				hook.DetachHook(gameState, robot);
		}				

		internal UpgradeCard(string name, UpgradeCardType upgradeCardType,
			int energyCost, IUpgradeCardHookBase[] hooks) 
			: base(name)
		{
			this.hooks = hooks;
			this.EnergyCost = energyCost;
			this.UpgradeCardType = upgradeCardType;
		}
	}
}
