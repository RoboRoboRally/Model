﻿namespace RoboRoboRally.Model.CardDB.Cards
{
	

	/// <summary>
	/// Base class for each type of card.
	/// </summary>
    public abstract class Card
    {
		/// <summary>
		/// Card identifier.
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Ctor with card name (it's identifier).
		/// </summary>
		protected Card(string name)
		{
			this.Name = name;
		}
    }
}
