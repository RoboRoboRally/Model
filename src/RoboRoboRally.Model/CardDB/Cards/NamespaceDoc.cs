﻿namespace RoboRoboRally.Model.CardDB.Cards
{
	/// <summary>
	/// <para>
	/// This namespace contains definition of each card
	/// along with their logic. Namespace <see cref="CardEffects"/>
	/// contains logic of programming cards and namespace
	/// <see cref="UpgradeCardHooks"/> contains logic of 
	/// upgrade cards.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{
	}
}
