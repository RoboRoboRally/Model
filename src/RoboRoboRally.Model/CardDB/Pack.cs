﻿using System.Collections.Generic;
using System.Linq;
using RoboRoboRally.Model.CardDB.Cards;
using System.Collections;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.CardDB
{
	/// <summary>
	/// Helper data structures that is used to represent 
	/// different piles in the player's pack.
	/// </summary>
	class QueueList<T> : IEnumerable<T>
	{
		IList<T> elements;

		/// <summary>
		/// Returns number of elements in this queue list.
		/// </summary>
		public int Count()
		{
			return elements.Count;
		}

		/// <summary>
		/// Shuffles elements in this queue list randomly.
		/// </summary>
		public void Shuffle()
		{
			elements = elements.OrderBy<T, int>(_ => MyRandom.Random.Next()).ToList();			
		}

		/// <summary>
		/// Enqueues the passed item into this queue list.
		/// </summary>
		public void Enqueue(T item)
		{
			elements.Add(item);
		}

		/// <summary>
		/// Removes first occurrence of the passed item from this 
		/// queue list.
		/// </summary>
		/// <returns>true if any element was removed,
		/// false otherwise</returns>
		public bool Remove(T item)
		{
			return elements.Remove(item);
		}

		/// <summary>
		/// Enqueues list of new elements into this list.
		/// </summary>
		public void EnqueueList(QueueList<T> toAppend)
		{
			while (toAppend.Count() > 0)
			{
				var item = toAppend.Dequeue();
				Enqueue(item);
			}
		}

		/// <summary>
		/// Dequeues one element from this list. Does
		/// not perform any checks.
		/// </summary>
		public T Dequeue()
		{
			var item = elements[0];
			elements.RemoveAt(0);
			return item;
		}

		/// <summary>
		/// Returns enumerator that enumerates the elements
		/// from the start of the queue to its end.
		/// </summary>
		public IEnumerator<T> GetEnumerator()
		{
			return elements.GetEnumerator();
		}

		/// <summary>
		/// Returns enumerator that enumerates the elements
		/// from the start of the queue to its end.
		/// </summary>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return elements.GetEnumerator();
		}

		public QueueList()
		{
			elements = new List<T>();
		}
	}

	/// <summary>
	/// This class represents pack of upgrade cards.
	/// </summary>
	public class Pack<T> where T : Card
	{
		QueueList<T> drawingPile;
		QueueList<T> discardPile;

		/// <summary>
		/// Returns total size of the pack
		/// (size of discard pile + size of drawing pile).
		/// </summary>
		/// <returns></returns>
		public int Size()
		{
			return drawingPile.Count() + discardPile.Count();
		}

		/// <summary>
		/// Shuffles packs drawing pile.
		/// </summary>
		public void ShuffleDrawingPile()
		{
			drawingPile.Shuffle();
		}

		/// <summary>
		/// Places one card at the top of discard pile.
		/// </summary>
		public void DiscardCard(T card)
		{
			discardPile.Enqueue(card);
		}

		/// <summary>
		/// Places the passed card to the top of
		/// discard pile (card at index [0] is placed
		/// first and so on)
		/// </summary>
		public void DiscardCards(T[] cards)
		{
			foreach (var card in cards)
				DiscardCard(card);
		}

		//copies pack content - used in initialization of model game
		/// <summary>
		/// Copies current pack and returns a new pack.
		/// </summary>
		public Pack<T> CopyPack()
		{			
			IList<T> allCards = new List<T>();
			foreach (var card in drawingPile)
				allCards.Add(card);
			foreach (var card in discardPile)
				allCards.Add(card);

			IDictionary<T, int> cardNums = new Dictionary<T, int>();
			foreach (var card in allCards)
			{
				if (cardNums.ContainsKey(card))
					cardNums[card]++;
				else
					cardNums.Add(card, 1);
			}

			return new Pack<T>(cardNums);
		}

		private bool TryBurnFromPile(T card, QueueList<T> pile)
		{
			T cardToBurn = null;
			foreach (var c in pile)
			{
				if (c.Name == card.Name)
				{
					cardToBurn = c;
					break;
				}
			}

			return pile.Remove(cardToBurn);
		}

		/// <summary>
		/// This method tries to burn a single card
		/// from the discard pile and returns flag
		/// about the success.
		/// </summary>
		/// <returns>true if the card was burned,
		/// false otherwise</returns>
		public bool TryBurnFromDiscardPile(T card)
		{
			return TryBurnFromPile(card, discardPile);
		}
		
		/// <summary>
		/// Draws a single card from the pack.
		/// </summary>
		public T Draw()
		{
			//draw one card
			if (drawingPile.Count() == 0)
			{
				discardPile.Shuffle();
				drawingPile.EnqueueList(discardPile);
			}

			return drawingPile.Dequeue();
		}

		/// <summary>
		/// Draws up to amount of cards (can return
		/// less, that is a valid behavior).
		/// </summary>
		public T[] Draw(int amount)
		{
			List<T> toReturn = new List<T>();

			for (int i = 0; i < amount; ++i)
			{
				if (drawingPile.Count() == 0)
				{
					discardPile.Shuffle();
					drawingPile.EnqueueList(discardPile);

					//or maybe throw an exception?? but this can probably happen in a normal game..
					//e.g. we don't have enough temporary upgrades (as is the case in real game for 6 players)
					if (drawingPile.Count() == 0)
						break;
				}

				toReturn.Add(Draw());
			}

			return toReturn.ToArray();
		}

		/// <summary>
		/// Pack ctor that constructs the pack from
		/// the passed dictionary. That is, for each 
		/// key value pair, there will be value cards
		/// of the type key.
		/// </summary>
		public Pack(IDictionary<T, int> cards)
		{
			this.drawingPile = new QueueList<T>();
			foreach (var rec in cards)
			{
				for (int i = 0; i < rec.Value; ++i)
					this.drawingPile.Enqueue(rec.Key);
			}
			this.discardPile = new QueueList<T>();
		}

		/// <summary>
		/// Ctor that constructs the pack from the passed
		/// cards.
		/// </summary>
		public Pack(T[] cards)
		{
			this.drawingPile = new QueueList<T>();
			foreach (var c in cards)
				drawingPile.Enqueue(c);
			this.discardPile = new QueueList<T>();
		}
    }
}
