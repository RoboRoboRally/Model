﻿
namespace RoboRoboRally.Model.CardDB
{
	/// <summary>
	/// <para>
	/// This namespace contains everything that is related to cards.
	/// First, it contains class (see <see cref="CardLoader"/>) that is
	/// able to parse cards from the GameData database.
	/// Then, it also contains definition of programming cards and their effects (see
	/// <see cref="Cards.CardEffects"/>),
	/// upgrade cards and their effects (which we call hooks)
	/// (see <see cref="Cards.UpgradeCardHooks"/>) and
	/// other classes related to cards, like <see cref="Pack{T}"/> so on.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceGroupDoc
	{
	}
}
