﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using System.Linq;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.CardEffects;

namespace RoboRoboRally.Model.MapDB
{
	/// <summary>
	/// <para>
	/// This exception is used during loading of maps
	/// from GameData databse by class <see cref="MapLoader"/>.
	/// </para>
	/// 
	/// <para>
	/// For example, it is thrown if the maps can not
	/// be parsed due to invalid xml or some invalid values
	/// in elements.
	/// </para>
	/// </summary>
    public class MapDBParsingException : Exception
    {
		/// <summary>
		/// Exception ctor with messaga.
		/// </summary>
        public MapDBParsingException(string message):
            base("Exception occurred during the parsing of xml map database files: "+ message)
        {
        }
    }

	/// <summary>
	/// This class is responsible for loading maps from
	/// GameData database that are stored there as xml files
	/// with xml schemas that define and constrain their content.
	/// </summary>
    public class MapLoader
    {
        IList<IMap> cachedGamePlans;
        IList<IMap> cachedStartingPlans;
        string mapDatabasePath;

		/// <summary>
		/// Ctor with path to map database directory inside 
		/// GameData. This path should point to directory
		/// ... /DB/MapDB
		/// </summary>
        public MapLoader(string mapDatabasePath)
        {
            cachedStartingPlans = new List<IMap>();
            cachedGamePlans = new List<IMap>();
            this.mapDatabasePath = mapDatabasePath;
        }

        private XmlReaderSettings GetPlanValidationSettings(string planSchemaPath)
        {
            //in addition to plan schema, there is also schema with common definitions
            string commonXsdPath = mapDatabasePath + "Common.xsd";

            XmlReaderSettings settings = new XmlReaderSettings();
            try
            {
                settings.Schemas.Add("Common", commonXsdPath);
                settings.Schemas.Add(null, planSchemaPath);
                settings.IgnoreComments = true;
            }
            catch (FileNotFoundException e)
            {
                throw new MapDBParsingException("could not find appropriate .xsd file with schema");
            }
            settings.ValidationType = ValidationType.Schema;

            return settings;
        }

        private XmlDocument GetValidatedPlan(string plansDirPath, string planPath)
        {
            //main schema for both game plans and starting plans has the same name - "PlanSchema.xsd"
            string planSchemaPath = plansDirPath + "PlanSchema.xsd";
            XmlReaderSettings readerSettings = GetPlanValidationSettings(planSchemaPath);

            XmlReader reader;
            try
            {
                reader = XmlReader.Create(planPath, readerSettings);
            }
            catch (XmlSchemaException e)
            {
                throw new MapDBParsingException("error during xml schema loading ... details: " + e.Message);
            }

            XmlDocument document = new XmlDocument();

            //possible exception because of invalid malformed xml
            try
            {
                document.Load(reader);
            }
            catch (XmlSchemaException e)
            {
                throw new MapDBParsingException("error during xml document validation ... details: " + e.Message);
            }
            
            return document;
        }

        private WallLocation GetWallLocation(XmlNode wallNode)
        {
            if (wallNode.Attributes.Count != 1)
                throw new MapDBParsingException("parsing walls - expecting 1 attribute");

            switch (wallNode.Attributes[0].Value)
            {
                case nameof(WallLocation.Up):
                    return WallLocation.Up;
                case nameof(WallLocation.Down):
                    return WallLocation.Down;
                case nameof(WallLocation.Right):
                    return WallLocation.Right;
                case nameof(WallLocation.Left):
                    return WallLocation.Left;
                default:
                    throw new MapDBParsingException("parsing walls - expecting location value");
            }
        }

        private Direction GetWallFacingDirection(XmlNode wallNode)
        {
            if (wallNode.Attributes.Count != 1)
                throw new MapDBParsingException("parsing walls - expecting 1 attribute");

            WallLocation wallLocation = GetWallLocation(wallNode);
            switch (wallLocation)
            {
                case WallLocation.Up:
                    return new Direction(DirectionType.Down);
                case WallLocation.Down:
                    return new Direction(DirectionType.Up);
                case WallLocation.Left:
                    return new Direction(DirectionType.Right);
                case WallLocation.Right:
                    return new Direction(DirectionType.Left);
                default:
                    throw new MapDBParsingException("parsing walls - expecting location value");
            }
        }

        private PushPanelWall ParsePushPanelWall(XmlNode pushPanelNode, Tile tile)
        {
            if (pushPanelNode.ChildNodes.Count != 2 && pushPanelNode.ChildNodes.Count != 3)
                throw new MapDBParsingException("parsing push panel walls - expecting 2 or 3 register childs");

            Direction facingDirection = GetWallFacingDirection(pushPanelNode);

            IList<int> registers = new List<int>();
            foreach (XmlNode regNode in pushPanelNode.ChildNodes)
            {
                if (!Int32.TryParse(regNode.InnerText, out int reg))
                    throw new MapDBParsingException("parsing push panel walls - expecting valid register number");

                registers.Add(reg);
            }

            if (registers.Count == 2)
            {
                if (registers[0] != 2 || registers[1] != 4)
                    throw new MapDBParsingException("parsing push panel walls - invalid register numbers");
            }
            else if (registers.Count == 3)
            {
                if (registers[0] != 1 || registers[1] != 3 || registers[2] != 5)
                    throw new MapDBParsingException("parsing push panel walls - invalid register numbers");
            }

            return new PushPanelWall(registers.ToArray(), tile, facingDirection);
        }

        private LaserWall ParseLaserWall(XmlNode laserWallNode, Tile tile)
        {
            if (laserWallNode.ChildNodes.Count != 1)
                throw new MapDBParsingException("parsing laser walls - expecting one child with count");

            if (!Int32.TryParse(laserWallNode.ChildNodes[0].InnerText, out int count))
                throw new MapDBParsingException("parsing laser walls - expecting valid laser count");

            if (count < 1 && count > 3)
                throw new MapDBParsingException("parsing laser walls - expecting valid laser count");

            Direction facingDirection = GetWallFacingDirection(laserWallNode);

			/* TODO: FIX THIS, THIS SHOULD NOT BE HARDCODED*/
			Damage dmg = new Damage
			{
				DamageCards = new DamageCard[count]
			};
			for (int i = 0; i < count; ++i)
				dmg.DamageCards[i] = new DamageCard("Spam", new CardEffect[1]{ new PlayTopCardEffect() });


			return new LaserWall(dmg, count, tile, facingDirection);
        }

        private EmptyWall ParseEmptyWall(XmlNode wallNode, Tile tile)
        {
            Direction facingDirection = GetWallFacingDirection(wallNode);
            return new EmptyWall(tile, facingDirection);
        }

        private Wall ParseSingleWall(XmlNode node, Tile tile)
        {
            switch (node.Name)
            {
                case nameof(EmptyWall):
                    return ParseEmptyWall(node, tile);
                case nameof(LaserWall):
                    return ParseLaserWall(node, tile);
                case nameof(PushPanelWall):
                    return ParsePushPanelWall(node, tile);
                default:
                    throw new MapDBParsingException("parsing walls - invalid type of wall");
            }
        }

        private IList<Wall> ParseSingleTileWalls(XmlNode tileNode, Tile tile)
        {
            if (tileNode.ChildNodes.Count > 4)
                throw new MapDBParsingException("parsing walls - invalid number of walls in a tile");

            IList<Wall> walls = new List<Wall>();

            foreach (XmlNode wall in tileNode.ChildNodes)
            {
                walls.Add(ParseSingleWall(wall, tile));
            }
            
            return walls;
        }
        
        private Direction ParseTileOutDirection(XmlNode tile)
        {
            if (tile.Attributes.Count != 1)
                throw new MapDBParsingException("parsing tile with out direction - expecting one attribute with direction");
            string dir = tile.Attributes[0].Value;

            switch (dir)
            {
                case nameof(DirectionType.Down):
                    return new Direction(DirectionType.Down);
                case nameof(DirectionType.Up):
                    return new Direction(DirectionType.Up);
                case nameof(DirectionType.Left):
                    return new Direction(DirectionType.Left);
                case nameof(DirectionType.Right):
                    return new Direction(DirectionType.Right);
                default:
                    throw new MapDBParsingException("parsing tile with out direction - expecting valid outgoing direction");
            }
        }

        private RotationType ParseTileRotation(XmlNode tile)
        {
            if (tile.Attributes.Count != 1)
                throw new MapDBParsingException("parsing tile with rotation - expecting one attribute with rotation type");

            string rotation = tile.Attributes[0].Value;

            switch (rotation)
            {
                case nameof(RotationType.Right):
                    return RotationType.Right;
                case nameof(RotationType.None):
                    return RotationType.None;
                case nameof(RotationType.Left):
                    return RotationType.Left;
                case nameof(RotationType.Back):
                    return RotationType.Back;
                default:
                    throw new MapDBParsingException("parsing tile with rotation - expecting valid rotation type");
            }
        }

        private BlueBeltTile ParseBlueBeltTile(XmlNode blueBeltTile)
        {
            Direction outDir = ParseTileOutDirection(blueBeltTile);
            return new BlueBeltTile(outDir);
        }

        private GreenBeltTile ParseGreenBeltTile(XmlNode greenBeltTile)
        {
            Direction outDir = ParseTileOutDirection(greenBeltTile);
            return new GreenBeltTile(outDir);
        }

        private EnergyTile ParseEnergyTile(XmlNode energyTile)
        {
            if (energyTile.Attributes.Count != 1)
                throw new MapDBParsingException("parsing energy tile - expecting one attribute with amount");
            if (!Int32.TryParse(energyTile.Attributes[0].Value, out int amount))
                throw new MapDBParsingException("parsing energy tile - expecting valid amount value");
            if(amount!=1)
                throw new MapDBParsingException("parsing energy tile - expecting valid amount value");

            return new EnergyTile(amount);
        }

        private RotatingTile ParseRotatingTile(XmlNode rotatingTile)
        {
            RotationType rType = ParseTileRotation(rotatingTile);
            return new RotatingTile(rType);
        }

        private StartingTile ParseStartingTile(XmlNode startingTile)
        {
            Direction outDir = ParseTileOutDirection(startingTile);
            return new StartingTile(outDir);
        }

        private TextTile ParseTextTile(XmlNode textTile)
        {
            if (textTile.Attributes.Count != 1)
                throw new MapDBParsingException("parsing text tile - expecting one attribute with text");

            return new TextTile(textTile.Attributes[0].Value);
        }

        private Tile ParseSingleTile(XmlNode tileNode)
        {
            switch (tileNode.Name)
            {
                case nameof(BlueBeltTile):
                    return ParseBlueBeltTile(tileNode);
                case nameof(GreenBeltTile):
                    return ParseGreenBeltTile(tileNode);
                case nameof(EnergyTile):
                    return ParseEnergyTile(tileNode);
                case nameof(RotatingTile):
                    return ParseRotatingTile(tileNode);
                case nameof(StartingTile):
                    return ParseStartingTile(tileNode);
                case nameof(TextTile):
                    return ParseTextTile(tileNode);
                case nameof(EmptyTile):
                    return new EmptyTile();
				case nameof(PitTile):
					return new PitTile();
                default:
                    throw new MapDBParsingException("parsing tile - invalid tile type");
            }
        }

        private void ParseTileRow(XmlNode tileRowNode, int rowIndex, Tile[,] tiles, IList<Wall> walls)
        {
            for (int i = 0; i < tileRowNode.ChildNodes.Count; ++i)
            {
                XmlNode tileNode = tileRowNode.ChildNodes[i];
                Tile tile = ParseSingleTile(tileNode);
                tiles[rowIndex, i] = tile;
                var newWalls = ParseSingleTileWalls(tileNode, tile);
                foreach (var wall in newWalls)
                    walls.Add(wall);                
            }
        }

        private XmlNodeList GetTileRowNodes(XmlDocument document)
        {
            return document.DocumentElement.SelectNodes("//MapTiles/TileRow");
        }

        private int GetRowCount(XmlDocument document)
        {
            XmlNodeList tileRowList = GetTileRowNodes(document);
            if (tileRowList.Count == 0)
                throw new MapDBParsingException("parsing plan - plan has to contain at least one row of tiles");

            return tileRowList.Count;
        }

        private int GetColumnCount(XmlDocument document)
        {
            XmlNodeList tileRowList = GetTileRowNodes(document);

            if (tileRowList.Count == 0)
                throw new MapDBParsingException("parsing plan - plan has to contain at least one row of tiles");

            //check that there is same number of tiles in each node
            int tileCount = tileRowList[0].ChildNodes.Count;
            if(tileCount == 0)
                throw new MapDBParsingException("parsing plan - plan has to contain at least one row of tiles");

            foreach (XmlNode tileRow in tileRowList)
            {
                if (tileRow.ChildNodes.Count != tileCount)
                    throw new MapDBParsingException("parsing plan - plan has to contain same number of tiles on each row");
            }
            return tileCount;            
        }

        private MapSize GetMapSize(XmlDocument document)
        {
            int rowCount = GetRowCount(document);
            int colCount = GetColumnCount(document);
            return new MapSize(rowCount, colCount);
        }

        private string GetPlanID(XmlDocument document)
        {
            XmlNode MapIDnode = document.DocumentElement.SelectSingleNode("//ID");
            return MapIDnode.InnerText;
        }

        private IMap ParseValidatedPlan(XmlDocument document)
        {
            string mapID = GetPlanID(document);
            MapSize mapSize = GetMapSize(document);

            Tile[,] mapTiles = new Tile[mapSize.RowCount, mapSize.ColCount];
            IList<Wall> walls = new List<Wall>();

            XmlNodeList tileRowList = GetTileRowNodes(document);

            for (int i = 0; i < tileRowList.Count; ++i)
            {
                ParseTileRow(tileRowList[i], i, mapTiles, walls);
            }
            
            return new Map(mapID, mapSize, mapTiles, walls);
        }

        private bool ArePlanIDsUnique(IList<IMap> plans)
        {
            if (plans.Select(x => x.ID).Distinct().Count() == plans.Count)
                return true;
            return false;
        }

        private IList<IMap> LoadPlans(string plansDirPath)
        {
            string[] plansPaths = Directory.GetFiles(plansDirPath, "*.xml", SearchOption.TopDirectoryOnly);
            
            if(plansPaths.Length == 0)
                throw new MapDBParsingException("there are no plans present at the provided path");
            
            IList<IMap> plans = new List<IMap>();
            foreach (var planPath in plansPaths)
            {
                XmlDocument xmlDocument = GetValidatedPlan(plansDirPath, planPath);
                plans.Add(ParseValidatedPlan(xmlDocument));
            }
            
            if (!ArePlanIDsUnique(plans))
                throw new MapDBParsingException("IDs of loaded plans are not unique");            

            return plans;
        }

		/// <summary>
		/// This method loads every game plan that 
		/// is present in the GameData database.
		/// </summary>
		/// <returns>list of loaded game plans</returns>
		/// <exception cref="MapDBParsingException">is thrown
		/// if parsing of any plan fails or if there are no
		/// game plans present</exception>
		public IList<IMap> LoadGamePlans()
        {
            if (cachedGamePlans.Count > 0)
                return cachedGamePlans;

            string gamePlansDirPath = mapDatabasePath + @"/GamePlans/";
            cachedGamePlans = LoadPlans(gamePlansDirPath);
            return cachedGamePlans;
        }

		/// <summary>
		/// This method loads every starting plan that 
		/// is present in the GameData database.
		/// </summary>
		/// <returns>list of loaded starting plans</returns>
		/// <exception cref="MapDBParsingException">is thrown
		/// if parsing of any plan fails or if there are no
		/// starting plans present</exception>
		public IList<IMap> LoadStartingPlans()
        {
            if (cachedStartingPlans.Count > 0)
                return cachedStartingPlans;

            string startingPlansDirPath = mapDatabasePath + @"/StartingPlans/";
            cachedStartingPlans = LoadPlans(startingPlansDirPath);
            return cachedStartingPlans;
        }

		/// <summary>
		/// This method tries to load game plan that has
		/// the provided ID. Note that in order to find
		/// such game plan, all other game plans have
		/// to be loaded.
		/// </summary>
		/// <param name="gamePlanID">game plan id as a string</param>
		/// <returns>map with the requested id</returns>
		/// <exception cref="MapDBParsingException">is thrown
		/// if parsing of any plan fails or if there is no
		/// game plans present with the provided id</exception>
		public IMap LoadGamePlanByID(string gamePlanID)
        {
            IList<IMap> gamePlans = LoadGamePlans();
            foreach (var plan in gamePlans)
                if (plan.ID == gamePlanID)
                    return plan;

            throw new MapDBParsingException("there is no game plan with the provided ID");
        }

		/// <summary>
		/// This method tries to load starting plan that has
		/// the provided ID. Note that in order to find
		/// such starting plan, all other starting plans have
		/// to be loaded.
		/// </summary>
		/// <param name="startingPlanID">starting plan id as a string</param>
		/// <returns>map with the requested id</returns>
		/// <exception cref="MapDBParsingException">is thrown
		/// if parsing of any plan fails or if there is no
		/// starting plans present with the provided id</exception>
		public IMap LoadStartingPlanByID(string startingPlanID)
        {
            IList<IMap> startingPlans = LoadStartingPlans();
            foreach (var plan in startingPlans)
                if (plan.ID == startingPlanID)
                    return plan;

            throw new MapDBParsingException("there is no starting plan with the provided ID");
        }
    }
}
