﻿using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Walls
{
	/// <summary>
	/// This class represents wall with
	/// some lasers attached to it.
	/// </summary>
    public class LaserWall : Wall
    {
		/// <summary>
		/// Number of lasers attached to this wall.
		/// </summary>
        public int Count { get; private set; }

		/// <summary>
		/// Total damage done by this laser wall.
		/// </summary>
		public Damage Damage { get; private set; }

		/// <summary>
		/// Laser wall ctor with total damage,
		/// number of lasers on this wall, tile reference 
		/// and facing direction (wall location is deduced).
		/// </summary>
        public LaserWall(Damage damage, int count, Tile tile, Direction facingDirection) : 
            base(tile, facingDirection)
        {
            this.Count = count;
			this.Damage = damage;
        }
    }
}
