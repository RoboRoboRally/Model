﻿using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Walls
{
	/// <summary>
	/// This class represents wall with attached
	/// push panel.
	/// </summary>
    public class PushPanelWall : Wall
    {
		/// <summary>
		/// Register numbers to which this push panel
		/// wall reacts.
		/// </summary>
        public int[] Registers { get; private set; }

		/// <summary>
		/// Push panel ctor with register numbers,
		/// tile reference and facing direction
		/// (wall location is deduced).
		/// </summary>
        public PushPanelWall(int[] registers, Tile tile, Direction facingDirection) :
            base(tile, facingDirection)
        {
            this.Registers = registers;
        }
    }
}
