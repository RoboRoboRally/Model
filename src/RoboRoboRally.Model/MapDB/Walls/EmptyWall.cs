﻿using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Walls
{
	/// <summary>
	/// This class represents empty wall 
	/// (without any decorator like laser).
	/// </summary>
    public class EmptyWall : Wall
    {
		/// <summary>
		/// Empty wall ctor with tile reference and
		/// facing direction (wall location is deduced).
		/// </summary>
        public EmptyWall(Tile tile, Direction facingDirection) :
            base(tile, facingDirection)
        {

        }
    }
}
