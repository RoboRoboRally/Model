﻿using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Walls
{
	/// <summary>
	/// <para>
	/// This namespace contains classes that represent 
	/// individual walls.
	/// </para>
	/// 
	/// <para>
	/// Note that logic of these walls (what happens
	/// if robot is affected by e.g. laser wall) is not
	/// part of this namespace. This logic is contained
	/// in activation phase.
	/// </para>
	/// 
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{

	}

	/// <summary>
	/// Helper enum for representing
	/// wall location on tile.
	/// </summary>
    public enum WallLocation
    {
		/// <summary>
		/// Up location - wall is placed on tile's
		/// top side
		/// </summary>
        Up,
		/// <summary>
		/// Down location - wall is placed on tile's
		/// bottom side
		/// </summary>
		Down,
		/// <summary>
		/// Right location - wall is placed on tile's
		/// right side
		/// </summary>
		Right,
		/// <summary>
		/// Left location - wall is placed on tile's
		/// left side
		/// </summary>
		Left
    }

	/// <summary>
	/// <para>
	/// Base class for each type of wall.
	/// </para>
	/// 
	/// <para>
	/// Wall is represented by reference to tile
	/// it is placed on, and it's location or facing
	/// direction (note that we could deduce one from
	/// the other, but both are present for easier manipulation 
	/// in some cases).
	/// </para>
	/// </summary>
    public abstract class Wall
    {
		/// <summary>
		/// Reference to tile this wall is placed on
		/// </summary>
        public Tile Tile { get; private set; }

		/// <summary>
		/// Facing direction of this tile. If facing direction
		/// is up, it means that wall is placed on the tile's
		/// bottom side and so on.
		/// </summary>
        public Direction FacingDirection { get; private set; }

		/// <summary>
		/// Wall location of this tile. See <see cref="WallLocation"/>.
		/// </summary>
        public WallLocation WallLocation { get; private set; }

		/// <summary>
		/// Changes reference to a different tile. This
		/// is used in some rare scenarios during game setup.
		/// </summary>
		internal void SetNewTile(Tile t)
		{
			Tile = t;
		}

        private void SetWallLocation(Direction facingDir)
        {
            if (facingDir.X == 1 && facingDir.Y == 0)
                WallLocation = WallLocation.Left;
            else if (facingDir.X == 0 && facingDir.Y == 1)
                WallLocation = WallLocation.Down;
            else if (facingDir.X == -1 && facingDir.Y == 0)
                WallLocation = WallLocation.Right;
            else if (facingDir.X == 0 && facingDir.Y == -1)
                WallLocation = WallLocation.Up;
        }

		/// <summary>
		/// Wall ctor with tile, and facing direction 
		/// (wall location is deduced)
		/// </summary>
        protected Wall(Tile tile, Direction facingDirection)
        {
            this.Tile = tile;
            this.FacingDirection = facingDirection;
            SetWallLocation(facingDirection);
        }
    }
}
