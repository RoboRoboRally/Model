﻿using System;
using System.Collections.Generic;
using System.Linq;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Settings;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB
{
	/// <summary>
	/// This exception is used during map manipulation
	/// with <see cref="MapFactory"/> class.
	/// </summary>
	public class MapFactoryException : Exception
    {
		/// <summary>
		/// Exception ctor with message
		/// </summary>
        public MapFactoryException(string message) :
            base("Exception occurred in method of map factory: " + message)
        {}
    }

	/// <summary>
	/// Helper struct that is used to encapsulate modifications
	/// done by GameTerminal to map during game setup (remember
	/// that antenna, reboot tiles and checkpoints are not part
	/// of map definition). Based on this struct and other
	/// info in <see cref="MapSettings"/>, model can then
	/// correctly reconstruct how exactly had the map been setup
	/// by GameTerminal.
	/// </summary>
	public struct MapModifications
    {
        /// <summary>
		/// Rotation of game plan before map confirmation.
		/// (starting plan can not be rotated).
		/// </summary>
        public RotationType GamePlanRotation { get; set; }

		/// <summary>
		/// Information about placed antenna tile.
		/// </summary>
		public TileRecord<PriorityAntennaTile> PriorityAntenna { get; set; }

		/// <summary>
		/// Information about placed reboot tile(s).
		/// </summary>
		public TileRecord<RebootTile>[] RebootTiles { get; set; }

		/// <summary>
		/// Information about placed checkpoint tile(s).
		/// </summary>
		public TileRecord<CheckpointTile>[] CheckpointTiles { get; set; }
    }

	/// <summary>
	/// <para>
	/// This class may be used for various types of map manipulation.
	/// This includes map rotation, joining of maps and map serialization
	/// and deserialization.
	/// </para>
	/// 
	/// <para>
	/// This class is used mainly in GameTerminal but it is also
	/// used during new game setup.
	/// </para>
	/// 
	/// <para>
	/// From perspective of GameTerminal, this class is used during 
	/// game setup. First, it can be used to rotate map, then it can be
	/// used to join starting and game plan when the game setup is confirmed.
	/// Following that, game terminal can use this method to serialize the
	/// joined map into a helper struct <see cref="MapSettings"/>, which can 
	/// then be sent to server, and then model, and model can 
	/// deserialize how was the map set up by GameTerminal by methods
	/// on this class.
	/// </para>
	/// </summary>
    public class MapFactory
    {
        private Wall RotateWall(Wall oldWall, Tile newTile, RotationType rotationType)
        {
            Direction newFacingDir = oldWall.FacingDirection.Rotate(rotationType);

            if (oldWall is EmptyWall emptyWall)
                return new EmptyWall(newTile, newFacingDir);
            else if (oldWall is LaserWall laserWall)
                return new LaserWall(laserWall.Damage, laserWall.Count, newTile, newFacingDir);
            else if (oldWall is PushPanelWall pushPanelWall)
                return new PushPanelWall(pushPanelWall.Registers, newTile, newFacingDir);
            else
                throw new MapFactoryException("can not rotate wall - unrecognized wall type");
        }

        private BlueBeltTile RotateBlueBeltTile(BlueBeltTile blueBeltTile, RotationType rotationType)
        {
            Direction newDir = blueBeltTile.OutDirection.Rotate(rotationType);
            return new BlueBeltTile(newDir);
        }

        private GreenBeltTile RotateGreenBeltTile(GreenBeltTile greenBeltTile, RotationType rotationType)
        {
            Direction newDir = greenBeltTile.OutDirection.Rotate(rotationType);
            return new GreenBeltTile(newDir);
        }

        private RebootTile RotateRebootTile(RebootTile rebootTile, RotationType rotationType)
        {
            Direction newDir = rebootTile.OutDirection.Rotate(rotationType);
            return new RebootTile(newDir);
        }

        private StartingTile RotateStartingTile(StartingTile startingTile, RotationType rotationType)
        {
            Direction newDir = startingTile.OutDirection.Rotate(rotationType);
            return new StartingTile(newDir);
        }

		private PriorityAntennaTile RotatePriorityAntennaTile(PriorityAntennaTile tile, RotationType rotation)
		{
			Direction newDir = tile.OutDirection.Rotate(rotation);
			return new PriorityAntennaTile(newDir);
		}

        private Tile RotateTile(Tile tile, RotationType rotationType)
        {
			if (tile is GreenBeltTile greenBeltTile)
				return RotateGreenBeltTile(greenBeltTile, rotationType);
			else if (tile is BlueBeltTile blueBeltTile)
				return RotateBlueBeltTile(blueBeltTile, rotationType);
			else if (tile is RebootTile rebootTile)
				return RotateRebootTile(rebootTile, rotationType);
			else if (tile is StartingTile startingTile)
				return RotateStartingTile(startingTile, rotationType);
			else if (tile is PriorityAntennaTile antennaTile)
				return RotatePriorityAntennaTile(antennaTile, rotationType);
			else if (tile is EmptyTile ||
				tile is RotatingTile ||
				tile is EnergyTile ||
				tile is CheckpointTile ||
				tile is PitTile ||
				tile is TextTile)
				return tile;
			else
				throw new MapFactoryException("can not rotate tile - unrecognized tile type");
        }

        private MapCoordinates GetNewCoordinates(MapCoordinates oldCoordinates,
            MapSize newMapSize, RotationType rotationType)
        {
            //first, transpose the coordinates
            int newRowIndex = oldCoordinates.ColIndex;
            int newColIndex = oldCoordinates.RowIndex;

            //second, we rotate the coordinates
            //if we rotate "to the left"
            if (rotationType == RotationType.Left)
            {
                //we need to swap some rows
                //namely, we swap 0th row with m-1 st row
                //we swap 1st row with m-2 nd row and so on

                newRowIndex = newMapSize.RowCount - 1 - newRowIndex;
            }
            //if we rotate "to the right"
            else if (rotationType == RotationType.Right)
            {
                //we need to swap some columns
                //namely, we swap 0th column with n-1 st column
                //we swap 1st column with n-2 nd column and so on
                newColIndex = newMapSize.ColCount - 1 - newColIndex;
            }
            else if (rotationType == RotationType.Back)
            {
                //in order to rotate the matrix by 180, we need to do:
                //transposition
                //swap columns
                //transposition
                //swap columns
                //we have already done the transposition, so first, swap columns

                //accessing the row count here is actually correct - when we rotate
                //rectangular matrix, we need to access the number of columns in the "transposed"
                //matrix ... when we rotate by 90 degrees, this number is correctly stored in the newMapSize
                //but it is not when we rotate by 180 and the matrix is rectangular
                //but the numer of columns in this transposed version is number of rows
                //in the new map size...
                newColIndex = newMapSize.RowCount - 1 - newColIndex;

                //now, transposition
                int swapTmp = newRowIndex;
                newRowIndex = newColIndex;
                newColIndex = swapTmp;

                //and swap columns again
                newColIndex = newMapSize.ColCount - 1 - newColIndex;
            }

            return new MapCoordinates(newRowIndex, newColIndex);
        }


        private IList<Wall> RotateWalls(IMap oldMap, MapSize newMapSize,
            Tile[,] newTiles, RotationType rotationType)
        {
            IList<Wall> rotatedWalls = new List<Wall>();

            for (int i = 0; i < oldMap.MapSize.RowCount; ++i)
            {
                for (int j = 0; j < oldMap.MapSize.ColCount; ++j)
                {
                    var oldTileWalls = oldMap.GetTileWalls(oldMap[i, j]);

                    MapCoordinates newCoordinates = GetNewCoordinates(
                        new MapCoordinates(i, j), newMapSize, rotationType
                        );

                    foreach (var oldWall in oldTileWalls)
                    {
                        rotatedWalls.Add(RotateWall(oldWall,
                            newTiles[newCoordinates.RowIndex, newCoordinates.ColIndex], rotationType)
                            );
                    }
                }
            }
            return rotatedWalls;
        }

        private Tile[,] RotateTiles(IMap oldMap, MapSize newMapSize, RotationType rotationType)
        {
            Tile[,] newTiles = new Tile[newMapSize.RowCount, newMapSize.ColCount];

            for (int i = 0; i < oldMap.MapSize.RowCount; ++i)
            {
                for (int j = 0; j < oldMap.MapSize.ColCount; ++j)
                {
                    Tile newTile = RotateTile(oldMap[i, j], rotationType);
                    MapCoordinates newCoordinates = GetNewCoordinates(
                        new MapCoordinates(i, j), newMapSize, rotationType
                        );
                    newTiles[newCoordinates.RowIndex, newCoordinates.ColIndex] = newTile;

                }
            }
            return newTiles;
        }

        private MapSize GetNewMapSize(MapSize oldMapSize, RotationType rotationType)
        {
            if (rotationType == RotationType.Back)
                return oldMapSize;

            MapSize newMapSize = new MapSize(oldMapSize.ColCount, oldMapSize.RowCount);
            return newMapSize;
        }

		/// <summary>
		/// This method may be used to rotate map. Note that
		///	this method returns new map and the reference to the
		///	old map should not be used anymore.
		/// </summary>
		/// <param name="toRotate">map to rotate</param>
		/// <param name="rotationType">type of rotation</param>
		/// <returns>rotated map</returns>
        public IMap RotateMap(IMap toRotate, RotationType rotationType)
        {
            if (rotationType == RotationType.None)
                return toRotate;

            MapSize newMapSize = GetNewMapSize(toRotate.MapSize, rotationType);

            Tile[,] newTiles = RotateTiles(toRotate, newMapSize, rotationType);
            IList<Wall> newWalls = RotateWalls(toRotate, newMapSize, newTiles, rotationType);

            return new Map(toRotate.ID, newMapSize, newTiles, newWalls);
        }

        //-----------------------------------------------------------

		private List<Wall> GetAllWalls(IMap map)
		{
			List<Wall> walls = new List<Wall>();
			for (int i = 0; i < map.MapSize.RowCount; ++i)
			{
				for (int j = 0; j < map.MapSize.ColCount; ++j)
				{
					walls.AddRange(map.GetTileWalls(map[i, j]));
				}
			}
			return walls;
		}

        private Tile[,] MergeTiles(IMap gamePlan, IMap startingPlan, MapSize newMapSize)
        {
            Tile[,] newTiles = new Tile[newMapSize.RowCount, newMapSize.ColCount];

            for (int i = 0; i < gamePlan.MapSize.RowCount; ++i)
            {
                for (int j = 0; j < gamePlan.MapSize.ColCount; ++j)
                    newTiles[i, j] = gamePlan[i, j];
            }

            for (int i = 0; i < startingPlan.MapSize.RowCount; ++i)
            {
                for (int j = 0; j < startingPlan.MapSize.ColCount; ++j)
                    newTiles[i + gamePlan.MapSize.RowCount, j] = startingPlan[i, j];
            }
            return newTiles;
        }

        private IList<Wall> MergeWalls(IMap gamePlan, IMap startingPlan)
        {
			var gamePlanWalls = GetAllWalls(gamePlan);
			var startingPlanWalls = GetAllWalls(startingPlan);

			gamePlanWalls.AddRange(startingPlanWalls);

			return gamePlanWalls;
        }
		
		/// <summary>
		/// <para>
		/// This method joins together one starting plan and one game 
		/// plan. 
		/// </para>
		/// 
		/// <para>
		/// Note that there are no more methods for joining
		/// additional plans together - only one game plan and
		/// one starting plan can be joined. This is because of
		/// other project limitations.
		/// </para>
		/// 
		/// <para>
		/// Also, it is important to understand, how are the
		/// maps joined. Let's say that game plan is matrix A with
		/// dimensions mxn and starting plan is matrix B with
		/// dimensions pxn. Then, the new plan has dimensions
		/// m+pxn and with plan A having its top left tile
		/// at position [0, 0] and plan B having its top left
		/// tile at position [m, 0]. 
		/// </para>
		/// 
		/// </summary>
		/// <param name="gamePlan">game plan to join</param>
		/// <param name="startingPlan">starting plan to join</param>
		/// <returns>new map that represents joined plans</returns>
		public IMap JoinMaps(IMap gamePlan, IMap startingPlan)
		{
			MapSize newMapSize = new MapSize(gamePlan.MapSize.RowCount + startingPlan.MapSize.RowCount,
			  gamePlan.MapSize.ColCount);

			Tile[,] mergedTiles = MergeTiles(gamePlan, startingPlan, newMapSize);

			IList<Wall> mergedWalls = MergeWalls(gamePlan, startingPlan);

			IMap newMap = new Map(gamePlan.ID + "_" + startingPlan.ID, newMapSize, mergedTiles, mergedWalls);

			return newMap;
		}
		
		/// <summary>
		/// <para>
		/// This method serializes plans joined by method
		/// <see cref="JoinMaps(IMap, IMap)"/> and returns struct
		/// that is used to sent information from Game Terminal to
		/// model about map setup in new game. (so, it is expected
		/// that this method is used only from Game Terminal)
		/// </para>
		/// 
		/// <para>
		/// Note that this method also accepts rotation of game
		/// plan before it was joined to the starting plan. This
		/// is necessary as deducing the rotation from joined plans
		/// is rather cubersome.
		/// </para>
		/// </summary>
		/// <param name="joinedPlans">joined starting and game
		/// plan</param>
		/// <param name="gamePlanRotation">rotation of game
		/// plan before joining the plans</param>
		/// <returns>map settings that can be passed to
		/// model for a new game setup</returns>
		public MapSettings SerializeMap(IMap joinedPlans, RotationType gamePlanRotation)
		{
			VerifyFinalMapTilePresence(joinedPlans);

			return new MapSettings()
			{
				GamePlanID = joinedPlans.ID.Split('_')[0],
				StartingPlanID = joinedPlans.ID.Split('_')[1],
				MapModifications = new MapModifications()
				{
					GamePlanRotation = gamePlanRotation,
					CheckpointTiles = joinedPlans.GetTileRecords<CheckpointTile>(),
					PriorityAntenna = joinedPlans.GetTileRecords<PriorityAntennaTile>()[0],
					RebootTiles = joinedPlans.GetTileRecords<RebootTile>()
				}
			};
		}

		private void VerifyFinalMapTilePresence(IMap joinedMap)
		{
			if (joinedMap.GetTileRecords<RebootTile>().Count() == 0)
				throw new MapFactoryException("can not verify map tile presence - there are no reboot tiles");

			if(joinedMap.GetTileRecords<CheckpointTile>().Count() == 0)
				throw new MapFactoryException("can not verify map tile presence - there are no checkpoint tiles");

			var ctrs = joinedMap.GetTileRecords<CheckpointTile>();

			for (int i = 1; i <= ctrs.Count(); ++i)
			{
				if (!ctrs.Any(c => c.Tile.Number == i))
					throw new MapFactoryException("can not verify map tile presence - checkpoint tiles are not in sequence");
			}

			if (joinedMap.GetTileRecords<PriorityAntennaTile>().Count() != 1)
				throw new MapFactoryException("can not verify map tile presence - there is not one antenna tiles");
		}

		private void SetMapModificationsTiles(IMap joinedMap, MapModifications modifications)
		{
			foreach (var ctr in modifications.CheckpointTiles)
				joinedMap.SetCheckpointTile(ctr.Tile, ctr.Coords);

			foreach (var rtr in modifications.RebootTiles)
				joinedMap.SetRebootTile(rtr.Tile, rtr.Coords);

			joinedMap.SetPriorityAntennaTile(modifications.PriorityAntenna.Tile, modifications.PriorityAntenna.Coords);

		}

		/// <summary>
		/// <para>
		/// This method is used during new game setup by Model
		/// and it is not expected that it would be used by any
		/// other part of the project.
		/// </para>
		/// 
		/// <para>
		/// This method reconstructs map setup done by Game Terminal
		/// from struct <see cref="MapSettings"/>. Note that 
		/// this method also accepts path to GameData, as it has to
		/// be accessed during the process of deserialization.
		/// </para>
		/// </summary>
		/// <param name="mapDatabasePath">path to map database in 
		/// GameData, that is folder ... /DB/MapDB/</param>
		/// <param name="mapSettings">struct describing map setup
		/// done by GameTerminal</param>
		/// <returns>deserialized map</returns>
		public IMap DeserializeMap(string mapDatabasePath, MapSettings mapSettings)
		{
			MapLoader mapLoader = new MapLoader(mapDatabasePath);

			IMap gamePlan;
			IMap startingPlan;
			try
			{
				gamePlan = mapLoader.LoadGamePlanByID(mapSettings.GamePlanID);
				startingPlan = mapLoader.LoadStartingPlanByID(mapSettings.StartingPlanID);
			}
			catch (MapDBParsingException e)
			{
				throw new MapFactoryException("can not deserialize map - map loader threw an exception, details: " + e.Message);
			}

			var rotatedGamePlan = RotateMap(gamePlan, mapSettings.MapModifications.GamePlanRotation);

			var joinedPlans = JoinMaps(rotatedGamePlan, startingPlan);

			SetMapModificationsTiles(joinedPlans, mapSettings.MapModifications);

			VerifyFinalMapTilePresence(joinedPlans);

			return joinedPlans;
		}
    }
}
