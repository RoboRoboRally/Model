﻿namespace RoboRoboRally.Model.MapDB
{
	/// <summary>
	/// <para>
	/// This namespace contains functionality that is related 
	/// to maps. This means map representation during running
	/// game, tile definitions, wall definitions,
	/// functionality for loading maps from GameData database
	/// or functionality for map deserialization that
	/// is used when new game settings are passed to Model
	/// with intention of starting new game.
	/// </para>
	/// 
	/// <para>
	/// One functionality that would be maybe expected
	/// in this namespace and is absent is functinality related
	/// to map elements. That is, tiles like blue belts do not know
	/// how to perform their movement or pits do not know how to
	/// perform reboot - this functionality is placed into 
	/// activation phase.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceGroupDoc
    {
    }
}
