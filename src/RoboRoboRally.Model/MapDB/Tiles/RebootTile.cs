﻿using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents reboot tile which
	/// is specified by outgoing direction.
	/// </summary>
    public class RebootTile : Tile
    {
		/// <summary>
		/// Outgoing direction of this reboot tile.
		/// </summary>
        public Direction OutDirection { get; private set; }

		/// <summary>
		/// Reboot tile ctor with outgoing direction.
		/// </summary>
        public RebootTile(Direction outDirection)
        {
            this.OutDirection = outDirection;
        }
    }
}
