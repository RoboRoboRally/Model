﻿namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// Represents empty map tile (with no tile element,
	/// walls can still be present on this tile).
	/// </summary>
    public class EmptyTile: Tile
    {
    }
}
