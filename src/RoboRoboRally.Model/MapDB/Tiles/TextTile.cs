﻿namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents tile with text which
	/// is specified by it's text (this tile is used
	/// mainly for map display on GameTerminal).
	/// </summary>
    public class TextTile: Tile
    {
		/// <summary>
		/// Text of this tile.
		/// </summary>
        public string Text { get; private set; }

		/// <summary>
		/// Text tile ctor with tile's text.
		/// </summary>
        public TextTile(string text)
        {
            this.Text = text;
        }
    }
}
