﻿namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents energy tile which is 
	/// specified by amount of gained energy.
	/// </summary>
    public class EnergyTile : Tile
    {
		/// <summary>
		/// Amount of gained energy from this energy
		/// tile.
		/// </summary>
        public int EnergyAmount { get; private set; }

		/// <summary>
		/// Energy tile with energy amount gained from
		/// this energy tile.
		/// </summary>
        public EnergyTile(int energyAmount)
        {
            this.EnergyAmount = energyAmount;
        }
    }
}
