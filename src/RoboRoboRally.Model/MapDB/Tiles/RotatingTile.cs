﻿using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents rotating tile which
	/// is specified by amount of rotation.
	/// </summary>
    public class RotatingTile : Tile
    {
		/// <summary>
		/// Amount of rotation.
		/// </summary>
        public RotationType RotationType { get; private set; }

		/// <summary>
		/// Rotation tile ctor with rotation amount.
		/// </summary>
        public RotatingTile(RotationType rotationType)
        {
            this.RotationType = rotationType;
        }
    }
}
