﻿namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents checkpoint tile which
	/// is specified by checkpoint number.
	/// </summary>
    public class CheckpointTile : Tile
    {
		/// <summary>
		/// Checkpoint number.
		/// </summary>
        public int Number { get; private set; }

		/// <summary>
		/// Checkpoint tile ctor with it's number.
		/// </summary>
        public CheckpointTile(int number)
        {
            this.Number = number;
        }
    }
}
