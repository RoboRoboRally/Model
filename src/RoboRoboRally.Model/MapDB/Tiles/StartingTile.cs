﻿using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents starting tile which
	/// is specified by outgoing direction.
	/// </summary>
    public class StartingTile : Tile
    {
		/// <summary>
		/// Outgoing direction of this starting tile.
		/// </summary>
        public Direction OutDirection { get; private set; }

		/// <summary>
		/// Starting tile ctor with outgoing direction.
		/// </summary>
        public StartingTile(Direction outDirection)
        {
            this.OutDirection = outDirection;
        }
    }
}
