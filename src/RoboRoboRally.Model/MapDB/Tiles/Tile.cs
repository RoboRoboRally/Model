﻿namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// <para>
	/// This namespace contains classes that
	/// represent individual map tiles.
	/// </para>
	/// 
	/// <para>
	/// Note that logic of these tiles (what
	/// happens if robot steps on them) is not
	/// part of this namespace. This logic is contained
	/// in activation phase.
	/// </para>
	/// 
	/// <para>
	/// Also, note that individual tiles do not know which
	/// walls are present on them. Instead, walls have reference
	/// to tiles on which they are placed.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{

	}

	/// <summary>
	/// Base class for all map tiles.
	/// </summary>
    public abstract class Tile
    {
    }
}
