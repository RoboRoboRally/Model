﻿using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents tile with priority
	/// antenna which is specified by outgoing direction.
	/// </summary>
    public class PriorityAntennaTile : Tile
    {
		/// <summary>
		/// Antenna tile outgoing direction.
		/// </summary>
		public Direction OutDirection { get; private set; }

		/// <summary>
		/// Priority antenna tile ctor with outgoing direction.
		/// </summary>
		public PriorityAntennaTile(Direction outDirection)
		{
			this.OutDirection = outDirection;
		}
    }
}
