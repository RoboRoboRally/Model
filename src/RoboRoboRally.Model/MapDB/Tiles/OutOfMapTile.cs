﻿namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// This class represents out of map tile. Note
	/// that this tile is never part of any map, it
	/// is created on the fly when a tile with coordinates
	/// out of map is requested.
	/// </summary>
    public class OutOfMapTile : Tile
    {
        
    }
}
