﻿using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB.Tiles
{
	/// <summary>
	/// Base class for belt tiles. Each belt tile
	/// has an outgoing direction and movement amount.
	/// </summary>
    public abstract class BeltTile: Tile
    {
		/// <summary>
		/// Movement amount of this belt tile
		/// </summary>
        public int MoveAmount { get; private set; }
		/// <summary>
		/// Outgoing direction of belt tile
		/// </summary>
        public Direction OutDirection { get; private set; }
        
		/// <summary>
		/// Belt tile ctor with outgoing direction and movement amount.
		/// </summary>
        protected BeltTile(Direction outDirection, int moveAmount)
        {
            this.OutDirection = outDirection;
            this.MoveAmount = moveAmount;
        }
    }

	/// <summary>
	/// This class represents green belt tile that
	/// moves each robot by one tile in the outgoing direction.
	/// </summary>
    public class GreenBeltTile: BeltTile
    {
		/// <summary>
		/// Green belt ctor with outgoing direction for this
		/// green belt tile.
		/// </summary>
        public GreenBeltTile(Direction outDirection) : base(outDirection, 1)
        {
        }
    }

	/// <summary>
	/// This class represents blue belt tile that
	/// moves each robot by two tiles in the outgoing direction.
	/// </summary>
	public class BlueBeltTile : BeltTile
    {
		/// <summary>
		/// Blue belt ctor with outgoing direction for this
		/// blue belt tile
		/// </summary>
		public BlueBeltTile(Direction outDirection) : base(outDirection, 2)
        {
        }
    }
}
