﻿using System;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB
{
	/// <summary>
	/// This struct represents position of single
	/// tile on map. Note that it is expected, that
	/// map is implemented as a matrix and this is
	/// reflected in design of this class - position
	/// is represented by two fields, RowIndex 
	/// and ColIndex.
	/// </summary>
    public struct MapCoordinates
    {
		/// <summary>
		/// row index of this map position
		/// </summary>
        public int RowIndex { get; private set; }
		/// <summary>
		/// column index of this map position
		/// </summary>
        public int ColIndex { get; private set; }

		/// <summary>
		/// Ctor with row and column index.
		/// </summary>
		public MapCoordinates(int RowIndex, int ColIndex)
		{
			this.RowIndex = RowIndex;
			this.ColIndex = ColIndex;
		}

		/// <summary>
		/// This method returns manhattan distance of
		/// this position to another position.
		/// </summary>
		/// <param name="coords">position to which the manhattan
		/// distance should be calculated</param>
		/// <returns>manhattan distance</returns>
		public int GetManhattanDistance(MapCoordinates coords)
		{
			return Math.Abs(coords.RowIndex - RowIndex) + Math.Abs(coords.ColIndex - ColIndex);
		}

		/// <summary>
		/// This method returns euclidean distance of
		/// this position to another position.
		/// </summary>
		/// <param name="coords">position to which the euclidean
		/// distance should be calculated</param>
		/// <returns>euclidean distance</returns>
		public int GetEuclideanDistance(MapCoordinates coords)
		{
			return Convert.ToInt32(Math.Sqrt(Math.Pow(coords.ColIndex - ColIndex, 2) + Math.Pow(coords.RowIndex - RowIndex, 2)));
		}

		/// <summary>
		/// <para>
		/// Overloaded addition operator with <see cref="Direction"/>
		/// (note that only four directions may be represented
		/// by this class)
		/// </para>
		///  
		/// <para>
		/// For example: [x, y] + Direction.Up = 
		/// [x - 1, y] and so on
		/// </para>
		/// </summary>
		/// <returns>new coordinates</returns>
		public static MapCoordinates operator +(MapCoordinates mapCoordinates, Direction dir)
		{
			return new MapCoordinates(-1 * dir.Y + mapCoordinates.RowIndex, dir.X + mapCoordinates.ColIndex);
		}

		/// <summary>
		/// <para>
		/// Overloaded addition operator with <see cref="DirectionType"/>
		/// (note that only four directions may be represented
		/// by this class)
		/// </para>
		///  
		/// <para>
		/// For example: [x, y] + DirectionType.Up = 
		/// [x - 1, y] and so on
		/// </para>
		/// </summary>
		/// <param name="mapCoordinates"></param>
		/// <param name="dir"></param>
		/// <returns></returns>
		public static MapCoordinates operator +(MapCoordinates mapCoordinates, DirectionType dir)
		{
			return mapCoordinates + new Direction(dir);
		}

		/// <summary>
		/// <para>
		/// Overloaded addition operator with <see cref="Direction"/>
		/// (note that only four directions may be represented
		/// by this class)
		/// </para>
		///  
		/// <para>
		/// For example: [x, y] + Direction.Up = 
		/// [x - 1, y] and so on
		/// </para>
		/// </summary>
		/// <returns>new coordinates</returns>
		public static MapCoordinates operator +(Direction dir, MapCoordinates mapCoordinates)
		{
			return mapCoordinates + dir;
		}

		/// <summary>
		/// <para>
		/// Overloaded addition operator with <see cref="DirectionType"/>
		/// (note that only four directions may be represented
		/// by this class)
		/// </para>
		///  
		/// <para>
		/// For example: [x, y] + DirectionType.Up = 
		/// [x - 1, y] and so on
		/// </para>
		/// </summary>
		/// <param name="mapCoordinates"></param>
		/// <param name="dir"></param>
		/// <returns></returns>
		public static MapCoordinates operator +(DirectionType dir, MapCoordinates mapCoordinates)
		{
			return new Direction(dir) + mapCoordinates;
		}		

		/// <summary>
		/// Overloaded equality operator on two coordinates,
		/// two coordinates are equal, if their row indices
		/// as well as col indices are equal.
		/// </summary>
		/// <returns>true if they are equal, false otherwise</returns>
		public static bool operator ==(MapCoordinates c1, MapCoordinates c2)
		{
			if (c1.RowIndex == c2.RowIndex && c1.ColIndex == c2.ColIndex)
				return true;
			return false;
		}

		/// <summary>
		/// Overloaded non-equality operator on two coordinates,
		/// two coordinates are equal, if their row indices
		/// as well as col indices are equal.
		/// </summary>
		/// <returns>true if they are not equal, false otherwise</returns>
		public static bool operator !=(MapCoordinates c1, MapCoordinates c2)
		{
			return !(c1 == c2);
		}

        
    }
}
