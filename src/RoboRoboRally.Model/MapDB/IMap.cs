﻿using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.MapDB.Tiles;
using System.Collections.Generic;
using System.Linq;
using System;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.MapDB
{
	/// <summary>
	/// Exception related to map operations, this exception is thrown
	/// if an invalid operation is performed on interface <see cref="IMap"/>.
	/// One example would be setting multiple antenna tiles.
	/// </summary>
	public class MapInvalidOperationException : Exception
	{
		/// <summary>
		/// Exception constructor with message.
		/// </summary>
		/// <param name="message">exceoption message</param>
		public MapInvalidOperationException(string message) :
			base("Exception occurred during communication with map: " + message)
		{ }
	}

	/// <summary>
	/// Helper struct that holds map size.
	/// </summary>
	public struct MapSize
	{
		/// <summary>
		/// number of rows in map
		/// </summary>
		public int RowCount { get; private set; }
		/// <summary>
		/// number of columns in map
		/// </summary>
		public int ColCount { get; private set; }

		/// <summary>
		/// constructor for map size
		/// </summary>
		/// <param name="RowCount">number of rows</param>
		/// <param name="ColCount">number of columns</param>
		public MapSize(int RowCount, int ColCount)
		{
			this.RowCount = RowCount;
			this.ColCount = ColCount;
		}
	}

	/// <summary>
	/// Helper struct for retrieving some tile with coordinates
	/// from map (tiles do not remember their coordinates).
	/// </summary>
	/// <typeparam name="T">tile type restricted to classes
	/// derived from type <see cref="Tile"/></typeparam>
	public struct TileRecord<T> where T: Tile
	{
		/// <summary>
		/// Tile reference
		/// </summary>
		public T Tile { get; set; }

		/// <summary>
		/// Tile coordinates
		/// </summary>
		public MapCoordinates Coords { get; set; }
	}

	/// <summary>
	/// Helper struct for retrieving some wall with coordinates
	/// from map (again, walls do not remember their coordinates).
	/// </summary>
	/// <typeparam name="T">type of wall restricted to
	/// classes dervied from <see cref="Wall"/></typeparam>
	public struct WallRecord<T> where T : Wall
	{
		/// <summary>
		/// Wall reference
		/// </summary>
		public T Wall { get; set; }
		
		/// <summary>
		/// Wall coordinates.
		/// </summary>
		public MapCoordinates Coords { get; set; }
	}

	/// <summary>
	/// Helper struct for retrieving information about
	/// tiles affected by lasers. This struct basically
	/// holds coordinates of each affected tile and how 
	/// this laser affects each such tile.
	/// </summary>
	public struct LaserPath
	{
		/// <summary>
		/// Damage distributed along the path.
		/// </summary>
		public Damage Damage { get; set; }

		/// <summary>
		/// Number of lasers affecting the path.
		/// </summary>
		public int LaserCount { get; set; }

		/// <summary>
		/// Laser direction.
		/// </summary>
		public DirectionType LaserDirection { get; set; }

		/// <summary>
		/// First map tile affected by laser.
		/// </summary>
		public MapCoordinates Start { get; set; }
		
		/// <summary>
		/// First map tile not affected by laser. This means
		/// that this tile is exclusive and it can happen
		/// that End == Start (in that case, no tile is affected
		/// by laser).
		/// </summary>
		public MapCoordinates End { get; set; }
	}


	/// <summary>
	/// <para>
	/// This is interface for maps from the RoboRally game and it can 
	/// be used to represent starting plans, game plans as well as plans
	/// joined together.
	/// </para>
	/// 
	/// <para>
	/// It is expected, that classes implementing this interface 
	/// represnt the whole map as a matrix as this is reflected both
	/// in method signatures and in other areas of Model. With matrix
	/// representation, we mean that each tile can be accessed by two
	/// coordinates [rowIndex, colIndex] with top-left corner having coordinates
	/// [0, 0] tile to the right has coordinates [0, 1] and tile to the bottom
	/// has coordinates [1, 0].
	/// </para>
	/// </summary>
	public interface IMap
    {
		/// <summary>
		/// Map ID
		/// </summary>
        string ID { get; }
		/// <summary>
		/// Size of this map in row count and column count.
		/// </summary>
        MapSize MapSize { get; }

		/// <summary>
		/// Tile indexer that can be used to access map's tile
		/// with call map[coords]
		/// </summary>
		/// <param name="mapCoordinates">tile coordinates</param>
		/// <returns>valid tile, if the coordinates are
		/// invalid (out of map), then this method
		/// returns <see cref="OutOfMapTile"/></returns>
        Tile this[MapCoordinates mapCoordinates] { get; }
		/// <summary>
		/// Tile indexer that can be used to access map's tile
		/// with call map[0, 0]
		/// </summary>
		/// <param name="rowIndex">tile row index coordinate</param>
		/// <param name="colIndex">tile col index coordinate</param>
		/// <returns>valid tile, if the coordinates are
		/// invalid (out of map), then this method
		/// returns <see cref="OutOfMapTile"/></returns>
		Tile this[int rowIndex, int colIndex] { get; }

		/// <summary>
		/// There are several tile types that are not part of map defintion
		/// and they can be configured after loading it. One of such
		/// type is priority antenna tile. This method sets such tile.
		/// </summary>
		/// <param name="tile">tile to be set</param>
		/// <param name="coords">map coordinates for new tile, note
		/// that it is expected that before setting this tile, there
		/// is an <see cref="EmptyTile"/> at these coordinates</param>
		/// <exception cref="MapInvalidOperationException">is thrown
		/// if the provided tile can not be set at the provided
		/// coordinates; this can happen if current map tile
		/// at the provided coordinates is not <see cref="EmptyTile"/></exception>
		void SetPriorityAntennaTile(PriorityAntennaTile tile, MapCoordinates coords);
		/// <summary>
		/// There are several tile types that are not part of map defintion
		/// and they can be configured after loading it. One of such
		/// type is reboot tile. This method sets such tile.
		/// </summary>
		/// <param name="tile">tile to be set</param>
		/// <param name="coords">map coordinates for new tile, note
		/// that it is expected that before setting this tile, there
		/// is an <see cref="EmptyTile"/> at these coordinates</param>
		/// <exception cref="MapInvalidOperationException">is thrown
		/// if the provided tile can not be set at the provided
		/// coordinates; this can happen if current map tile
		/// at the provided coordinates is not <see cref="EmptyTile"/></exception>
		void SetRebootTile(RebootTile tile, MapCoordinates coords);
		/// <summary>
		/// There are several tile types that are not part of map defintion
		/// and they can be configured after loading it. One of such
		/// type is checkpoint tile. This method sets such tile.
		/// </summary>
		/// <param name="tile">tile to be set</param>
		/// <param name="coords">map coordinates for new tile, note
		/// that it is expected that before setting this tile, there
		/// is an <see cref="EmptyTile"/> at these coordinates</param>
		/// <exception cref="MapInvalidOperationException">is thrown
		/// if the provided tile can not be set at the provided
		/// coordinates; this can happen if current map tile
		/// at the provided coordinates is not <see cref="EmptyTile"/></exception>
		void SetCheckpointTile(CheckpointTile tile, MapCoordinates coords);

		/// <summary>
		/// This method tries to clear tile previously set by using
		/// methods <see cref="SetPriorityAntennaTile(PriorityAntennaTile, MapCoordinates)"/>,
		/// <see cref="SetRebootTile(RebootTile, MapCoordinates)"/> or
		/// <see cref="SetCheckpointTile(CheckpointTile, MapCoordinates)"/> and
		/// tries to restore it to an <see cref="EmptyTile"/>.
		/// </summary>
		/// <param name="coords">coordinates of the tile that should be cleared</param>
		/// <exception cref="MapInvalidOperationException">is thrown
		/// if type of the tile at the provided coordinates is not
		/// one of the following: <see cref="PriorityAntennaTile"/>,
		/// <see cref="RebootTile"/> or <see cref="CheckpointTile"/></exception>
		void ClearSetTile(MapCoordinates coords);

		/// <summary>
		/// Returns tile records for all tiles of 
		/// provided type. See <see cref="TileRecord{T}"/>.
		/// </summary>
		/// <typeparam name="T">tile type</typeparam>
		/// <returns>array of tile records</returns>
		TileRecord<T>[] GetTileRecords<T>() where T : Tile;
		/// <summary>
		/// Returns wall records for all walls of 
		/// provided type. See <see cref="WallRecord{T}"/>.
		/// </summary>
		/// <typeparam name="T">wall type</typeparam>
		/// <returns>array of wall records</returns>
		WallRecord<T>[] GetWallRecords<T>() where T : Wall;
		/// <summary>
		/// Returns each wall that is present on
		/// the provided tile.
		/// </summary>
		/// <param name="tile">tile reference</param>
		/// <returns>array of walls for the
		/// provided tile (array is empty if there
		/// are no walls)</returns>
		Wall[] GetTileWalls(Tile tile);
		/// <summary>
		/// Returns walls that are present at the
		/// provided tile that also match the provided
		/// type.
		/// </summary>
		/// <typeparam name="T">wall type</typeparam>
		/// <param name="tile">tile reference</param>
		/// <returns>array of walls (array is empty, if 
		/// no walls are present)</returns>
		T[] GetTileWalls<T>(Tile tile) where T : Wall;

		/// <summary>
		/// This method checks, if the tile at the
		/// provided coordinates is a rebooting one.
		/// This matches two types of tiles, namely
		/// <see cref="PitTile"/> and <see cref="OutOfMapTile"/>.
		/// </summary>
		/// <param name="position">tile coordinates</param>
		/// <returns>true if tile at the provided coordinates
		/// is rebooting, false otherwise</returns>
		bool IsRebootPosition(MapCoordinates position);

		/// <summary>
		/// This method checks, if there is any wall
		/// between two tiles at the provided coordinates.
		/// </summary>
		/// <param name="coords1">first tile coords</param>
		/// <param name="coords2">second tile coords</param>
		/// <returns>true if there is a wall, false otherwise</returns>
		bool IsWallBetween(MapCoordinates coords1, MapCoordinates coords2);

		/// <summary>
		/// Returns all laser paths for laser walls
		/// at this map. See <see cref="LaserPath"/>.
		/// </summary>
		/// <returns>array of laser paths</returns>
		LaserPath[] GetLaserWallLaserPaths();
	}

	/// <summary>
	/// Class that represents maps from RoboRally game.
	/// It is implemented as a matrix as prescribed by
	/// <see cref="IMap"/> interface.
	/// </summary>
    class Map : IMap
    {
        private Tile[,] tiles;
        private IList<Wall> walls;
        private Dictionary<Tile, IList<Wall>> tileWalls;
		private Dictionary<Tile, MapCoordinates> tileCoordinates;

		/// <inheritdoc/>
        public string ID { get; private set; }
		/// <inheritdoc/>
        public MapSize MapSize { get; private set; }

		/// <inheritdoc/>
        public Tile this[MapCoordinates mapCoordinates]
        {
            get
            {
                return this[mapCoordinates.RowIndex, mapCoordinates.ColIndex];
            }
            private set
            {
                this[mapCoordinates.RowIndex, mapCoordinates.ColIndex] = value;
            }
        }

		/// <inheritdoc/>
		public Tile this[int rowIndex, int colIndex]
        {
            get
            {
                if ((rowIndex < 0 || rowIndex >= MapSize.RowCount) ||
                    (colIndex < 0 || colIndex >= MapSize.ColCount))
                    return new OutOfMapTile();
                else
                    return tiles[rowIndex, colIndex];
            }
            private set
            {
                //this is private, we do not have to do the checks
                tiles[rowIndex, colIndex] = value;
            }
        }

		/// <inheritdoc/>
		public TileRecord<T>[] GetTileRecords<T>() where T : Tile
		{
			IList<TileRecord<T>> tileRecords = new List<TileRecord<T>>();

			foreach (var tile in tiles)
			{
				if (tile is T casted)
					tileRecords.Add(new TileRecord<T>() { Coords = tileCoordinates[tile], Tile = casted });

			}

			return tileRecords.ToArray();
		}

		/// <inheritdoc/>
		public T[] GetTileWalls<T>(Tile tile) where T : Wall
		{
			IList<T> tileWallsCasted = new List<T>();
			if (tileWalls.ContainsKey(tile))
			{
				foreach (var w in tileWalls[tile])
				{
					if (w is T casted)
						tileWallsCasted.Add(casted);
				}
			}

			return tileWallsCasted.ToArray();
		}

		/// <inheritdoc/>
		public Wall[] GetTileWalls(Tile tile)
		{
			return GetTileWalls<Wall>(tile);
		}

		/// <inheritdoc/>
		public bool IsRebootPosition(MapCoordinates position)
		{
			if (this[position] is PitTile ||
				this[position] is OutOfMapTile)
				return true;
			return false;
		}

		/// <inheritdoc/>
		public bool IsWallBetween(MapCoordinates coords1, MapCoordinates coords2)
		{
			var tile1 = this[coords1];
			var tile2 = this[coords2];

			var walls1 = GetTileWalls(tile1);
			var walls2 = GetTileWalls(tile2);

			foreach (var wall in walls1)
			{
				if (coords2 + wall.FacingDirection == coords1)
					return true;
			}
			foreach (var wall in walls2)
			{
				if (coords1 + wall.FacingDirection == coords2)
					return true;
			}
			return false;
		}

		/// <inheritdoc/>
		public WallRecord<T>[] GetWallRecords<T>() where T:Wall
		{
			IList<WallRecord<T>> wallRecords = new List<WallRecord<T>>();

			foreach (var wall in walls)
			{
				if (wall is T casted)
				{
					var t = casted.Tile;
					var coords = tileCoordinates[t];

					wallRecords.Add(new WallRecord<T>() { Coords = coords, Wall = casted });
				}
			}

			return wallRecords.ToArray();
		}

		private void SetNewTile(Tile newTile, MapCoordinates coords)
		{
			var oldTile = tiles[coords.RowIndex, coords.ColIndex];

			tiles[coords.RowIndex, coords.ColIndex] = newTile;

			tileCoordinates.Remove(oldTile);
			tileCoordinates.Add(newTile, coords);

			//there might also be some walls on this tile ... we need to correct them
			if (tileWalls.ContainsKey(oldTile))
			{
				var walls = tileWalls[oldTile];

				tileWalls.Remove(oldTile);

				foreach (var wall in walls)
					wall.SetNewTile(newTile);

				tileWalls.Add(newTile, walls);
			}		
		}

		/// <inheritdoc/>
		public void SetPriorityAntennaTile(PriorityAntennaTile tile, MapCoordinates coords)
		{
			if (!(this[coords] is EmptyTile))
				throw new MapInvalidOperationException("can not set antenna tile - tile is not empty");

			SetNewTile(tile, coords);
		}

		/// <inheritdoc/>
		public void SetRebootTile(RebootTile tile, MapCoordinates coords)
		{
			if (!(this[coords] is EmptyTile))
				throw new MapInvalidOperationException("can not set reboot tile - tile is not empty");

			SetNewTile(tile, coords);
		}

		/// <inheritdoc/>
		public void SetCheckpointTile(CheckpointTile tile, MapCoordinates coords)
		{
			if (!(this[coords] is EmptyTile))
				throw new MapInvalidOperationException("can not set checkpoint tile - tile is not empty");

			SetNewTile(tile, coords);
		}

		/// <inheritdoc/>
		public void ClearSetTile(MapCoordinates coords)
		{
			if (!(this[coords] is PriorityAntennaTile ||
				this[coords] is CheckpointTile ||
				this[coords] is RebootTile))
				throw new MapInvalidOperationException("can not clear tile - it was not set");

			SetNewTile(new EmptyTile(), coords);
		}

		private LaserPath GetLaserPath(WallRecord<LaserWall> laserWall)
		{
			var dmg = laserWall.Wall.Damage;

			var start = laserWall.Coords;

			var dir = laserWall.Wall.FacingDirection;

			var end = start;

			while (true)
			{
				if (this[end] is OutOfMapTile ||
					this[end] is PriorityAntennaTile)
					break;

				var prev = end;
				end += dir;

				if (IsWallBetween(end, prev))
					break;
			}

			return new LaserPath() { Damage = dmg, Start = start, End = end, LaserCount = laserWall.Wall.Count, LaserDirection = dir.GetDirectionType() };
		}

		/// <inheritdoc/>
		public LaserPath[] GetLaserWallLaserPaths()
		{
			var laserWalls = GetWallRecords<LaserWall>();

			List<LaserPath> laserPaths = new List<LaserPath>();

			foreach (var laserWall in laserWalls)
			{
				laserPaths.Add(GetLaserPath(laserWall));
			}

			return laserPaths.ToArray();
		}

		/// <summary>
		/// Map constructor.
		/// </summary>
		/// <param name="ID">map id</param>
		/// <param name="mapSize">map size</param>
		/// <param name="tiles">2d array of tiles</param>
		/// <param name="walls">list of walls</param>
		public Map(string ID, MapSize mapSize,
            Tile[,] tiles, IList<Wall> walls)
        {
            this.ID = ID;
            this.MapSize = mapSize;
            this.tiles = tiles;
            this.walls = walls;

			//initialize coordinates for tiles
			tileCoordinates = new Dictionary<Tile, MapCoordinates>();
			for (int i = 0; i < mapSize.RowCount; ++i)
			{
				for (int j = 0; j < mapSize.ColCount; ++j)
				{
					tileCoordinates.Add(tiles[i, j], new MapCoordinates(i, j));
				}
			}

			//initialize walls for tiles
            tileWalls = new Dictionary<Tile, IList<Wall>>();

            //initialize dictionary
            foreach (var wall in walls)
            {
                if (tileWalls.ContainsKey(wall.Tile))
                    tileWalls[wall.Tile].Add(wall);
                else
                    tileWalls.Add(wall.Tile, new List<Wall>() { wall });
            }
        }
    }
    
}
