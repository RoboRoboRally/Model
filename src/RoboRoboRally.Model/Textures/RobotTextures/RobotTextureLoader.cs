﻿using System.Linq;

namespace RoboRoboRally.Model.Textures.RobotTextures
{
	/// <summary>
	/// This namespace contains everything related to
	/// loading robot textures.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{

	}

	/// <summary>
	/// This class is responsible for loading robot textures.
	/// This includes both front view and top view.
	/// </summary>
	public class RobotTextureLoader : TextureLoader
    {
		/// <summary>
		/// Constructor, which requires path to directory
		/// with robot textures in Game Data, that is
		/// directory .../Textures/RobotTextures/
		/// </summary>
		/// <param name="robotTexturesDirePath">path to robot textures directory</param>
		public RobotTextureLoader(string robotTexturesDirePath) :
			base(robotTexturesDirePath)
		{ }

		private string GetDirNameFromRobot(string robotName)
		{
			return new string(robotName.ToCharArray().Where(c => !char.IsWhiteSpace(c)).ToArray());
		}

		/// <summary>
		/// Returns top view texture of robot from it's name.
		/// </summary>
		/// <param name="robotName">robot name</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetRobotTopViewTexture(string robotName)
		{
			var dirName = GetDirNameFromRobot(robotName);

			return LoadTexture(baseTextureDirectory + @"/" + dirName + "/", "TopView", robotName + "_TopView", ".png");
		}

		/// <summary>
		/// Returns front view texture of robot from it's name.
		/// </summary>
		/// <param name="robotName">robot name</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetRobotFrontViewTexture(string robotName)
		{
			var dirName = GetDirNameFromRobot(robotName);

			return LoadTexture(baseTextureDirectory + @"/" + dirName + "/", "FrontView", robotName + "_FrontView", ".png");
		}				
    }
}
