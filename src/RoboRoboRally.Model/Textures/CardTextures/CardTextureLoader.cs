﻿namespace RoboRoboRally.Model.Textures.CardTextures
{
	/// <summary>
	/// This namespace contains everything related to
	/// loading card textures.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{

	}

	/// <summary>
	/// This class is responsible for loading card textures.
	/// This includes ugprade card textures, programming
	/// card textures and choice card textures.
	/// </summary>
	public class CardTextureLoader : TextureLoader
	{
		string programmingCardsDirPath;
		string upgradeCardsDirPath;
		string choiceCardsDirPath;

		/// <summary>
		/// Constructor, which requires path to directory
		/// with card textures in Game Data, that is
		/// directory .../Textures/CardTextures/
		/// </summary>
		/// <param name="cardTexturesDirPath">path to card textures directory</param>
		public CardTextureLoader(string cardTexturesDirPath) :
			base(cardTexturesDirPath)
		{
			this.programmingCardsDirPath = cardTexturesDirPath + @"/ProgrammingCards/";
			this.upgradeCardsDirPath = cardTexturesDirPath + @"/UpgradeCards/";
			this.choiceCardsDirPath = cardTexturesDirPath + @"/UpgradeCards/ChoiceCards/";
		}

		/// <summary>
		/// Constructor that should be used solely from mobile application
		/// as this constructor sets up several private fields correctly. 
		/// To better understand relation of mobile application and texture loading
		/// functionality of model, see summary of <see cref="TextureLoader"/>.
		/// </summary>
		public CardTextureLoader() :
			base("GameData/Textures/CardTextures")
		{
			this.programmingCardsDirPath = "GameData/Textures/CardTextures/ProgrammingCards/";
			this.upgradeCardsDirPath = "GameData/Textures/CardTextures/UpgradeCards/";
			this.choiceCardsDirPath = "GameData/Textures/CardTextures/UpgradeCards/ChoiceCards/";
		}

		/// <summary>
		/// Returns programming card texture.
		/// </summary>
		/// <param name="programmingCardName">card identifier</param>
		/// <param name="playerColor">player's color</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetProgrammingCardTexture(string programmingCardName, string playerColor)
		{
			return LoadTexture(programmingCardsDirPath + playerColor + @"/", programmingCardName, playerColor + "_" + programmingCardName, defaultExtension);
		}

		/// <summary>
		/// Returns back texture of programming card.
		/// </summary>
		/// <param name="playerColor">player's color</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetProgrammingCardBackTexture(string playerColor)
		{
			return LoadTexture(programmingCardsDirPath, "ProgrammingCardBack");
		}

		/// <summary>
		/// Returns texture of upgrade card.
		/// </summary>
		/// <param name="upgradeCardName">card identifier</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetUpgradeCardTexture(string upgradeCardName)
		{
			return LoadTexture(upgradeCardsDirPath, upgradeCardName);
		}

		/// <summary>
		/// Returns back texture of upgrade card.
		/// </summary>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetUpgradeCardBackTexture()
		{
			return LoadTexture(upgradeCardsDirPath, "UpgradeCardBack");
		}

		/// <summary>
		/// Returns choice card texture.
		/// </summary>
		/// <param name="upgradeCardName">upgrade card identifier</param>
		/// <param name="choiceCardName">choice card identifier</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetChoiceCardTexture(string upgradeCardName, string choiceCardName)
		{
			return LoadTexture(choiceCardsDirPath, upgradeCardName + "_" + choiceCardName);
		}
		
		//------------------
		//----- for mobile app
		//------------------

		/// <summary>
		/// This method returns path to programming card
		/// texture within the GameData database. This
		/// method should be used in mobile application only.
		/// To better understand why, see summary of 
		/// <see cref="TextureLoader"/>.
		/// </summary>
		/// <param name="programmingCardName"></param>
		/// <param name="playerColor"></param>
		/// <returns>path to programming card texture
		/// prepared for mobile application</returns>
		public string GetProgrammingCardTexturePath(string programmingCardName, string playerColor)
		{
			return programmingCardsDirPath + playerColor + @"/" + programmingCardName + defaultExtension;
		}

		/// <summary>
		/// This method returns path to back texture
		/// of programming card within the GameData database.
		/// This method should be used in mobile application only.
		/// To better understand why, see summary of 
		/// <see cref="TextureLoader"/>.
		/// </summary>
		/// <returns>path to back texture of programming card
		/// prepared for mobile application</returns>
		public string GetProgrammingCardBackTexturePath()
		{
			return programmingCardsDirPath + "ProgrammingCardBack" + defaultExtension;
		}

		/// <summary>
		/// This method returns path to invalid card texture
		/// within the GameData databse. This methoud
		/// should be used in mobile application only.
		/// To better understand why, see summary of 
		/// <see cref="TextureLoader"/>.
		/// </summary>
		/// <returns>path to invalid card texture
		/// prepared for mobile application</returns>
		public string GetInvalidCardTexturePath()
		{
			return baseTextureDirectory + "InvalidTexture" + defaultExtension;
		}

		/// <summary>
		/// This method returns path to back texture of
		/// upgrade card within the GameData database.
		/// This method should be used in mobile application only.
		/// To better understand why, see summary of 
		/// <see cref="TextureLoader"/>.
		/// </summary>
		/// <returns>path to back texture of upgrade card
		/// prepared for mobile application</returns>
		public string GetUpgradeCardBackTexturePath()
		{
			return upgradeCardsDirPath + "UpgradeCardBack" + defaultExtension;
		}

		/// <summary>
		/// This method returns path to upgrade card texture
		/// within the GameData databse. This method should
		/// be used in mobile application only.
		/// To better understand why, see summary of 
		/// <see cref="TextureLoader"/>.
		/// </summary>
		/// <param name="upgradeCardName">upgrade card name</param>
		/// <returns>path to upgrade card texture 
		/// prepared for mobile application</returns>
		public string GetUpgradeCardTexturePath(string upgradeCardName)
		{
			return upgradeCardsDirPath + upgradeCardName + defaultExtension;
		}

		/// <summary>
		/// This method returns path to choice card texture
		/// within the GameData databse. This method should
		/// be used in mobile application only.
		/// To better understand why, see summary of 
		/// <see cref="TextureLoader"/>.
		/// </summary>
		/// <param name="upgradeCardName">upgrade card identifier</param>
		/// <param name="choiceCardName">choice card identifier</param>
		/// <returns>path to choice card texture
		/// prepared for mobile application</returns>
		public string GetChoiceCardTexturePath(string upgradeCardName, string choiceCardName)
		{
			return choiceCardsDirPath + upgradeCardName + "_" + choiceCardName + defaultExtension;
		}	

	}
}
