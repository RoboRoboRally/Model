﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RoboRoboRally.Model.Textures
{
	/// <summary>
	/// <para>
	/// This namespace contains everything that is related to
	/// Model's functionality of loading textures from database
	/// GameData.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceGroupDoc
	{
	}


	/// <summary>
	/// This exception is thrown when an error
	/// occurs during texture loading. However, note
	/// that each texture folder is provided with
	/// "invalid texture" image, so this exception is thrown
	/// in rare occassions, like missing invalid texture in 
	/// the folders.
	/// </summary>
	public class TextureLoadingException : Exception
	{
		/// <summary>
		/// Exception ctor with message.
		/// </summary>
		/// <param name="msg">exception messaage</param>
		public TextureLoadingException(string msg) :
			base("Exception occured during texture loading: " + msg)
		{ }
	}

	/// <summary>
	/// <para>
	/// This class provides common functionality for other
	/// texture loaders like <see cref="MapTextures.MapTextureLoader"/>
	/// or <see cref="CardTextures.CardTextureLoader"/>. This includes
	/// loading textures, precaching or loading invalid texture
	/// if the requested texture was not found.
	/// </para>
	/// 
	/// <para>
	/// It is worth mentioning, why each method for texture loading returns
	/// the image as an array of bytes instead of some wrapper like 
	/// Bitmap class. The reason for this is mobile application that uses
	/// these texture loaders. Mobile application is written in C# aswell
	/// and it runs under some form of Mono runtime. For some reason, assembly
	/// that defines this Bitmap class can not be used under this environment. And
	/// for that, we had to change signature of each metho so that it returns
	/// only array of bytes, which can, however, be easily used to create Bitmap.
	/// </para>
	/// 
	/// <para>
	/// We had find out about few other limitations that mobile application imposes
	/// on Model. 
	/// </para>
	/// 
	/// <para>
	/// First, it can not even used model's methods for texture loading,
	/// as Android has to have each texture included in application in a special form 
	/// as an embedded resource and there was no way of using the methods that 
	/// can be with no problem used in Game Terminal. For this reason, the methods
	/// provided to mobile applicaiton 
	/// (like <see cref="CardTextures.CardTextureLoader.GetUpgradeCardBackTexturePath"/>)
	/// only return relative path to this texture from which, mobile
	/// can then construct path to load the embedded resource in a proper manner.
	/// </para>
	/// 
	/// <para>
	/// Second limitation is that while constructing the path to texture, model
	/// has to provide its full name, including extension. There was no way
	/// around that and now texture loaders need to know the extensions of
	/// individual textures (however, they are only jpegs and pngs).
	/// </para> 
	/// 
	/// </summary>
	public abstract class TextureLoader
	{
		/// <summary>
		/// Each texture loader that inherits from this class 
		/// has to setup this field - it is a root of texture
		/// directory and it should always contain invalid texture image.
		/// </summary>
		protected string baseTextureDirectory;
		/// <summary>
		/// This field defines default extension of each texture.
		/// This is limitation imposed by mobile application. See 
		/// summary of <see cref="TextureLoader"/> for more details.
		/// </summary>
	    protected const string defaultExtension = ".jpg";

		Dictionary<string, byte[]> cachedTextures;

		/// <summary>
		/// Constructor, which accepts path to base texture
		/// directory, which should contain invalid texture image.
		/// </summary>
		/// <param name="baseTextureDirectory">path to base texture
		/// directory in GameData</param>
		protected TextureLoader(string baseTextureDirectory)
		{
			this.cachedTextures = new Dictionary<string, byte[]>();
			this.baseTextureDirectory = baseTextureDirectory + "/";
		}

		/// <summary>
		/// Loads texture with the passed properties. This texture
		/// is then cached for further calls. If no texture is found,
		/// this method returns invalid texture. If even invalid texture
		/// is found, this method throws exception.
		/// </summary>
		/// <param name="dirPath">full path to directory</param>
		/// <param name="textureName">name of the texture file</param>
		/// <param name="extension">optional extension parameter, default is .jpg</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		/// <exception cref="TextureLoadingException">is thrown if no 
		/// invalid texture is found (in case the original texture
		/// was not found)</exception>
		protected byte[] LoadTexture(string dirPath, string textureName, string extension = defaultExtension)
		{
			return LoadTexture(dirPath, textureName, textureName, extension);
		}

		/// <summary>
		/// Loads texture with the passed properties. This texture
		/// is then cached for further calls. If no texture is found,
		/// this method returns invalid texture. If even invalid texture
		/// is found, this method throws exception.
		/// </summary>
		/// <param name="dirPath">full path to directory</param>
		/// <param name="textureName">name of the texture file</param>
		/// <param name="textureId">texture id for caching - this can be
		/// handy in some situations as there might be multiple texture
		/// with the same name (placed in different folders)</param>
		/// <param name="extension">optional extension parameter, default is .jpg</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		/// <exception cref="TextureLoadingException">is thrown if no 
		/// invalid texture is found (in case the original texture
		/// was not found)</exception>
		protected byte[] LoadTexture(string dirPath, string textureName, string textureId, string extension = defaultExtension)
		{
			if (cachedTextures.ContainsKey(textureId))
				return cachedTextures[textureId];

			try
			{
				byte[] img = System.IO.File.ReadAllBytes(dirPath + textureName + extension);
				cachedTextures.Add(textureId, img);
				return img;
			}
			catch (Exception ex) when (ex is FileNotFoundException || ex is DirectoryNotFoundException)
			{
				return GetInvalidTexture();
			}
		}

		/// <summary>
		/// This method tries to load invalid texture. It is used
		/// if the original texture was not found. This method may
		/// throw exception if the invalid texture is not found.
		/// </summary>
		/// <returns>invalid texture loaded as an array of bytes</returns>
		/// <exception cref="TextureLoadingException">is thrown if no 
		/// invalid texture is found (in case the original texture
		/// was not found)</exception>
		protected byte[] GetInvalidTexture()
		{
			string invalidTextureName = "InvalidTexture";

			if (cachedTextures.ContainsKey(invalidTextureName))
				return cachedTextures[invalidTextureName];

			try
			{
				byte[] img = System.IO.File.ReadAllBytes(baseTextureDirectory + invalidTextureName + defaultExtension);
				cachedTextures.Add(invalidTextureName, img);
				return img;
			}
			catch (FileNotFoundException e)
			{
				throw new TextureLoadingException("can not load invalid texture");
			}
		}

	}
}
