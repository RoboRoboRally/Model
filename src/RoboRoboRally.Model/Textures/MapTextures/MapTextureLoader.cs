﻿using System;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Textures.MapTextures
{
	/// <summary>
	/// This namespace contains everything related to
	/// loading map textures.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{

	}

	/// <summary>
	/// Helper class for returning texture info about tiles
	/// from <see cref="MapTextureLoader"/>.
	/// </summary>
	public class TileTextureInfo
	{
		/// <summary>
		/// Texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>.
		/// </summary>
		public byte[] Texture { get; set; }

		/// <summary>
		/// Rotation value.
		/// </summary>
		public int RotationEnumValue { get; set; } = 0;

		/// <summary>
		/// Flip value.
		/// </summary>
		public int FlipEnumValue { get; set; } = 0;
	}

	/// <summary>
	/// <para>
	/// This class is responsible for loading any textures
	/// that are related to map - this includes tiles and walls.
	/// </para>
	/// 
	/// <para>
	/// Note that methods within this class were adapted to
	/// requirements and implementation in Game Terminal which
	/// has to display game's map.
	/// </para>
	/// 
	/// <para>
	/// Firstly, concerning tiles - GameData holds one texture 
	/// of tile for type of tile. That means, some tiles need
	/// to be rotated before they are displayed and finding a proper
	/// rotation for a tile with one outgoing belt and two ingoing
	/// belts requires good understanding of other parts of model.
	/// For this reason, methods that load tiles prepare this rotation
	/// for outside world (GameTerminal). This prepared rotation
	/// is included in a helper class <see cref="TileTextureInfo"/>
	/// that is returned from these methods. (Note that as we 
	/// can not return Bitmap class (see <see cref="TextureLoader"/>
	/// for explanation), we need workaround in the form of other fields
	/// in the helper class.
	/// </para>
	/// 
	/// <para>
	/// Secondly, concerning walls - current implementation relies on the fact that
	/// Game Terminal will assemble full wall texture from an empty wall oriented horizontally
	/// and different decorators (for example, two laser heads) which
	/// are again oriented horizontally. The reason for this is that we
	/// did not want to have so many combinations of different textures.
	/// </para>
	/// </summary>
	public class MapTextureLoader : TextureLoader
	{
		string tileTexturesDirPath;
		string wallTexturesDirPath;

		/// <summary>
		/// Constructor, which requires path to directory
		/// with map textures in Game Data, that is directory
		/// .../Textures/MapTextures/
		/// </summary>
		/// <param name="mapTexturesDirPath">path to map textures directory</param>
		public MapTextureLoader(string mapTexturesDirPath) :
			base(mapTexturesDirPath)
		{
			tileTexturesDirPath = mapTexturesDirPath + @"/Tiles/";
			wallTexturesDirPath = mapTexturesDirPath + @"/Walls/";
		}

		/// <summary>
		/// Returns push panel texture oriented horizontally 
		/// facing up.
		/// </summary>
		/// <param name="pushPanelWall">wall with push panel</param>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetPushPanelTexture(PushPanelWall pushPanelWall)
		{
			if (pushPanelWall.Registers.Length == 2)
			{
				if (pushPanelWall.Registers[0] == 2 && pushPanelWall.Registers[1] == 4)
					return LoadTexture(wallTexturesDirPath, "PushPanel_b", ".png");
				else
					return GetInvalidTexture();
			}
			else if (pushPanelWall.Registers.Length == 3)
			{
				if (pushPanelWall.Registers[0] == 1 &&
					pushPanelWall.Registers[1] == 3 &&
					pushPanelWall.Registers[2] == 5)
					return LoadTexture(wallTexturesDirPath, "PushPanel_a", ".png");
				else
					return GetInvalidTexture();
			}
			else
				return GetInvalidTexture();
		}

		/// <summary>
		/// This method returns laser head texture
		/// oriented horizontally facing up.
		/// </summary>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetLaserHeadTexture()
		{
			return LoadTexture(wallTexturesDirPath, "LaserHead", ".png");
		}

		/// <summary>
		/// This method empty wall texture
		/// oriented horizontally.
		/// </summary>
		/// <returns>texture loaded as an array of bytes, to understand
		/// why we return array of bytes, see summary of <see cref="TextureLoader"/>
		/// </returns>
		public byte[] GetEmptyWallTexture()
		{
			return LoadTexture(wallTexturesDirPath, "EmptyWall", ".png");
		}		

		private TileTextureInfo GetInvalidTileTextureInfo()
		{
			return new TileTextureInfo() { Texture = GetInvalidTexture() };
		}

		private TileTextureInfo GetNamedTileTexture(string textureName)
		{
			return new TileTextureInfo() { Texture = LoadTexture(tileTexturesDirPath, textureName) };
		}

		private TileTextureInfo GetRotatingTileTexture(RotatingTile tile)
		{
			if (tile.RotationType == RotationType.Left)
			{
				return new TileTextureInfo() { Texture = LoadTexture(tileTexturesDirPath, "RotatingGear_left") };
			}
			else if (tile.RotationType == RotationType.Right)
			{
				return new TileTextureInfo() { Texture = LoadTexture(tileTexturesDirPath, "RotatingGear_right") };
			}
			else
				return GetInvalidTileTextureInfo();
		}

		private TileTextureInfo GetRebootTileTexture(RebootTile tile)
		{
			var rotation = GetRotationTransformationValue(DirectionType.Up, tile.OutDirection);

			return new TileTextureInfo()
			{
				Texture = LoadTexture(tileTexturesDirPath, "RebootTile"),
				RotationEnumValue = rotation
			};
		}

		private TileTextureInfo GetPriorityAntennaTileTexture(PriorityAntennaTile tile)
		{
			var rotation = GetRotationTransformationValue(DirectionType.Up, tile.OutDirection);

			return new TileTextureInfo()
			{
				Texture = LoadTexture(tileTexturesDirPath,
				"PriorityAntennaTile"),
				RotationEnumValue = rotation
			};
		}

		private int GetRotationTransformationValue(DirectionType bitmapDirection, Direction intendedDirection)
		{
			var degreesBetween = new Direction(bitmapDirection).GetDegreesBetween(intendedDirection);

			var rotationEnumValue = degreesBetween / 90;

			//this is really ugly, but this method is doing the rotation in an opposite
			//coordinate system (y axis pointing down)
			if (rotationEnumValue == 1)
				rotationEnumValue = 3;
			else if (rotationEnumValue == 3)
				rotationEnumValue = 1;

			return rotationEnumValue;
		}

		private int GetFlipTransformationValue(Direction outgoingDirection, Direction ingoingDirection)
		{
			var flipValue = 0;
			//this flip is actually correct (ingoing direction == from right to left => 270) !!
			int degrees = outgoingDirection.GetDegreesBetween(ingoingDirection);
			bool flip = degrees == 90 ? true : false;

			if (flip)
			{
				var beltDir = outgoingDirection.GetDirectionType();
				if (beltDir == DirectionType.Left || beltDir == DirectionType.Right)
					flipValue = 6; //RotateNoneFlipY
				else
					flipValue = 4; //RotateNoneFlipX
			}

			return flipValue;
		}

		private TileTextureInfo GetTwoBeltMergedTexture(BeltTile beltTile, Direction ingoingDirection)
		{
			int rotation = GetRotationTransformationValue(DirectionType.Up, beltTile.OutDirection);

			int flip = GetFlipTransformationValue(beltTile.OutDirection, ingoingDirection);

			if (beltTile is BlueBeltTile)
			{
				return new TileTextureInfo()
				{
					Texture = LoadTexture(tileTexturesDirPath, "BlueBelt_merged1"),
					RotationEnumValue = rotation,
					FlipEnumValue = flip
				};
			}
			else
			{
				//otherwise, we do not have the correct textures ...
				return GetInvalidTileTextureInfo();
			}
		}		

		private TileTextureInfo GetSingleCurvedBeltTexture(BeltTile beltTile, Direction ingoingDirection)
		{
			int rotation = GetRotationTransformationValue(DirectionType.Up, beltTile.OutDirection);

			int flip = GetFlipTransformationValue(beltTile.OutDirection, ingoingDirection);

			if (beltTile is BlueBeltTile)
			{
				return new TileTextureInfo()
				{
					Texture = LoadTexture(tileTexturesDirPath, "BlueBelt_curved_up"),
					RotationEnumValue = rotation,
					FlipEnumValue = flip
				};
			}
			else if (beltTile is GreenBeltTile)
			{
				return new TileTextureInfo()
				{
					Texture = LoadTexture(tileTexturesDirPath, "GreenBelt_curved_up"),
					RotationEnumValue = rotation,
					FlipEnumValue = flip
				};
			}
			else
				return GetInvalidTileTextureInfo();
		}

		private TileTextureInfo GetStraightBeltTexture(BeltTile beltTile)
		{
			var rotation = GetRotationTransformationValue(DirectionType.Up, beltTile.OutDirection);

			if (beltTile is BlueBeltTile)
			{
				var texture = LoadTexture(tileTexturesDirPath, "BlueBelt_up");
				return new TileTextureInfo() { Texture = texture, RotationEnumValue = rotation };
			}
			else if (beltTile is GreenBeltTile)
			{
				var texture = LoadTexture(tileTexturesDirPath, "GreenBelt_up");
				return new TileTextureInfo() { Texture = texture, RotationEnumValue = rotation };
			}
			else
				return GetInvalidTileTextureInfo();
		}

		private TileTextureInfo GetBeltTileTexture<T>(IMap map, MapCoordinates coordinates, T beltTile) where T : BeltTile
		{
			var outDir = beltTile.OutDirection;

			Tuple<Direction, Direction> perpDirs = outDir.GetPerpDirections();
			Direction oppositeDir = outDir.GetOppositeDirection();

			var perpCoord1 = coordinates + perpDirs.Item1;
			var perpCoord2 = coordinates + perpDirs.Item2;
			var oppositeCoord = coordinates + oppositeDir;

			//although it can be the case, that both of these conditions are true
			//and the texture would be in that case different, we do not currently have these textures
			if (map[perpCoord1] is T perpBeltTile && (perpCoord1 + perpBeltTile.OutDirection == coordinates))
			{
				if (map[oppositeCoord] is T oppBeltTile && (oppositeCoord + oppBeltTile.OutDirection == coordinates))
					return GetTwoBeltMergedTexture(beltTile, perpDirs.Item1);
				else
					return GetSingleCurvedBeltTexture(beltTile, perpDirs.Item1);
			}
			else if (map[perpCoord2] is T perpBeltTile2 && (perpCoord2 + perpBeltTile2.OutDirection == coordinates))
			{
				if (map[oppositeCoord] is T oppBeltTile && (oppositeCoord + oppBeltTile.OutDirection == coordinates))
					return GetTwoBeltMergedTexture(beltTile, perpDirs.Item2);
				else
					return GetSingleCurvedBeltTexture(beltTile, perpDirs.Item2);
			}
			else
			{
				//we have a straight belt
				return GetStraightBeltTexture(beltTile);
			}
		}

		/// <summary>
		/// This method is used to get
		/// tile texture.
		/// </summary>
		/// <param name="map">map</param>
		/// <param name="coords">texture coordinates</param>
		/// <returns>information about tile texture, see
		/// <see cref="TileTextureInfo"/> for more info</returns>
		public TileTextureInfo GetTileTexture(IMap map, MapCoordinates coords)
		{
			var tile = map[coords];

			if (tile is EmptyTile ||
				tile is StartingTile ||
				tile is PitTile ||
				tile is CheckpointTile ||
				tile is EnergyTile)
			{
				return GetNamedTileTexture(tile.GetType().Name);
			}
			else if (tile is TextTile)
			{
				return GetNamedTileTexture(typeof(EmptyTile).Name);
			}
			else if (tile is RotatingTile rotatingTile)
				return GetRotatingTileTexture(rotatingTile);
			else if (tile is RebootTile rebootTile)
				return GetRebootTileTexture(rebootTile);
			else if (tile is PriorityAntennaTile antennaTile)
				return GetPriorityAntennaTileTexture(antennaTile);
			else if (tile is BeltTile beltTile)
			{
				if (beltTile is GreenBeltTile gBeltTile)
					return GetBeltTileTexture(map, coords, gBeltTile);
				else if (beltTile is BlueBeltTile bBeltTile)
					return GetBeltTileTexture(map, coords, bBeltTile);
				else
					return GetInvalidTileTextureInfo();
			}
			else
			{
				return GetInvalidTileTextureInfo();
			}
		}

		/// <summary>
		/// This method is used to get
		/// tile texture.
		/// </summary>
		/// <param name="map">map</param>
		/// <param name="rowIndex">texture row index within map</param>
		/// <param name="colIndex">texture col index within map</param>
		/// <returns>information about tile texture, see
		/// <see cref="TileTextureInfo"/> for more info</returns>
		public TileTextureInfo GetTileTexture(IMap map, int rowIndex, int colIndex)
		{
			return GetTileTexture(map, new MapCoordinates(rowIndex, colIndex));
		}
    }
}
