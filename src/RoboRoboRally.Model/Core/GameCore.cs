﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

using RoboRoboRally.Model.Settings;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.CardDB;

namespace RoboRoboRally.Model.Core
{
	/// <summary>
	/// This exception can be thrown in many ocassions while
	/// the game is running. Such exception is caught by the main
	/// loop of the game, error is reported to the server and 
	/// the game terminates correctly.
	/// </summary>
	class RunningGameException : Exception
	{
		/// <summary>
		/// Exception ctor with message.
		/// </summary>
		public RunningGameException(string message) :
			base("Exception occurred while the game was running: " + message)
		{ }
	}

	/// <summary>
	/// Helper struct for players request to either
	/// leave or join game.
	/// </summary>
	struct PlayerGameParticipationRequest
	{
		public string RobotName { get; set; }

		public PlayerGameParticipationStatus Request { get; set; }
	}

	/// <summary>
	/// Enum for expressing players intention to join or leave game.
	/// </summary>
	enum PlayerGameParticipationStatus
	{
		Join,
		Leave
	}

	/// <summary>
	/// Enum for represting current state of the game.
	/// </summary>
	enum GameRunningState
	{
		NotRunning, Running, Paused
	}

	/// <summary>
	/// Enum for game flow requests passed
	/// from the server to the running game.
	/// </summary>
	enum GameRunningRequest
	{
		Pause, Resume, Terminate
	}

	/// <summary>
	/// <para>
	/// This class represents skeleton of a running game. It 
	/// first receives settings of a new game from <see cref="GameCore"/>
	/// and then it repeatedly performs the main game loop in a new thread. 
	/// </para>
	/// 
	/// <para>
	/// The newly launched game needs to receive messages from the server
	/// (like pausing the game, etc). For this purpose, this class holds
	/// lock that is shared with the <see cref="Game"/> class and it 
	/// protects several shared data structures for receiving these
	/// messages. All accesses to the public shared structures need to
	/// first obtain the shared lock.
	/// </para>
	/// </summary>
	class GameCore
    {
		//lock shared between game core and game object
		private object gameCoreLock;

		private IGameState gameState;

		private PlayerSettings defaultPlayerSettings;

		/// <summary>
		/// Current state of the game. This field is shared
		/// and accesses to it have to protected by first acquiring
		/// the shared lock.
		/// </summary>
		public GameRunningState GameRunningState { get; private set; }

		/// <summary>
		/// Queue for requests that want to change 
		/// participation status of some player in the
		/// game (leave or join).
		/// This structure is shared and accesses to it have to
		/// be protected by first acquiring the shared lock.
		/// </summary>
		public Queue<PlayerGameParticipationRequest> PlayerGameParticipationRequests { get; private set; }

		/// <summary>
		/// Queue of requests to change the game flow.
		/// This structure is shared and accesses to it have to
		/// be protected by first acquiring the shared lock.
		/// </summary>
		public Queue<GameRunningRequest> GameRunningRequests { get; private set; }

		/// <summary>
		/// Ctor that receives lock shared by <see cref="Game"/> class and
		/// that initializes shared and private data structures.
		/// </summary>
		public GameCore(object gameCoreLock)
		{
			this.gameCoreLock = gameCoreLock;
			this.GameRunningState = GameRunningState.NotRunning;
			this.PlayerGameParticipationRequests = new Queue<PlayerGameParticipationRequest>();
			GameRunningRequests = new Queue<GameRunningRequest>();
		}

		private void EndGame()
		{
			gameState.Notifier.GameIsEnding();
			Thread.Sleep(100);

			lock (gameCoreLock)
			{
				GameRunningState = GameRunningState.NotRunning;
				GameRunningRequests.Clear();

				PlayerGameParticipationRequests.Clear();

				gameState.Notifier.GameOver();
				gameState = null;
			}
		}

		private Robot CreateNewRobot(string robotName)
		{
			var startingTiles = gameState.Map.GetTileRecords<StartingTile>();

			if (gameState.GameActiveRobots.Length >= startingTiles.Length)
				throw new RunningGameException("Can not add new player - player limit has been reached");

			//dummy values ... they have to be changed because of the check above
			MapCoordinates startingPosition = new MapCoordinates();
			Direction startingDirection = new Direction();

			var freeStartingTiles = startingTiles.Where(st => !gameState.IsTileOccupied(st.Coords)).ToList();
			
			var startingTile = freeStartingTiles[MyRandom.Random.Next(0, freeStartingTiles.Count)];

			startingPosition = startingTile.Coords;
			startingDirection = startingTile.Tile.OutDirection;

			//pack
			var pack = defaultPlayerSettings.PlayerProgrammingCards.CopyPack();
			pack.ShuffleDrawingPile();

			var energyAmount = defaultPlayerSettings.EnergyAmount;

			var registerCount = defaultPlayerSettings.RegisterBoardSize;

			var registerBoard = new RegisterBoard(defaultPlayerSettings.RegisterBoardSize);

			var defaultDamage = defaultPlayerSettings.DefaultPlayerDamage;

			var upgradeBoard = new UpgradeCardBoard(defaultPlayerSettings.UpgradeCardsLimits);

			return new Robot(robotName, energyAmount, startingDirection, startingPosition, pack, registerBoard, upgradeBoard, defaultDamage);
		}

		private void PossiblyModifyPlayerCount()
		{
			lock (gameCoreLock)
			{
				List<string> readyToJoinPlayers = new List<string>();
				List<string> readyToLeavePlayers = new List<string>();				

				//this 
				while (PlayerGameParticipationRequests.Count > 0)
				{
					var request = PlayerGameParticipationRequests.Dequeue();

					if (request.Request == PlayerGameParticipationStatus.Join)
					{
						if (readyToLeavePlayers.Contains(request.RobotName))
						{
							readyToLeavePlayers.RemoveAll(r => r == request.RobotName);
						}
						else
						{
							readyToJoinPlayers.Add(request.RobotName);
						}
					}
					else if (request.Request == PlayerGameParticipationStatus.Leave)
					{
						if (readyToJoinPlayers.Contains(request.RobotName))
						{
							readyToJoinPlayers.RemoveAll(r => r == request.RobotName);
						}
						else
						{
							readyToLeavePlayers.Add(request.RobotName);
						}
					}
				}

				//first, remove players - we may eliminate some collisions
				foreach (var player in readyToLeavePlayers)
				{
					var robot = gameState.GameActiveRobots.FirstOrDefault(x => x.Name == player);

					if (robot == null)
					{
						throw new RunningGameException($"can not remove player {player} from game as he does not play");
					}

					gameState.PutRobotToSleepBeforeTurn(robot);
				}

				//now, handle new player that want to join
				foreach (var player in readyToJoinPlayers)
				{

					if (gameState.GameActiveRobots.Any(r => r.Name == player))
					{
						throw new RunningGameException($"can not add player {player} ... this player is already playing the game");
					}

					if (gameState.FinishedRobots.Any(r => r.Name == player))
					{
						throw new RunningGameException($"can not add player {player} ... this player had already finished the game");
					}

					if (gameState.DormantRobots.Any(r => r.Name == player))
					{
						var dormantRobot = gameState.DormantRobots.First(r => r.Name == player);

						//we will wait one turn if there was some robot occupying the
						//inactive robot's previous position
						if (!gameState.TryRestoreDormantRobot(dormantRobot))
							PlayerGameParticipationRequests.Enqueue(new PlayerGameParticipationRequest() { RobotName = player, Request = PlayerGameParticipationStatus.Join });
					}
					//otherwise, we create a new robot
					else
					{
						Robot newRobot = CreateNewRobot(player);
						gameState.AddNewRobot(newRobot);
					}
				}
			}
		}

		//returns true if the game should terminate
		private bool HandleInputRequests()
		{
			lock (gameCoreLock)
			{
				if (GameRunningRequests.Count > 0)
				{
					if (GameRunningRequests.Any(r => r == GameRunningRequest.Terminate))
						return true;

					if (GameRunningRequests.Last() == GameRunningRequest.Resume)
					{
						GameRunningRequests.Clear();
						return false;
					}
					else
					{
						GameRunningState = GameRunningState.Paused;
						gameState.Notifier.GamePaused();
						GameRunningRequests.Clear();
					}
				}
				else
					return false;
			}

			//if we reach this, the game is paused and we have to wait for resume or termination
			while (true)
			{
				lock (gameCoreLock)
				{
					if (GameRunningRequests.Count > 0)
					{
						if (GameRunningRequests.Any(r => r == GameRunningRequest.Terminate))
							return true;

						if (GameRunningRequests.Last() == GameRunningRequest.Resume)
						{
							GameRunningState = GameRunningState.Running;
							GameRunningRequests.Clear();
							gameState.Notifier.GameResumed();
							return false;
						}
						else
						{
							GameRunningRequests.Clear();
						}
					}
				}
				Thread.Sleep(100);
			}

		}

		private void MainGameLoop(Robot[] startingRobots)
		{
			try
			{
				gameState.Notifier.GameStarted(gameState.Map.MapSize);

				foreach (var robot in startingRobots)
					gameState.AddNewRobot(robot);
				
				while (true)
				{
					if (HandleInputRequests())
						break;

					//now, check if there are new players that want to join
					PossiblyModifyPlayerCount();

					//if there are no players that want to play, then reset the whole game
					if (gameState.GameActiveRobots.Length == 0)
						break;

					gameState.PerformSingleGameLoop();

					//checkpoints ...
					if (gameState.GameActiveRobots.Length == 0)
						break;
				}
			}
			catch (RunningGameException e)
			{
				gameState.Notifier.FatalRunningGameError("msg: " + e.Message + "... stack trace:" + e.StackTrace);
			}
			catch (Exception e)
			{
				gameState.Notifier.FatalUnexpectedError("msg: " + e.Message + "... stack trace:" + e.StackTrace);
			}
			finally
			{
				EndGame();
			}
		}

		/// <summary>
		/// Special method for passing asynchronous messages from clients
		/// (players) to a running game. These messages are related to
		/// uses of some special upgrade cards, see <see cref="CardDB.Cards.UpgradeCard"/>
		/// for more information about implementation of upgrade cards.
		/// </summary>
		public void AddAsyncUpgradeCardUseInfo(AsyncUpgradeCardUseInfo useInfo)
		{
			gameState.AsyncUpgradeCardUseInfoQueue.Enqueue(useInfo);
		}

		/// <summary>
		/// Launches new game in a new thread.
		/// </summary>
		public void StartGame(NewGameSetup newGameSetup)
		{
			this.gameState = newGameSetup.GameState;
			this.defaultPlayerSettings = newGameSetup.DefaultPlayerSettings;

			//run the new game main loop in a new thread
			this.GameRunningState = GameRunningState.Running;
			new Thread(() => MainGameLoop(newGameSetup.StartingPlayers)).Start();

		}		
	}
}
