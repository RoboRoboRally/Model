﻿using RoboRoboRally.Model.CardDB.Cards;

namespace RoboRoboRally.Model.Core
{
	/// <summary>
	/// <para>
	/// This class represents player's register board
	/// with programming cards - it has some fixed number
	/// of registers and a programming card can be placed
	/// in each such register.
	/// </para>
	/// 
	/// <para>
	/// Note that registers are number from 1 to 5, so first
	/// register has number 1 and so on.
	/// </para>
	/// </summary>
    class RegisterBoard
    {
		ProgrammingCard[] registers;

		/// <summary>
		/// Number of registers of this register board.
		/// </summary>
		public int RegisterCount
		{
			get	{ return registers.Length; }
		}

		/// <summary>
		/// Ctor with number of registers of this register board.
		/// </summary>
		public RegisterBoard(int registerCount)
		{
			if (registerCount < 1)
				throw new RunningGameException("can not construct register board with less than 1 register");

			this.registers = new ProgrammingCard[registerCount];
		}

		/// <summary>
		/// Returns programming card at the specified register number
		/// </summary>
		/// <exception cref="RunningGameException">
		/// is thrown if an invalid register number was 
		/// passed</exception>
		public ProgrammingCard GetRegisterCard(int registerNumber)
		{
			if (registerNumber < 1 || registerNumber > registers.Length)
				throw new RunningGameException("can not retrieve register card - invalid register number");
			
			//beware, registers are numbered from 1 ... registerCount
			return registers[registerNumber - 1];
		}

		/// <summary>
		/// Removes program from this register board and 
		/// invalidates each registers - they can not be 
		/// no longer played.
		/// </summary>
		public ProgrammingCard[] RemoveProgram()
		{
			if (registers[0] == null)
				throw new RunningGameException("can not remove program - cards are invalid");

			ProgrammingCard[] currentProgram = new ProgrammingCard[RegisterCount];
			for (int i = 0; i < RegisterCount; ++i)
			{
				currentProgram[i] = registers[i];
				registers[i] = null;
			}

			return currentProgram;
		}

		/// <summary>
		/// Replaces programming card at the specified register number.
		/// </summary>
		/// <exception cref="RunningGameException">is thrown
		/// if an invalid register number was passed</exception>
		public void ReplaceCard(int registerNumber, ProgrammingCard newCard)
		{
			if (registerNumber < 1 || registerNumber > registers.Length)
				throw new RunningGameException("can not replace card at the provided register number");

			registers[registerNumber - 1] = newCard;
		}

		/// <summary>
		/// Places new program to this register board. Number
		/// of passed programming cards has to match number of 
		/// registers of this register board.
		/// </summary>
		/// <param name="cardsToPlace"></param>
		public void PlaceNewProgram(ProgrammingCard[] cardsToPlace)
		{
			if (cardsToPlace.Length != registers.Length)
				throw new RunningGameException("can not set new program - number of registers and number of cards to place differ");

			for (int i = 0; i < cardsToPlace.Length; ++i)
				registers[i] = cardsToPlace[i];
		}

		
    }
}
