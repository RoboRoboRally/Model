﻿using System.Collections.Generic;
using System.Linq;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks.AsyncCardHooks;

namespace RoboRoboRally.Model.Core.HookableGameSteps
{
	/// <summary>
	/// <para>
	/// This namespace contains classes that are related to upgrade cards.
	/// These classes represent steps onto which upgrade cards can hook their
	/// effects. Upgrade cards are described in detail in <see cref="UpgradeCard"/>.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{ }

	/// <summary>
	/// Base return value for all steps.
	/// </summary>
	class HookRetBase
	{

	}

	/// <summary>
	/// Base arguments for all steps.
	/// </summary>
	class HookArgsBase
	{
		public IGameState GameState { get; set; }
		
		/// <summary>
		/// Upgrade card holder.
		/// </summary>
		public Robot Robot { get; set; }
	}

	/// <summary>
	/// <para>
	/// Base class for each step. Note that each class derived
	/// from this can be empty - it only needs to supply the
	/// type of return value and type of argument and the main method
	/// can remain the same.
	/// </para>
	/// 
	/// <para>
	/// Note that steps are tightly related to upgrade cards which are
	/// explained in <see cref="UpgradeCard"/>.
	/// </para>
	/// </summary>
    abstract class HookableGameStep<TRet, TArg> where TRet: HookRetBase where TArg:HookArgsBase
    {
		Dictionary<Robot, List<IUpgradeCardHook<TRet, TArg>>> attachedHooks;

		public HookableGameStep()
		{
			attachedHooks = new Dictionary<Robot, List<IUpgradeCardHook<TRet, TArg>>>();
		}

		/// <summary>
		/// Main method, that performs each hook attached
		/// to this step. 
		/// </summary>
		/// <param name="args"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public TRet PerformHooks(TArg args, TRet defaultValue)
		{
			if (!attachedHooks.ContainsKey(args.Robot))
				return defaultValue;

			//robots that are inactive (they rebooted) can not use upgrades
			if (!args.GameState.TurnActiveRobots.Contains(args.Robot))
				return defaultValue;

			//collection of attached hooks may be modified during discard of temporary 
			//upgrade card => we need to preenumerate the collection into new collection
			var hooks = attachedHooks[args.Robot].ToArray();

			
			foreach (var hook in hooks)
			{
				//these are two special cases ...
				//this is ugly and should be revisited later
				if (hook is AsyncCardOfferHook h)
				{
					h.PerformHook(args, defaultValue);
					continue;
				}
				else if (hook is AsyncCardOfferEndHook h2)
				{
					h2.PerformHook(args, defaultValue);
					continue;
				}

				//in some cases, like with pressor beam upgrade card, there
				//are hooks that require some additional verification for use of upgrade
				//card, like if there were some robots shot
				if (!hook.VerifyHookUse(args))
					continue;

				var uCard = args.Robot.GetUpgradeCardForHook(hook);


				if (hook is IChoicelessUpgradeCardHook<TRet, TArg> choicelessHook)
				{
					bool use = true;
					if (choicelessHook.RequiresApprove())
					{
						var useInfo = args.GameState.InputHandler.OfferChoicelessUpgradeCard(args.Robot.Name, uCard.Name);
						if (!useInfo.UseCard)
							use = false;
					}

					if (use)
					{
						defaultValue = choicelessHook.PerformHook(args, defaultValue);

						args.GameState.Notifier.UpgradeCardUsed(args.Robot.Name, uCard.Name);

						if (uCard.UpgradeCardType == UpgradeCardType.Temporary)
							args.Robot.DiscardUpgradeCard(args.GameState, uCard.Name);

					}

				}
				else if (hook is IChoiceUpgradeCardHook<TRet, TArg> choiceHook)
				{
					var useInfo = args.GameState.InputHandler.OfferChoiceUpgradeCard(args.Robot.Name, uCard.Name, choiceHook.GetChoices());

					if (useInfo.UseCard)
					{
						defaultValue = choiceHook.PerformHook(args, defaultValue, useInfo.ChosenChoice);

						args.GameState.Notifier.UpgradeCardUsed(args.Robot.Name, uCard.Name, useInfo.ChosenChoice);

						if (uCard.UpgradeCardType == UpgradeCardType.Temporary)
							args.Robot.DiscardUpgradeCard(args.GameState, uCard.Name);
					}

				}
			}

			return defaultValue;
		}

		/// <summary>
		/// Attaches the passed hook to this step.
		/// </summary>
		public void AttachHook(Robot robot, IUpgradeCardHook<TRet, TArg> hook)
		{
			if (!attachedHooks.ContainsKey(robot))
				attachedHooks.Add(robot, new List<IUpgradeCardHook<TRet, TArg>>());

			attachedHooks[robot].Add(hook);
		}

		/// <summary>
		/// Detaches the passed hook from this step.
		/// </summary>
		public void DetachHook(Robot robot, IUpgradeCardHook<TRet, TArg> hook)
		{
			attachedHooks[robot].Remove(hook);
		}
	}



}
