﻿using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Core.HookableGameSteps
{
	class DrawAmountStepRet : HookRetBase
	{
		public int DrawAmount { get; set; }
	}

	class DrawAmountStep : HookableGameStep<DrawAmountStepRet, HookArgsBase>
	{

	}


	class PushDirectionStepRet : HookRetBase
	{
		public Direction Direction { get; set; }

		public int Amount { get; set; }
	}

	class PushDirectionStep : HookableGameStep<PushDirectionStepRet, HookArgsBase>
	{

	}


	class DamageStepRet : HookRetBase
	{
		public Damage Damage { get; set; }
	}

	class RebootDamageStep : HookableGameStep<DamageStepRet, HookArgsBase>
	{

	}


	class PushDamageStep : HookableGameStep<DamageStepRet, HookArgsBase>
	{

	}

	class LaserDamageStep : HookableGameStep<DamageStepRet, ShotTargetsStepArgs>
	{

	}

	class ShootingDirectionStepRet : HookRetBase
	{
		public HashSet<Direction> Directions { get; set; }
	}

	class ShootingDirectionStep : HookableGameStep<ShootingDirectionStepRet, HookArgsBase>
	{

	}

	class ShootingTargetsStepArgs : HookArgsBase
	{
		public List<Direction> ShootingDirections { get; set; }
	}

	class ShootingTargetsStepRet : HookRetBase
	{
		public HashSet<Robot> RobotsToShoot { get; set; }
	}

	class ShootingTargetsStep : HookableGameStep<ShootingTargetsStepRet, ShootingTargetsStepArgs>
	{

	}

	class ShotTargetsStepArgs : HookArgsBase
	{
		public List<Robot> ShotRobots { get; set; }
	}

	class ShotTargetsStep : HookableGameStep<HookRetBase, ShotTargetsStepArgs>
	{

	}


	class BeforePlayingRegisterStep : HookableGameStep<HookRetBase, HookArgsBase>
	{

	}

	class AfterPlayingRegisterStep : HookableGameStep<HookRetBase, HookArgsBase>
	{

	}

	class BeforeActivationPhaseStep : HookableGameStep<HookRetBase, HookArgsBase>
	{

	}

	class AfterActivationPhaseStep : HookableGameStep<HookRetBase, HookArgsBase>
	{

	}

	class BeforeRebootStep : HookableGameStep<HookRetBase, HookArgsBase>
	{

	}

}
