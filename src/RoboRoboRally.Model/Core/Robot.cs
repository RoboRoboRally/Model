﻿using System;
using System.Collections.Generic;

using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks;
using RoboRoboRally.Model.Core.HookableGameSteps;

namespace RoboRoboRally.Model.Core
{
	/// <summary>
	/// Helper struct for determining robot's priority
	/// (not that in our implemneted version, priority
	/// is determined with the help of priority antenna)
	/// </summary>
	struct RobotPriority : IComparable<RobotPriority>
	{
		/// <summary>
		/// Manhattan distance from robot's position
		/// to antenna tile.
		/// </summary>
		public int DistanceToAntenna { get; set; }

		/// <summary>
		/// Angle between robot's facing direction
		/// and antenna's facing direction.
		/// </summary>
		public double Angle { get; set; }

		/// <summary>
		/// Compare to method for sorting robot's by priority.
		/// </summary>
		/// <param name="other">other robot</param>
		/// <returns>-1 if other robot's priority is
		/// bigger, 1 otherwise</returns>
		public int CompareTo(RobotPriority other)
		{
			if (DistanceToAntenna < other.DistanceToAntenna)
				return -1;
			else if (DistanceToAntenna == other.DistanceToAntenna)
			{
				if (Angle < other.Angle)
					return -1;
				return 1;
			}
			else
				return 1;
		}
	}

	/// <summary>
	/// This class represents single player in the 
	/// RoboRally game.
	/// </summary>
	public class Robot
    {
		private RegisterBoard registerBoard;

		private Pack<ProgrammingCard> programmingCards;

		private IList<int> visitedCheckpoints;

		private UpgradeCardBoard upgradeBoard;

		private int registersRevealedThisTurn;

		private Dictionary<string, IUpgradeCardHook<HookRetBase, HookArgsBase>> asyncHooks;

		internal int RegistersPlayedThisTurn { get; private set; }

		internal string Name { get; private set; }

		internal int EnergyAmount { get; private set; }

		internal Direction FacingDirection { get; private set; }

		internal MapCoordinates Position { get; private set; }

		/// <summary>
		/// Robot's default damage with lasers.
		/// </summary>
		internal Damage DefaultDamage { get; private set; }

		internal Robot(
			string name,
			int energyAmount,
			Direction facingDirection,
			MapCoordinates position,
			Pack<ProgrammingCard> programmingCards,
			RegisterBoard registerBoard,
			UpgradeCardBoard upgradeCardBoard,
			Damage defaultDamage
			)
		{
			this.Name = name;
			this.EnergyAmount = energyAmount;
			this.FacingDirection = facingDirection;
			this.Position = position;
			this.programmingCards = programmingCards;
			this.registerBoard = registerBoard;
			this.visitedCheckpoints = new List<int>();
			this.RegistersPlayedThisTurn = 0;
			this.registersRevealedThisTurn = 0;
			this.DefaultDamage = defaultDamage;
			this.upgradeBoard = upgradeCardBoard;
			this.asyncHooks = new Dictionary<string, IUpgradeCardHook<HookRetBase, HookArgsBase>>();
		}

		/// <summary>
		/// Changes robot's energy amount
		/// </summary>
		/// <param name="toAddAmount">amount of energy to add, so passing
		/// -1 substracts one energy from robot's energy state</param>
		internal void ChangeEnergyAmount(IGameState gameState, int toAddAmount)
		{
			var oldAmount = EnergyAmount;

			EnergyAmount += toAddAmount;

			gameState.Notifier.RobotEnergyChanged(Name, EnergyAmount, oldAmount);
		}

		/// <summary>
		/// Moves robot to the passed new position. The position is simply
		/// changed and this method does not perform any checks whatsover.
		/// </summary>
		internal void Move(IGameState gameState, MapCoordinates newPosition)
		{
			//move without any checks

			var oldPos = Position;

			Position = newPosition;

			gameState.Notifier.RobotMoved(Name, oldPos, Position);
		}

		/// <summary>
		/// Pushes robot to the passed new position. The position is simply
		/// changed and this method does not perform any checks whatsoever.
		/// </summary>
		/// <param name="gameState"></param>
		/// <param name="newPosition"></param>
		internal void Push(IGameState gameState, MapCoordinates newPosition)
		{
			//push without any checks, it is mainly about the notification

			var oldPos = Position;

			Position = newPosition;

			gameState.Notifier.RobotGotPushed(Name, oldPos, newPosition);
		}

		/// <summary>
		/// Set's robot to some invalid map position - this method is used
		/// to free some tiles during some complicated collision resolving
		/// (like during reboots).
		/// </summary>
		internal void InvalidatePosition()
		{
			Position = new MapCoordinates(-1, -1);
		}

		/// <summary>
		/// Set's robot to new position. Does not perform any checks
		/// about validity of position.
		/// </summary>
		internal void SetNewPosition(MapCoordinates newPosition)
		{
			Position = newPosition;
		}

		/// <summary>
		/// Set's robot to new position. Does not perform any checks
		/// about validity of position.
		/// </summary>
		internal void SetNewPosition(int rowIndex, int colIndex)
		{
			SetNewPosition(new MapCoordinates(rowIndex, colIndex));
		}

		internal void SetNewFacingDirection(Direction newFacingDirection)
		{
			FacingDirection = newFacingDirection;
		}

		internal void SetNewFacingDirection(DirectionType dir)
		{
			SetNewFacingDirection(new Direction(dir));
		}

		internal void Rotate(IGameState gameState, RotationType rotationType)
		{
			FacingDirection = FacingDirection.Rotate(rotationType);

			gameState.Notifier.RobotRotated(Name, rotationType);
		}



		//-----------------
		//--- Programming phase
		//-----------------
		/// <summary>
		/// This method draws the passed amount of cards from
		/// player's pack. May throw exception, if there
		/// are not enough cards in player's pack.
		/// </summary>
		/// <exception cref="RunningGameException">is
		/// thrown if there are not enough cards in 
		/// player's pack.</exception>
		internal ProgrammingCard[] DrawCards(int amount)
		{
			var drawnCards = programmingCards.Draw(amount);
			if (drawnCards.Length != amount)
				throw new RunningGameException($"can not draw {amount} cards - player's pack has only {drawnCards.Length} cards");

			return drawnCards;
		}

		/// <summary>
		/// Draws all cards from player's pack
		/// </summary>
		internal ProgrammingCard[] DrawAllCards()
		{
			var drawnCards = programmingCards.Draw(programmingCards.Size());
			return drawnCards;
		}

		/// <summary>
		/// This method sets player's program for next turn.
		/// Length of the passed array of programming cards has
		/// to match robot's number of registers. If it does not,
		/// an exception is thrown.
		/// </summary>
		/// <exception cref="RunningGameException">
		/// is thrown if length of the passed array of cards
		/// does not match robot's number of registers</exception>
		internal void SetNewProgram(ProgrammingCard[] chosenCards)
		{
			if (chosenCards.Length != registerBoard.RegisterCount)
				throw new RunningGameException($"can not set new program - expecting {registerBoard.RegisterCount} cards, but received {chosenCards.Length}");

			RegistersPlayedThisTurn = 0;
			registersRevealedThisTurn = 0;
			registerBoard.PlaceNewProgram(chosenCards);
		}

		/// <summary>
		/// Discards the passed cards to this robot's pack.
		/// That means, it places them at the top of robot's 
		/// programming card discard pile.
		/// </summary>
		internal void DiscardCards(ProgrammingCard[] toDiscard)
		{
			programmingCards.DiscardCards(toDiscard);
		}


		//-----------------
		//--- Activation phase
		//-----------------
		/// <summary>
		/// Reveals robot's next non-reavealed register
		/// and returns the revealed card.
		/// </summary>
		/// <exception cref="RunningGameException">is thrown
		/// if all cards have been revealed this turn</exception>
		internal ProgrammingCard RevealNextRegister()
		{
			if (registersRevealedThisTurn == registerBoard.RegisterCount)
				throw new RunningGameException("can not reveal more registers - all registers were revealed");

			registersRevealedThisTurn++;

			var card = registerBoard.GetRegisterCard(registersRevealedThisTurn);

			return card;
		}

		/// <summary>
		/// Plays robot's next non-played register.
		/// </summary>
		/// <exception cref="RunningGameException">
		/// is thrown, if all registers from this turn
		/// have been played already</exception>
		internal void PlayNextRegister(IGameState gameState)
		{
			if (RegistersPlayedThisTurn == registerBoard.RegisterCount)
				throw new RunningGameException("can not play more registers - all registers were played");

			//this is just convention that we increment before we actually played the card
			//(it makes more sense from things like reboot or again card)
			RegistersPlayedThisTurn++;

			PlayRegister(gameState, RegistersPlayedThisTurn);
		}

		/// <summary>
		/// This method performs again card at some register number 
		/// that has been already played.
		/// </summary>
		/// <exception cref="RunningGameException">is thrown
		/// if the card at the passed register number has not
		/// been played this turn yet</exception>
		internal void PlayRegisterAgain(IGameState gameState, int registerNumber)
		{
			if (registerNumber > RegistersPlayedThisTurn)
				throw new RunningGameException("can not play register again - this register has not been played yet");

			//careful - we need to do check as this method is not used only with "Again" card where we would
			//just return..
			if (registerNumber < 1)
				throw new RunningGameException("can not play register again - register number is < 1");

			PlayRegister(gameState, registerNumber);
		}

		private void PlayRegister(IGameState gameState, int registerNumber)
		{
			var card = registerBoard.GetRegisterCard(registerNumber);

			card.PlayCard(gameState, this, registerNumber);
		}

		/// <summary>
		/// Replaces card at one of the registers with the
		/// passed card. This method correctly handles discard of
		/// cards in all situations and no further actions are required.
		/// </summary>
		internal void ReplaceRegisterCard(IGameState gameState, ProgrammingCard newCard, int registerNumber)
		{
			var oldCard = registerBoard.GetRegisterCard(registerNumber);

			registerBoard.ReplaceCard(registerNumber, newCard);

			//card that is replaced has been already played
			if (registerNumber <= RegistersPlayedThisTurn)
			{
				if (!(oldCard is DamageCard))
					programmingCards.DiscardCard(oldCard);
			}
			//if we did not play the card, place it back into the pack
			else
			{
				programmingCards.DiscardCard(oldCard);
			}

			//anyway, we notify about this card being discarded
			gameState.Notifier.RegisterCardReplaced(Name, registerNumber, oldCard.Name, newCard.Name);
		}

		/// <summary>
		/// Changes programming of currently revealed registers to
		/// the passed new card.
		/// </summary>
		internal void ChangeCurrentRegisterProgramming(IGameState gameState, ProgrammingCard newCard)
		{
			//special card for temporary upgrade that can change programming of current register
			//current implementation basically adds this new card permantently into the pack

			var oldCard = registerBoard.GetRegisterCard(registersRevealedThisTurn);

			registerBoard.ReplaceCard(registersRevealedThisTurn, newCard);

			gameState.Notifier.RegisterCardReplaced(Name, registersRevealedThisTurn, oldCard.Name, newCard.Name);
		}

		/// <summary>
		/// Discards robot's program for this turn. Works correctly
		/// even if not all registers were played (e.g. because
		/// of reboot).
		/// </summary>
		internal void DiscardCurrentProgram()
		{
			//we need to be a little more careful ... this method may be called
			//in situations like reboot or after one whole turn and we need to 
			//properly handle damage cards - played damage cards are not discarded
			//but not-played ones are
			var currentProgram = registerBoard.RemoveProgram();

			//first loops handles cards that were played this turn
			for (int i = 0; i < RegistersPlayedThisTurn; ++i)
			{
				if (!(currentProgram[i] is DamageCard))
					programmingCards.DiscardCard(currentProgram[i]);
			}

			//this loop handles cards that were not played
			for (int i = RegistersPlayedThisTurn; i < currentProgram.Length; ++i)
			{
				programmingCards.DiscardCard(currentProgram[i]);
			}
		}

		/// <summary>
		/// Inflicts robot the passed damage. That means,
		/// it places damage cards from damage struct
		/// into player's discard pile.
		/// </summary>
		internal void SufferDamage(IGameState gameState, Damage damage)
		{
			programmingCards.DiscardCards(damage.DamageCards);

			gameState.Notifier.RobotSufferedDamage(Name, damage);
		}

		/// <summary>
		/// This method tries to burn some type of card from
		/// player's discard pile. This is a special case method
		/// used e.g. by spam folder card, that can burn one spam
		/// card from player's pack.
		/// </summary>
		/// <returns>if the burn succeeded, false
		/// otherwise</returns>
		internal bool TryBurnCardFromDiscardPile(ProgrammingCard card)
		{
			return programmingCards.TryBurnFromDiscardPile(card);			
		}

		/// <summary>
		/// Returns number of robot's last visited checkpoint.
		/// If no checkpoints were visited, returns 0.
		/// </summary>
		internal int GetLastVisitedCheckpoint()
		{
			if (visitedCheckpoints.Count == 0)
				return 0;
			else
				return visitedCheckpoints[visitedCheckpoints.Count - 1];
		}

		/// <summary>
		/// Add's new visited checkpoint to this robot.
		/// </summary>
		/// <exception cref="RunningGameException"> is thrown
		/// if the passed checkpoint is not bigger by one than
		/// the this robot'S last visited checkpoint</exception>
		internal void AddVisitedCheckpoint(int checkpointNum)
		{
			if (visitedCheckpoints.Count == 0)
			{
				if (checkpointNum != 1)
					throw new RunningGameException($"can not add new visited checkpoint - checkpoin number is {checkpointNum} and it should be 1");
			}
			else
			{
				if (checkpointNum != visitedCheckpoints[visitedCheckpoints.Count - 1] + 1)
					throw new RunningGameException($"can not add new visited checkpoint ... last visited checkpoint is {visitedCheckpoints[visitedCheckpoints.Count - 1]}, provided number is {checkpointNum} ... checkpoints have to be in sequence");
			}

			visitedCheckpoints.Add(checkpointNum);
		}

		/// <summary>
		/// Returns all upgrade cards owned by this robot.
		/// </summary>
		internal UpgradeCard[] GetOwnedUpgradeCards()
		{
			return upgradeBoard.GetAllUpgradeCards();
		}

		/// <summary>
		/// Returns this robot's limit for individual upgrade card types.
		/// </summary>
		internal Dictionary<UpgradeCardType, int> GetUpgradeCardLimits()
		{
			return upgradeBoard.GetCardLimits();
		}

		/// <summary>
		/// This method discard all player's upgrade cards
		/// and returns them to the game's upgrade card pack.
		/// </summary>
		internal void DiscardAllUpgradeCards(IGameState gameState)
		{
			var allCards = upgradeBoard.RemoveAllUpgradeCards();

			foreach (var c in allCards)
			{
				gameState.PlaceUpgradeCardToPack(c);

				gameState.Notifier.UpgradeCardDiscarded(Name, c.Name);
			}
		}

		/// <summary>
		/// This method discards a single upgrade card and places
		/// it back into the game's upgrade card pack.
		/// </summary>
		internal void DiscardUpgradeCard(IGameState gameState, string upgradeCardName)
		{
			//this order is important, we can not remove the card until we detach all
			//hooks 
			var card = upgradeBoard.GetCard(upgradeCardName);

			card.CardDiscarded(gameState, this);

			gameState.PlaceUpgradeCardToPack(card);

			upgradeBoard.RemoveCard(upgradeCardName);

			//we want the notification definitely here

			gameState.Notifier.UpgradeCardDiscarded(Name, upgradeCardName);
		}

		/// <summary>
		/// This method adds a new upgrade card to the collection of
		/// upgrade cards owned by this player.
		/// </summary>
		/// <exception cref="RunningGameException">is thrown
		/// if this new added upgrade card violates this player's
		/// limit on that particular upgrade card type</exception>
		internal void ObtainUpgradeCard(IGameState gameState, UpgradeCard card)
		{
			upgradeBoard.AddCard(card);

			//this thing attaches all of the hooks
			card.CardObtained(gameState, this);

			gameState.Notifier.UpgradeCardObtained(Name, card.Name);
		}

		/// <summary>
		/// This class retrieves robot's owned upgrade card based on 
		/// the passed hook. In order to understand implementation
		/// of upgrade cards, see <see cref="UpgradeCard"/>.
		/// </summary>
		/// <exception cref="RunningGameException">is thrown
		/// if robot does not own upgrade card with the
		/// provided hook</exception>
		internal UpgradeCard GetUpgradeCardForHook(IUpgradeCardHookBase hook)
		{
			var allCards = upgradeBoard.GetAllUpgradeCards();

			foreach (var card in allCards)
			{
				foreach (var h in card.GetHooks())
				{
					if (h == hook)
						return card;
				}
			}

			throw new RunningGameException("can not retrieve robot's upgrade card from hook - no such card");
		}

		/// <summary>
		/// This method tries to retrieve attached async hook for the passed
		/// upgrade card. In order to understand implementation
		/// of upgrade cards, see <see cref="UpgradeCard"/>.
		/// </summary>
		/// <returns>true if this robot has attached
		/// async upgrade hook, false otherwise</returns>
		internal bool HasAsyncHook(string upgradeCardName, out IUpgradeCardHook<HookRetBase, HookArgsBase> asyncHook)
		{
			if (asyncHooks.ContainsKey(upgradeCardName))
			{
				asyncHook = asyncHooks[upgradeCardName];
				return true;
			}

			asyncHook = null;
			return false;
		}

		/// <summary>
		/// This method attaches new async hook to this player.
		/// In order to understand implementation
		/// of upgrade cards, see <see cref="UpgradeCard"/>.
		/// </summary>
		internal void AttachAsyncHook(string cardName, IUpgradeCardHook<HookRetBase, HookArgsBase> hook)
		{
			if (asyncHooks.ContainsKey(cardName))
				throw new RunningGameException("can not attach async hook to player - there is already such hook");

			asyncHooks.Add(cardName, hook);
		}

		/// <summary>
		/// This method detaches async hook from this player.
		/// In order to understand implementation
		/// of upgrade cards, see <see cref="UpgradeCard"/>.
		/// </summary>
		internal void DetachAsyncHook(string cardName, IUpgradeCardHook<HookRetBase, HookArgsBase> hook)
		{
			if (!asyncHooks.ContainsKey(cardName))
				throw new RunningGameException("can not detach async hook from player - no hook under the provided cardname");

			if (asyncHooks[cardName] != hook)
				throw new RunningGameException("can not detach async hook from player - player does not have the provided hook attached");

			asyncHooks.Remove(cardName);
		}


		struct Vec2
		{
			public double X { get; set; }
			public double Y { get; set; }
		}

		/// <summary>
		/// Retrieves priority of this robot based
		/// on the coordinates and facing direction of priority
		/// antenna.
		/// </summary>
		/// <returns>robot's priority, see <see cref="RobotPriority"/>
		/// </returns>
		internal RobotPriority GetDefaultPriority(MapCoordinates antennaCoords, Direction antennaOutDirection)
		{
			int dist = Position.GetManhattanDistance(antennaCoords);

			//now we need to determine the angle
			Vec2 baseVec = new Vec2() { X = antennaOutDirection.X, Y = antennaOutDirection.Y };
			var vecY = antennaCoords.RowIndex - Position.RowIndex;
			var vecX = Position.ColIndex - antennaCoords.ColIndex;

			Vec2 vec = new Vec2() { X = vecX, Y = vecY };

			double sin = vec.X * baseVec.Y - baseVec.X * vec.Y;
			double cos = vec.X * baseVec.X + vec.Y * baseVec.Y;

			var angle = Math.Atan2(sin, cos) * (180 / Math.PI);

			if (angle < 0)
				angle += 360;

			return new RobotPriority() { Angle = angle, DistanceToAntenna = dist };
		}

	}
}
