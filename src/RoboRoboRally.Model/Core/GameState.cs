﻿using System.Linq;
using System.Collections.Generic;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Core.Phases;
using System.Collections.Concurrent;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoiceHooks;
using RoboRoboRally.Model.CardDB.Cards.UpgradeCardHooks.ChoicelessHooks;

namespace RoboRoboRally.Model.Core
{
	/// <summary>
	/// <para>
	/// This interface is used to represent current state
	/// of a running robo rally game. It holds current 
	/// players, map, pack with upgrade cards or phases
	/// that are performed each round. Also, it holds
	/// reference to two important interfaces - <see cref="IGameEventNotifier"/>
	/// and <see cref="IInputHandler"/> that allow the running game
	/// to communicate with server and therefore clients. 
	/// </para>
	/// 
	/// <para>
	/// Note that sole purpose for this interface is mocking
	/// and there is actually only one class implementing this
	/// interface, that is <see cref="Game"/>.
	/// </para>
	/// </summary>
	public interface IGameState
	{
		/// <summary>
		/// Returns robots that are active for the current turn.
		/// Note that robots that for example rebooted are no longer
		/// active and are not part of this list. All robots participating
		/// in the game are returned by <see cref="GameActiveRobots"/>.
		/// </summary>
		Robot[] TurnActiveRobots { get; }
		/// <summary>
		/// Returns robots that are deactivated for the current turn.
		/// This includes robots that for example rebooted. All robots
		/// participating in the game are returned by <see cref="GameActiveRobots"/>.
		/// </summary>
		Robot[] TurnDeactivatedRobots { get; }
		/// <summary>
		/// Returns robots that are participating in the game. This includes
		/// both active and deactivated robots.
		/// </summary>
		Robot[] GameActiveRobots { get; }
		/// <summary>
		/// Returns dormant robots, which are robots, that were already in control
		/// of some player, but the player left. State of such robot
		/// is saved upon player leaving and is later restored if a new
		/// palyer joins in for this robot.
		/// </summary>
		Robot[] DormantRobots { get; }
		/// <summary>
		/// Returns robots that have already finished the game. These robots
		/// can be no longer controlled by any player.
		/// </summary>
		Robot[] FinishedRobots { get; }


		IGameEventNotifier Notifier { get; }
		IInputHandler InputHandler { get; }
		IMap Map { get; }		

		/// <summary>
		/// Number of registers for each robot.
		/// </summary>
		int RegisterCount { get; }

		/// <summary>
		/// Performs a single turn of the robo rally game
		/// (that is, it performs each phase).
		/// </summary>
		void PerformSingleGameLoop();
		/// <summary>
		/// Puts the passed robot to sleep (that robot is now dormant)
		/// before start of a new turn. This method is used if e.g. one
		/// player leaves (which is handled, at the begginning
		/// of each turn).
		/// </summary>
		void PutRobotToSleepBeforeTurn(Robot robot);
		/// <summary>
		/// Puts the passed robot to sleep (that robot is now dormant)
		/// during a turn. 
		/// </summary>
		void PutRobotToSleepDuringTurn(Robot robot);
		/// <summary>
		/// Adds a new player to the game.
		/// </summary>
		void AddNewRobot(Robot robot);

		/// <summary>
		/// This method tries to restore state of a dormant robot
		/// (this happens if a robot was controlled, then player left
		/// and then someone joined for that robot again). This restore
		/// can fail, because the position that was previously occupied
		/// by this robot is now occupied.
		/// </summary>
		/// <returns>true if the restore was successful, false
		/// otherwise</returns>
		bool TryRestoreDormantRobot(Robot robot);
		/// <summary>
		/// Returns action of the passed type. In order to understand
		/// purpose of actions, see namespace <see cref="Actions"/>.
		/// </summary>
		T GetAction<T>() where T : IAction;
		/// <summary>
		/// Returns phase of the passed type.
		/// </summary>
		T GetPhase<T>() where T : PhaseBase;
		/// <summary>
		/// This method checks if there is any robot occupying
		/// the passed position. If yes, it returns true and sets
		/// the out parametr, if no the passed out paramter is set
		/// to null and it returns false.
		/// </summary>
		/// <param name="occupyingRobot">out parametr - robot
		/// that is occupying the passed position or null if the 
		/// position is not occupied</param>
		/// <returns>true if there is any robot occupying
		/// the passed position, false otherwise</returns>
		bool IsTileOccupied(MapCoordinates coords, out Robot occupyingRobot);
		/// <summary>
		/// This method checks if there is any robot
		/// occupying the passed position and returns
		/// flag.
		/// </summary>
		/// <returns>true if there is any robot occupying
		/// the passed position, false otherwise</returns>
		bool IsTileOccupied(MapCoordinates coords);
		/// <summary>
		/// Marks the passed robot as a finished one
		/// </summary>
		void RobotFinished(Robot r);
		/// <summary>
		/// Deactivates the passed robot for this turn. This is
		/// used e.g. during reboot.
		/// </summary>
		void DeactivateRobotForThisTurn(Robot r);
		/// <summary>
		/// This method returns robots that are active this turn
		/// (this excludes e.g. deactivated ones) in order of their
		/// default priority as specified by the robo rally rules
		/// (by using antenna). Robot at index [0] has the highest
		/// priority, and so on.
		/// </summary>
		Robot[] GetTurnActiveRobotsByDefaultPriority();

		/// <summary>
		/// Draws up to the passed amount of cards from the pack
		/// of upgrade cards (there do not have to be enough, which
		/// is correct behavior).
		/// </summary>
		UpgradeCard[] DrawUpgradeCards(int amount);
		/// <summary>
		/// Places the passed upgrade card back to the pack of 
		/// upgrade cards.
		/// </summary>
		void PlaceUpgradeCardToPack(UpgradeCard card);

		/// <summary>
		/// This method processes async requests from clients to
		/// use their async upgrade cards. In order to understand 
		/// implementation of upgrade cards better, see <see cref="CardDB.Cards.UpgradeCard"/>.
		/// </summary>
		void ProcessAsyncUpgradeCardUses();
		/// <summary>
		/// This is a shared queue between the main game loop
		/// thread and basically class Game, which is used by
		/// server to pass async requests from clients. For this
		/// reason, this queue has to be concurrent. Requests
		/// passed into this queue are processed by method
		/// <see cref="ProcessAsyncUpgradeCardUses"/> and then cleared
		/// at the start of each turn by method <see cref="ClearAsnycUpgradesUsesQueue"/>.
		/// </summary>
		ConcurrentQueue<AsyncUpgradeCardUseInfo> AsyncUpgradeCardUseInfoQueue { get; }
		/// <summary>
		/// This method clears the <see cref="AsyncUpgradeCardUseInfoQueue"/> at the
		/// start of each turn.
		/// </summary>
		void ClearAsnycUpgradesUsesQueue();
	}

	/// <summary>
	/// Class that represents current state of the game.
	/// See <see cref="IGameState"/> for more details.
	/// </summary>
    class GameState : IGameState
    {
		private PhaseBase[] phases;

		private IAction[] actions;

		private Pack<UpgradeCard> upgradeCards;
		
		private List<Robot> turnActiveRobots;

		private List<Robot> turnDeactivatedRobots;

		//this list contains robots from both - turnActive list and turnDeactivated list
		private List<Robot> gameActiveRobots;

		//dormantRobots are robots that were already controlled but 
		//player left the game
		private List<Robot> dormantRobots;

		private List<Robot> finishedRobots;

		/// <inheritdoc/>
		public Robot[] TurnActiveRobots
		{
			get { return turnActiveRobots.ToArray(); }
		}

		/// <inheritdoc/>
		public Robot[] TurnDeactivatedRobots
		{
			get { return turnDeactivatedRobots.ToArray(); }
		}

		/// <inheritdoc/>
		public Robot[] GameActiveRobots
		{
			get { return gameActiveRobots.ToArray(); }
		}

		/// <inheritdoc/>
		public Robot[] DormantRobots
		{
			get	{ return dormantRobots.ToArray(); }
		}

		/// <inheritdoc/>
		public Robot[] FinishedRobots
		{
			get	{ return finishedRobots.ToArray(); }
		}

		//this queue contains both choiceless and choice upgrade cards ...
		/// <inheritdoc/>
		public ConcurrentQueue<AsyncUpgradeCardUseInfo> AsyncUpgradeCardUseInfoQueue { get; private set; }

		/// <inheritdoc/>
		public IMap Map { get; private set; }

		/// <inheritdoc/>
		public IGameEventNotifier Notifier { get; private set; }

		/// <inheritdoc/>
		public IInputHandler InputHandler { get; private set; }

		/// <inheritdoc/>
		public int RegisterCount { get; private set; }


		internal GameState(
			PhaseBase[] phases,
			IAction[] actions,
			Pack<UpgradeCard> upgradeCards,
			IMap map,
			IGameEventNotifier notifier,
			IInputHandler inputHandler,
			int registerCount
			)
		{
			this.turnActiveRobots = new List<Robot>();
			this.turnDeactivatedRobots = new List<Robot>();
			this.dormantRobots = new List<Robot>();
			this.finishedRobots = new List<Robot>();
			this.gameActiveRobots = new List<Robot>();

			this.actions = new IAction[actions.Length];
			this.phases = new PhaseBase[phases.Length];

			this.upgradeCards = upgradeCards;
			this.Map = map;
			this.Notifier = notifier;
			this.InputHandler = inputHandler;
			this.RegisterCount = registerCount;

			for (int i = 0; i < actions.Length; ++i)
				this.actions[i] = actions[i];

			for (int i = 0; i < phases.Length; ++i)
				this.phases[i] = phases[i];

			this.AsyncUpgradeCardUseInfoQueue = new ConcurrentQueue<AsyncUpgradeCardUseInfo>();
		}

		/// <inheritdoc/>
		public void PerformSingleGameLoop()
		{
			TurnBegan();

			//go over each phase and perform it
			foreach (var phase in phases)
			{
				phase.PerformPhase(this);
			}

			TurnOver();
		}

		/// <summary>
		/// Resets some internal structures at the beggining of each turn.
		/// </summary>
		internal void TurnBegan()
		{
			if (gameActiveRobots.Count == 0)
				throw new RunningGameException("can not start new turn - there are no active robots");

			Notifier.NewTurnBegan();

			turnActiveRobots.Clear();
			turnDeactivatedRobots.Clear();

			//at the beggining of each turn, we place every robot into
			//robots active for this turn
			turnActiveRobots.AddRange(gameActiveRobots);

		}

		/// <summary>
		/// Resets some internal structures at the end of each turn.
		/// </summary>
		internal void TurnOver()
		{
			turnDeactivatedRobots.Clear();
			turnActiveRobots.Clear();

			Notifier.TurnEnded();
		}

		/// <inheritdoc/>
		public void PutRobotToSleepBeforeTurn(Robot robot)
		{
			if(!gameActiveRobots.Contains(robot))
				throw new RunningGameException($"can not put robot name {robot.Name} to sleep - this robot is not active");

			gameActiveRobots.Remove(robot);

			dormantRobots.Add(robot);

			Notifier.RobotLeftGame(robot.Name);
		}

		/// <inheritdoc/>
		public void PutRobotToSleepDuringTurn(Robot robot)
		{
			if(!gameActiveRobots.Contains(robot))
				throw new RunningGameException($"can not put robot name {robot.Name} to sleep - this robot is not active");

			gameActiveRobots.Remove(robot);

			if (!turnActiveRobots.Contains(robot) && !turnDeactivatedRobots.Contains(robot))
				throw new RunningGameException($"can not put robot name {robot.Name} to sleep - he is not in one of the robots lists for turn");

			//robot has to be in of these lists
			turnActiveRobots.Remove(robot);
			turnDeactivatedRobots.Remove(robot);

			//remember, that we discard programs of each robot at the END of turn
			//wihtout differentiating between turn active and deactivated robot
			robot.DiscardCurrentProgram();
			
			
			dormantRobots.Add(robot);

			Notifier.RobotLeftGame(robot.Name);
		}

		/// <inheritdoc/>
		public void AddNewRobot(Robot robot)
		{
			if (gameActiveRobots.Contains(robot) || gameActiveRobots.Any(r => r.Name == robot.Name))
				throw new RunningGameException($"can not add new robot with name {robot.Name}, robot with the same name is already playing the game");

			if (finishedRobots.Any(r => r.Name == robot.Name))
				throw new RunningGameException($"can not add new robot with name {robot.Name}, robot with the same name has already finished the game");

			if (gameActiveRobots.Any(r => r.Position == robot.Position))
				throw new RunningGameException($"can not add new robot with name {robot.Name}, robot's position is already occupied");

			gameActiveRobots.Add(robot);

			Notifier.RobotJoinedGame(robot.Name, robot.FacingDirection, robot.Position,
				robot.EnergyAmount, robot.GetLastVisitedCheckpoint(), 
				robot.GetOwnedUpgradeCards().Select(c => c.Name).ToArray());
		}

		/// <inheritdoc/>
		public bool TryRestoreDormantRobot(Robot robot)
		{
			if (!dormantRobots.Contains(robot))
				throw new RunningGameException($"can not restore dormant robot named {robot.Name} - this robot was not asleep");

			//if the previous tile of dormant robot is occupied, then
			//we simply wait one additional turn
			if (IsTileOccupied(robot.Position))
				return false;

			//otherwise, we can restore
			dormantRobots.Remove(robot);

			AddNewRobot(robot);

			return true;
		}

		/// <inheritdoc/>
		public void RobotFinished(Robot robot)
		{
			//in a rare instance, even robot that was deactivated for this turn may actually finish...
			if (!gameActiveRobots.Contains(robot))
				throw new RunningGameException($"can not finish with robot {robot.Name} - he is not active");

			if (!turnActiveRobots.Contains(robot) && !turnDeactivatedRobots.Contains(robot))
				throw new RunningGameException($"can not finish with robot {robot.Name} -  he is not in one of the robots lists for turn");

			gameActiveRobots.Remove(robot);

			turnActiveRobots.Remove(robot);
			turnDeactivatedRobots.Remove(robot);

			//we need to get the upgrade cards and place them back into the pack
			//(this call includes both)
			robot.DiscardAllUpgradeCards(this);			

			finishedRobots.Add(robot);

			Notifier.RobotFinishedGame(robot.Name);
		}

		/// <inheritdoc/>
		public void DeactivateRobotForThisTurn(Robot robot)
		{
			if (!gameActiveRobots.Contains(robot))
				throw new RunningGameException($"can not deactivate robot {robot.Name} for this turn - this robotis not one of the active robots");

			//cards are discarded, for each robot, at the end of activation phase

			if (turnActiveRobots.Contains(robot))
			{
				turnActiveRobots.Remove(robot);
				turnDeactivatedRobots.Add(robot);
			}
			else if (turnDeactivatedRobots.Contains(robot))
			{
				//nothing, this robot has been already deactivate this turn
			}
			else
				throw new RunningGameException($"can not deactivate robot {robot.Name} for this turn - he is not in one of the robots lists for this turn");

		}

		/// <inheritdoc/>
		public T GetAction<T>() where T : IAction
		{
			foreach (var action in actions)
			{
				if (action is T castedAction)
				{
					return castedAction;
				}
			}

			throw new RunningGameException("Can not retrieve action from game state");			
		}

		/// <inheritdoc/>
		public T GetPhase<T>() where T : PhaseBase
		{
			foreach (var phase in phases)
			{
				if (phase is T phaseCasted)
				{
					return phaseCasted;
				}
			}

			throw new RunningGameException("Can not retrieve appropriate phase from game state");
		}

		/// <inheritdoc/>
		public bool IsTileOccupied(MapCoordinates coords, out Robot occupyingRobot)
		{
			foreach (var r in gameActiveRobots)
			{
				if (r.Position == coords)
				{
					occupyingRobot = r;
					return true;
				}
			}
			occupyingRobot = null;
			return false;
		}

		/// <inheritdoc/>
		public bool IsTileOccupied(MapCoordinates coords)
		{
			return IsTileOccupied(coords, out Robot r);
		}

		/// <inheritdoc/>
		public Robot[] GetTurnActiveRobotsByDefaultPriority()
		{
			SortedList<RobotPriority, Robot> robotsByPriority = new SortedList<RobotPriority, Robot>();

			var antennaRecords = Map.GetTileRecords<PriorityAntennaTile>();

			if(antennaRecords.Length == 0)
				throw new RunningGameException("Can not return robots by their default priority ... there are no antenna tiles on the map - this can not happen");

			if (antennaRecords.Length >1)
				throw new RunningGameException("Can not return robots by their default priority ... there are multiple antenna tiles on the map - this can not happen");

			var antenna = antennaRecords[0];

			foreach (var robot in turnActiveRobots)
			{
				robotsByPriority.Add(robot.GetDefaultPriority(antenna.Coords, antenna.Tile.OutDirection), robot);
			}

			IList<Robot> robots = new List<Robot>();

			foreach (var record in robotsByPriority)
			{
				robots.Add(record.Value);
			}

			return robots.ToArray();
		}

		/// <inheritdoc/>
		public UpgradeCard[] DrawUpgradeCards(int amount)
		{
			return upgradeCards.Draw(amount);
		}

		/// <inheritdoc/>
		public void PlaceUpgradeCardToPack(UpgradeCard card)
		{
			//we definitely do not want any notifications here
			//as this may be called also during upgrade phase
			upgradeCards.DiscardCard(card);
		}

		/// <inheritdoc/>
		public void ClearAsnycUpgradesUsesQueue()
		{
			while (AsyncUpgradeCardUseInfoQueue.TryDequeue(out AsyncUpgradeCardUseInfo useInfo))
			{ }
		}

		/// <inheritdoc/>
		public void ProcessAsyncUpgradeCardUses()
		{
			while (AsyncUpgradeCardUseInfoQueue.TryDequeue(out AsyncUpgradeCardUseInfo useInfo))
			{
				//this eliminates the case when robot rebooted or finished game in the meantime 
				if (!turnActiveRobots.Any(r => r.Name == useInfo.RobotName))
					continue;

				var robot = turnActiveRobots.First(r => r.Name == useInfo.RobotName);
				
				//we want to check, that this robot still has this hook 
				//(we could have two same messages after each other)

				//pick the associated hook for the card (we can do with only one hook for each implemented
				//async card)
				if (robot.HasAsyncHook(useInfo.CardName, out var asyncHook))
				{
					if (useInfo is AsyncChoiceUpgradeCardUseInfo choiceUseInfo)
					{
						var choiceHook = (IChoiceUpgradeCardHook<HookRetBase, HookArgsBase>)asyncHook;

						choiceHook.PerformHook(new HookArgsBase() { GameState = this, Robot = robot }, new HookRetBase(), choiceUseInfo.ChosenChoice);

						Notifier.UpgradeCardUsed(robot.Name, useInfo.CardName, choiceUseInfo.ChosenChoice);
					}
					else
					{
						var choicelessHook = (IChoicelessUpgradeCardHook<HookRetBase, HookArgsBase>)asyncHook;

						choicelessHook.PerformHook(new HookArgsBase() { GameState = this, Robot = robot }, new HookRetBase());

						Notifier.UpgradeCardUsed(robot.Name, useInfo.CardName);
					}

					InputHandler.EndAsyncUpgradeCardOffer(robot.Name, useInfo.CardName);

					//we have only temporary async cards
					robot.DiscardUpgradeCard(this, useInfo.CardName);
				}
			}
		}

	}
}
