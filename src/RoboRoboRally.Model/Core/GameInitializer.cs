﻿using System;
using System.Collections.Generic;
using System.Linq;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core.Phases;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Settings;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Core
{
	/// <summary>
	/// Helper struct that is used during new game setup - 
	/// struct <see cref="GameSettings"/> is converted into this
	/// one and passed to main game loop.
	/// </summary>
	struct NewGameSetup
	{
		/// <summary>
		/// Constructed game state.
		/// </summary>
		public IGameState GameState { get; set; }

		/// <summary>
		/// Default player settings saved from the
		/// <see cref="GameSettings"/> so that they can be
		/// used if new players join the game.
		/// </summary>
		public PlayerSettings DefaultPlayerSettings { get; set; }

		/// <summary>
		/// Starting players of the new game.
		/// </summary>
		public Robot[] StartingPlayers { get; set; }
	}

	/// <summary>
	/// This class is used at the begginning of lauching new game.
	/// It converts information passed from the server into form that
	/// can be subsequently used by <see cref="GameCore"/> for performing
	/// main game loop.
	/// </summary>
    class GameInitializer
    {
		private string gameDataDirPath;

		private string mapDBPath;
		private string cardDBPath;

		private IMap GetMap(MapSettings mapSettings)
		{
			try
			{
				MapFactory mapFactory = new MapFactory();
				return mapFactory.DeserializeMap(mapDBPath, mapSettings);
			}
			catch (Exception ex) when (ex is MapFactoryException || ex is MapInvalidOperationException)
			{
				throw new InvalidGameOperationException("can not setup new game - error occured during deserialization of map settings, details: " + ex.Message);
			}
		}

		private Pack<ProgrammingCard> GetPlayerPack(GameSettings gameSettings)
		{
			var pack = gameSettings.PlayerSettings.PlayerProgrammingCards.CopyPack();
			pack.ShuffleDrawingPile();
			return pack;
		}

		private TileRecord<StartingTile> GetStartingTileForPlayer(List<TileRecord<StartingTile>> startingTiles, string player)
		{
			//just randomly select one of the starting tiles...
			var index = MyRandom.Random.Next(0, startingTiles.Count);
			
			var tr = startingTiles[index];
			startingTiles.RemoveAt(index);
			return tr;
		}

		private Robot[] GetPlayers(IMap map, string[] players, GameSettings gameSettings)
		{
			if (players.Length == 0)
				throw new InvalidGameOperationException("can not setup new game - no players provided");

			var startingTiles = map.GetTileRecords<StartingTile>().ToList();

			if (startingTiles.Count < players.Length)
				throw new InvalidGameOperationException("can not setup new game - there are more players than there are starting tiles");

			IList<Robot> robots = new List<Robot>();

			foreach (var player in players)
			{
				if (robots.Any(r => r.Name == player))
					throw new InvalidGameOperationException("can not setup new game - duplicate player name");

				var startingTile = GetStartingTileForPlayer(startingTiles, player);
				var playerPack = GetPlayerPack(gameSettings);

				if (playerPack.Size() < gameSettings.PlayerSettings.RegisterBoardSize)
					throw new InvalidGameOperationException("can not setup new game - there are less programming cards in starting pack than there are registers, this is not allowed");

				robots.Add(new Robot(player, 
					gameSettings.PlayerSettings.EnergyAmount,
					startingTile.Tile.OutDirection,
					startingTile.Coords,
					playerPack,
					new RegisterBoard(gameSettings.PlayerSettings.RegisterBoardSize),
					new UpgradeCardBoard(gameSettings.PlayerSettings.UpgradeCardsLimits),
					gameSettings.PlayerSettings.DefaultPlayerDamage
					));
			}

			return robots.ToArray();
		}

		private Pack<UpgradeCard> GetUpgradeCardPack(GameSettings gameSettings)
		{
			var pack = gameSettings.CardSettings.UpgradeCards;
			pack.ShuffleDrawingPile();
			return pack;
		}
		
		private IAction[] GetGameStateActions(GameSettings gameSettings)
		{
			List<IAction> actions = new List<IAction>();
			actions.Add(new ShootingAction());
			actions.Add(new MovementAction());
			actions.Add(new PushAction());
			actions.Add(new RebootAction(gameSettings.EnvironmentSettings.DefaultRebootDamage));

			return actions.ToArray();
		}

		private PhaseBase[] GetPhases(GameSettings gameSettings)
		{
			return gameSettings.GameFlowSettings.Phases;
		}

		private IGameState GetGameState(GameSettings gameSettings, IInputHandler inputHandler,
			IGameEventNotifier notifier, IList<string> players)
		{
			var map = GetMap(gameSettings.MapSettings);

			var phases = GetPhases(gameSettings);

			var actions = GetGameStateActions(gameSettings);

			var upgradePack = GetUpgradeCardPack(gameSettings);

			var regCount = gameSettings.PlayerSettings.RegisterBoardSize;

			return new GameState(phases, actions, upgradePack, map, notifier, inputHandler, regCount);
		}

		private PlayerSettings GetDefaultPlayerSettings(GameSettings gameSettings)
		{
			return gameSettings.PlayerSettings;
		}

		/// <summary>
		/// This method is used when server calls method
		/// <see cref="Game.StartGame(GameSettings, string[], IInputHandler, IGameEventNotifier)"/>.
		/// It converts the passed argument into struct
		/// <see cref="NewGameSetup"/> which can then be used by
		/// <see cref="GameCore"/> in the main game loop.
		/// </summary>
		public NewGameSetup GetNewGameSetup(GameSettings gameSettings, IInputHandler inputHandler,
			IGameEventNotifier notifier, string[] players)
		{
			MyRandom.Random = new Random(gameSettings.Seed);

			var gameState = GetGameState(gameSettings, inputHandler, notifier, players);

			var phases = GetPhases(gameSettings);

			var defaultPlayerSettings = GetDefaultPlayerSettings(gameSettings);

			var robots = GetPlayers(gameState.Map, players, gameSettings);

			return new NewGameSetup() { GameState = gameState, DefaultPlayerSettings = defaultPlayerSettings, StartingPlayers = robots };
		}

		/// <summary>
		/// While setting up a new game, Model needs to access GameData for
		/// loading maps and so on. As GameData is distributed separately
		/// Model needs to know the path to the folder with content of GameData
		/// that has to be provided from the outside (in our case, from the server).
		/// This parameter is the aforementioned path. The exact folder that it
		/// should point to is the one that contains folders "DB" and "Textures".
		/// </summary>
		public GameInitializer(string gameDataDirPath)
		{
			this.gameDataDirPath = gameDataDirPath;
			mapDBPath = gameDataDirPath + @"/DB/MapDB/";
			cardDBPath = gameDataDirPath + @"/DB/CardDB/";
		}
    }
}
