﻿using RoboRoboRally.Model.CardDB.Cards;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Core.Phases
{
	/// <summary>
	/// This class represents upgrade phase in the robo
	/// rally game and it is responsible for purchasing upgrades.
	/// </summary>
	public class UpgradePhase : PhaseBase
	{
		int previouslyBought;
		List<UpgradeCard> previousOffer;

		public UpgradePhase() :
			base(nameof(UpgradePhase))
		{
			this.previouslyBought = 0;
			this.previousOffer = new List<UpgradeCard>();
		}

		private List<UpgradeCard> GetCardsToOffer(IGameState gameState)
		{
			//offered upgrade cards are determined as follows

			//1. draw upgrade cards as many as there are players
			//2. determine priority for buying (default priority that is not affected by
			//any upgrades)
			//3. offer one card for each player in the order of priority
			//4. now, in the next round
			//	• if someone bought at least one card in the previous round, then draw
			//	  as many cards to fill the offer so taht it equals number of robots
			//	• otherwise, draw new cards

			List<UpgradeCard> toOffer = new List<UpgradeCard>();

			if (previouslyBought > 0)
			{
				toOffer.AddRange(previousOffer);

				if (toOffer.Count < gameState.TurnActiveRobots.Count())
				{
					int diff = gameState.TurnActiveRobots.Count() - toOffer.Count;
					toOffer.AddRange(gameState.DrawUpgradeCards(diff));
				}
				else if (toOffer.Count > gameState.TurnActiveRobots.Count())
				{
					while (toOffer.Count > gameState.TurnActiveRobots.Count())
					{
						var c = toOffer[toOffer.Count - 1];
						toOffer.Remove(c);
						gameState.PlaceUpgradeCardToPack(c);
					}
				}
			}
			//we are drawing a new set of cards
			else
			{
				//this also works in the first iteration (only that there are no cards left to place back)
				foreach (var c in previousOffer)
					gameState.PlaceUpgradeCardToPack(c);

				toOffer.AddRange(gameState.DrawUpgradeCards(gameState.TurnActiveRobots.Count()));
			}

			return toOffer;
		}

		private void BuyForSingleRobot(IGameState gameState, Robot robot, List<UpgradeCard> cardsToOffer)
		{
			gameState.Notifier.RobotIsBuying(robot.Name, cardsToOffer.ToArray().Select(c => c.Name).ToArray());

			var offer = new PurchaseOffer()
			{
				RobotName = robot.Name,
				EnergyAmount = robot.EnergyAmount,
				OfferedCards = cardsToOffer.ToArray(),
				OwnedCards = robot.GetOwnedUpgradeCards(),
				UpgradeCardsLimits = robot.GetUpgradeCardLimits()
			};

			var response = gameState.InputHandler.OfferUpgradeCardPurchase(offer);

			var purchasedCard = response.PurchasedCard;

			if (purchasedCard != null)
			{
				if (!cardsToOffer.Contains(purchasedCard))
					throw new RunningGameException("can not purchase card - purchased card is not among the offered ones");

				if (robot.EnergyAmount < purchasedCard.EnergyCost)
					throw new RunningGameException("can not purchase card - robot does not have enough energy");

				var ownedCards = robot.GetOwnedUpgradeCards();

				//discard has to happen 
				if (ownedCards.Where(c => c.UpgradeCardType == purchasedCard.UpgradeCardType).Count() >= robot.GetUpgradeCardLimits()[purchasedCard.UpgradeCardType])
				{
					if (response.DiscardedCard == null)
						throw new RunningGameException("can not purchase card - robot has reached limits on upgrade card type - expecting discard");

					robot.DiscardUpgradeCard(gameState, response.DiscardedCard.Name);
				}

				robot.ObtainUpgradeCard(gameState, purchasedCard);

				robot.ChangeEnergyAmount(gameState, -1 * purchasedCard.EnergyCost);

				cardsToOffer.Remove(purchasedCard);

				previouslyBought++;
			}
		}

		/// <inheritdoc/>
		protected override void PerformPhaseImpl(IGameState gameState)
		{
			var cardsToOffer = GetCardsToOffer(gameState);

			//it can actually happen, that there are no upgrade cards in the pack
			if (cardsToOffer.Count == 0)
			{
				gameState.Notifier.UpgradePhaseSkipped();
				previouslyBought = 0;
				previousOffer.Clear();
				return;
			}

			var robotsByPriority = gameState.GetTurnActiveRobotsByDefaultPriority();
			
			gameState.Notifier.UpgradePhasePriorityDetermined(robotsByPriority.Select(r => r.Name).ToArray());
			
			previouslyBought = 0;

			previousOffer.Clear();

			foreach (var robot in robotsByPriority)
			{
				if (cardsToOffer.Count == 0)
					break;

				BuyForSingleRobot(gameState, robot, cardsToOffer);
			}

			previousOffer.AddRange(cardsToOffer);

		}	
	}
}
