﻿namespace RoboRoboRally.Model.Core.Phases
{
	/// <summary>
	/// <para>
	/// This namespace contains each phase with it's core
	/// logic.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{
	}

	/// <summary>
	/// Base class for all phases.
	/// </summary>
    public abstract class PhaseBase
    {
		string phaseName;

		/// <summary>
		/// Method that is performed before each phase.
		/// </summary>
		protected virtual void Before(IGameState gameState)
		{
			gameState.Notifier.PhaseStarted(phaseName);
		}

		/// <summary>
		/// Method that is perfromed after each phase.
		/// </summary>
		protected virtual void After(IGameState gameState)
		{
			gameState.Notifier.PhaseEnded(phaseName);
		}

		/// <summary>
		/// Main body of each phase.
		/// </summary>
		protected abstract void PerformPhaseImpl(IGameState gameState);

		/// <summary>
		/// This method performs the whole phase, including method
		/// before phase body, phase body itslef and method after
		/// phase body.
		/// </summary>
		public void PerformPhase(IGameState gameState)
		{
			Before(gameState);
			PerformPhaseImpl(gameState);
			After(gameState);
		}

		/// <summary>
		/// Phase ctor with phase name.
		/// </summary>
		protected PhaseBase(string phaseName)
		{
			this.phaseName = phaseName;
		}
    }
}
