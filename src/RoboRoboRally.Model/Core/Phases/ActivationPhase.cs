﻿using System.Collections.Generic;
using System.Linq;
using RoboRoboRally.Model.Core.Actions;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Core.Phases
{
	/// <summary>
	/// <para>
	/// This class represents activation phase in the robo
	/// rally game. It's main body performs each register and
	/// activates static map elements like belts, checkpoints,
	/// rotating gears or robots' lasers.
	/// </para>
	/// </summary>
    public class ActivationPhase : PhaseBase
    {
		internal BeforeActivationPhaseStep BeforeActivationPhaseStep { get; private set; }

		internal AfterActivationPhaseStep AfterActivationPhaseStep { get; private set; }
		
		internal BeforePlayingRegisterStep BeforePlayingRegisterStep { get; private set; }

		internal AfterPlayingRegisterStep AfterPlayingRegisterStep { get; private set; }

		public ActivationPhase() :
			base(nameof(ActivationPhase))
		{
			BeforeActivationPhaseStep = new BeforeActivationPhaseStep();

			AfterActivationPhaseStep = new AfterActivationPhaseStep();

			BeforePlayingRegisterStep = new BeforePlayingRegisterStep();

			AfterPlayingRegisterStep = new AfterPlayingRegisterStep();
		}

		#region MAPELEMENTS

		/*
		These are tile drawings for some of the examples
			---------
			|		|
			|		|
			|		|
			---------
	
			--------- ---------
			|		| |		  |
			|		| |	 	  |
			|		| |		  |
			--------- ---------
	
			--------- --------- ---------
			|		| |		  | |		|
			|		| |	 	  | |		|
			|		| |		  | |		|
			--------- --------- ---------
	
			--------- ---------
			|		| |		  |
			|		| |	 	  |
			|		| |		  |
			--------- ---------
			---------
			|		|
			|		|
			|		|
			---------
	
			--------- ---------
			|		| |		  |
			|		| |	 	  |
			|		| |		  |
			--------- ---------
					  ---------
					  |		  |
					  |		  |
					  |		  |
					  ---------
	
		*/


		//special struct for the activation of push panels and belts
		private struct IntendedMovement
		{
			public Robot Robot { get; set; }

			public MapCoordinates IntendedPosition { get; set; }

			public Direction Direction { get; set; }
		}

		private void FilterWallObstacles(IGameState gameState, List<IntendedMovement> intendedMovements)
		{
			//remove such records, where there is a wall between the robot and the intended position
			intendedMovements.RemoveAll(x => gameState.Map.IsWallBetween(x.IntendedPosition, x.Robot.Position));
		}

		private void FilterTileObstacles(IGameState gameState, List<IntendedMovement> intendedMovements)
		{
			intendedMovements.RemoveAll(x => gameState.Map[x.IntendedPosition] is PriorityAntennaTile);
		}

		private void FilterDuplicates(List<IntendedMovement> intendedMovements)
		{
			//removes records where the robots would collide by ending up on the same tile
			while (true)
			{
				bool duplicate = false;
				for (int i = 0; i < intendedMovements.Count; ++i)
				{
					var pos = intendedMovements[i].IntendedPosition;
					for (int j = i + 1; j < intendedMovements.Count; ++j)
					{
						if (intendedMovements[j].IntendedPosition == pos)
						{
							duplicate = true;
							break;
						}
					}
					if (duplicate)
					{
						intendedMovements.RemoveAll(x => x.IntendedPosition == pos);
						break;
					}
				}
				if (!duplicate)
					break;
			}
		}

		private void FilterCyclesOfLengthTwo(List<IntendedMovement> intendedMovements)
		{
			//this method removes records with cycles
			while (true)
			{
				bool cycle = false;

				Robot r1 = null;
				Robot r2 = null;

				for (int i = 0; i < intendedMovements.Count; ++i)
				{
					var robotPos = intendedMovements[i].Robot.Position;
					var intendedPos = intendedMovements[i].IntendedPosition;
					for (int j = i + 1; j < intendedMovements.Count; ++j)
					{
						if (intendedMovements[j].IntendedPosition == robotPos && intendedMovements[j].Robot.Position == intendedPos)
						{
							cycle = true;
							r1 = intendedMovements[i].Robot;
							r2 = intendedMovements[j].Robot;
							break;
						}
					}
					if (cycle)
					{
						break;
					}
				}

				if (cycle)
				{
					intendedMovements.RemoveAll(x => x.Robot == r1);
					intendedMovements.RemoveAll(x => x.Robot == r2);
				}
				else
				{
					break;
				}
			}
		}

		private RotationType GetBeltRotation(IGameState gameState, IntendedMovement intendedMovement)
		{
			var tile = gameState.Map[intendedMovement.IntendedPosition];
			if (tile is BeltTile beltTile)
			{
				var perpDirections = intendedMovement.Direction.GetPerpDirections();
				if (perpDirections.Item1 == beltTile.OutDirection ||
					perpDirections.Item2 == beltTile.OutDirection)
				{
					var dirCpy = intendedMovement.Direction;
					if (dirCpy.Rotate(RotationType.Right) == beltTile.OutDirection)
						return RotationType.Right;
					else
						return RotationType.Left;
				}
			}
			return RotationType.None;
		}

		private void PerformBeltStep(IGameState gameState, List<IntendedMovement> intendedMovements)
		{
			//this method is responsible for performing one correct step of belts ...
			//this method is shared by both green belts and blue belts as blue belts
			//are implemented as two steps of single belt (without any special handling) ...
			//this has some drawbacks, as we do not adhere the rules of roborally in one special weird case,
			//but is far more easier to implement as we can reuse everything from green belts
			#region explanation
			/*
			Other than that, there are following weird situations that can happen with belts

			Situation 1:
			--------- ---------
			|		| |		  |
			|	R1	| |	 R2	  |
			|	1>	| |	  	  |
			--------- ---------
			Res: R1 does not move (it doesnt push R2)

			Situation 2:
			--------- --------- ---------
			|		| |		  | |		|
			|	R1	| |	 R2	  | |		|
			|	1>	| |	 1>	  | |		|
			--------- --------- ---------
			Res: In this case, both robots move one space to the right
			
			Situation 3:
			--------- --------- ---------
			|		| |		  | |		|
			|	R1 W| |	 R2	  | |		|
			|	1>	| |	 1>	  | |		|
			--------- --------- ---------
			Res: In this case, only R2 is moved to the right

			Situation 4:
			--------- ---------
			|		| |		  |
			|	R1	| |	 	  |
			|	1>	| |		  |
			--------- ---------
					  ---------
					  |		  |
					  |^ R2	  |
					  |1	  |
					  ---------
			Res: in this case, the robots do not move

			Situation 5:
			--------- --------- ---------
			|		| |		  | |		|
			|	R1	| |	  R2  | |  R3	|
			|	1>	| |	  1>  | |		|
			--------- --------- ---------
			Res: The robots do not move

			Situation 6:
			--------- ---------
			|		| |		  |
			|	R1	| |	 R2	  |
			|	1>	| |	 <1	  |
			--------- ---------
			Res: Nothing happens

			Algorithm that can handle this is similiar to the one with push panels
			1. We first indentify intended positions ... positions that the robot would end up on
			2. Now, we need to filter this list
				a. We need to remove cases where there is a wall between the belt and the tile (Situation 3)
				b. We need to remove duplicate records ... that is situation 4
				c. We need to remove records with cycle of length 2 ... this is situation 6
			3. Now, there are still some cases left where we can not move like situation 5 and 1
			4. To detect these, we do the same as with push panels 
			5. We will iterate over the list of records
				a. We look at the intended position
					1. If it is free, then this robot will definitely move ... call move Action 
					   (maybe also a turn action) and remove this record from the list
					2. If it is not free and the occupying robot is part of the list, then continue
					3. If it is not free and the occuyping robot is not part of the list, then remove this record
				b. We repeat this until the number of records did not change in one full loop
			6. If there are no more records, we end.
			7. If the number of records is the same as at the beggining, we need to have a cycle

			 */
			#endregion explanation
			//this handles situation 3
			FilterWallObstacles(gameState, intendedMovements);
			//this handles situation 4
			FilterDuplicates(intendedMovements);
			//this handles situation 6
			FilterCyclesOfLengthTwo(intendedMovements);

			//now the final loop
			while (true)
			{
				int count = intendedMovements.Count;
				if (count == 0)
					break;

				for (int i = 0; i < intendedMovements.Count; ++i)
				{
					var pos = intendedMovements[i].IntendedPosition;
					if (gameState.IsTileOccupied(pos, out Robot r))
					{
						if (intendedMovements.Any(x => x.Robot == r))
							continue;
						else
						{
							//we remove this record
							intendedMovements.RemoveAt(i);
							break;
						}
					}
					else
					{
						//get rotation (may return RotationType.None)
						var rotation = GetBeltRotation(gameState, intendedMovements[i]);
						//now, perform the movement action 
						var moveAction = gameState.GetAction<MovementAction>();
						moveAction.PerformAction(new MovementActionArgs()
						{
							Amount = 1,
							Direction = intendedMovements[i].Direction,
							GameState = gameState,
							Robot = intendedMovements[i].Robot
						});
						//and also, perform the rotation action
						if (rotation != RotationType.None)
						{
							intendedMovements[i].Robot.Rotate(gameState, rotation);
						}

						//and remove the record
						intendedMovements.RemoveAt(i);
						break;
					}
				}

				if (intendedMovements.Count == count)
				{
					//we have a cycle
					//TODO: handle a cycle
					throw new RunningGameException("belt fatal error - unhandled cycle");
				}

			}

		}

		//may be used with green belt tiles as well as with blue belt tiles
		private List<IntendedMovement> GetBeltIntendedMovement<T>(IGameState gameState) where T : BeltTile
		{
			List<IntendedMovement> intendedMovements = new List<IntendedMovement>();

			foreach (var robot in gameState.GameActiveRobots)
			{
				if (gameState.Map[robot.Position] is T beltTile)
				{
					intendedMovements.Add(new IntendedMovement()
					{
						Direction = beltTile.OutDirection,
						IntendedPosition = robot.Position + beltTile.OutDirection,
						Robot = robot
					});
				}
			}

			return intendedMovements;
		}


		//2.
		internal void ActivateGreenBelts(IGameState gameState)
		{
			gameState.Notifier.GreenBeltsActivated();

			var intendedMovements = GetBeltIntendedMovement<GreenBeltTile>(gameState);
			PerformBeltStep(gameState, intendedMovements);
		}

		//1.
		internal void ActivateBlueBelts(IGameState gameState)
		{
			gameState.Notifier.BlueBeltsActivated();

			//as already noted, blue belts are basically implemeneted as two separate steps
			//of green belt
			//perform one step
			var intendedMovements = GetBeltIntendedMovement<BlueBeltTile>(gameState);
			PerformBeltStep(gameState, intendedMovements);
			//perform the second step
			intendedMovements = GetBeltIntendedMovement<BlueBeltTile>(gameState);
			PerformBeltStep(gameState, intendedMovements);
		}

		private List<IntendedMovement> GetPushPanelIntendedMovement(IGameState gameState, int registerNumber)
		{
			List<IntendedMovement> intendedMovements = new List<IntendedMovement>();

			foreach (var robot in gameState.GameActiveRobots)
			{
				var pushPanelWalls = gameState.Map.GetTileWalls<PushPanelWall>(gameState.Map[robot.Position]);

				//with push panels, this excludes the situation 6
				if (pushPanelWalls.Length != 1)
					continue;
				else
				{
					var ppWall = pushPanelWalls[0];
					if (!ppWall.Registers.Contains(registerNumber))
						continue;
					intendedMovements.Add(new IntendedMovement()
					{
						Direction = ppWall.FacingDirection,
						IntendedPosition = ppWall.FacingDirection + robot.Position,
						Robot = robot
					});
				}
			}

			return intendedMovements;
		}

		private List<IntendedMovement> GetPushPanelPushRecords(IGameState gameState, int registerNumber)
		{
			List<IntendedMovement> pushRecords = new List<IntendedMovement>();

			foreach (var robot in gameState.GameActiveRobots)
			{
				var pushPanelWalls = gameState.Map.GetTileWalls<PushPanelWall>(gameState.Map[robot.Position]);

				//excluding situation 6
				if (pushPanelWalls.Length != 1)
					continue;
				else
				{
					MapCoordinates newPos = pushPanelWalls[0].FacingDirection + robot.Position;
					if (gameState.Map.IsWallBetween(robot.Position, newPos))
					{
						//excluding robot R1 from situation 2
					}
					else
					{
						pushRecords.Add(new IntendedMovement()
						{
							Direction = pushPanelWalls[0].FacingDirection,
							IntendedPosition = newPos,
							Robot = robot
						});
					}
				}
			}

			//now, exclude cases with situation 1
			while (true)
			{
				bool duplicate = false;

				for (int i = 0; i < pushRecords.Count; ++i)
				{
					var pos = pushRecords[i].IntendedPosition;
					for (int j = i + 1; j < pushRecords.Count; ++j)
					{
						if (pushRecords[j].IntendedPosition == pos)
						{
							duplicate = true;
							break;
						}
					}
					if (duplicate)
					{
						pushRecords.RemoveAll(x => x.IntendedPosition == pos);
						break;
					}
				}
				if (!duplicate)
					break;
			}

			//now, detect if there are cycles of length 2
			//this handles situation 7
			while (true)
			{
				bool cycle = false;

				Robot r1 = null;
				Robot r2 = null;

				for (int i = 0; i < pushRecords.Count; ++i)
				{
					var robotPos = pushRecords[i].Robot.Position;
					var intendedPos = pushRecords[i].IntendedPosition;
					for (int j = i + 1; j < pushRecords.Count; ++j)
					{
						if (pushRecords[j].IntendedPosition == robotPos && pushRecords[j].Robot.Position == intendedPos)
						{
							cycle = true;
							r1 = pushRecords[i].Robot;
							r2 = pushRecords[j].Robot;
							break;
						}
					}
					if (cycle)
					{
						break;
					}
				}

				if (cycle)
				{
					pushRecords.RemoveAll(x => x.Robot == r1);
					pushRecords.RemoveAll(x => x.Robot == r2);
				}
				else
				{
					break;
				}
			}

			return pushRecords;
		}

		//3.
		internal void ActivatePushPanels(IGameState gameState, int registerNumber)
		{
			#region explanation
			/*
			 There are several weird situations that can occur with the push pannels

			Situation 1:
			--------- ---------
			|		| |		  |
			|		| |	 R2	 P|
			|		| |		  |
			--------- ---------
			---------
			|		|
			|	R1	|
			|	P	|
			---------
			Res: nothing happens

			Situation 2:

			--------- ---------
			|		| |		  |
			|		| |	 R2	 P|
			|		| |		  |
			--------- ---------
			---------
			|	W	|
			|	R1	|
			|	P	|
			---------
			Res: R2 is pushed onto the tile

			Situation 3:
			--------- ---------
			|		| |		  |
			|		| |	 R2  P|
			|		| |		  |
			--------- ---------
					  ---------
					  |		  |
					  |	 R1	  |
					  |	 P	  |
					  ---------
			Res: R2 is pushed left, R1 is pushed top

			Situation 4: cycle
			--------- ---------
			|	P	| |		  |
			|	R3	| |	 R2	P |
			|		| |		  |
			--------- ---------
			--------- ---------
			|		| |		  |
			|P	R4	| |	 R1	  |
			|		| |	  P	  |
			--------- ---------
			Res: normally, we would want that all robots switch places
			but currently, the cycle is detected but an exception is thrown

			Situation 5:

			--------- ---------
			|	P	| |		  |
			|	R3	| |	 R2	P |
			|		| |		  |
			--------- ---------
			--------- ---------
			|		| |		  |
			|P	R4	| |	 R1	  |
			|		| |	  	  |
			--------- ---------
			Res: R4 is pushed first - it pushes R1 to the right, then R3 is pushed and then R2

			Situation 6:

			---------
			|		|
			|P	R1	|
			|	P	|
			---------
			Res: nothing happens

			Situation 7:
			--------- ---------
			|		| |		  |
			|P  R1	| |	 R2	 P|
			|		| |		  |
			--------- ---------
			Res: nothing happens 
			 */

			/*
			 In order to correctly solve all of the above cases, following algorithm will work:
			 1. Get each robot that will be pushed
				a. this excludes R1 from the situation 6
				b. this also excludes R1 from situation 2
				c. this also excludes R1 and R2 from situation 1
				d. this should also exclude R1 and R2 from situation 7... that is, we need to detect
				   cycles of length 2
			 2. Get their intended positions - these are the positions after push
			 3. Form a list of these positions, that is List<Tuple<Robot,MapCoordinates>>
			 4. Iterate over the list, and now
				a. try to validate the position - that means, check if the intended position is free
				b. if it is free, then we can do push - call PushAction and remove this record from the list
				c. if it is occupied, then check if the occupying robot is part of the list
					1. if it is, then skip
					2. if it is not part of the list, then perform push action and remove this record from the list
			 5. If we did not free any records, then we must have a cycle ... in this case, we do not 
			    perform any push action ... this case has to be handled in a special manner by the plotter, so
				call a special method on the server
			 */
			#endregion explanation

			gameState.Notifier.PushPanelsActivated();

			//first method excludes situation 6
			var intendedMovements = GetPushPanelIntendedMovement(gameState, registerNumber);
			//this excludes situation 2
			FilterWallObstacles(gameState, intendedMovements);
			//this excludes priority antenna tile (even though this is already correctly handled in push action)
			FilterTileObstacles(gameState, intendedMovements);
			//this excludes situation 1
			FilterDuplicates(intendedMovements);
			//this excludes situation 7
			FilterCyclesOfLengthTwo(intendedMovements);

			//this main cycle is similiar to the one with belts, but they differ in few minor steps
			while (true)
			{
				int initialCount = intendedMovements.Count;
				if (initialCount == 0)
					break;

				for (int i = 0; i < intendedMovements.Count; ++i)
				{
					var pushRecord = intendedMovements[i];
					if (gameState.IsTileOccupied(pushRecord.IntendedPosition, out Robot r))
					{
						if (intendedMovements.Any(x => x.Robot == r))
						{
							continue;
						}
					}
					//in other cases, perform the push and remove this record...
					//this includes cases 4. b. and 4. c. 2.
					var pushAction = gameState.GetAction<PushAction>();
					pushAction.PerformAction(new PushActionArgs() { Robot = pushRecord.Robot, Direction = pushRecord.Direction,
						GameState = gameState, Amount = 1 });
					intendedMovements.RemoveAt(i);
					break;
				}

				if (intendedMovements.Count == initialCount)
				{
					//in this case, we must have a cycle
					//TODO: Handle a cycle
					throw new RunningGameException("not handled cycle with push panels");
				}
			}

			foreach (var robot in gameState.GameActiveRobots)
			{
				if (gameState.Map.IsRebootPosition(robot.Position))
					gameState.GetAction<RebootAction>().PerformAction(new ActionArgsBase() { GameState = gameState, Robot = robot });
			}
		}

		//4.
		internal void ActivateRotatingGears(IGameState gameState)
		{
			//first, notify 
			gameState.Notifier.RotatingGearsActivated();

			var rotatingTiles = gameState.Map.GetTileRecords<RotatingTile>();

			foreach (var robot in gameState.GameActiveRobots)
			{
				foreach (var rotatingTile in rotatingTiles)
				{
					if (robot.Position == rotatingTile.Coords)
					{
						var rotation = rotatingTile.Tile.RotationType;
						robot.Rotate(gameState, rotation);
					}
				}
			}
		}
		

		private void DamageRobotsAlongLaserPath(IGameState gameState, LaserPath laserPath)
		{
			var cur = laserPath.Start;

			while (cur != laserPath.End)
			{
				if (gameState.IsTileOccupied(cur, out Robot r))
				{
					r.SufferDamage(gameState, laserPath.Damage);

					//lasers (from both laser walls and robots) shoot only first target
					break;
				}

				cur += laserPath.LaserDirection;
				
			}
		}

		//5.
		internal void ActivateLasers(IGameState gameState)
		{
			//Notes on lasers:
			//Robots that are hit take one spam damage card for each damage they received
			//Lasers do not hit more than one robot ... they hit the first one in their path

			//first, notify
			gameState.Notifier.WallLasersActivated();

			var laserPaths = gameState.Map.GetLaserWallLaserPaths();

			foreach (var path in laserPaths)
			{
				DamageRobotsAlongLaserPath(gameState, path);
			}
		}

		//6.
		internal void ShootWithRobots(IGameState gameState)
		{
			//as shooting may be affected by many upgrades, it is, for clarity, moved into 
			//a separate action

			//the robots shoot in order by their priority ... this
			//priority is not affected by any upgrade cards, it is determined as a distance from 
			//the priority antenna
			var robotsByPriority = gameState.GetTurnActiveRobotsByDefaultPriority();

			foreach (var robot in robotsByPriority)
			{
				gameState.Notifier.RobotLasersActivated(robot.Name);

				gameState.GetAction<ShootingAction>().PerformAction(new ActionArgsBase() { GameState = gameState, Robot = robot });
			}
		}

		//7.
		internal void ActivateEnergySpaces(IGameState gameState, int registerNumber)
		{
			//in our version of rules, we add the robot energy cube only when it stands on
			//the energy tile and it is the last round
			if (registerNumber != gameState.RegisterCount)
				return;

			//first, notify 
			gameState.Notifier.EnergySpacesActivated();

			var energyTileRecords = gameState.Map.GetTileRecords<EnergyTile>();

			foreach (var robot in gameState.GameActiveRobots)
			{
				foreach (var energyTile in energyTileRecords)
				{
					if (robot.Position == energyTile.Coords)
					{
						robot.ChangeEnergyAmount(gameState, energyTile.Tile.EnergyAmount);
					}
				}
			}
		}

		//8.
		internal void ActivateCheckpoints(IGameState gameState)
		{
			gameState.Notifier.CheckpointsActivated();

			var checkpointTiles = gameState.Map.GetTileRecords<CheckpointTile>();

			//first, let's record each visited checkpoint
			foreach (var robot in gameState.GameActiveRobots)
			{
				if (gameState.Map[robot.Position] is CheckpointTile checkpointTile)
				{
					if (checkpointTile.Number == robot.GetLastVisitedCheckpoint() + 1)
					{
						robot.AddVisitedCheckpoint(checkpointTile.Number);
						gameState.Notifier.RobotVisitedCheckpoint(robot.Name, checkpointTile.Number);
					}
				}
			}

			//now, we need to check if someone finished the game or not
			var finishedRobot = gameState.GameActiveRobots.FirstOrDefault(r => r.GetLastVisitedCheckpoint() == checkpointTiles.Length);
			//only one robot can finish per turn... null value indicates that none of the robots finished
			if (finishedRobot == null)
				return;

			gameState.RobotFinished(finishedRobot);

			//if there are no more robots, then simply return and the game
			//simulation will end correctly
			if (gameState.GameActiveRobots.Length == 0)
				return;

			//ask if the game should continue ...
			//there are currently only two options, either each player continues (and leaves manually)
			//or each player leaves
			if (!gameState.InputHandler.ShouldGameContinue())
			{
				//if the game should not continue, then deactivate each robot
				//model expects, that the method call also sets termination request of the game
				//(this way, the game will terminate correctly)
				foreach (var robot in gameState.GameActiveRobots)
				{
					gameState.PutRobotToSleepDuringTurn(robot);
				}
			}

		}

		internal void ActivateStaticMapElements(IGameState gameState, int registerNumber)
		{
			gameState.Notifier.MapElementsActivationBegan();

			/*
			 Map elements are activated in the following order
			 1. Blue Belts that move robots by 2
			 2. Green belts that move roboty by 1
			 3. Push Panels 
			 4. Rotating tile
			 5. Lasers
			 6. RobotsShoot
			 7. Energy tiles
			 8. Checkpoints
			*/
			gameState.ProcessAsyncUpgradeCardUses();
			ActivateBlueBelts(gameState);
			gameState.ProcessAsyncUpgradeCardUses();
			ActivateGreenBelts(gameState);
			gameState.ProcessAsyncUpgradeCardUses();
			ActivatePushPanels(gameState, registerNumber);
			gameState.ProcessAsyncUpgradeCardUses();
			ActivateRotatingGears(gameState);
			gameState.ProcessAsyncUpgradeCardUses();
			ActivateLasers(gameState);
			gameState.ProcessAsyncUpgradeCardUses();
			ShootWithRobots(gameState);
			gameState.ProcessAsyncUpgradeCardUses();
			ActivateEnergySpaces(gameState, registerNumber);
			gameState.ProcessAsyncUpgradeCardUses();
			ActivateCheckpoints(gameState);
			gameState.ProcessAsyncUpgradeCardUses();
		}

		#endregion

		#region REGISTERACTIVATION

		internal void ActivateRegister(IGameState gameState, int registerNumber)
		{
			gameState.Notifier.RegisterActivationBegan(registerNumber);

			gameState.ProcessAsyncUpgradeCardUses();

			foreach (var robot in gameState.TurnActiveRobots)
			{
				var c = robot.RevealNextRegister();

				gameState.Notifier.ProgrammingCardRevealed(robot.Name, registerNumber, c.Name);
			}

			gameState.ProcessAsyncUpgradeCardUses();

			//get robots by their priority
			var robotsByPriority = gameState.GetTurnActiveRobotsByDefaultPriority();

			gameState.Notifier.ActivationPhasePriorityDetermined(robotsByPriority.Select(r => r.Name).ToArray());

			foreach (var robot in robotsByPriority)
			{
				//some robot may have been pushed
				if (!gameState.TurnActiveRobots.Contains(robot))
					continue;

				BeforePlayingRegisterStep.PerformHooks(new HookArgsBase() { GameState = gameState, Robot = robot }, new HookRetBase());

				gameState.ProcessAsyncUpgradeCardUses();

				robot.PlayNextRegister(gameState);

				gameState.ProcessAsyncUpgradeCardUses();

				AfterPlayingRegisterStep.PerformHooks(new HookArgsBase() { GameState = gameState, Robot = robot }, new HookRetBase());
			}
		}
		#endregion

		/// <inheritdoc/>
		protected override void PerformPhaseImpl(IGameState gameState)
		{
			//before offerring some async cards, clear non-processed requests
			//from previous turn that could've ended up in the queue
			gameState.ClearAsnycUpgradesUsesQueue();

			foreach (var r in gameState.GameActiveRobots)
				BeforeActivationPhaseStep.PerformHooks(new HookArgsBase() { GameState = gameState, Robot = r }, new HookRetBase());


			for (int registerNumber = 1; registerNumber <= gameState.RegisterCount; ++registerNumber)
			{
				ActivateRegister(gameState, registerNumber);

				ActivateStaticMapElements(gameState, registerNumber);

				//special case ... it can happen, that one of the robots finished during
				//the activation of checkpoints and there might be no more active robots ...
				//(e.g. because other players did not want to continue)
				//in order to prevent the notifications, we need to do this check...
				if (gameState.GameActiveRobots.Length == 0)
					break;
			}

			//at the end of activation phase, we also have to discard program for each robot
			//so we have to iterate either through activeRobots or through turnActiveRobots
			//which depends on whether or not we call discard program when we reboot ...
			foreach (var r in gameState.GameActiveRobots)
				r.DiscardCurrentProgram();

			foreach (var r in gameState.GameActiveRobots)
				AfterActivationPhaseStep.PerformHooks(new HookArgsBase() { GameState = gameState, Robot = r }, new HookRetBase());
		}
	}
}
