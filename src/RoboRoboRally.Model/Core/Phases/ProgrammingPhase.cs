﻿using RoboRoboRally.Model.Core.HookableGameSteps;
using System.Linq;

namespace RoboRoboRally.Model.Core.Phases
{
	/// <summary>
	/// This class represents programming phase of the
	/// robo rally game. It's main body programs each robot
	/// for the next turn.
	/// </summary>
    public class ProgrammingPhase : PhaseBase
    {
		internal DrawAmountStep DrawAmountStep { get; private set; }

		private int defaultDrawAmount;

		/// <summary>
		/// Phase ctor that receives number of cards that should be drawn
		/// for each robot at the start of programming phase as a parameter.
		/// </summary>
		public ProgrammingPhase(int defaultDrawAmount) :
			base(nameof(ProgrammingPhase))
		{
			this.defaultDrawAmount = defaultDrawAmount;
			DrawAmountStep = new DrawAmountStep();
		}

		private ProgramOffer[] GetProgramOffers(IGameState gameState)
		{
			if (gameState.TurnActiveRobots.Length == 0)
				throw new RunningGameException("can not start programming robots as there are no active robots - this can not happen");

			ProgramOffer[] robotProgramOffers = new ProgramOffer[gameState.TurnActiveRobots.Length];

			for (int i = 0; i < gameState.TurnActiveRobots.Length; ++i)
			{
				var robot = gameState.TurnActiveRobots[i];
				//get amount of cards to draw
				//this can be later modified by upgrade card
				//var toDrawAmount = GetCardAmountToDraw(robot);
				var drawAmountRet = DrawAmountStep.PerformHooks(new HookArgsBase() { GameState = gameState, Robot = robot }, new DrawAmountStepRet() { DrawAmount = defaultDrawAmount });
				var toDrawAmount = drawAmountRet.DrawAmount;

				//draw up to @toDrawAmount of cards from player's deck
				var cards = robot.DrawCards(toDrawAmount);

				if (cards.Length < gameState.RegisterCount)
					throw new RunningGameException("can not start programming robots - there are less programming cards than there are registers - this can not happen");

				robotProgramOffers[i] = new ProgramOffer() { Amount = gameState.RegisterCount, RobotName = robot.Name, CardsToChoose = cards };
			}

			return robotProgramOffers;
		}
		
		private void SetRobotPrograms(IGameState gameState, ProgramOffer[] programOffers, ProgramChoice[] programChoice)
		{
			//here, we have to be careful - the program choices do not have to be necessarily in the same
			//order, they only have to contain records for the same robots

			//first, check that there is a record for each robot from program offer
			foreach (var offer in programOffers)
			{
				if (!programChoice.Any(r => r.RobotName == offer.RobotName))
					throw new RunningGameException($"can not set robot program - no matching record for robot {offer.RobotName}");
			}

			for (int i = 0; i < programOffers.Length; ++i)
			{
				var offer = programOffers[i];
				var choice = programChoice.First(r => r.RobotName == offer.RobotName);

				//Careful here about case with empty program ... rules of roborally are as follows:
				//"All programming cards are discarded; now player takes first @registerCount cards
				//from its pack (shuffling discarded cards if necessary)
				//and places them, in this order to his registers 

				var robot = gameState.TurnActiveRobots.First(r => r.Name == offer.RobotName);

				if (choice.EmptyProgram)
				{
					//first, discard cards so that it does not happen that there is not enough cards
					robot.DiscardCards(offer.CardsToChoose);

					var newCards = robot.DrawCards(offer.Amount);

					robot.SetNewProgram(newCards);
				}
				else
				{
					if (choice.ChosenCards.Length != offer.Amount)
						throw new RunningGameException("can not set robot program - the amount of chosen cards does not match the requested amount of cards");

					//we have to quite complicatedly select cards that will be discarded
					//for that, we need to convert the array to list as we will be removing some items
					var cardsToDiscard = offer.CardsToChoose.ToList();

					foreach (var c in choice.ChosenCards)
					{
						for (int j = 0; j < cardsToDiscard.Count; ++j)
						{
							if (cardsToDiscard[j].Name == c.Name)
							{
								cardsToDiscard.RemoveAt(j);
								break;
							}
						}
					}

					robot.DiscardCards(cardsToDiscard.ToArray());

					robot.SetNewProgram(choice.ChosenCards);
				}
			}
		}

		/// <inheritdoc/>
		protected override void PerformPhaseImpl(IGameState gameState)
		{
			var programOffers = GetProgramOffers(gameState);

			var robotPrograms = gameState.InputHandler.GetPrograms(programOffers);

			if (robotPrograms.Length != programOffers.Length)
				throw new RunningGameException("can not program robot - received less robot programs than there were program offers");

			SetRobotPrograms(gameState, programOffers, robotPrograms);
		}

	}
}
