﻿using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;
using System.Linq;

namespace RoboRoboRally.Model.Core.Actions
{
	/// <summary>
	/// This action is used when robot is shooting from it's laser.
	/// </summary>
	class ShootingAction : ActionBase<ActionArgsBase>, IAction
	{
		public LaserDamageStep LaserDamageStep { get; private set; }

		public ShootingDirectionStep ShootingDirectionStep { get; private set; }

		public ShootingTargetsStep ShootingTargetsStep { get; private set; }

		public ShotTargetsStep ShotTargetsStep { get; private set; }

		public ShootingAction()
		{
			this.LaserDamageStep = new LaserDamageStep();

			this.ShootingDirectionStep = new ShootingDirectionStep();

			this.ShootingTargetsStep = new ShootingTargetsStep();

			this.ShotTargetsStep = new ShotTargetsStep();
		}

		private List<Direction> GetShootingDirections(IGameState gameState, Robot shootingRobot)
		{
			var directions = new HashSet<Direction>() { shootingRobot.FacingDirection };

			var directionsRet = ShootingDirectionStep.PerformHooks(new HookArgsBase() { Robot = shootingRobot, GameState = gameState },
				new ShootingDirectionStepRet() { Directions = directions });

			return directions.ToList();
		}

		private List<Robot> GetRobotsToShoot(IGameState gameState, Robot shootingRobot, List<Direction> shootingDirections)
		{
			HashSet<Robot> robotsToShoot = new HashSet<Robot>();
			foreach (var direction in shootingDirections)
			{
				var curCoords = shootingRobot.Position;

				while (true)
				{
					var nextCoords = curCoords + direction;

					if (gameState.Map.IsWallBetween(curCoords, nextCoords))
						break;

					if (gameState.IsTileOccupied(nextCoords, out Robot occRobot))
					{
						robotsToShoot.Add(occRobot);
						break;
					}

					if (gameState.Map[nextCoords] is OutOfMapTile ||
						gameState.Map[nextCoords] is PriorityAntennaTile)
						break;

					curCoords = nextCoords;
				}
			}

			var robotsToShootRet = ShootingTargetsStep.PerformHooks(
				new ShootingTargetsStepArgs() { GameState = gameState, Robot = shootingRobot, ShootingDirections = shootingDirections },
				new ShootingTargetsStepRet() { RobotsToShoot = robotsToShoot }
				);

			return robotsToShootRet.RobotsToShoot.ToList();
		}

		/// <inheritdoc/>
		public override void PerformAction(ActionArgsBase args)
		{
			var shootingRobot = args.Robot;
			var gameState = args.GameState;

			var directions = GetShootingDirections(gameState, shootingRobot);

			var targets = GetRobotsToShoot(gameState, shootingRobot, directions);

			var damageRet = LaserDamageStep.PerformHooks(
				new ShotTargetsStepArgs() { GameState = gameState, Robot = shootingRobot, ShotRobots = targets },
				new DamageStepRet() { Damage = shootingRobot.DefaultDamage }
				);

			//deal damage to each target
			foreach (var target in targets)
			{
				target.SufferDamage(gameState, damageRet.Damage);
			}

			//shot targets may be affected by several upgrades
			ShotTargetsStep.PerformHooks(
				new ShotTargetsStepArgs() { GameState = gameState, Robot = shootingRobot, ShotRobots = targets },
				new HookRetBase()
				);
		}
	}
}
