﻿using System.Linq;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Core.Actions
{
	/// <summary>
	/// This action is used when some robot is rebooting.
	/// </summary>
	class RebootAction : ActionBase<ActionArgsBase>, IAction
	{
		//if there are any problems with serialization, this can be also
		//put into game settings
		//by default, this should be two spam cards
		Damage defaultRebootDamage;

		public RebootDamageStep RebootDamageStep { get; set; }

		public BeforeRebootStep BeforeRebootStep { get; set; }

		struct RebootTileInfo
		{
			public MapCoordinates RebootPosition { get; set; }

			public Direction OutDirection { get; set; }
		}
		
		public RebootAction(Damage defaultRebootDamage)
		{
			this.defaultRebootDamage = defaultRebootDamage;

			RebootDamageStep = new RebootDamageStep();

			BeforeRebootStep = new BeforeRebootStep();
		}

		/*
		With reboot, there are, as with everything in this game, many wierd situations that can occur.
		First, one would probably expect, following behavior of next scenario (R2 performed reboot)

		--------- ---------
		|Rtoken>| |		  |
		|	R1	| |	 R2	  |
		|		| |		  |
		--------- ---------

		--------- ---------
		|Rtoken>| |		  |
		|		| |	 R1	  |
		|		| |		  |
		--------- ---------

		--------- ---------
		|Rtoken>| |		  |
		|	R2	| |	 R1	  |
		|		| |		  |
		--------- ---------

		In order to implement this correctly, we have to do an ugly trick and before
		selecting the position for reboot (reboot token or starting tile), we have to set
		R2's position to some invalid value so that it does not block algorithm for choosing the
		reboot tile.

		Another key thing for correct behavior is detecting of cycles ... 
		Let's consider following scenario (R3 called reboot)
		--------- --------- ---------
		|Rtoken>| |		  | |		|
		|	R1	| |	 R2	  | |	Pit	|
		|		| |		  | |		|
		--------- --------- ---------
		---------
		|		|
		|	R3	|
		|		|
		---------

		In this case, we have to prevent R3 from rebooting onto reboot token - in this case
		this cycle has to be detected and R3 will reboot to some starting tile or other
		reboot token that does not contain cycle. (basically, we have to prevent that
		during potentiall pushes, a robot would step on a new pit tile or out of map tile)
			 */

		private bool IsValidRT(IGameState gameState, TileRecord<RebootTile> rt)
		{
			var currentPos = rt.Coords;
			while (true)
			{
				//if this tile is occupied by some obstacle, like out of map tile, pit tile or antenna tile, then return false
				if (gameState.Map.IsRebootPosition(currentPos) ||
					gameState.Map[currentPos] is PriorityAntennaTile)
					return false;

				//if this tile is also not occupied by any robot, then return true, we have 
				//one free tile that is required to potentially push the robots
				if (!gameState.IsTileOccupied(currentPos))
					return true;

				//if it is, then we need to go to the next tile - consider the next tile
				//but check if these tiles are not separated by walls, if they are
				//then return false
				var nextCoords = currentPos + rt.Tile.OutDirection;
				if (gameState.Map.IsWallBetween(currentPos, nextCoords))
					return false;

				// if they are not, then continue with loop
				currentPos = nextCoords;
			}
		}

		private RebootTileInfo GetRebootPosition(IGameState gameState, MapCoordinates rebootingRobotPosition)
		{
			//By default robo rally rules, the reboot position is determined as follows
			//• if the robot is on the starting plan, then, 
			//		a) if there is not a reboot token, then he reboots to his starting position
			//		b) if there is a reboot token, the he reboots to this token
			//• if the robot is on the game plan, then he reboots to the closest reboot token

			//however, rules do not say anything about particularly weird scenarios that can occur 
			//if the reboot token is pointed to the wall and so on;
			//due to this inexactness, and the fact that in model, we are not able to distinguish
			//between starting plan and game plan, algorithm will simply pick either 
			//closest valid reboot token, or closest free starting tile (valid reboot token is
			//one that would not create a cycle)
			

			if (gameState.Map[rebootingRobotPosition] is RebootTile rt )
				return new RebootTileInfo() { OutDirection = rt.OutDirection, RebootPosition = rebootingRobotPosition };
			if (gameState.Map[rebootingRobotPosition] is StartingTile st)
				return new RebootTileInfo() { OutDirection = st.OutDirection, RebootPosition = rebootingRobotPosition };

			var rebootTiles = gameState.Map.GetTileRecords<RebootTile>();
			var startingTiles = gameState.Map.GetTileRecords<StartingTile>();

			var freeStartingTiles = startingTiles.Where(x => !gameState.IsTileOccupied(x.Coords)).ToArray();

			var freeSTTuples = freeStartingTiles.Select(x => (rebootingRobotPosition.GetManhattanDistance(x.Coords), x)).ToArray();

			var closestFreeST = freeSTTuples.OrderBy(x => x.Item1).First();

			var closerRTs = rebootTiles.Select(x => (rebootingRobotPosition.GetManhattanDistance(x.Coords), x)).Where(x => x.Item1 <= closestFreeST.Item1);

			//if there are no closerRTs, then we simply take the closest starting
			//tile as a tile for reboot
			if(closerRTs.Count() == 0)
				return new RebootTileInfo() { OutDirection = closestFreeST.x.Tile.OutDirection, RebootPosition = closestFreeST.x.Coords };

			//now, ok, we have some reboot token tiles to choose from and we want to choose the closest valid one
			closerRTs = closerRTs.OrderBy(x => x.Item1);

			foreach (var closerRT in closerRTs)
			{
				if (IsValidRT(gameState, closerRT.x))
				{
					return new RebootTileInfo() { OutDirection = closerRT.x.Tile.OutDirection, RebootPosition = closerRT.x.Coords };
				}
			}

			//if we reach this line then none of the closer reboot tiles are appropriate and we will return the 
			//closest starting tile
			return new RebootTileInfo() { OutDirection = closestFreeST.x.Tile.OutDirection, RebootPosition = closestFreeST.x.Coords };

		}

		/// <inheritdoc/>
		public override void PerformAction(ActionArgsBase args)
		{

			//1. invalidate rebooting robot's position
			//2. determine reboot position (be careful about cycles)
			//3. now, if the determined reboot position is occupied by some robot that is not the rebooting
			//	 robot, then this robot is rebooting to some reboot token tile and pushes will occur
			//4. now, simply deal damage to robot  and mark this robot as invalid for the remaining of this turn
			//note that we do not cancel programming or discard cards in register board in any way
			//... cancellation of programming is done automatically as this robot remains
			//inactive for the remainder of this turn and the discards are done for all
			//active robots at the end of this turn

			BeforeRebootStep.PerformHooks(new HookArgsBase() { GameState = args.GameState, Robot = args.Robot }, new HookRetBase());

			var rebootingRobot = args.Robot;
			var gameState = args.GameState;
			var oldPos = rebootingRobot.Position;
			var oldDir = rebootingRobot.FacingDirection;

			rebootingRobot.InvalidatePosition();

			//passing the oldPos to GetRebootPosition is crucial - as we are determining
			//the reboot position based on the original position of Robot and its position
			//has been already invalidated !!!!!
			var rebootTileInfo = GetRebootPosition(gameState, oldPos);
			var rebootPosition = rebootTileInfo.RebootPosition;
			var rebootDirection = rebootTileInfo.OutDirection;

			//notify about the reboot
			gameState.Notifier.RobotIsRebooting(rebootingRobot.Name, rebootPosition, rebootDirection);

			//2.
			if (gameState.IsTileOccupied(rebootPosition, out Robot occuypingRobot) && occuypingRobot != rebootingRobot)
			{
				var pushAction = gameState.GetAction<PushAction>();
				pushAction.PerformAction(new PushActionArgs() { GameState = gameState, Robot = occuypingRobot,
					Direction = rebootDirection, Amount = 1,
				});

				//3.
				if (gameState.IsTileOccupied(rebootPosition))
					throw new RunningGameException("can not perform reboot - robot is occupying reboot token even after push - this can not happen");
			}

			//4.
			rebootingRobot.SetNewPosition(rebootPosition);
			rebootingRobot.SetNewFacingDirection(rebootDirection);

			//actually, in one special case, it would make sense to not notify about the teleport ...
			//that would be the case when the robot did not change either position or direction ...
			//however, plotter is waiting for this notification and so we notify anyway
			gameState.Notifier.RobotTeleported(rebootingRobot.Name, oldPos, rebootPosition, rebootDirection);

			//5.
			//damage can be modified by upgrades
			var damageRet = RebootDamageStep.PerformHooks(new HookArgsBase() { GameState = gameState, Robot = rebootingRobot }, new DamageStepRet() { Damage = defaultRebootDamage });
			if(damageRet.Damage.DamageCards.Length > 0)
				rebootingRobot.SufferDamage(gameState, damageRet.Damage);


			//6.
			gameState.DeactivateRobotForThisTurn(rebootingRobot);
			//cards are discarded, for each robot, at the end of activation phase, so this is correct
		
		}

	}
}
