﻿using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Core.Actions
{	
	/// <summary>
	/// Arguments for push action.
	/// </summary>
	class PushActionArgs : ActionArgsBase
	{
		/// <summary>
		/// Push direction.
		/// </summary>
		public Direction Direction { get; set; }

		/// <summary>
		/// Push amount (number of tiles).
		/// </summary>
		public int Amount { get; set; }
	}

	/// <summary>
	/// <para>
	/// This action is used when some robot got pushed by another
	/// source (be it robot or for example push panel.
	/// </para>
	/// 
	/// <para>
	/// This action tries to push the robot as much as possible (up to amount) in the 
	/// direction provided as argument. Important thing is, that it does not handle any
	/// actions related to reboot - if there is a reboot tile (pit or out of map) during the
	/// push, the robot simply stops at this tile and it is expected that the caller handles
	/// the reboot on his own (this implementation is because of some complicated cases
	/// with collisions).
	/// </para>
	/// </summary>
	class PushAction : ActionBase<PushActionArgs>, IAction
	{
		private void PushRobotToNextObstacle(PushActionArgs args)
		{
			var pushedRobot = args.Robot;
			var gameState = args.GameState;

			var validatedPos = pushedRobot.Position;

			while (args.Amount > 0)
			{
				if (gameState.Map.IsRebootPosition(validatedPos))
					break;

				var intendedPos = validatedPos + args.Direction;

				if (gameState.Map[intendedPos] is PriorityAntennaTile)
					break;

				if (gameState.Map.IsWallBetween(validatedPos, intendedPos))
					break;
				
				if (gameState.IsTileOccupied(intendedPos))
					break;

				validatedPos = intendedPos;

				args.Amount--;
			}

			if (validatedPos != pushedRobot.Position)
				pushedRobot.Push(gameState, validatedPos);
		}

		/// <inheritdoc/>
		public override void PerformAction(PushActionArgs args)
		{
			var pushedRobot = args.Robot;
			var gameState = args.GameState;

			//1. push robot as much as possible
			//	- if args.amount == 0, then we end
			//	- otherwise, we had to stopped because of something
			//		2. if we are on reboot, then return
			//		3. if we stopped because of other robot, then try to push him 
			//		4. now, move our robot by the distance to this pushed robot (this can be done without any checks)

			PushRobotToNextObstacle(args);

			if (args.Amount == 0)
				return;

			if (gameState.Map.IsRebootPosition(pushedRobot.Position))
				return;

			if (gameState.IsTileOccupied(pushedRobot.Position + args.Direction, out Robot occRobot)
				&& !gameState.Map.IsWallBetween(pushedRobot.Position, pushedRobot.Position + args.Direction))
			{
				PerformAction(new PushActionArgs()
				{
					GameState = gameState,
					Robot = occRobot,
					Direction = args.Direction,
					Amount = args.Amount
				});

				if (occRobot.Position == pushedRobot.Position + args.Direction)
					return;

				//we can safely push pushed robot to this position
				var toPushPos = occRobot.Position + args.Direction.GetOppositeDirection();

				pushedRobot.Push(gameState, toPushPos);			
			}
		}
	}
}
