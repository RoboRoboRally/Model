﻿using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Core.HookableGameSteps;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Util;
using System.Collections.Generic;

namespace RoboRoboRally.Model.Core.Actions
{
	/// <summary>
	/// Arguments for movement action.
	/// </summary>
	class MovementActionArgs : ActionArgsBase
	{
		/// <summary>
		/// Direction of movement.
		/// </summary>
		public Direction Direction { get; set; }

		/// <summary>
		/// Movement amount (number of tiles).
		/// </summary>
		public int Amount { get; set; }
	}

	/// <summary>
	/// Action for robot movement. Note that this action should be used only
	/// in cases when the movement was initiated by the robot or with belts. It
	/// does not have to work as expected in any other cases.
	/// </summary>
	class MovementAction : ActionBase<MovementActionArgs>, IAction
	{
		public PushDirectionStep PushDirectionStep { get; private set; }

		public PushDamageStep PushDamageStep { get; private set; }

		public MovementAction()
		{
			PushDirectionStep = new PushDirectionStep();

			PushDamageStep = new PushDamageStep();
		}

		/*
		There are several problems that need to be kept in mind while implementing this. 
		Of course, some cases are simple - these are all cases with no robots in the moving
		robot's path. However, if there are any, then we need to be careful, especially in situations
		that combine both pushes and reboots.

		For example, following scenario should be, by the games rules implemented in the following
		way (executing Move 1)

		--------- --------- ---------
		|		| |		  | |		|
		|	R1	| |	 R2	  | |		|
		|	>>	| |		  | |  Pit	|
		--------- --------- ---------
					  v
		--------- --------- ---------
		|		| |		  | |		|
		|	R1	| |	 	  | |	R2	|
		|	>>	| |		  | |  Pit	|
		--------- --------- ---------
					  v
		--------- --------- ---------
		|		| |		  | |		|
		|		| |	 R1	  | |	R2	|
		|		| |		  | |  Pit	|
		--------- --------- ---------
					  v
		--------- --------- ---------
		|		| |		  | |		|
		|		| |	 R1	  | |		|
		|		| |		  | |  Pit	|
		--------- --------- ---------

		... especially, notice the second and third step - reboot happened
		after R1 moved to its next tile - this is quite important in scenarios where
		R1 could be pushed in some way due to wierdly placed reboot token ...
		Because of this, the implementation has been redone so that reboots are handled
		at the caller's site (otherwise, this could not be implemented in a reasonable manner)
			 
		*/

		private void MoveRobotToNextObstacle(MovementActionArgs args)
		{
			var movingRobot = args.Robot;
			var gameState = args.GameState;

			var validatedPos = movingRobot.Position;

			while (args.Amount > 0)
			{
				if (gameState.Map.IsRebootPosition(validatedPos))
					break;

				var intendedPos = validatedPos + args.Direction;

				if (gameState.Map[intendedPos] is PriorityAntennaTile)
					break;

				if (gameState.Map.IsWallBetween(validatedPos, intendedPos))
					break;

				if (gameState.IsTileOccupied(intendedPos))
					break;

				validatedPos = intendedPos;

				args.Amount--;
			}

			if (validatedPos != movingRobot.Position)
				movingRobot.Move(gameState, validatedPos);
		}


		/// <inheritdoc/>
		public override void PerformAction(MovementActionArgs args)
		{
			var movingRobot = args.Robot;
			var gameState = args.GameState;
			
			if (args.Amount < 0)
			{
				//this is easier and more convenient to work with
				args.Amount = args.Amount * -1;
				args.Direction = args.Direction.GetOppositeDirection();
			}

			HashSet<Robot> damagedRobots = new HashSet<Robot>();

			while (true)
			{
				MoveRobotToNextObstacle(args);

				if (gameState.Map.IsRebootPosition(movingRobot.Position))
				{
					gameState.GetAction<RebootAction>().PerformAction(new ActionArgsBase()
					{
						GameState = gameState, Robot = movingRobot
					});
					break;
				}

				if (args.Amount == 0)
					break;

				//here, we stopped moving because of some obstacle, check if we bumped into robot
				if (gameState.IsTileOccupied(movingRobot.Position + args.Direction, out Robot occRobot)
					&& !gameState.Map.IsWallBetween(movingRobot.Position, movingRobot.Position + args.Direction))
				{
					//in this branch, we bumped into robot, so let's try to push it

					//push during movement can be affected by upgrades (we can choose which side and amount)

					var modifiedDefaults = PushDirectionStep.PerformHooks(
						new HookArgsBase() { GameState = gameState, Robot = movingRobot },
						new PushDirectionStepRet() { Amount = args.Amount, Direction = args.Direction });

					var pushDirection = modifiedDefaults.Direction;
					var pushAmount = modifiedDefaults.Amount;

					gameState.GetAction<PushAction>().PerformAction(new PushActionArgs()
					{
						Robot = occRobot,
						GameState = gameState,
						Amount = pushAmount,
						Direction = pushDirection
					});

					//if occ robot did not move, then we are done
					if (occRobot.Position == movingRobot.Position + args.Direction)
						break;

					//else, we actually pushed the robot and we may deal damage to it
					if (!damagedRobots.Contains(occRobot))
					{
						var damageStepRet = PushDamageStep.PerformHooks(new HookArgsBase() { GameState = gameState, Robot = movingRobot }, new DamageStepRet() { Damage = new Damage() { DamageCards = new DamageCard[0] } });
						var damage = damageStepRet.Damage;
						if (damage.DamageCards.Length > 0)
						{
							occRobot.SufferDamage(gameState, damage);
							damagedRobots.Add(occRobot);
						}
					}

					if (pushDirection != args.Direction)
					{
						//move robot only one tile forward, because we moved robot to the side
						//this special handling is mainly because of reboots ... some weird things can happen
						//as always (we want to handle potential reboot of pushed robot first)
						movingRobot.Move(gameState, movingRobot.Position + args.Direction);
						args.Amount--;
					}
					else
					{
						MoveRobotToNextObstacle(args);
					}

					//our moving robot, can not be possibly among the robots
					//that are rebooting
					foreach (var robot in gameState.GameActiveRobots)
					{
						if(gameState.Map.IsRebootPosition(robot.Position))
							gameState.GetAction<RebootAction>().PerformAction(new ActionArgsBase() {
								GameState = gameState, Robot = robot });
					}

					//after the previous step, our robot can not be pushed to reboot position
					//... it can be pushed one tile, but nothing too wierd can happen
					//... the question is, if we should stop executing the movement action
					//after this push or not ... current implementation does not stop the execution
					//but it can be implemented either way

					//if we reach end, we have pushed the robot out of the way 
				}
				//otherwise, we did not bump into robot and we are done
				else
					break;
			}
		}
	}
}
