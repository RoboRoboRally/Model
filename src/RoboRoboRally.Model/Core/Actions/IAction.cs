﻿
namespace RoboRoboRally.Model.Core.Actions
{
	/// <summary>
	/// <para>
	/// This namespace contains Actions - larger chunks of logic
	/// that are either too big to be contained in some phase
	/// or they are used throughout different parts of code. So, there
	/// is for example action for reboot, because reboot can be 
	/// called in different situations (effect of programming card,
	/// effect of upgrade card, stepping on a pit tile, ...).
	/// </para>
	/// 
	/// <para>
	/// In order for the actions to be accessible, they are accessible
	/// through interface <see cref="IGameState"/> (which is passed around
	/// a lot).
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
	{ }


	/// <summary>
	/// Base interface for all actions - even though it is currently
	/// empty, we need some so each action can be stored in the
	/// same collection.
	/// </summary>
	public interface IAction
	{	}

	/// <summary>
	/// Base class for arguments to each action - each action
	/// can create class derived from this and accept some 
	/// additional arguments.
	/// </summary>
	class ActionArgsBase
	{
		public IGameState GameState { get; set; }

		/// <summary>
		/// Robot affected by the action (for example, with reboot, 
		/// this robot is the rebooting one).
		/// </summary>
		public Robot Robot { get; set; }
	}

	/// <summary>
	/// Typed base class for each action.
	/// </summary>
	abstract class ActionBase<TArg> where TArg : ActionArgsBase
	{
		/// <summary>
		/// Function that performs the whole action.
		/// </summary>
		public abstract void PerformAction(TArg args);
	}
}
