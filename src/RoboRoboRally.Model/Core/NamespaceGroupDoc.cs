﻿namespace RoboRoboRally.Model.Core
{
	/// <summary>
	/// <para>
	/// This namespace contains some important types that are
	/// related to core game functionality. For example, there is 
	/// a class with main game loop - <see cref="GameCore"/>, class
	/// that represents state of currently running game - 
	/// <see cref="GameState"/>, class <see cref="Robot"/> or 
	/// individual phases of each round resiniding in
	/// namespace<see cref="Phases"/>.
	/// </para>
	/// 
	/// <para>
	/// Note that, there are some types missing in this namespace
	/// that would one consider curcial for the core game logic, like 
	/// effects of individual programming cards or map definition.
	/// These can be found in ther respective namespace, namely
	/// <see cref="CardDB"/> and <see cref="MapDB"/>.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceGroupDoc
    {
    }
}
