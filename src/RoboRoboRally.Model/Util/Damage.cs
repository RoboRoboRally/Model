﻿using RoboRoboRally.Model.CardDB.Cards;

namespace RoboRoboRally.Model.Util
{
	/// <summary>
	/// This struct represents damage in the form
	/// of some damage cards.
	/// </summary>
	public struct Damage
	{
		/// <summary>
		/// Damage of this instance.
		/// </summary>
		public DamageCard[] DamageCards { get; set; }
	}
}
