﻿namespace RoboRoboRally.Model.Util
{
	/// <summary>
	/// Represents 3 types of rotations - to left by 90 degrees, 
	/// back by 180 degrees and to right by 90 degrees. (Also, 
	/// default None rotation value).
	/// </summary>
    public enum RotationType
    {
		/// <summary>
		/// no rotation
		/// </summary>
        None = 0,
		/// <summary>
		/// rotation to left by 90 degrees
		/// </summary>
        Left = 1,
		/// <summary>
		/// rotation by 180 degrees
		/// </summary>
        Back = 2,
		/// <summary>
		/// rotation to right by 90 degrees
		/// </summary>
        Right = 3
    }

	/// <summary>
	/// Helper struct that lightly encapsulates enum 
	/// <see cref="RotationType"/> with a few 
	/// helper methods.
	/// </summary>
    public struct Rotation
    {
		/// <summary>
		/// Encapsulated rotation
		/// </summary>
        public RotationType RotationType { get; private set; }

		/// <summary>
		/// Increases the rotation by 90 degrees.
		/// </summary>
        public void Increase()
        {
            if (RotationType == RotationType.Right)
                RotationType = RotationType.None;
            else
                RotationType++;
        }

		/// <summary>
		/// Decreases the rotation by 90 degrees.
		/// </summary>
        public void Decrease()
        {
            if (RotationType == RotationType.None)
                RotationType = RotationType.Right;
            else
                RotationType--;
        }

		/// <summary>
		/// Ctor from <see cref="RotationType"/>.
		/// </summary>
		/// <param name="rotationType">rotation</param>
        public Rotation(RotationType rotationType)
        {
            this.RotationType = rotationType;
        }
    }
}
