﻿namespace RoboRoboRally.Model.Util
{
	/// <summary>
	/// <para>
	/// This namespace contains various small helper classes,
	/// structs and other types that are used throughout the whole project,
	/// like <see cref="Direction"/> and <see cref="Damage"/> 
	/// structs.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
    {
    }
}
