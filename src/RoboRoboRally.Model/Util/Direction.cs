﻿using System;

namespace RoboRoboRally.Model.Util
{
	/// <summary>
	/// Enum for four different directions (we do not need any
	/// other directions), it is primarily used
	/// in conjunction with <see cref="Direction"/> struct.
	/// </summary>
    public enum DirectionType
    {
		/// <summary>
		/// direction to right
		/// </summary>
        Right,
		/// <summary>
		/// direction to up
		/// </summary>
		Up,
		/// <summary>
		/// direction to left
		/// </summary>
		Left,
		/// <summary>
		/// direction to down
		/// </summary>
		Down
    }
    
	/// <summary>
	/// <para>
	/// This struct represents direction vector in a cartesian coordinate
	/// system. 
	/// </para>
	/// 
	/// <para>
	/// It is important to note, that this struct can represent only four
	/// directions - Up, Down, Left, Right and it is primarily used
	/// together with <see cref="DirectionType"/> enum. These four
	/// directions are all the directions we need as robots can
	/// face only them, they can move only in these directions and 
	/// so on.
	/// </para>
	/// 
	/// <para>
	/// These four directions are represented by unit vectors in
	/// that particular direction.
	/// </para>
	/// </summary>
    public struct Direction
    {
		/// <summary>
		/// X coordinate
		/// </summary>
        public int X { get; private set; }
		/// <summary>
		/// Y coordinate
		/// </summary>
        public int Y { get; private set; }


		/// <summary>
		/// Rotates direction by a certain rotation.
		/// </summary>
		/// <param name="rotationType">vector's rotation</param>
		/// <returns>copy of the rotated vector</returns>
        public Direction Rotate(RotationType rotationType)
        {
            //the 2D rotation matrix is
            /*
                 cos alpha  | -sin alpha
            A =  ------------------------
                 sin alpha  |  cos alpha

            => resulting direction is A*x
             */

            int degrees = (int)rotationType * 90;

            //in our case, the values are only 1, 0, -1, so we can cast
            int cos = (int)Math.Cos(degrees * Math.PI / 180);
            int sin = (int)Math.Sin(degrees * Math.PI / 180);

            int resX = X * cos + Y * -sin;
            int resY = X * sin + Y * cos;

            return new Direction(resX, resY);
        }

		/// <summary>
		/// Rotates direction by a certain rotation.
		/// </summary>
		/// <param name="rotation">vector's rotation</param>
		/// <returns>copy of the rotated vector</returns>
		public Direction Rotate(Rotation rotation)
        {
            return Rotate(rotation.RotationType);            
        }

		/// <summary>
		/// This method can be used to retrieve perpendicular 
		/// directions to the direction on which this method 
		/// is called.
		/// </summary>
		/// <returns>tuple of perpendicular directions</returns>
		public Tuple<Direction, Direction> GetPerpDirections()
		{
			return new Tuple<Direction, Direction>(
				new Direction(-1 * Y, X),
				new Direction(Y, -1 * X)
				);
		}

		/// <summary>
		/// This method gets an opposite direction.
		/// </summary>
		/// <returns>an opposite direction</returns>
		public Direction GetOppositeDirection()
		{
			return new Direction(-1 * X, -1 * Y);
		}

		/// <summary>
		/// Overloaded equality operator for two instances of
		/// <see cref="Direction"/>.
		/// </summary>
		/// <param name="d1">first direction</param>
		/// <param name="d2">second direction</param>
		/// <returns>true if equal, false otherwise</returns>
		public static bool operator ==(Direction d1, Direction d2)
		{
			if (d1.X == d2.X && d1.Y == d2.Y)
				return true;
			return false;
		}

		/// <summary>
		/// Overloaded non-equality operator for two instances of
		/// <see cref="Direction"/>.
		/// </summary>
		/// <param name="d1">first direction</param>
		/// <param name="d2">second direction</param>
		/// <returns>true if not equal, false otherwise</returns>
		public static bool operator !=(Direction d1, Direction d2)
		{
			return !(d1 == d2);
		}

		/// <summary>
		/// As <see cref="DirectionType"/> can represent 
		/// only the same directions as <see cref="Direction"/>, we 
		/// can retrieve enum value from the direction.
		/// </summary>
		/// <returns>one of the four directions</returns>
		public DirectionType GetDirectionType()
		{
			if (X == 1 && Y == 0)
				return DirectionType.Right;
			else if (X == 0 && Y == 1)
				return DirectionType.Up;
			else if (X == -1 && Y == 0)
				return DirectionType.Left;
			else if (X == 0 && Y == -1)
				return DirectionType.Down;
			else
				throw new ArgumentException("Can not get direction type from the Direction values");
		}

		/// <summary>
		/// Overloaded equality operator (<see cref="Direction"/>
		/// and <see cref="DirectionType"/> can represent only
		/// the same directions).
		/// </summary>
		/// <param name="d">instance od direction</param>
		/// <param name="dt">value of direction type enum</param>
		/// <returns>true if equal, false otherwise</returns>
		public static bool operator ==(Direction d, DirectionType dt)
		{
			if (dt == DirectionType.Up && d.X == 0 && d.Y == 1)
				return true;
			if (dt == DirectionType.Down && d.X == 0 && d.Y == -1)
				return true;
			if (dt == DirectionType.Left && d.X == -1 && d.Y == 0)
				return true;
			if (dt == DirectionType.Right && d.X == 1 && d.Y == 0)
				return true;
			return false;
		}

		/// <summary>
		/// Overloaded non-equality operator (<see cref="Direction"/>
		/// and <see cref="DirectionType"/> can represent only
		/// the same directions).
		/// </summary>
		/// <param name="d">instance od direction</param>
		/// <param name="dt">value of direction type enum</param>
		/// <returns>true if not equal, false otherwise</returns>
		public static bool operator !=(Direction d, DirectionType dt)
		{
			return !(d == dt);
		}

		/// <summary>
		/// Overloaded equality operator (<see cref="Direction"/>
		/// and <see cref="DirectionType"/> can represent only
		/// the same directions).
		/// </summary>
		/// <param name="dt">value of direction type enum</param>
		/// <param name="d">instance od direction</param>
		/// <returns>true if equal, false otherwise</returns>
		public static bool operator ==(DirectionType dt, Direction d)
		{
			return d == dt;
		}

		/// <summary>
		/// Overloaded non-equality operator (<see cref="Direction"/>
		/// and <see cref="DirectionType"/> can represent only
		/// the same directions).
		/// </summary>
		/// <param name="dt">value of direction type enum</param>
		/// <param name="d">instance od direction</param>
		/// <returns>true if not equal, false otherwise</returns>
		public static bool operator !=(DirectionType dt, Direction d)
		{
			return d != dt;
		}

		/// <summary>
		/// This method returns amount of degrees between two
		/// directions.
		/// </summary>
		/// <param name="otherDir">direction relative to this direction</param>
		/// <returns>clockwise amount of degrees (therefore, always
		/// positive)</returns>
		public int GetDegreesBetween(Direction otherDir)
		{
			var dotProduct = this.GetDotProduct(otherDir);

			var det = this.X * otherDir.Y - this.Y * otherDir.X;

			var f = Math.Atan2(det, dotProduct);

			int fDeg = (int)(f * 180 / Math.PI);
			fDeg = fDeg % 360;
			if (fDeg < 0)
				fDeg += 360;

			return fDeg;
		}

		private double GetDotProduct(Direction otherDir)
		{
			return this.X * otherDir.X + this.Y * otherDir.Y;
		}

		/// <summary>
		/// Ctor from coordinates, as described in this struct's summary,
		/// this Direction can only represent four directions, see it for 
		/// more information about construction.
		/// </summary>
		/// <param name="X">direction's x coordinate</param>
		/// <param name="Y">direction's y coordinate</param>
		/// <exception cref="ArgumentException">is thrown if the passed values
		/// do not match the four direction constraint</exception>
		public Direction(int X, int Y)
		{
			if (!((X == 0 && Y == 1) || (X == 0 && Y == -1) ||
				(X == -1 && Y == 0) || (X == 1 && Y == 0)))
				throw new ArgumentException("can not construct direction - invalid X and Y values - only 4 directions are allowed");

			this.X = X;
			this.Y = Y;
		}

		/// <summary>
		/// Ctor from enum value of <see cref="DirectionType"/>,
		/// as described in this struct's summary,
		/// this Direction can only represent four directions, see it for 
		/// more information about construction.
		/// </summary>
		/// <param name="directionType">direction</param>
		/// <exception cref="ArgumentException">is thrown if
		/// the passed direction is not one of the four values Up,
		/// Left, Right or Down.</exception>
		public Direction(DirectionType directionType)
        {
            switch (directionType)
            {
                case DirectionType.Up:
                    this.X = 0;
                    this.Y = 1;
                    break;
                case DirectionType.Down:
                    this.X = 0;
                    this.Y = -1;
                    break;
                case DirectionType.Right:
                    this.X = 1;
                    this.Y = 0;
                    break;
                case DirectionType.Left:
                    this.X = -1;
                    this.Y = 0;
                    break;
                default:
                    throw new ArgumentException("Not implemented direction type");
            }
        }
    }
}
