﻿using System;

namespace RoboRoboRally.Model.Util
{
	/// <summary>
	/// Custom public random class (we need access
	/// from differents part of code to the same instance).
	/// </summary>
    static class MyRandom
    {
		/// <summary>
		/// Random number generator that is initiated during 
		/// game intialization and that is used in differents 
		/// part of code.
		/// </summary>
		public static Random Random { get; set; } = new Random();
    }
}
