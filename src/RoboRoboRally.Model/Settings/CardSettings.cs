﻿using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;

namespace RoboRoboRally.Model.Settings
{
	/// <summary>
	/// This settings contain card settings that are 
	/// not related to players like upgrade cards.
	/// </summary>
    public class CardSettings
    {
		/// <summary>
		/// Deck of upgrade cards used during game.
		/// </summary>
		public Pack<UpgradeCard> UpgradeCards { get; set; }
    }
}
