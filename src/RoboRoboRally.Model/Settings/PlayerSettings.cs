﻿using System.Collections.Generic;
using RoboRoboRally.Model.CardDB;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Settings
{
	/// <summary>
	/// This class contains various starting settings for each player,
	/// like starting energy, number of registers or deck.
	/// </summary>
    public class PlayerSettings
    {
		/// <summary>
		/// Initial deck for each player. This deck is shuffled each
		/// time new player joins.
		/// </summary>
		public Pack<ProgrammingCard> PlayerProgrammingCards { get; set; }

		/// <summary>
		/// Initial energy amount for each player.
		/// </summary>
		public int EnergyAmount { get; set; }

		/// <summary>
		/// Number of registers for each player.
		/// </summary>
		public int RegisterBoardSize { get; set; }

		/// <summary>
		/// Default damage of lasers for each robot.
		/// </summary>
		public Damage DefaultPlayerDamage { get; set; }

		/// <summary>
		/// Each player can hold only limited number of upgrade
		/// cards from each upgrade type. This field configures
		/// these limits.
		/// </summary>
		public Dictionary<UpgradeCardType, int> UpgradeCardsLimits { get; set; }
	}
}
