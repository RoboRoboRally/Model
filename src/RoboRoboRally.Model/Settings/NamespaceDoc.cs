﻿namespace RoboRoboRally.Model.Settings
{
	/// <summary>
	/// <para>
	/// When the server is launching new instance of a game,
	/// it can configure several things about this new game, like
	/// map, player decks, number and order of phases and so on.
	/// All of these configurable settings can be found in
	/// this namespace.
	/// </para>
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
	class NamespaceDoc
    {
    }
}
