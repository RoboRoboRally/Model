﻿using RoboRoboRally.Model.MapDB;

namespace RoboRoboRally.Model.Settings
{
	/// <summary>
	/// These settings configure everything that is 
	/// related to map.
	/// </summary>
	public class MapSettings
	{
		/// <summary>
		/// ID of starting plan.
		/// </summary>
		public string StartingPlanID { get; set; }

		/// <summary>
		/// Id of game plan.
		/// </summary>
		public string GamePlanID { get; set; }

		/// <summary>
		/// This field contains various map settings that are not
		/// part of the map database and which were configured manually
		/// during the game setup. This concerns placable map elements
		/// like antenna, checkpoints and reboot checkpoints, but also
		/// rotation of game plan. For more information, see
		/// contents of <see cref="MapModifications"/>.
		/// </summary>
		public MapModifications MapModifications { get; set; }
	}
}
