﻿using RoboRoboRally.Model.Core.Phases;

namespace RoboRoboRally.Model.Settings
{
	/// <summary>
	/// These settings are concerned with game flow, that means
	/// actions that will be executed each turn, for example
	/// number and order of phases.
	/// </summary>
    public class GameFlowSettings
    {
		/// <summary>
		/// Phases in order of execution. For each turn,
		/// phase at index [0] will be executed first, then 
		/// the one at index [1] and so on.
		/// </summary>
		public PhaseBase[] Phases { get; set; }
	}
}
