﻿using RoboRoboRally.Model.Util;

namespace RoboRoboRally.Model.Settings
{
	/// <summary>
	/// These settings configure various aspects about
	/// the game environment.
	/// </summary>
    public class EnvironmentSettings
    {
		/// <summary>
		/// This field configures damage each robot takes
		/// during reboot.
		/// </summary>
		public Damage DefaultRebootDamage { get; set; }
	}
}
