﻿using System;

namespace RoboRoboRally.Model.Settings
{
	/// <summary>
	/// This settings class simply encapsulates all of the 
	/// game settings into one class. 
	/// </summary>
    public class GameSettings
    {
		/// <summary>
		/// Settings about game flow, see <see cref="GameFlowSettings"/>
		/// for more informations.
		/// </summary>
		public GameFlowSettings GameFlowSettings { get; set; }

		/// <summary>
		/// Settings about game environment, see
		/// <see cref="EnvironmentSettings"/> for more informations.
		/// </summary>
		public EnvironmentSettings EnvironmentSettings { get; set; }

		/// <summary>
		/// Settings about map, see <see cref="MapSettings"/>
		/// for more informations.
		/// </summary>
		public MapSettings MapSettings { get; set; }

		/// <summary>
		/// Settings about cards, see <see cref="CardSettings"/>
		/// for more infromations.
		/// </summary>
		public CardSettings CardSettings { get; set; }

		/// <summary>
		/// Settings about players, see <see cref="PlayerSettings"/>
		/// for more informations.
		/// </summary>
		public PlayerSettings PlayerSettings { get; set; }

		/// <summary>
		/// Seed configuration for those few random aspects in the 
		/// game, like deck shuffling.
		/// </summary>
		public int Seed { get; set; } = DateTime.Now.Millisecond;
    }
}
