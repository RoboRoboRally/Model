﻿namespace RoboRoboRally.Model
{
	/// <summary>
	/// <para>
	/// This top-most namespace contains the most important
	/// types that are related to communication from outside
	/// world (in our case, server) to a running game as well
	/// as types related to communication from
	/// a running game to outside world (again, server).
	/// </para>
	/// 
	/// <para>
	/// Communication from server to a running game is provided 
	/// by class <see cref="Game"/>.
	/// </para>
	/// 
	/// <para>
	/// Communication from a running game to server is provided
	/// by two interfaces - one that is concerned with player input, that
	/// is <see cref="IInputHandler"/> and one that is concerned
	/// with notifications about various game events, that is
	/// <see cref="IGameEventNotifier"/>.
	/// </para>
	/// 
	/// <para>
	/// Other types in this namespace are mostly helper classes and
	/// structs that are used in these two directions of communication.
	/// </para> 
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }
}
