﻿using System;
using RoboRoboRally.Model.Core;
using RoboRoboRally.Model.Settings;

namespace RoboRoboRally.Model
{
	/// <summary>
	/// This exception may be thrown on several operations that are related to
	/// game manipulation. That is, this exception may occur when server tries
	/// to perform invalid operation on class <see cref="Game"/>.
	/// </summary>
	public class InvalidGameOperationException : Exception
	{
		/// <summary>
		/// Exception ctor with message.
		/// </summary>
		/// <param name="message">expcetion message</param>
		public InvalidGameOperationException(string message) :
			base("Exception occurred in communication with the game: " + message)
		{ }
	}

	/// <summary>
	/// <para>
	/// From the perspective of server, this is the core class of model and
	/// from it's point of view, this class represents communication channel
	/// to running instance of a game. As such, it can be used to manipulate
	/// game flow (pause, start, end, resume), modify players of the game 
	/// (add, remove) or pass async messages to the game from clients.
	/// </para>
	/// 
	/// <para>
	/// Communication from model to server is provided via instances of classes 
	/// that implement <see cref="IGameEventNotifier"/> or <see cref="IInputHandler"/>
	/// interface. See them for more details.
	/// </para>
	/// 
	/// <para>
	/// It is important to note, that one instance of this class can be used to manipulate 
	/// only one instance of running game at a time. In order to launch two games simultaneously,
	/// new instance of this class has to be created.
	/// </para>
	/// 
	/// <para>
	/// Other important thing to note: messages from the server that are passed to an already
	/// running game are hadndled at certain time points. For example, game flow may be changed
	/// only at the beggining of each turn. This concerns also modification of player count.
	/// Async messages are also handled at ceratin time points in the respective phases. These
	/// time points depend highly on the async message, but important is that they are not
	/// handled immediately.
	/// </para>
	/// </summary>
	public class Game
    {
		private object gameCoreLock;

		private GameInitializer initializer;

		private GameCore gameCore;

		/// <summary>
		/// This method tries to launch new running game in a new thread.
		/// </summary>
		/// <param name="gameSettings">settings of the new game, see <see cref="GameSettings"/>
		/// for additional information</param>
		/// <param name="players">starting players of the new game</param>
		/// <param name="inputHandler">this paramater will enable the running game
		/// to retrieve input from clients, see <see cref="IInputHandler"/> interface for
		/// more information</param>
		/// <param name="notifier">this paramater will enable the running game
		/// to notify the clients about various events during game,
		/// see <see cref="IGameEventNotifier"/> interface for more information</param>
		/// <exception cref="InvalidGameOperationException">is thrown either when there is
		/// already a running game, or if the game setup fails (e.g. due to invalid GameSettings)</exception>
		public void StartGame(GameSettings gameSettings, string[] players,
			IInputHandler inputHandler, IGameEventNotifier notifier)
        {
			lock (gameCoreLock)
			{
				if (gameCore.GameRunningState != GameRunningState.NotRunning)
					throw new InvalidGameOperationException("can not start a game that is already running");

				gameCore.StartGame(initializer.GetNewGameSetup(gameSettings, inputHandler, notifier, players));
			}
        }

		/// <summary>
		/// This method tries to add a new player to an already running game.
		/// </summary>
		/// <param name="robotName">name of new player to be added to the running game</param>
		/// <exception cref="InvalidGameOperationException">is thrown if the game is not running
		/// at all, or if there there is already player with such name present in the game</exception>
		public void AddPlayer(string robotName)
		{
			lock (gameCoreLock)
			{
				if (gameCore.GameRunningState == GameRunningState.NotRunning)
					throw new InvalidGameOperationException("can not add players to a game that is not running");

				gameCore.PlayerGameParticipationRequests.Enqueue(new PlayerGameParticipationRequest() { RobotName = robotName, Request = PlayerGameParticipationStatus.Join });
			}
		}

		/// <summary>
		/// This method tries to remove player from an already running game.
		/// </summary>
		/// <param name="robotName">name of player to be removed from the running game</param>
		/// <exception cref="InvalidGameOperationException">is thrown if the game is not running
		/// at all, or if there is no such player present in the running game</exception>
		public void RemovePlayer(string robotName)
		{
			lock (gameCoreLock)
			{
				if (gameCore.GameRunningState == GameRunningState.NotRunning)
					throw new InvalidGameOperationException("can not remove players from a game that is not running");

				gameCore.PlayerGameParticipationRequests.Enqueue(new PlayerGameParticipationRequest() { RobotName = robotName, Request = PlayerGameParticipationStatus.Leave });
			}
		}

		/// <summary>
		/// This method tries to pause an already running game. Multiple pause requests
		/// may be issued before the pause request is handled, they are ignored.
		/// </summary>
		/// <exception cref="InvalidGameOperationException">is thrown if the game
		/// is not actually running at all</exception>
		public void PauseGame()
		{
			lock (gameCoreLock)
			{
				if(gameCore.GameRunningState == GameRunningState.NotRunning)
					throw new InvalidGameOperationException("can not resume game - there is no instance of launched game");

				gameCore.GameRunningRequests.Enqueue(GameRunningRequest.Pause);
			}
		}		

		/// <summary>
		/// This method tries to resume game that has been paused. Multiple resume
		/// requests (even ones on a non-paused game) are ignored.
		/// </summary>
		/// <exception cref="InvalidGameOperationException">is thrown if the game
		/// is not actually running at all</exception>
		public void ResumeGame()
		{
			lock (gameCoreLock)
			{
				if(gameCore.GameRunningState == GameRunningState.NotRunning)
					throw new InvalidGameOperationException("can not resume game - there is no instance of launched game");

				gameCore.GameRunningRequests.Enqueue(GameRunningRequest.Resume);
			}
			
		}

		/// <summary>
		/// This method tries to terminate already running game. Multiple termination
		/// requests are ignored. This request is handled preferentially from the 
		/// other requests. It is also handled even though the game is paused.
		/// </summary>
		/// <exception cref="InvalidGameOperationException">is thrown if the game
		/// is not actually running at all</exception>
		public void EndGame()
		{
			lock (gameCoreLock)
			{
				if(gameCore.GameRunningState == GameRunningState.NotRunning)
					throw new InvalidGameOperationException("can not end game that is not running");

				gameCore.GameRunningRequests.Enqueue(GameRunningRequest.Terminate);

			}
		}

		/// <summary>
		/// This method tries to pass async message from client to model that
		/// is related to use of some upgrade card (that can be used over longer period
		/// of time, like through whole activation phase). Multiple same requests
		/// are ignored.
		/// </summary>
		/// <param name="useInfo">this parameter contains information about
		/// the use of upgrade card at some client</param>
		/// <exception cref="InvalidGameOperationException">is thrown if the game 
		/// is not running at all</exception>
		public void AddAsyncUpgradeCardUseInfo(AsyncUpgradeCardUseInfo useInfo)
		{
			lock (gameCoreLock)
			{
				if (gameCore.GameRunningState == GameRunningState.NotRunning)
					throw new InvalidGameOperationException("can not add new async upgrade card use info - game is not running");

				gameCore.AddAsyncUpgradeCardUseInfo(useInfo);
			}
		}

		/// <param name="gameDataDirPath">
		/// While setting up a new game, Model needs to access GameData for
		/// loading maps and so on. As GameData is distributed separately
		/// Model needs to know the path to the folder with content of GameData
		/// that has to be provided from the outside (in our case, from the server).
		/// This parameter is the aforementioned path. The exact folder that it
		/// should point to is the one that contains folders "DB" and "Textures".
		/// </param>
		public Game(string gameDataDirPath)
		{
			initializer = new GameInitializer(gameDataDirPath);
			gameCoreLock = new object();
			gameCore = new GameCore(gameCoreLock);
		}
	}
}
