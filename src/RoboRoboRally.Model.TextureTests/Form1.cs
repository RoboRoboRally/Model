﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Moq;
using RoboRoboRally.Model.MapDB;
using RoboRoboRally.Model.MapDB.Tiles;
using RoboRoboRally.Model.Textures.MapTextures;
using RoboRoboRally.Model.Textures.CardTextures;
using RoboRoboRally.Model.Util;
using RoboRoboRally.Model.MapDB.Walls;
using RoboRoboRally.Model.Textures.RobotTextures;

namespace RoboRoboRally.Model.TextureTests
{
	public partial class Form1 : Form
	{
		string mapTexturesPath = @"D:\Coding\LargeProjects\RoboRoboRally\GameData\GameData\Textures\MapTextures\";
		string cardTexturesPath = @"D:\Coding\LargeProjects\RoboRoboRally\GameData\GameData\Textures\CardTextures\";
		string robotTexturesPath = @"D:\Coding\LargeProjects\RoboRoboRally\GameData\GameData\Textures\RobotTextures\";
		MapTextureLoader mapTextureLoader;
		CardTextureLoader cardTextureLoader;
		RobotTextureLoader robotTextureLoader;

		public Form1()
		{
			InitializeComponent();
			mapTextureLoader = new MapTextureLoader(mapTexturesPath);
			cardTextureLoader = new CardTextureLoader(cardTexturesPath);
			robotTextureLoader = new RobotTextureLoader(robotTexturesPath);
		}

		private void DrawImage(object sender, PaintEventArgs e, byte[] texture, int rotation = 0, int flip = 0)
		{
			PictureBox pb = (PictureBox)sender;
			Bitmap b = (Bitmap)((new ImageConverter()).ConvertFrom(texture));
			b.RotateFlip((RotateFlipType)rotation);
			b.RotateFlip((RotateFlipType)flip);
			e.Graphics.DrawImage(b, new Rectangle(0, 0, pb.Width, pb.Height));
		}

		private void DrawTileTexture(object sender, PaintEventArgs e, TileTextureInfo tti)
		{
			PictureBox pb = (PictureBox)sender;
			Bitmap b = (Bitmap)((new ImageConverter()).ConvertFrom(tti.Texture));
			b.RotateFlip((RotateFlipType)tti.RotationEnumValue);
			b.RotateFlip((RotateFlipType)tti.FlipEnumValue);
			e.Graphics.DrawImage(b, new Rectangle(0, 0, pb.Width, pb.Height));
		}


		private void pictureBox1_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5,5)]).Returns(new EmptyTile());
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));			
		}

		private void pictureBox2_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new EnergyTile(1));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox3_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new CheckpointTile(1));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox4_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new PitTile());
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox5_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new StartingTile(new Direction(DirectionType.Up)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox6_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new RebootTile(new Direction(DirectionType.Up)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox7_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new RebootTile(new Direction(DirectionType.Left)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox8_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new RebootTile(new Direction(DirectionType.Down)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox9_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new RebootTile(new Direction(DirectionType.Right)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox10_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new PriorityAntennaTile(new Direction(DirectionType.Down)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox11_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new RotatingTile(RotationType.Left));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox12_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new RotatingTile(RotationType.Right));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox13_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new GreenBeltTile(new Direction(DirectionType.Up)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox14_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new BlueBeltTile(new Direction(DirectionType.Left)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox15_Paint(object sender, PaintEventArgs e)
		{

			PictureBox pb = (PictureBox)sender;
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new GreenBeltTile(new Direction(DirectionType.Down)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox23_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(map => map[new MapCoordinates(5, 5)]).Returns(new BlueBeltTile(new Direction(DirectionType.Up)));
			mock.Setup(map => map[new MapCoordinates(5, 4)]).Returns(new BlueBeltTile(new Direction(DirectionType.Right)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox24_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(map => map[new MapCoordinates(5, 5)]).Returns(new BlueBeltTile(new Direction(DirectionType.Up)));
			mock.Setup(map => map[new MapCoordinates(5, 4)]).Returns(new GreenBeltTile(new Direction(DirectionType.Right)));
			mock.Setup(map => map[new MapCoordinates(6, 5)]).Returns(new BlueBeltTile(new Direction(DirectionType.Up)));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox18_Paint(object sender, PaintEventArgs e)
		{
			PictureBox pb = (PictureBox)sender;
			Bitmap b = (Bitmap)((new ImageConverter()).ConvertFrom(cardTextureLoader.GetProgrammingCardTexture("Spam", "Blue")));
			e.Graphics.DrawImage(b, new Rectangle(0, 0, pb.Width, pb.Height));
		}

		private void pictureBox17_Paint(object sender, PaintEventArgs e)
		{
			PictureBox pb = (PictureBox)sender;
			Bitmap b = (Bitmap)((new ImageConverter()).ConvertFrom(cardTextureLoader.GetProgrammingCardTexture("Move2", "Blue")));
			e.Graphics.DrawImage(b, new Rectangle(0, 0, pb.Width, pb.Height));

		}


		private void pictureBox19_Paint(object sender, PaintEventArgs e)
		{
			PictureBox pb = (PictureBox)sender;
			Bitmap b = (Bitmap)((new ImageConverter()).ConvertFrom(cardTextureLoader.GetProgrammingCardTexture("Again", "Orange")));
			e.Graphics.DrawImage(b, new Rectangle(0, 0, pb.Width, pb.Height));
		}

		private void pictureBox20_Paint(object sender, PaintEventArgs e)
		{
			PictureBox pb = (PictureBox)sender;
			//Bitmap b = (Bitmap)((new ImageConverter()).ConvertFrom(cardTextureLoader.GetProgrammingCardTexture("Again", "Violet")));
			//e.Graphics.DrawImage(b, new Rectangle(0, 0, pb.Width, pb.Height));
		}

		private void pictureBox21_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, cardTextureLoader.GetUpgradeCardTexture("Boink"));
		}

		private void pictureBox22_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, cardTextureLoader.GetUpgradeCardTexture("abc"));
		}

		private void pictureBox26_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, mapTextureLoader.GetEmptyWallTexture());
		}

		private void pictureBox27_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, mapTextureLoader.GetPushPanelTexture(
				new PushPanelWall(new int[] { 1, 3, 5 }, null, new Direction(DirectionType.Down))));
		}

		private void pictureBox28_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, mapTextureLoader.GetPushPanelTexture(
				new PushPanelWall(new int[] { 2, 4 }, null, new Direction(DirectionType.Down))));
		}

		private void pictureBox29_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, mapTextureLoader.GetLaserHeadTexture());
		}

		private void pictureBox30_Paint(object sender, PaintEventArgs e)
		{
			var mock = new Mock<IMap>();
			mock.Setup(m => m[new MapCoordinates(5, 5)]).Returns(new TextTile("abc"));
			DrawTileTexture(sender, e, mapTextureLoader.GetTileTexture(mock.Object, 5, 5));
		}

		private void pictureBox31_Paint(object sender, PaintEventArgs e)
		{
			//RED
			DrawImage(sender, e, robotTextureLoader.GetRobotFrontViewTexture("Hulk X90"));
		}

		private void pictureBox32_Paint(object sender, PaintEventArgs e)
		{
			//BLUE
			DrawImage(sender, e, robotTextureLoader.GetRobotTopViewTexture("Spin bot"));
		}

		private void pictureBox33_Paint(object sender, PaintEventArgs e)
		{
			//ORANGE
			DrawImage(sender, e, robotTextureLoader.GetRobotFrontViewTexture("Twonky"));
		}

		private void pictureBox34_Paint(object sender, PaintEventArgs e)
		{
			//PURPLE
			DrawImage(sender, e, robotTextureLoader.GetRobotTopViewTexture("Hammer bot"));
		}

		private void pictureBox35_Paint(object sender, PaintEventArgs e)
		{
			//YELLOW
			DrawImage(sender, e, robotTextureLoader.GetRobotFrontViewTexture("Smash bot"));
		}

		private void pictureBox36_Paint(object sender, PaintEventArgs e)
		{
			//GREEN
			DrawImage(sender, e, robotTextureLoader.GetRobotTopViewTexture("Zoom bot"));
		}

		private void pictureBox37_Paint(object sender, PaintEventArgs e)
		{
			//invalid
			DrawImage(sender, e, robotTextureLoader.GetRobotTopViewTexture("abc bot"));
		}

		private void pictureBox38_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, cardTextureLoader.GetChoiceCardTexture("Zoop", "Right"));
		}

		private void pictureBox39_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, cardTextureLoader.GetChoiceCardTexture("Refresh", "LeftTurn"));
		}

		private void pictureBox40_Paint(object sender, PaintEventArgs e)
		{
			DrawImage(sender, e, cardTextureLoader.GetChoiceCardTexture("SideArms", "Left"));
		}		
	}
}
